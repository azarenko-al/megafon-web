// ************************************************************************ //
// The types declared in this file were generated from data read from the
// WSDL File described below:
// WSDL     : http://localhost/ApiService/Service.svc?singleWsdl
//  >Import : http://localhost/ApiService/Service.svc?singleWsdl>0
//  >Import : http://localhost/ApiService/Service.svc?singleWsdl>1
//  >Import : http://localhost/ApiService/Service.svc?singleWsdl>2
// Encoding : utf-8
// Version  : 1.0
// (16.09.2020 11:53:32 - - $Rev: 86412 $)
// ************************************************************************ //

unit api_local_ApiService_09_16;

interface

uses Soap.InvokeRegistry, Soap.SOAPHTTPClient;

const
  IS_OPTN = $0001;
  IS_UNBD = $0002;
  IS_NLBL = $0004;
  IS_REF  = $0080;


type

  // ************************************************************************ //
  // The following types, referred to in the WSDL document are not being represented
  // in this file. They are either aliases[@] of other types represented or were referred
  // to but never[!] declared in the document. The types from the latter category
  // typically map to predefined/known XML or Embarcadero types; however, they could also 
  // indicate incorrect WSDL documents that failed to declare or import a schema type.
  // ************************************************************************ //
  // !:boolean         - "http://www.w3.org/2001/XMLSchema"[Gbl]
  // !:string          - "http://www.w3.org/2001/XMLSchema"[Gbl]
  // !:double          - "http://www.w3.org/2001/XMLSchema"[Gbl]

  TCalcResponse2       = class;                 { "http://schemas.datacontract.org/2004/07/ApiService"[GblCplx] }
  TCalcResponse        = class;                 { "http://schemas.datacontract.org/2004/07/ApiService"[GblElm] }
  TCalcRequest2        = class;                 { "http://schemas.datacontract.org/2004/07/ApiService"[GblCplx] }
  TCalcRequest         = class;                 { "http://schemas.datacontract.org/2004/07/ApiService"[GblElm] }
  TCalcPointRequest2   = class;                 { "http://schemas.datacontract.org/2004/07/ApiService"[GblCplx] }
  TCalcPointRequest    = class;                 { "http://schemas.datacontract.org/2004/07/ApiService"[GblElm] }

  ArrayOfTCalcPointRequest = array of TCalcPointRequest2;   { "http://schemas.datacontract.org/2004/07/ApiService"[GblCplx] }


  // ************************************************************************ //
  // XML       : TCalcResponse, global, <complexType>
  // Namespace : http://schemas.datacontract.org/2004/07/ApiService
  // ************************************************************************ //
  TCalcResponse2 = class(TRemotable)
  private
    FCalculationCode: string;
    FCalculationCode_Specified: boolean;
    FMessage_: string;
    FMessage__Specified: boolean;
    FType_: string;
    FType__Specified: boolean;
    procedure SetCalculationCode(Index: Integer; const Astring: string);
    function  CalculationCode_Specified(Index: Integer): boolean;
    procedure SetMessage_(Index: Integer; const Astring: string);
    function  Message__Specified(Index: Integer): boolean;
    procedure SetType_(Index: Integer; const Astring: string);
    function  Type__Specified(Index: Integer): boolean;
  published
    property CalculationCode: string  Index (IS_OPTN or IS_NLBL) read FCalculationCode write SetCalculationCode stored CalculationCode_Specified;
    property Message_:        string  Index (IS_OPTN or IS_NLBL) read FMessage_ write SetMessage_ stored Message__Specified;
    property Type_:           string  Index (IS_OPTN or IS_NLBL) read FType_ write SetType_ stored Type__Specified;
  end;



  // ************************************************************************ //
  // XML       : TCalcResponse, global, <element>
  // Namespace : http://schemas.datacontract.org/2004/07/ApiService
  // ************************************************************************ //
  TCalcResponse = class(TCalcResponse2)
  private
  published
  end;



  // ************************************************************************ //
  // XML       : TCalcRequest, global, <complexType>
  // Namespace : http://schemas.datacontract.org/2004/07/ApiService
  // ************************************************************************ //
  TCalcRequest2 = class(TRemotable)
  private
    FCalculationCode: string;
    FCalculationCode_Specified: boolean;
    FCanChangePositionH: Boolean;
    FCanChangePositionH_Specified: boolean;
    FCanChangePositionV: Boolean;
    FCanChangePositionV_Specified: boolean;
    FClientAntennaHeight: Double;
    FClientAntennaHeight_Specified: boolean;
    FClientAntennaLatitude: Double;
    FClientAntennaLatitude_Specified: boolean;
    FClientAntennaLongitude: Double;
    FClientAntennaLongitude_Specified: boolean;
    FClientHWTypeID: string;
    FClientHWTypeID_Specified: boolean;
    FClientLatitude: Double;
    FClientLatitude_Specified: boolean;
    FClientLongitude: Double;
    FClientLongitude_Specified: boolean;
    FMinSpeed: Double;
    FMinSpeed_Specified: boolean;
    FPoints: ArrayOfTCalcPointRequest;
    FPoints_Specified: boolean;
    FTechnology: string;
    FTechnology_Specified: boolean;
    procedure SetCalculationCode(Index: Integer; const Astring: string);
    function  CalculationCode_Specified(Index: Integer): boolean;
    procedure SetCanChangePositionH(Index: Integer; const ABoolean: Boolean);
    function  CanChangePositionH_Specified(Index: Integer): boolean;
    procedure SetCanChangePositionV(Index: Integer; const ABoolean: Boolean);
    function  CanChangePositionV_Specified(Index: Integer): boolean;
    procedure SetClientAntennaHeight(Index: Integer; const ADouble: Double);
    function  ClientAntennaHeight_Specified(Index: Integer): boolean;
    procedure SetClientAntennaLatitude(Index: Integer; const ADouble: Double);
    function  ClientAntennaLatitude_Specified(Index: Integer): boolean;
    procedure SetClientAntennaLongitude(Index: Integer; const ADouble: Double);
    function  ClientAntennaLongitude_Specified(Index: Integer): boolean;
    procedure SetClientHWTypeID(Index: Integer; const Astring: string);
    function  ClientHWTypeID_Specified(Index: Integer): boolean;
    procedure SetClientLatitude(Index: Integer; const ADouble: Double);
    function  ClientLatitude_Specified(Index: Integer): boolean;
    procedure SetClientLongitude(Index: Integer; const ADouble: Double);
    function  ClientLongitude_Specified(Index: Integer): boolean;
    procedure SetMinSpeed(Index: Integer; const ADouble: Double);
    function  MinSpeed_Specified(Index: Integer): boolean;
    procedure SetPoints(Index: Integer; const AArrayOfTCalcPointRequest: ArrayOfTCalcPointRequest);
    function  Points_Specified(Index: Integer): boolean;
    procedure SetTechnology(Index: Integer; const Astring: string);
    function  Technology_Specified(Index: Integer): boolean;
  public
    destructor Destroy; override;
  published
    property CalculationCode:        string                    Index (IS_OPTN or IS_NLBL) read FCalculationCode write SetCalculationCode stored CalculationCode_Specified;
    property CanChangePositionH:     Boolean                   Index (IS_OPTN) read FCanChangePositionH write SetCanChangePositionH stored CanChangePositionH_Specified;
    property CanChangePositionV:     Boolean                   Index (IS_OPTN) read FCanChangePositionV write SetCanChangePositionV stored CanChangePositionV_Specified;
    property ClientAntennaHeight:    Double                    Index (IS_OPTN) read FClientAntennaHeight write SetClientAntennaHeight stored ClientAntennaHeight_Specified;
    property ClientAntennaLatitude:  Double                    Index (IS_OPTN) read FClientAntennaLatitude write SetClientAntennaLatitude stored ClientAntennaLatitude_Specified;
    property ClientAntennaLongitude: Double                    Index (IS_OPTN) read FClientAntennaLongitude write SetClientAntennaLongitude stored ClientAntennaLongitude_Specified;
    property ClientHWTypeID:         string                    Index (IS_OPTN or IS_NLBL) read FClientHWTypeID write SetClientHWTypeID stored ClientHWTypeID_Specified;
    property ClientLatitude:         Double                    Index (IS_OPTN) read FClientLatitude write SetClientLatitude stored ClientLatitude_Specified;
    property ClientLongitude:        Double                    Index (IS_OPTN) read FClientLongitude write SetClientLongitude stored ClientLongitude_Specified;
    property MinSpeed:               Double                    Index (IS_OPTN) read FMinSpeed write SetMinSpeed stored MinSpeed_Specified;
    property Points:                 ArrayOfTCalcPointRequest  Index (IS_OPTN or IS_NLBL) read FPoints write SetPoints stored Points_Specified;
    property Technology:             string                    Index (IS_OPTN or IS_NLBL) read FTechnology write SetTechnology stored Technology_Specified;
  end;



  // ************************************************************************ //
  // XML       : TCalcRequest, global, <element>
  // Namespace : http://schemas.datacontract.org/2004/07/ApiService
  // ************************************************************************ //
  TCalcRequest = class(TCalcRequest2)
  private
  published
  end;



  // ************************************************************************ //
  // XML       : TCalcPointRequest, global, <complexType>
  // Namespace : http://schemas.datacontract.org/2004/07/ApiService
  // ************************************************************************ //
  TCalcPointRequest2 = class(TRemotable)
  private
    FObjectAntennaHeight: Double;
    FObjectAntennaHeight_Specified: boolean;
    FObjectID: string;
    FObjectID_Specified: boolean;
    FObjectLatitude: Double;
    FObjectLatitude_Specified: boolean;
    FObjectLongitude: Double;
    FObjectLongitude_Specified: boolean;
    FObjectTypeSysname: string;
    FObjectTypeSysname_Specified: boolean;
    procedure SetObjectAntennaHeight(Index: Integer; const ADouble: Double);
    function  ObjectAntennaHeight_Specified(Index: Integer): boolean;
    procedure SetObjectID(Index: Integer; const Astring: string);
    function  ObjectID_Specified(Index: Integer): boolean;
    procedure SetObjectLatitude(Index: Integer; const ADouble: Double);
    function  ObjectLatitude_Specified(Index: Integer): boolean;
    procedure SetObjectLongitude(Index: Integer; const ADouble: Double);
    function  ObjectLongitude_Specified(Index: Integer): boolean;
    procedure SetObjectTypeSysname(Index: Integer; const Astring: string);
    function  ObjectTypeSysname_Specified(Index: Integer): boolean;
  published
    property ObjectAntennaHeight: Double  Index (IS_OPTN) read FObjectAntennaHeight write SetObjectAntennaHeight stored ObjectAntennaHeight_Specified;
    property ObjectID:            string  Index (IS_OPTN or IS_NLBL) read FObjectID write SetObjectID stored ObjectID_Specified;
    property ObjectLatitude:      Double  Index (IS_OPTN) read FObjectLatitude write SetObjectLatitude stored ObjectLatitude_Specified;
    property ObjectLongitude:     Double  Index (IS_OPTN) read FObjectLongitude write SetObjectLongitude stored ObjectLongitude_Specified;
    property ObjectTypeSysname:   string  Index (IS_OPTN or IS_NLBL) read FObjectTypeSysname write SetObjectTypeSysname stored ObjectTypeSysname_Specified;
  end;



  // ************************************************************************ //
  // XML       : TCalcPointRequest, global, <element>
  // Namespace : http://schemas.datacontract.org/2004/07/ApiService
  // ************************************************************************ //
  TCalcPointRequest = class(TCalcPointRequest2)
  private
  published
  end;


  // ************************************************************************ //
  // Namespace : http://tempuri.org/
  // soapAction: http://tempuri.org/ICalcService/%operationName%
  // transport : http://schemas.xmlsoap.org/soap/http
  // style     : document
  // use       : literal
  // binding   : BasicHttpBinding_ICalcService
  // service   : Service1
  // port      : BasicHttpBinding_ICalcService
  // URL       : http://localhost/ApiService/Service.svc
  // ************************************************************************ //
  ICalcService = interface(IInvokable)
  ['{BA45EEE6-B5A4-8C4A-35F6-E3616F4A4F6A}']
    function  Echo(const Value: string): string; stdcall;
    function  EchoCalcService(const Value: string): string; stdcall;
    function  RadioVisbilityCalculate(const Value: TCalcRequest2): TCalcResponse2; stdcall;
  end;

function GetICalcService(UseWSDL: Boolean=System.False; Addr: string=''; HTTPRIO: THTTPRIO = nil): ICalcService;


implementation
  uses System.SysUtils;

function GetICalcService(UseWSDL: Boolean; Addr: string; HTTPRIO: THTTPRIO): ICalcService;
const
  defWSDL = 'http://localhost/ApiService/Service.svc?singleWsdl';
  defURL  = 'http://localhost/ApiService/Service.svc';
  defSvc  = 'Service1';
  defPrt  = 'BasicHttpBinding_ICalcService';
var
  RIO: THTTPRIO;
begin
  Result := nil;
  if (Addr = '') then
  begin
    if UseWSDL then
      Addr := defWSDL
    else
      Addr := defURL;
  end;
  if HTTPRIO = nil then
    RIO := THTTPRIO.Create(nil)
  else
    RIO := HTTPRIO;
  try
    Result := (RIO as ICalcService);
    if UseWSDL then
    begin
      RIO.WSDLLocation := Addr;
      RIO.Service := defSvc;
      RIO.Port := defPrt;
    end else
      RIO.URL := Addr;
  finally
    if (Result = nil) and (HTTPRIO = nil) then
      RIO.Free;
  end;
end;


procedure TCalcResponse2.SetCalculationCode(Index: Integer; const Astring: string);
begin
  FCalculationCode := Astring;
  FCalculationCode_Specified := True;
end;

function TCalcResponse2.CalculationCode_Specified(Index: Integer): boolean;
begin
  Result := FCalculationCode_Specified;
end;

procedure TCalcResponse2.SetMessage_(Index: Integer; const Astring: string);
begin
  FMessage_ := Astring;
  FMessage__Specified := True;
end;

function TCalcResponse2.Message__Specified(Index: Integer): boolean;
begin
  Result := FMessage__Specified;
end;

procedure TCalcResponse2.SetType_(Index: Integer; const Astring: string);
begin
  FType_ := Astring;
  FType__Specified := True;
end;

function TCalcResponse2.Type__Specified(Index: Integer): boolean;
begin
  Result := FType__Specified;
end;

destructor TCalcRequest2.Destroy;
var
  I: Integer;
begin
  for I := 0 to System.Length(FPoints)-1 do
    System.SysUtils.FreeAndNil(FPoints[I]);
  System.SetLength(FPoints, 0);
  inherited Destroy;
end;

procedure TCalcRequest2.SetCalculationCode(Index: Integer; const Astring: string);
begin
  FCalculationCode := Astring;
  FCalculationCode_Specified := True;
end;

function TCalcRequest2.CalculationCode_Specified(Index: Integer): boolean;
begin
  Result := FCalculationCode_Specified;
end;

procedure TCalcRequest2.SetCanChangePositionH(Index: Integer; const ABoolean: Boolean);
begin
  FCanChangePositionH := ABoolean;
  FCanChangePositionH_Specified := True;
end;

function TCalcRequest2.CanChangePositionH_Specified(Index: Integer): boolean;
begin
  Result := FCanChangePositionH_Specified;
end;

procedure TCalcRequest2.SetCanChangePositionV(Index: Integer; const ABoolean: Boolean);
begin
  FCanChangePositionV := ABoolean;
  FCanChangePositionV_Specified := True;
end;

function TCalcRequest2.CanChangePositionV_Specified(Index: Integer): boolean;
begin
  Result := FCanChangePositionV_Specified;
end;

procedure TCalcRequest2.SetClientAntennaHeight(Index: Integer; const ADouble: Double);
begin
  FClientAntennaHeight := ADouble;
  FClientAntennaHeight_Specified := True;
end;

function TCalcRequest2.ClientAntennaHeight_Specified(Index: Integer): boolean;
begin
  Result := FClientAntennaHeight_Specified;
end;

procedure TCalcRequest2.SetClientAntennaLatitude(Index: Integer; const ADouble: Double);
begin
  FClientAntennaLatitude := ADouble;
  FClientAntennaLatitude_Specified := True;
end;

function TCalcRequest2.ClientAntennaLatitude_Specified(Index: Integer): boolean;
begin
  Result := FClientAntennaLatitude_Specified;
end;

procedure TCalcRequest2.SetClientAntennaLongitude(Index: Integer; const ADouble: Double);
begin
  FClientAntennaLongitude := ADouble;
  FClientAntennaLongitude_Specified := True;
end;

function TCalcRequest2.ClientAntennaLongitude_Specified(Index: Integer): boolean;
begin
  Result := FClientAntennaLongitude_Specified;
end;

procedure TCalcRequest2.SetClientHWTypeID(Index: Integer; const Astring: string);
begin
  FClientHWTypeID := Astring;
  FClientHWTypeID_Specified := True;
end;

function TCalcRequest2.ClientHWTypeID_Specified(Index: Integer): boolean;
begin
  Result := FClientHWTypeID_Specified;
end;

procedure TCalcRequest2.SetClientLatitude(Index: Integer; const ADouble: Double);
begin
  FClientLatitude := ADouble;
  FClientLatitude_Specified := True;
end;

function TCalcRequest2.ClientLatitude_Specified(Index: Integer): boolean;
begin
  Result := FClientLatitude_Specified;
end;

procedure TCalcRequest2.SetClientLongitude(Index: Integer; const ADouble: Double);
begin
  FClientLongitude := ADouble;
  FClientLongitude_Specified := True;
end;

function TCalcRequest2.ClientLongitude_Specified(Index: Integer): boolean;
begin
  Result := FClientLongitude_Specified;
end;

procedure TCalcRequest2.SetMinSpeed(Index: Integer; const ADouble: Double);
begin
  FMinSpeed := ADouble;
  FMinSpeed_Specified := True;
end;

function TCalcRequest2.MinSpeed_Specified(Index: Integer): boolean;
begin
  Result := FMinSpeed_Specified;
end;

procedure TCalcRequest2.SetPoints(Index: Integer; const AArrayOfTCalcPointRequest: ArrayOfTCalcPointRequest);
begin
  FPoints := AArrayOfTCalcPointRequest;
  FPoints_Specified := True;
end;

function TCalcRequest2.Points_Specified(Index: Integer): boolean;
begin
  Result := FPoints_Specified;
end;

procedure TCalcRequest2.SetTechnology(Index: Integer; const Astring: string);
begin
  FTechnology := Astring;
  FTechnology_Specified := True;
end;

function TCalcRequest2.Technology_Specified(Index: Integer): boolean;
begin
  Result := FTechnology_Specified;
end;

procedure TCalcPointRequest2.SetObjectAntennaHeight(Index: Integer; const ADouble: Double);
begin
  FObjectAntennaHeight := ADouble;
  FObjectAntennaHeight_Specified := True;
end;

function TCalcPointRequest2.ObjectAntennaHeight_Specified(Index: Integer): boolean;
begin
  Result := FObjectAntennaHeight_Specified;
end;

procedure TCalcPointRequest2.SetObjectID(Index: Integer; const Astring: string);
begin
  FObjectID := Astring;
  FObjectID_Specified := True;
end;

function TCalcPointRequest2.ObjectID_Specified(Index: Integer): boolean;
begin
  Result := FObjectID_Specified;
end;

procedure TCalcPointRequest2.SetObjectLatitude(Index: Integer; const ADouble: Double);
begin
  FObjectLatitude := ADouble;
  FObjectLatitude_Specified := True;
end;

function TCalcPointRequest2.ObjectLatitude_Specified(Index: Integer): boolean;
begin
  Result := FObjectLatitude_Specified;
end;

procedure TCalcPointRequest2.SetObjectLongitude(Index: Integer; const ADouble: Double);
begin
  FObjectLongitude := ADouble;
  FObjectLongitude_Specified := True;
end;

function TCalcPointRequest2.ObjectLongitude_Specified(Index: Integer): boolean;
begin
  Result := FObjectLongitude_Specified;
end;

procedure TCalcPointRequest2.SetObjectTypeSysname(Index: Integer; const Astring: string);
begin
  FObjectTypeSysname := Astring;
  FObjectTypeSysname_Specified := True;
end;

function TCalcPointRequest2.ObjectTypeSysname_Specified(Index: Integer): boolean;
begin
  Result := FObjectTypeSysname_Specified;
end;

initialization
  { ICalcService }
  InvRegistry.RegisterInterface(TypeInfo(ICalcService), 'http://tempuri.org/', 'utf-8');
  InvRegistry.RegisterDefaultSOAPAction(TypeInfo(ICalcService), 'http://tempuri.org/ICalcService/%operationName%');
  InvRegistry.RegisterInvokeOptions(TypeInfo(ICalcService), ioDocument);
  { ICalcService.Echo }
  InvRegistry.RegisterMethodInfo(TypeInfo(ICalcService), 'Echo', '',
                                 '[ReturnName="EchoResult"]', IS_OPTN or IS_NLBL);
  InvRegistry.RegisterParamInfo(TypeInfo(ICalcService), 'Echo', 'Value', '',
                                '', IS_NLBL);
  InvRegistry.RegisterParamInfo(TypeInfo(ICalcService), 'Echo', 'EchoResult', '',
                                '', IS_NLBL);
  { ICalcService.EchoCalcService }
  InvRegistry.RegisterMethodInfo(TypeInfo(ICalcService), 'EchoCalcService', '',
                                 '[ReturnName="EchoCalcServiceResult"]', IS_OPTN or IS_NLBL);
  InvRegistry.RegisterParamInfo(TypeInfo(ICalcService), 'EchoCalcService', 'Value', '',
                                '', IS_NLBL);
  InvRegistry.RegisterParamInfo(TypeInfo(ICalcService), 'EchoCalcService', 'EchoCalcServiceResult', '',
                                '', IS_NLBL);
  { ICalcService.RadioVisbilityCalculate }
  InvRegistry.RegisterMethodInfo(TypeInfo(ICalcService), 'RadioVisbilityCalculate', '',
                                 '[ReturnName="RadioVisbilityCalculateResult"]', IS_OPTN or IS_NLBL);
  InvRegistry.RegisterParamInfo(TypeInfo(ICalcService), 'RadioVisbilityCalculate', 'Value', '',
                                '[Namespace="http://schemas.datacontract.org/2004/07/ApiService"]', IS_NLBL);
  InvRegistry.RegisterParamInfo(TypeInfo(ICalcService), 'RadioVisbilityCalculate', 'RadioVisbilityCalculateResult', '',
                                '[Namespace="http://schemas.datacontract.org/2004/07/ApiService"]', IS_NLBL);
  RemClassRegistry.RegisterXSInfo(TypeInfo(ArrayOfTCalcPointRequest), 'http://schemas.datacontract.org/2004/07/ApiService', 'ArrayOfTCalcPointRequest');
  RemClassRegistry.RegisterXSClass(TCalcResponse2, 'http://schemas.datacontract.org/2004/07/ApiService', 'TCalcResponse2', 'TCalcResponse');
  RemClassRegistry.RegisterExternalPropName(TypeInfo(TCalcResponse2), 'Message_', '[ExtName="Message"]');
  RemClassRegistry.RegisterExternalPropName(TypeInfo(TCalcResponse2), 'Type_', '[ExtName="Type"]');
  RemClassRegistry.RegisterXSClass(TCalcResponse, 'http://schemas.datacontract.org/2004/07/ApiService', 'TCalcResponse');
  RemClassRegistry.RegisterXSClass(TCalcRequest2, 'http://schemas.datacontract.org/2004/07/ApiService', 'TCalcRequest2', 'TCalcRequest');
  RemClassRegistry.RegisterExternalPropName(TypeInfo(TCalcRequest2), 'Points', '[ArrayItemName="TCalcPointRequest"]');
  RemClassRegistry.RegisterXSClass(TCalcRequest, 'http://schemas.datacontract.org/2004/07/ApiService', 'TCalcRequest');
  RemClassRegistry.RegisterXSClass(TCalcPointRequest2, 'http://schemas.datacontract.org/2004/07/ApiService', 'TCalcPointRequest2', 'TCalcPointRequest');
  RemClassRegistry.RegisterXSClass(TCalcPointRequest, 'http://schemas.datacontract.org/2004/07/ApiService', 'TCalcPointRequest');

end.