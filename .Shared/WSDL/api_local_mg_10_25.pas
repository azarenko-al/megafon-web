// ************************************************************************ //
// The types declared in this file were generated from data read from the
// WSDL File described below:
// WSDL     : http://localhost/MegaGIS/Service1.svc?singleWsdl
//  >Import : http://localhost/MegaGIS/Service1.svc?singleWsdl>0
//  >Import : http://localhost/MegaGIS/Service1.svc?singleWsdl>1
//  >Import : http://localhost/MegaGIS/Service1.svc?singleWsdl>2
// Encoding : utf-8
// Version  : 1.0
// (25.10.2020 21:52:47 - - $Rev: 86412 $)
// ************************************************************************ //

unit api_local_mg_10_25;

interface

uses Soap.InvokeRegistry, Soap.SOAPHTTPClient, System.Types;

const
  IS_OPTN = $0001;
  IS_UNBD = $0002;
  IS_NLBL = $0004;
  IS_REF  = $0080;


type

  // ************************************************************************ //
  // The following types, referred to in the WSDL document are not being represented
  // in this file. They are either aliases[@] of other types represented or were referred
  // to but never[!] declared in the document. The types from the latter category
  // typically map to predefined/known XML or Embarcadero types; however, they could also 
  // indicate incorrect WSDL documents that failed to declare or import a schema type.
  // ************************************************************************ //
  // !:string          - "http://www.w3.org/2001/XMLSchema"[Gbl]
  // !:float           - "http://www.w3.org/2001/XMLSchema"[Gbl]
  // !:base64Binary    - "http://www.w3.org/2001/XMLSchema"[Gbl]

  CalculationResult2   = class;                 { "http://schemas.datacontract.org/2004/07/ApiService"[GblCplx] }
  CalculationResult    = class;                 { "http://schemas.datacontract.org/2004/07/ApiService"[GblElm] }
  Result2              = class;                 { "http://schemas.datacontract.org/2004/07/ApiService"[GblCplx] }
  Result               = class;                 { "http://schemas.datacontract.org/2004/07/ApiService"[GblElm] }
  CalculatePoint2      = class;                 { "http://schemas.datacontract.org/2004/07/ApiService"[GblCplx] }
  CalculatePoint       = class;                 { "http://schemas.datacontract.org/2004/07/ApiService"[GblElm] }

  ArrayOfCalculatePoint = array of CalculatePoint2;   { "http://schemas.datacontract.org/2004/07/ApiService"[GblCplx] }


  // ************************************************************************ //
  // XML       : CalculationResult, global, <complexType>
  // Namespace : http://schemas.datacontract.org/2004/07/ApiService
  // ************************************************************************ //
  CalculationResult2 = class(TRemotable)
  private
    FCalculationCode: string;
    FCalculationCode_Specified: boolean;
    FDescription: string;
    FDescription_Specified: boolean;
    FPoints: ArrayOfCalculatePoint;
    FPoints_Specified: boolean;
    FResult: string;
    FResult_Specified: boolean;
    FTechnology: string;
    FTechnology_Specified: boolean;
    procedure SetCalculationCode(Index: Integer; const Astring: string);
    function  CalculationCode_Specified(Index: Integer): boolean;
    procedure SetDescription(Index: Integer; const Astring: string);
    function  Description_Specified(Index: Integer): boolean;
    procedure SetPoints(Index: Integer; const AArrayOfCalculatePoint: ArrayOfCalculatePoint);
    function  Points_Specified(Index: Integer): boolean;
    procedure SetResult(Index: Integer; const Astring: string);
    function  Result_Specified(Index: Integer): boolean;
    procedure SetTechnology(Index: Integer; const Astring: string);
    function  Technology_Specified(Index: Integer): boolean;
  public
    destructor Destroy; override;
  published
    property CalculationCode: string                 Index (IS_OPTN or IS_NLBL) read FCalculationCode write SetCalculationCode stored CalculationCode_Specified;
    property Description:     string                 Index (IS_OPTN or IS_NLBL) read FDescription write SetDescription stored Description_Specified;
    property Points:          ArrayOfCalculatePoint  Index (IS_OPTN or IS_NLBL) read FPoints write SetPoints stored Points_Specified;
    property Result:          string                 Index (IS_OPTN or IS_NLBL) read FResult write SetResult stored Result_Specified;
    property Technology:      string                 Index (IS_OPTN or IS_NLBL) read FTechnology write SetTechnology stored Technology_Specified;
  end;



  // ************************************************************************ //
  // XML       : CalculationResult, global, <element>
  // Namespace : http://schemas.datacontract.org/2004/07/ApiService
  // ************************************************************************ //
  CalculationResult = class(CalculationResult2)
  private
  published
  end;



  // ************************************************************************ //
  // XML       : Result, global, <complexType>
  // Namespace : http://schemas.datacontract.org/2004/07/ApiService
  // ************************************************************************ //
  Result2 = class(TRemotable)
  private
    FMessage_: string;
    FMessage__Specified: boolean;
    FType_: string;
    FType__Specified: boolean;
    procedure SetMessage_(Index: Integer; const Astring: string);
    function  Message__Specified(Index: Integer): boolean;
    procedure SetType_(Index: Integer; const Astring: string);
    function  Type__Specified(Index: Integer): boolean;
  published
    property Message_: string  Index (IS_OPTN or IS_NLBL) read FMessage_ write SetMessage_ stored Message__Specified;
    property Type_:    string  Index (IS_OPTN or IS_NLBL) read FType_ write SetType_ stored Type__Specified;
  end;



  // ************************************************************************ //
  // XML       : Result, global, <element>
  // Namespace : http://schemas.datacontract.org/2004/07/ApiService
  // ************************************************************************ //
  Result = class(Result2)
  private
  published
  end;



  // ************************************************************************ //
  // XML       : CalculatePoint, global, <complexType>
  // Namespace : http://schemas.datacontract.org/2004/07/ApiService
  // ************************************************************************ //
  CalculatePoint2 = class(TRemotable)
  private
    FClientHWTypeID: string;
    FClientHWTypeID_Specified: boolean;
    FHeight: Single;
    FHeight_Specified: boolean;
    FLatitude: Single;
    FLatitude_Specified: boolean;
    FLocationImg: TByteDynArray;
    FLocationImg_Specified: boolean;
    FLongitude: Single;
    FLongitude_Specified: boolean;
    FObjectID: string;
    FObjectID_Specified: boolean;
    FProfileImg: TByteDynArray;
    FProfileImg_Specified: boolean;
    FZoneCopyImg: TByteDynArray;
    FZoneCopyImg_Specified: boolean;
    procedure SetClientHWTypeID(Index: Integer; const Astring: string);
    function  ClientHWTypeID_Specified(Index: Integer): boolean;
    procedure SetHeight(Index: Integer; const ASingle: Single);
    function  Height_Specified(Index: Integer): boolean;
    procedure SetLatitude(Index: Integer; const ASingle: Single);
    function  Latitude_Specified(Index: Integer): boolean;
    procedure SetLocationImg(Index: Integer; const ATByteDynArray: TByteDynArray);
    function  LocationImg_Specified(Index: Integer): boolean;
    procedure SetLongitude(Index: Integer; const ASingle: Single);
    function  Longitude_Specified(Index: Integer): boolean;
    procedure SetObjectID(Index: Integer; const Astring: string);
    function  ObjectID_Specified(Index: Integer): boolean;
    procedure SetProfileImg(Index: Integer; const ATByteDynArray: TByteDynArray);
    function  ProfileImg_Specified(Index: Integer): boolean;
    procedure SetZoneCopyImg(Index: Integer; const ATByteDynArray: TByteDynArray);
    function  ZoneCopyImg_Specified(Index: Integer): boolean;
  published
    property ClientHWTypeID: string         Index (IS_OPTN or IS_NLBL) read FClientHWTypeID write SetClientHWTypeID stored ClientHWTypeID_Specified;
    property Height:         Single         Index (IS_OPTN) read FHeight write SetHeight stored Height_Specified;
    property Latitude:       Single         Index (IS_OPTN) read FLatitude write SetLatitude stored Latitude_Specified;
    property LocationImg:    TByteDynArray  Index (IS_OPTN or IS_NLBL) read FLocationImg write SetLocationImg stored LocationImg_Specified;
    property Longitude:      Single         Index (IS_OPTN) read FLongitude write SetLongitude stored Longitude_Specified;
    property ObjectID:       string         Index (IS_OPTN or IS_NLBL) read FObjectID write SetObjectID stored ObjectID_Specified;
    property ProfileImg:     TByteDynArray  Index (IS_OPTN or IS_NLBL) read FProfileImg write SetProfileImg stored ProfileImg_Specified;
    property ZoneCopyImg:    TByteDynArray  Index (IS_OPTN or IS_NLBL) read FZoneCopyImg write SetZoneCopyImg stored ZoneCopyImg_Specified;
  end;



  // ************************************************************************ //
  // XML       : CalculatePoint, global, <element>
  // Namespace : http://schemas.datacontract.org/2004/07/ApiService
  // ************************************************************************ //
  CalculatePoint = class(CalculatePoint2)
  private
  published
  end;


  // ************************************************************************ //
  // Namespace : http://tempuri.org/
  // soapAction: http://tempuri.org/IApiIntegrationService/%operationName%
  // transport : http://schemas.xmlsoap.org/soap/http
  // style     : document
  // use       : literal
  // binding   : BasicHttpBinding_IApiIntegrationService
  // service   : IApiIntegrationServiceService
  // port      : BasicHttpBinding_IApiIntegrationService
  // URL       : http://localhost/MegaGIS/Service1.svc
  // ************************************************************************ //
  IApiIntegrationService = interface(IInvokable)
  ['{05D348DD-EAA6-AEEC-9FCF-6F07FA81CFE4}']
    function  Echo(const Value: string): string; stdcall;
    function  CalculateComplete(const content: CalculationResult2): Result2; stdcall;
  end;

function GetIApiIntegrationService(UseWSDL: Boolean=System.False; Addr: string=''; HTTPRIO: THTTPRIO = nil): IApiIntegrationService;


implementation
  uses System.SysUtils;

function GetIApiIntegrationService(UseWSDL: Boolean; Addr: string; HTTPRIO: THTTPRIO): IApiIntegrationService;
const
  defWSDL = 'http://localhost/MegaGIS/Service1.svc?singleWsdl';
  defURL  = 'http://localhost/MegaGIS/Service1.svc';
  defSvc  = 'IApiIntegrationServiceService';
  defPrt  = 'BasicHttpBinding_IApiIntegrationService';
var
  RIO: THTTPRIO;
begin
  Result := nil;
  if (Addr = '') then
  begin
    if UseWSDL then
      Addr := defWSDL
    else
      Addr := defURL;
  end;
  if HTTPRIO = nil then
    RIO := THTTPRIO.Create(nil)
  else
    RIO := HTTPRIO;
  try
    Result := (RIO as IApiIntegrationService);
    if UseWSDL then
    begin
      RIO.WSDLLocation := Addr;
      RIO.Service := defSvc;
      RIO.Port := defPrt;
    end else
      RIO.URL := Addr;
  finally
    if (Result = nil) and (HTTPRIO = nil) then
      RIO.Free;
  end;
end;


destructor CalculationResult2.Destroy;
var
  I: Integer;
begin
  for I := 0 to System.Length(FPoints)-1 do
    System.SysUtils.FreeAndNil(FPoints[I]);
  System.SetLength(FPoints, 0);
  inherited Destroy;
end;

procedure CalculationResult2.SetCalculationCode(Index: Integer; const Astring: string);
begin
  FCalculationCode := Astring;
  FCalculationCode_Specified := True;
end;

function CalculationResult2.CalculationCode_Specified(Index: Integer): boolean;
begin
  Result := FCalculationCode_Specified;
end;

procedure CalculationResult2.SetDescription(Index: Integer; const Astring: string);
begin
  FDescription := Astring;
  FDescription_Specified := True;
end;

function CalculationResult2.Description_Specified(Index: Integer): boolean;
begin
  Result := FDescription_Specified;
end;

procedure CalculationResult2.SetPoints(Index: Integer; const AArrayOfCalculatePoint: ArrayOfCalculatePoint);
begin
  FPoints := AArrayOfCalculatePoint;
  FPoints_Specified := True;
end;

function CalculationResult2.Points_Specified(Index: Integer): boolean;
begin
  Result := FPoints_Specified;
end;

procedure CalculationResult2.SetResult(Index: Integer; const Astring: string);
begin
  FResult := Astring;
  FResult_Specified := True;
end;

function CalculationResult2.Result_Specified(Index: Integer): boolean;
begin
  Result := FResult_Specified;
end;

procedure CalculationResult2.SetTechnology(Index: Integer; const Astring: string);
begin
  FTechnology := Astring;
  FTechnology_Specified := True;
end;

function CalculationResult2.Technology_Specified(Index: Integer): boolean;
begin
  Result := FTechnology_Specified;
end;

procedure Result2.SetMessage_(Index: Integer; const Astring: string);
begin
  FMessage_ := Astring;
  FMessage__Specified := True;
end;

function Result2.Message__Specified(Index: Integer): boolean;
begin
  Result := FMessage__Specified;
end;

procedure Result2.SetType_(Index: Integer; const Astring: string);
begin
  FType_ := Astring;
  FType__Specified := True;
end;

function Result2.Type__Specified(Index: Integer): boolean;
begin
  Result := FType__Specified;
end;

procedure CalculatePoint2.SetClientHWTypeID(Index: Integer; const Astring: string);
begin
  FClientHWTypeID := Astring;
  FClientHWTypeID_Specified := True;
end;

function CalculatePoint2.ClientHWTypeID_Specified(Index: Integer): boolean;
begin
  Result := FClientHWTypeID_Specified;
end;

procedure CalculatePoint2.SetHeight(Index: Integer; const ASingle: Single);
begin
  FHeight := ASingle;
  FHeight_Specified := True;
end;

function CalculatePoint2.Height_Specified(Index: Integer): boolean;
begin
  Result := FHeight_Specified;
end;

procedure CalculatePoint2.SetLatitude(Index: Integer; const ASingle: Single);
begin
  FLatitude := ASingle;
  FLatitude_Specified := True;
end;

function CalculatePoint2.Latitude_Specified(Index: Integer): boolean;
begin
  Result := FLatitude_Specified;
end;

procedure CalculatePoint2.SetLocationImg(Index: Integer; const ATByteDynArray: TByteDynArray);
begin
  FLocationImg := ATByteDynArray;
  FLocationImg_Specified := True;
end;

function CalculatePoint2.LocationImg_Specified(Index: Integer): boolean;
begin
  Result := FLocationImg_Specified;
end;

procedure CalculatePoint2.SetLongitude(Index: Integer; const ASingle: Single);
begin
  FLongitude := ASingle;
  FLongitude_Specified := True;
end;

function CalculatePoint2.Longitude_Specified(Index: Integer): boolean;
begin
  Result := FLongitude_Specified;
end;

procedure CalculatePoint2.SetObjectID(Index: Integer; const Astring: string);
begin
  FObjectID := Astring;
  FObjectID_Specified := True;
end;

function CalculatePoint2.ObjectID_Specified(Index: Integer): boolean;
begin
  Result := FObjectID_Specified;
end;

procedure CalculatePoint2.SetProfileImg(Index: Integer; const ATByteDynArray: TByteDynArray);
begin
  FProfileImg := ATByteDynArray;
  FProfileImg_Specified := True;
end;

function CalculatePoint2.ProfileImg_Specified(Index: Integer): boolean;
begin
  Result := FProfileImg_Specified;
end;

procedure CalculatePoint2.SetZoneCopyImg(Index: Integer; const ATByteDynArray: TByteDynArray);
begin
  FZoneCopyImg := ATByteDynArray;
  FZoneCopyImg_Specified := True;
end;

function CalculatePoint2.ZoneCopyImg_Specified(Index: Integer): boolean;
begin
  Result := FZoneCopyImg_Specified;
end;

initialization
  { IApiIntegrationService }
  InvRegistry.RegisterInterface(TypeInfo(IApiIntegrationService), 'http://tempuri.org/', 'utf-8');
  InvRegistry.RegisterDefaultSOAPAction(TypeInfo(IApiIntegrationService), 'http://tempuri.org/IApiIntegrationService/%operationName%');
  InvRegistry.RegisterInvokeOptions(TypeInfo(IApiIntegrationService), ioDocument);
  { IApiIntegrationService.Echo }
  InvRegistry.RegisterMethodInfo(TypeInfo(IApiIntegrationService), 'Echo', '',
                                 '[ReturnName="EchoResult"]', IS_OPTN or IS_NLBL);
  InvRegistry.RegisterParamInfo(TypeInfo(IApiIntegrationService), 'Echo', 'Value', '',
                                '', IS_NLBL);
  InvRegistry.RegisterParamInfo(TypeInfo(IApiIntegrationService), 'Echo', 'EchoResult', '',
                                '', IS_NLBL);
  { IApiIntegrationService.CalculateComplete }
  InvRegistry.RegisterMethodInfo(TypeInfo(IApiIntegrationService), 'CalculateComplete', '',
                                 '[ReturnName="CalculateCompleteResult"]', IS_OPTN or IS_NLBL);
  InvRegistry.RegisterParamInfo(TypeInfo(IApiIntegrationService), 'CalculateComplete', 'content', '',
                                '[Namespace="http://schemas.datacontract.org/2004/07/ApiService"]', IS_NLBL);
  InvRegistry.RegisterParamInfo(TypeInfo(IApiIntegrationService), 'CalculateComplete', 'CalculateCompleteResult', '',
                                '[Namespace="http://schemas.datacontract.org/2004/07/ApiService"]', IS_NLBL);
  RemClassRegistry.RegisterXSInfo(TypeInfo(ArrayOfCalculatePoint), 'http://schemas.datacontract.org/2004/07/ApiService', 'ArrayOfCalculatePoint');
  RemClassRegistry.RegisterXSClass(CalculationResult2, 'http://schemas.datacontract.org/2004/07/ApiService', 'CalculationResult2', 'CalculationResult');
  RemClassRegistry.RegisterExternalPropName(TypeInfo(CalculationResult2), 'Points', '[ArrayItemName="CalculatePoint"]');
  RemClassRegistry.RegisterXSClass(CalculationResult, 'http://schemas.datacontract.org/2004/07/ApiService', 'CalculationResult');
  RemClassRegistry.RegisterXSClass(Result2, 'http://schemas.datacontract.org/2004/07/ApiService', 'Result2', 'Result');
  RemClassRegistry.RegisterExternalPropName(TypeInfo(Result2), 'Message_', '[ExtName="Message"]');
  RemClassRegistry.RegisterExternalPropName(TypeInfo(Result2), 'Type_', '[ExtName="Type"]');
  RemClassRegistry.RegisterXSClass(Result, 'http://schemas.datacontract.org/2004/07/ApiService', 'Result');
  RemClassRegistry.RegisterXSClass(CalculatePoint2, 'http://schemas.datacontract.org/2004/07/ApiService', 'CalculatePoint2', 'CalculatePoint');
  RemClassRegistry.RegisterXSClass(CalculatePoint, 'http://schemas.datacontract.org/2004/07/ApiService', 'CalculatePoint');

end.