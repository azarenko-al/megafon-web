unit u_Task_send_to_Local_MG;

interface
uses
  u_web,


  u_logger,



  SysUtils,
  api_local_mg_10_25,
//  api_local_mg_10_20,

//  api_local_mg_09_01,
  u_Task_classes;


  function Task_Send_to_Local_MG(aTask: TTask; var aMessage: string): Boolean;


  function Task_Send_to_Local_MG_Echo(aEcho: string): string;

implementation

//     defWSDL = 'http://localhost/MegaGIS/Service1.svc?singleWsdl';
//http://localhost/ApiService/Service.svc?singleWsdl
//http://10.63.193.53:10073/mgbshpd/bshpd?wsdl



function Task_Send_to_Local_MG_Echo(aEcho: string): string;
var
  s: string;
  v: IApiIntegrationService;
begin
  v:=GetIApiIntegrationService; //(True, tRIM (Edit1.Text));

  Result := v.Echo (aEcho);

end;



//-------------------------------------------------------------------
function Task_Send_to_Local_MG(aTask: TTask; var aMessage: string): Boolean;
//-------------------------------------------------------------------
var
//  // URL       : http://localhost/MegaGIS/Service1.svc
//  // ************************************************************************ //
//  IApiIntegrationService = interface(IInvokable)
//  ['{05D348DD-EAA6-AEEC-9FCF-6F07FA81CFE4}']
//    function  Echo(const Value: string): string; stdcall;
//    function  CalculateComplete(const content: CalculationResult2): Result2; stdcall;
//  end;
//
//function GetIApiIntegrationService(UseWSDL: Boolean=System.False; Addr: string=''; HTTPRIO: THTTPRIO = nil): IApiIntegrationService;


  v: IApiIntegrationService;

  oCalculationResult: CalculationResult;
  oResult: Result2;
  oCalculatePoint : CalculatePoint;
  arrCalculatePoints : ArrayOfCalculatePoint;
  i: Integer;
 // s: string;
//  k: Integer;

begin
  Assert (Assigned (aTask));


  //----------------------------------------------------
  oCalculationResult:=CalculationResult.Create;
  oCalculationResult.CalculationCode:= aTask.GUID_str;
  oCalculationResult.Description:=aTask.calcResult.Description;// 'Description';
  oCalculationResult.Result     :=aTask.GetResultTypeStr();// '����������� ��������';
  oCalculationResult.Technology:=aTask.Technology;

  //----------------------------------------------------

  SetLength(arrCalculatePoints, aTask.Places.Count);

  for i:= 0 to aTask.Places.Count-1 do
  begin
    oCalculatePoint:=CalculatePoint.Create;
    arrCalculatePoints[i]:=oCalculatePoint;

    Assert (aTask.Places[i].ObjectID >= 0);

    oCalculatePoint.ObjectID := IntToStr(aTask.Places[i].ObjectID); //  dxMemData1['Object_ID']; //Trim(ed_ObjectID.Text); //'23-3453';

    oCalculatePoint.Latitude    := aTask.Places[i].CalcResult.WGS.client_antenna.lat;
    oCalculatePoint.Longitude   := aTask.Places[i].CalcResult.WGS.client_antenna.lon;
    oCalculatePoint.Height      := aTask.Places[i].CalcResult.client_antenna_height;
//    oCalculatePoint.ClientHWTypeID:='Cambium';

    oCalculatePoint.ClientHWTypeID:=aTask.Places[i].ClientHWTypeID;

{
    if Eq(aTask.Technology,'���') then
      oCalculatePoint.ClientHWTypeID:='HW_RTN900_18G_XMC-3_ODU_Typical, ������� 0.6'
    else
      oCalculatePoint.ClientHWTypeID:='Cambium ePMP 1000 6GHz, ������� 0.6';
 }


  //  oCalculatePoint.Technology:=aTask.Places[i].t //'���';
//    oCalculatePoint.ClientHWTypeID:='-';

///    if i < g_Config.MegaGIS.response_image_count then
    begin
      if FileExists(aTask.Places[i].CalcResult.FileName_Map) then
        oCalculatePoint.LocationImg:=FileToByteDynArray(aTask.Places[i].CalcResult.FileName_Map);

      if FileExists(aTask.Places[i].CalcResult.FileName_Profile) then
        oCalculatePoint.ProfileImg:=FileToByteDynArray(aTask.Places[i].CalcResult.FileName_Profile);

      if FileExists(aTask.Places[i].CalcResult.FileName_Zone) then
        oCalculatePoint.ZoneCopyImg:=FileToByteDynArray(aTask.Places[i].CalcResult.FileName_Zone);
    end;


  end;



  oCalculationResult.Points := arrCalculatePoints;

  //----------------------------------------------------

  try
    {
    Assert (aURL<>'');

    if aURL<>'' then
      v:=GetIService_MG(False, aURL) //(True, tRIM (Edit1.Text));
    else
     }

    v:=GetIApiIntegrationService; //(True, tRIM (Edit1.Text));

    oResult := v.CalculateComplete (oCalculationResult);

    aMessage:=oResult.Message_;
    FreeAndNil(oResult);

    Result:=True;

  except on E: Exception do
    begin
      g_Logger.Error('Task_Send_to_Local_MG ' + E.Message);

      Result:=False;

    end;

  end;



  for I := 0 to High(arrCalculatePoints) do
    FreeAndNil(arrCalculatePoints[i]);


  FreeAndNil(oCalculationResult);


end;


end.
