unit u_Task_send_to_Remote_MG_prom;

interface
uses
  u_web,
  u_func,
  u_Config,
  u_Logger,

  SysUtils,



  api_mg_prom_10_21,

  u_Task_classes;


  function Task_Send_to_IApiIntegrationService_MG_prom(aTask: TTask; var
      aMessage: string): Boolean;


implementation

//
//http://localhost/ApiService/Service.svc?singleWsdl
//http://10.63.193.53:10073/mgbshpd/bshpd?wsdl


//-------------------------------------------------------------------
function Task_Send_to_IApiIntegrationService_MG_prom(aTask: TTask; var
    aMessage: string): Boolean;
//-------------------------------------------------------------------
var
  v: IApiIntegrationService;

  oCalculationResult: CalculationResult2;
  oResult: Result2;
  oCalculatePoint : CalculatePoint;

  arrCalculatePoints : ArrayOfCalculatePoint;
  i: Integer;
  k: Integer;

begin
  Result:=False;

  Assert (Assigned (aTask));


  //----------------------------------------------------
  oCalculationResult:=CalculationResult.Create;
  oCalculationResult.CalculationCode:= aTask.GUID_str;
  oCalculationResult.Description:=aTask.calcResult.Description;// 'Description';
  oCalculationResult.Result     :=aTask.GetResultTypeStr();// '??????????? ????????';
  oCalculationResult.Technology :=aTask.Technology;


  //----------------------------------------------------

  SetLength(arrCalculatePoints, aTask.Places.Count);

  for i:= 0 to aTask.Places.Count-1 do
  begin
    oCalculatePoint:=CalculatePoint.Create;
    arrCalculatePoints[i]:=oCalculatePoint;

    Assert (aTask.Places[i].ObjectID >= 0);

    oCalculatePoint.ObjectID := IntToStr(aTask.Places[i].ObjectID); //  dxMemData1['Object_ID']; //Trim(ed_ObjectID.Text); //'23-3453';

    oCalculatePoint.Latitude  := aTask.Places[i].CalcResult.WGS.client_antenna.lat;
    oCalculatePoint.Longitude := aTask.Places[i].CalcResult.WGS.client_antenna.lon;
    oCalculatePoint.Height    := aTask.Places[i].CalcResult.client_antenna_height;


    oCalculatePoint.ClientHWTypeID:=aTask.Places[i].ClientHWTypeID;

{
    if Eq(aTask.Technology,'???') then
      oCalculatePoint.ClientHWTypeID:='HW_RTN900_18G_XMC-3_ODU_Typical, ??????? 0.6'
    else if Eq(aTask.Technology,'WiMax') then
//    else
      oCalculatePoint.ClientHWTypeID:='Cambium ePMP 1000 6GHz, ??????? 0.6';
 }

//    if i < g_Config.MegaGIS.response_image_count then
    begin

      with aTask.Places[i].CalcResult do
      begin
        if FileExists(FileName_map) then
          oCalculatePoint.LocationImg:=FileToByteDynArray(FileName_map);

        if FileExists(FileName_Profile) then
          oCalculatePoint.ProfileImg:=FileToByteDynArray(FileName_Profile);

        if FileExists(FileName_Zone) then
          oCalculatePoint.ZoneCopyImg:=FileToByteDynArray(FileName_Zone);

      end;
    end;

  end;

  oCalculationResult.Points := arrCalculatePoints;

  //----------------------------------------------------
  for I := 0 to g_Config.Send_ESB.attemps-1 do
  begin
    try

  //    if aURL<>'' then
  //      v:=GetIApiIntegrationService(False, aURL)
  //    else
//      v:=GetIApiIntegrationService;
      v:=GetIApiIntegrationService (False, g_Config.WSDL_URL);


//function GetIApiIntegrationService(UseWSDL: Boolean; Addr: string; HTTPRIO: THTTPRIO): IApiIntegrationService;
//const
//  defWSDL = 'C:\inetpub\wwwroot\MegaGIS\_WDSL\mg_prom.wsdl';
//  defURL  = 'http://vlg-nri-oahap01.megafon.ru/mgbshpd/mg';
//  defSvc  = 'IApiIntegrationServiceService';
//  defPrt  = 'IApiIntegrationServicePort';



      oResult := v.CalculateComplete (oCalculationResult);

     // Result :=oResult.Message_;

      aMessage:=oResult.Message_;
      FreeAndNil(oResult);

      Result:=True;
      Break;

    except on E: Exception do
      begin
          g_Logger.Error('Task_Send_to_IApiIntegrationService :' + E.Message);
      end;
    end;

    g_Logger.Error('Task_Send_to_IApiIntegrationService - ?????????? - sec:' + IntToStr(g_Config.Send_ESB.delay_sec));
    Delay_sec (g_Config.Send_ESB.delay_sec);

  end;

//  oCalculationResult.Points.

//  for i:= 0 to .Count-1 do
//    oCalculatePoint:=CalculatePoint.Create;

  for I := 0 to High(arrCalculatePoints) do
    FreeAndNil(arrCalculatePoints[i]);

  FreeAndNil(oCalculationResult);
//  FreeAndNil(oResult);


end;


end.

