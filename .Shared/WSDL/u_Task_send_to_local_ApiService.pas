unit u_Task_send_to_local_ApiService;


interface
uses


  SysUtils,

  api_local_ApiService_09_16,
  u_Task_classes;


  function Task_Send_to_Local_ApiService(aTask: TTask; aWSDL: string = ''):
      string;


implementation


//-------------------------------------------------------------------
function Task_Send_to_Local_ApiService(aTask: TTask; aWSDL: string = ''):
    string;
//-------------------------------------------------------------------
var
  v: ICalcService;

  oCalcRequest: TCalcRequest;
  oCalcResponse: TCalcResponse2;
 // oResult: TResult2;

  oCalcPointRequest : TCalcPointRequest;

  arrCalculatePoints : ArrayOfTCalcPointRequest;
  i: Integer;
  k: Integer;

begin
  //----------------------------------------------------
  oCalcRequest:=TCalcRequest.Create;
  oCalcRequest.CalculationCode:= aTask.GUID_str;

  oCalcRequest.ClientLatitude:=aTask.WGS.client_lat;
  oCalcRequest.ClientLongitude:=aTask.WGS.client_lon;

  oCalcRequest.ClientAntennaLatitude:=aTask.WGS.client_antenna_lat;
  oCalcRequest.ClientAntennaLongitude:=aTask.WGS.client_antenna_lon;
  oCalcRequest.ClientAntennaHeight:=aTask.client_antenna_height;


  oCalcRequest.CanChangePositionH:=aTask.is_can_change_antenna_height;
  oCalcRequest.CanChangePositionV:=aTask.is_can_change_antenna_location;

  oCalcRequest.Technology:=aTask.Technology;
  oCalcRequest.MinSpeed  :=aTask.bitrate_Mbps;


  //----------------------------------------------------

  SetLength(arrCalculatePoints, aTask.Places.Count);

  for i:= 0 to aTask.Places.Count-1 do
  begin
    oCalcPointRequest:=TCalcPointRequest.Create;
    arrCalculatePoints[i]:=oCalcPointRequest;

    oCalcPointRequest.ObjectID := IntToStr(aTask.Places[i].ObjectID); //  dxMemData1['Object_ID']; //Trim(ed_ObjectID.Text); //'23-3453';
   // oCalcPointRequest.ObjectTypeSysname:= aTask.Places[i].SysName;

    oCalcPointRequest.ObjectLatitude     := aTask.Places[i].Pos_WGS.Lat;
    oCalcPointRequest.ObjectLongitude    := aTask.Places[i].Pos_WGS.Lon;
    oCalcPointRequest.ObjectAntennaHeight:= aTask.Places[i].Height;

//    oCalculatePoint.ClientHWTypeID:='-';

  end;


  oCalcRequest.Points := arrCalculatePoints;

  //----------------------------------------------------

//   cb_WSDL.Text);

  try
    if aWSDL<>'' then
      v:=GetICalcService(True, aWSDL)
   else
      v:=GetICalcService; //(True, tRIM (Edit1.Text));

    oCalcResponse := v.RadioVisbilityCalculate (oCalcRequest);

  except on E: Exception do

  end;



  Result :='Type:'+oCalcResponse.Type_ +'  Message:'+  oCalcResponse.Message_;


  FreeAndNil(oCalcResponse);

  for i:= 0 to aTask.Places.Count-1 do
    FreeAndNil(arrCalculatePoints[i]);

  FreeAndNil(oCalcRequest);

end;


end.


{

  try
  except on E: Exception do

  end;


//-------------------------------------------------------------------
procedure Tfrm_Test.Button1_calcClick(Sender: TObject);
//-------------------------------------------------------------------
var
//TRPLS_CalcRequest

  oCalcRequest: TRPLS_CalcRequest;
  oCalcPoint: TRPLS_CalcPoint;

  oTask: TTask;
  oTaskPoint: TTaskPlace;

  arr: TRPLS_CalcPointArray;

begin
  oCalcRequest:=TRPLS_CalcRequest.Create;


  with t_Task do
  begin

    oCalcRequest.GUID                :=FieldValues['CalculationCode'];

    oCalcRequest.client_lat          := FieldByName('client_lat').AsFloat;
    oCalcRequest.client_lon          := FieldByName('client_lon').AsFloat;

    oCalcRequest.client_antenna_lat    := FieldByName('client_antenna_lat').AsFloat;
    oCalcRequest.client_antenna_lon    := FieldByName('client_antenna_lon').AsFloat;
    oCalcRequest.client_antenna_height := FieldByName('client_antenna_height').AsFloat;

    oCalcRequest.is_can_change_antenna_height   := FieldByName('is_can_change_antenna_height').AsBoolean;
    oCalcRequest.is_can_change_antenna_location := FieldByName('is_can_change_antenna_location').AsBoolean;

    oCalcRequest.bitrate_Mbps        := FieldByName('bitrate_Mbps').AsFloat;

   // oCalcRequest.ClientHWTypeID:= '-';
//    oCalcRequest.ClientHWTypeID:= FieldByName('bitrate_Mbps').AsFloat;

  //  oCalcRequest.Technology  := FieldByName('Technology').AsString;

  end;


  //t_Task_places.First;


  assert (t_Task_places.RecordCount > 0);

//  SetLength (oCalcRequest.Points, t_Task_places.RecordCount);
  SetLength (arr, t_Task_places.RecordCount);

  with t_Task_places do
    while not EOF do
    begin
      oCalcPoint:=TRPLS_CalcPoint.Create;
      arr[ RecNo-1 ]:=oCalcPoint;


      oCalcPoint.ObjectID            :=FieldByName('ObjectID').AsString;
      oCalcPoint.ObjectTypeSysname   :=FieldByName('Sysname').AsString;

      oCalcPoint.ObjectLatitude      :=FieldByName('Latitude').AsFloat;
      oCalcPoint.ObjectLongitude     :=FieldByName('Longitude').AsFloat;
      oCalcPoint.ObjectAntennaHeight :=FieldByName('Height').AsFloat;

//
//      oCalcPoint.Wgs.place_lat   :=aDataset.FieldByName('place_lat').AsFloat;
//      oCalcPoint.Wgs.place_Lon   :=aDataset.FieldByName('place_lon').AsFloat;
//      oCalcPoint.place_Height    :=aDataset.FieldByName('place_Height').AsFloat;

      Next;
    end;

    oCalcRequest.Points:=arr;


//  oTask.Places.LoadFromDataset(t_Task_places);



{
  with t_Task_places do
    while not EOF do
  begin
    oTaskPoint:=oTask.Points.AddItem;

//    oCalcPointRequest.ObjectTypeSysname   :='-';
    oTaskPoint.place_name  :=FieldByName('place_name').AsString;
    oTaskPoint.place_lat   :=FieldByName('place_lat').AsFloat;
    oTaskPoint.place_lon   :=FieldByName('place_lat').AsFloat;
    oTaskPoint.place_height:=FieldByName('place_height').AsFloat;


//    Insert(oCalcPointRequest, oCalcRequest.Points, MaxInt);

    Next;
  end;
  }



  oTask:=TTask.Create;
  oTask.LoadFromSOAP(oCalcRequest);


  Assert(Assigned(dmTask_Calc));
  dmTask_Calc.Calc (oTask);

//
//
//  Log ('CalcResponse -----------------------------');
//  Log (Format('CalculationCode: %s', [oCalcResponse.CalculationCode]));
//  Log (Format('Message_: %s',        [oCalcResponse.Message_]));
//  Log (Format('Type_: %s',           [oCalcResponse.Type_]));
//

end;
