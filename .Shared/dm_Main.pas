﻿unit dm_Main;

interface

uses
  dm_DB_Manager_custom,

  u_Config,


  u_Logger,

 // u_const_db,

  Dialogs, Forms,
  System.Classes, Data.DB, Data.Win.ADODB;

type
  TdmMain = class(TdmDB_Manager_custom)
    procedure DataModuleCreate(Sender: TObject);
  private
 //   function NRI_find1111111111(aLat, aLon: Double): string;

  public
    procedure Close;
    procedure Config_Load_;

    class procedure Init;

    function Open: Boolean;

  end;

var
  dmMain: TdmMain;


implementation

{%CLASSGROUP 'Vcl.Controls.TControl'}

{$R *.dfm}



class procedure TdmMain.Init;
begin
  if not Assigned(dmMain) then
    Application.CreateForm(TdmMain, dmMain);
end;

//-------------------------------------------------------------------
procedure TdmMain.Config_Load_;
//-------------------------------------------------------------------
// Load settings
//-------------------------------------------------------------------

begin

//if not g_Config.IsLoaded then
//  begin
  OpenTable(ADOTable1, 'Settings');
  g_Config.LoadFromDataset(ADOTable1);

  OpenTable(ADOTable1, 'settings_Clutter_Logic');
  g_Config.ClutterLogic.LoadFromDataset(ADOTable1);

 // g_Config.IsLoaded :=true;
//  end;


  // TODO -cMM: TdmMain.aResult_msg default body inserted
end;

//-------------------------------------------------------------------
procedure TdmMain.DataModuleCreate(Sender: TObject);
//-------------------------------------------------------------------
begin
  inherited;

  if ADOConnection1.Connected then
    ShowMessage ('TdmMain.DataModuleCreate  ADOConnection1.Connected');


 // Config_Load_();

end;

//-------------------------------------------------------------------
function TdmMain.Open: Boolean;
//-------------------------------------------------------------------
begin

//  if ADOConnection1.Connected then
//  begin
//    ShowMessage ('ADOConnection1.Connected');
//    CodeSite.SendError('ADOConnection1.Connected');
//  end;

  Assert (g_Config.ConnectionStr <> '', 'g_Config.ConnectionStr .....');

 // ShowMessage(g_Config.ConnectionStr );

  Result:=ADOConnection_Open ( g_Config.ConnectionStr );
  if not Result then
    g_Logger.Error('Connection error: ' +g_Config.ConnectionStr);


//  ShowMessage('Ok' );

end;


//-------------------------------------------------------------------
procedure TdmMain.Close;
//-------------------------------------------------------------------
begin
  ADOConnection1.Close;

end;



end.

