unit u_Config;

interface
uses
  IniFiles, SysUtils, Forms, DB,
  System.Generics.Defaults,  Dialogs, System.Generics.Collections,
  StrUtils, IOUtils;


type

  TConfig_Clutter_Logic_rec = record
    Clutter_code : byte;
    height : double;
    is_terminate_calc : Boolean;
  end;


  TConfig_Clutter_Logic_Dictionary = class (TDictionary<byte, TConfig_Clutter_Logic_rec>)
  public
    procedure LoadFromDataset(aDataset: DB.TDataset);
  end;

  TReliefClutters_Dictionary = class (TDictionary<byte, double>)
  public
    procedure LoadFromDataset(aDataset: DB.TDataset);
  end;

//   ReliefClutters_ : TDictionary<byte, double>;

  //--------------------------------
  TConfig = class
  //--------------------------------
  private
     Ftechnology_not_used: string;

  public
    Send_ESB :record
      attemps   : word;
      delay_sec : word;
    end;


    // INI
 //   Port : word;
//    Thread_count : integer;
    freq_Ghz : byte;

    Map_HTML_template : string;
    Map_print_HTML_template : string;

    ConnectionStr : string;
    Log_Dir : string;

    WSDL_URL : string;

  public

//    Provider=SQLOLEDB.1;Integrated Security=SSPI;Persist Security Info=False;Initial Catalog=megafon_WireLess;Data Source=ASTRA\MSSQLSERVER_2014

    ClutterLogic   : TConfig_Clutter_Logic_Dictionary; //TDictionary<integer, TConfig_Clutter>; //(<Config_Clutter>);
    ReliefClutters_: TReliefClutters_Dictionary;

 //   Dir_relief : string;
    Dir_Data : string;


    IMAGE_PROFILE_WIDTH  : word; // = 500;
    IMAGE_PROFILE_HEIGHT : word; // = 450;

    IMAGE_Map_WIDTH  : word; // = 500;
    IMAGE_Map_HEIGHT : word; // = 450;

    map_print_delay  : word;

    //--------------------------------------
//    MegaGIS: record
//      response_image_count : integer;
//    end;

    AutoCalc :record
      is_can_change_antenna_height : boolean;
      is_can_change_antenna_location: boolean;
      Max_good_calcs : byte;
    end;

    Client: record
      Iteration_count : Integer;
      Iteration_antenna_h : double;
      AMC_height : double;
    end;

    Place: record
      AMC_height : double;
    end;


    is_make_map_images : boolean;

    AutoRun_Enabled : Boolean;

    OnDisconnect_Delay_Sec : word;

    GraphicsMagick: record
      Path : string;
      Enabled : boolean;
    end;


//    ��������� ������ �������, [m]

  //  IsDebug : Boolean;


    constructor Create;
    destructor Destroy; override;

    procedure LoadFromIni(aFileName: string = '');

    procedure LoadFromDataset(aDataset: DB.TDataset);

    function GetRequestDir_ByGUID(aGUID: string): string;

//    procedure InitLogDir;

    function TechnologyAllowed(aValue: string): Boolean;

  end;


var
  g_Config: TConfig;


implementation

const
  DEF_MAIN = 'main';


// ---------------------------------------------------------------
constructor TConfig.Create;
// ---------------------------------------------------------------
begin
  inherited;

 // Dir_Data:='e:\__temp\';
//  Dir_temp:=GetApplicationDir + 'temp\';
//  Dir_relief :='';
  //Dir_temp   :='';

  //IsDebug :=True;

  IMAGE_PROFILE_WIDTH  := 600;
  IMAGE_PROFILE_HEIGHT := 500;

  IMAGE_MAP_WIDTH  := 700;
  IMAGE_MAP_HEIGHT := 700;


  //Port:=8090;

//  Map_HTML_template      :='W:\WEB\_html\leaflet.html';
//  Map_print_HTML_template:='W:\WEB\_html\map_print.html';

 // ConnectionStr:='Provider=SQLOLEDB.1;Integrated Security=SSPI;Persist Security Info=False;Initial Catalog=megafon_WireLess;Data Source=ASTRA\MSSQLSERVER_2014';


  ClutterLogic := TConfig_Clutter_Logic_Dictionary.Create;
  ReliefClutters_:=TReliefClutters_Dictionary.Create;

end;

destructor TConfig.Destroy;
begin
  inherited;

  FreeAndNil(ClutterLogic);
  FreeAndNil(ReliefClutters_);
end;



function TConfig.GetRequestDir_ByGUID(aGUID: string): string;
var
  s: string;
begin
  aGUID:=aGUID.Replace('{','').Replace('}','') ;

//  s:=DateToStr(Now);

  s:=FormatDateTime ('yyyy-mm-dd', Now);
  Result := IncludeTrailingBackslash(Dir_Data) + Format('%s\%s\', [ s, aGUID]);

  //  ed_GetCurrentProcessId.Text:= IntToStr ( GetCurrentProcessId );

//  Result := IncludeTrailingBackslash(Dir_Data) + Format('%s\', [  aGUID]);

//  Result := IncludeTrailingBackslash(Dir_Data)
///             + Format('%d\%s\%s\', [GetCurrentProcessId (), s, aGUID]);
end;


{
procedure TConfig.InitLogDir;
begin
  if Log_Dir='' then
    Log_Dir:='d:\log';
//    Log_Dir:= GetCommonApplicationDataDir_Local_AppData('SOAP server') + 'Log';

  Log_Dir:=IncludeTrailingBackslash(Log_Dir);

  ForceDirectories(Log_Dir);
end;
 }


//-------------------------------------------------------------------
procedure TConfig.LoadFromDataset(aDataset: DB.TDataset);
//-------------------------------------------------------------------
begin
  Assert ( not aDataset.IsEmpty, 'procedure TConfig.LoadFromDataset(aDataset: DB.TDataset); - IsEmpty');


  IMAGE_PROFILE_WIDTH :=aDataset['IMAGE_PROFILE_WIDTH'];
  IMAGE_PROFILE_HEIGHT:=aDataset['IMAGE_PROFILE_HEIGHT'];

  IMAGE_Map_WIDTH :=aDataset['IMAGE_Map_WIDTH'];
  IMAGE_Map_HEIGHT:=aDataset['IMAGE_Map_HEIGHT'];


  //--------------------------------------
 // rest_service_http   := aDataset.FieldByName('rest_service_http').AsString;
  Client.iteration_count     := aDataset.FieldByName('client_iteration_count').AsInteger;
  Client.iteration_antenna_h := aDataset.FieldByName('client_iteration_antenna_h').AsFloat;
  Client.AMC_height          := aDataset.FieldByName('client_AMC_height').AsFloat;
 // Assert (Client.AMC_height > 0, 'client_AMC_height');

  AutoCalc.is_can_change_antenna_height  :=aDataset.FieldByName('is_can_change_antenna_height').AsBoolean;
  AutoCalc.is_can_change_antenna_location:=aDataset.FieldByName('is_can_change_antenna_location').AsBoolean;

  AutoCalc.Max_good_calcs := aDataset.FieldByName('Max_good_calcs').AsInteger;
  Assert (AutoCalc.Max_good_calcs > 0, 'AutoCalc.Max_good_calcs > 0');


  //--------------------------------------
  Place.AMC_height:= aDataset.FieldByName('Place_AMC_height').AsFloat;

  is_make_map_images:=aDataset.FieldByName('is_make_map_images').AsBoolean;


//  Send_ESB.attemps :=aDataset.FieldByName('is_make_map_images').AsBoolean;
//
//[send_esb]
//attemps=5
//delay=1000


//  Thread_count := aDataset.FieldByName('Thread_count').AsInteger;


//  Dir_relief:=aDataset['IMAGE_Map_HEIGHT'];
//  Dir_temp  :=aDataset['IMAGE_Map_HEIGHT'];

end;

// ---------------------------------------------------------------
procedure TConfig.LoadFromIni(aFileName: string = '');
// ---------------------------------------------------------------
const
  DEF_MAIN = 'main';
var
  oIni: TIniFile;
begin
  if aFileName='' then
    aFileName:=ExtractFilePath (Application.ExeName)+ 'config.ini';

 Assert (FileExists (aFileName));

//    aFileName:=ChangeFileExt(Application.ExeName, '.ini');


  oIni:=TIniFile.Create(aFileName);

  ConnectionStr :=oIni.ReadString(DEF_MAIN, 'Connection', ConnectionStr);

//  Dir_relief:=oIni.ReadString(DEF_MAIN, 'Dir_relief', Dir_relief);
  Dir_Data  :=oIni.ReadString(DEF_MAIN, 'Dir_Data',   Dir_Data);
  Win32Check (Dir_Data <> '');
  Assert(Dir_Data <> '', 'Dir_Data');


  freq_Ghz:=oIni.ReadInteger(DEF_MAIN, 'freq_Ghz', 6);

//  IsDebug:=oIni.ReadBool(DEF_MAIN, 'IsDebug',    IsDebug);

  Log_Dir:=oIni.ReadString(DEF_MAIN, 'Log_dir', Log_Dir);
  Win32Check (Log_Dir <> '');
  Assert(Log_Dir <> '', 'Log_Dir');

  Ftechnology_not_used:=oIni.ReadString(DEF_MAIN, 'technology_not_used', '');


  //Port  :=oIni.ReadInteger(DEF_MAIN, 'Port', Port);
  Map_HTML_template:=oIni.ReadString(DEF_MAIN,       'Map_HTML_template',       Map_HTML_template);
  Map_print_HTML_template:=oIni.ReadString(DEF_MAIN, 'Map_print_HTML_template', Map_print_HTML_template);

  //  if not FileExists(Map_HTML_template) then
  Map_HTML_template:=ExtractFilePath(Application.ExeName) + Map_HTML_template;

  //  if not FileExists(Map_print_HTML_template) then
  Map_print_HTML_template:=ExtractFilePath(Application.ExeName) + Map_print_HTML_template;


  WSDL_URL := oIni.ReadString(DEF_MAIN, 'WSDL_URL','');


//  Queue:=oIni.ReadString(DEF_MAIN, 'Queue', Queue);

//  MegaGIS_WSDL:=oIni.ReadString(DEF_MAIN, 'MegaGIS_WSDL', '');
//  Assert (MegaGIS_WSDL<>'', 'ini - MegaGIS_WSDL');
  map_print_delay:=oIni.ReadInteger(DEF_MAIN, 'map_print_delay', 1000);

{
  IMAGE_PROFILE_WIDTH  :=oIni.ReadInteger(DEF_MAIN, 'IMAGE_PROFILE_WIDTH', IMAGE_PROFILE_WIDTH);
  IMAGE_PROFILE_HEIGHT :=oIni.ReadInteger(DEF_MAIN, 'IMAGE_PROFILE_HEIGHT', IMAGE_PROFILE_HEIGHT);

  IMAGE_Map_WIDTH      :=oIni.ReadInteger(DEF_MAIN, 'IMAGE_Map_WIDTH', IMAGE_Map_WIDTH);
  IMAGE_Map_HEIGHT     :=oIni.ReadInteger(DEF_MAIN, 'IMAGE_Map_HEIGHT', IMAGE_Map_HEIGHT);
  }
//
//      AutoCalc :record
//      is_can_change_antenna_height : boolean;
//      is_can_change_antenna_location: boolean;
//
//    end;


//  AutoCalc.is_can_change_antenna_height  :=oIni.ReadBool('AutoCalc', 'is_can_change_antenna_height',   False);
//  AutoCalc.is_can_change_antenna_location:=oIni.ReadBool('AutoCalc', 'is_can_change_antenna_location', False);


//  AutoCalc.is_can_change_antenna_height  :=LowerCase(oIni.ReadString(DEF_MAIN, 'is_can_change_antenna_height', ''))  ='true';
//  AutoCalc.is_can_change_antenna_location:=LowerCase(oIni.ReadString(DEF_MAIN, 'is_can_change_antenna_location', ''))='true';


//  ApiService_WSDL:=oIni.ReadString('ApiService', 'WSDL', '');
//  Assert(ApiService_WSDL <>'');
  //MegaGIS.response_image_count :=oIni.ReadInteger('MegaGIS', 'response_image_count', 5);


//    ApiService_WSDL : string; // ='http://localhost/ApiService/Service.svc?singleWsdl'


  Send_ESB.attemps   :=oIni.ReadInteger('Send_ESB', 'attemps',   5);
  Send_ESB.delay_sec :=oIni.ReadInteger('Send_ESB', 'delay_sec', 1);

  AutoRun_Enabled :=LowerCase(oIni.ReadString('AutoRun', 'enabled', 'false'))='true';

  OnDisconnect_Delay_Sec:=oIni.ReadInteger(DEF_MAIN, 'OnDisconnect_Delay_Sec', 60);

  GraphicsMagick.Path:=oIni.ReadString('GraphicsMagick', 'path','');
  GraphicsMagick.Enabled:=LowerCase(oIni.ReadString('GraphicsMagick', 'Enabled','true')) = 'true';
  Assert(g_Config.GraphicsMagick.Path <> '');



  FreeAndNil(oIni);

  //-------------------------------------------------
  //InitLogDir();

  //Prepare();

end;



function TConfig.TechnologyAllowed(aValue: string): Boolean;
begin
  Result:=Pos(aValue, Ftechnology_not_used) = 0;

end;



//-------------------------------------------------------------------
procedure TConfig_Clutter_Logic_Dictionary.LoadFromDataset(aDataset: DB.TDataset);
//-------------------------------------------------------------------
var
  rec: TConfig_Clutter_Logic_rec;
begin
  Assert (not aDataset.IsEmpty, 'procedure TConfig_Clutter_Logic_Dictionary.LoadFromDataset(aDataset: DB.TDataset);');


  Clear;

  with aDataset do
    while not EOF do begin
      rec.Clutter_code     := aDataset['Clutter_code'];
      rec.height           := aDataset.FieldByName('height').AsFloat;
      rec.is_terminate_calc:= aDataset.FieldByName('is_terminate_calc').AsBoolean;

      Add(rec.Clutter_code, rec);

      Next;
    end;


//settings_client_antenna_height

end;

//-------------------------------------------------------------------
procedure TReliefClutters_Dictionary.LoadFromDataset(aDataset: DB.TDataset);
//-------------------------------------------------------------------
var
  eHeight: Double;
  iCode: byte;
begin
  Clear;

  with aDataset do
    while not EOF do begin
      iCode  :=aDataset['Clutter_code'];
      eHeight:=aDataset.FieldByName('height').AsFloat;

      Add(iCode, eHeight);

      Next;
    end;

end;


initialization
  g_Config:=TConfig.Create;
  g_Config.LoadFromIni();

finalization
  FreeAndNil(g_Config);


end.

