unit u_const_msg;

interface
uses
  Messages, WinApi.Windows, StrUtils, IOutils;

const

  DEF_MONITOR_FORM_NAME = 'Tfrm_BSHPD_Server_monitor';


type
  TServiceMessageType = (mtLog,mtProgress,mtTaskCount);


procedure Monitor_Send_Log(aMsg: string);
procedure Monitor_Send_Task_Count(aCount: word);



implementation

//-------------------------------------------------------------------
procedure Monitor_Send_Log(aMsg: string);
//-------------------------------------------------------------------
var
  receiverHandle: THandle;
  DataStruct : CopyDataStruct;
//  s: string;
begin
  receiverHandle:=FindWindow(DEF_MONITOR_FORM_NAME, nil); // (PChar('Tfrm_BSHPD_Server_monitor') ) ;
  if receiverHandle=0 then
    Exit;
//  s:='Edit111111111111111111.text';

  DataStruct.dwData := Integer(mtLog);
//  DataStruct.cbData := length(Edit1.text)+1;
  DataStruct.cbData := 2*(Length(aMsg)+1);
  DataStruct.lpData := pchar(aMsg);

  SendMessage(receiverHandle, WM_CopyData, 0, integer(@DataStruct));

end;


procedure Test(aSenderHandle: THandle);
var
  receiverHandle: THandle;
  DataStruct : CopyDataStruct;
  s: string;
begin
  receiverHandle:=FindWindow('Tfrm_BSHPD_Server_monitor', nil); // (PChar('Tfrm_BSHPD_Server_monitor') ) ;

  s:='Edit111111111111111111.text';

  DataStruct.dwData := 0;
//  DataStruct.cbData := length(Edit1.text)+1;
  DataStruct.cbData := 2*(Length(S)+1);
  DataStruct.lpData := pchar(s);

  SendMessage(receiverHandle, WM_CopyData, aSenderHandle, integer(@DataStruct));

end;

//-------------------------------------------------------------------
procedure Monitor_Send_Task_Count(aCount: word);
//-------------------------------------------------------------------
var
  receiverHandle: THandle;
  DataStruct : CopyDataStruct;
//  s: string;
begin
  receiverHandle:=FindWindow(DEF_MONITOR_FORM_NAME, nil); // (PChar('Tfrm_BSHPD_Server_monitor') ) ;
  if receiverHandle=0 then
    Exit;
//  s:='Edit111111111111111111.text';

  DataStruct.dwData := Integer(mtTaskCount);
//  DataStruct.cbData := length(Edit1.text)+1;
  DataStruct.cbData := aCount;
//  DataStruct.lpData := pchar(aMsg);

  SendMessage(receiverHandle, WM_CopyData, 0, integer(@DataStruct));

end;


end.
