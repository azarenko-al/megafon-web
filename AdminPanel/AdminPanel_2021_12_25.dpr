program AdminPanel_2021_12_25;

uses
  Vcl.Forms,
  api_local_ApiService_09_16 in '..\.Shared\WSDL\api_local_ApiService_09_16.pas',
  api_local_mg_10_25 in '..\.Shared\WSDL\api_local_mg_10_25.pas',
  d_Map_print in '..\server\src\Map\d_Map_print.pas' {dlg_Map_print},
  d_Task in 'src\Task\d_Task.pas' {dlg_Task},
  dlg_Task_MDB_import in 'Tests\MDB import\dlg_Task_MDB_import.pas' {dlg_MDB_import},
  dm_DB_Manager_custom in 'W:\common XE\DB\dm_DB_Manager_custom.pas' {dmDB_Manager_custom: TDataModule},
  dm_MDB in 'Tests\MDB import\dm_MDB.pas' {dmMDB: TDataModule},
  dm_Queue in '..\server\src\task\dm_Queue.pas' {dmQueue: TDataModule},
  dm_Relief in '..\server\src\dm_Relief.pas' {dmRelief: TDataModule},
  dm_Task in '..\server\src\task\dm_Task.pas' {dmTask: TDataModule},
  dm_Task_Calc in '..\server\src\task\dm_Task_Calc.pas' {dmTask_calc: TDataModule},
  dm_Task_MDB_import in 'Tests\MDB import\dm_Task_MDB_import.pas' {dmTask_MDB: TDataModule},
  fr_Map in 'src\Map\fr_Map.pas' {frame_Map},
  fr_Map_View in 'src\Map\fr_Map_View.pas' {frame_Map_view},
  fr_MapFile in 'src\Lib\fr_MapFile.pas' {frame_MapFile},
  fr_Task in 'src\Task\fr_Task.pas' {frame_Task},
  u_Config in '..\.Shared\u_Config.pas',
  u_const_msg in '..\.Shared\u_const_msg.pas',
  u_cx_ini in 'W:\common XE\CX\u_cx_ini.pas',
  u_Logger in 'W:\common XE\u_Logger.pas',
  u_Task_classes in '..\server\src\task\u_Task_classes.pas',
  u_Task_send_to_Local_MG in '..\.Shared\WSDL\u_Task_send_to_Local_MG.pas',
  u_Task_send_to_Remote_MG_prom in '..\.Shared\WSDL\u_Task_send_to_Remote_MG_prom.pas',
  dm_Main in '..\.Shared\dm_Main.pas' {dmMain: TDataModule},
  fr_Profile_Rel_Graph in '..\Server\src\fr_Profile_Rel_Graph.pas' {frame_Profile_Rel_Graph},
  dm_Map in '..\Server\src\Map\dm_Map.pas' {dmMap: TDataModule},
  dm_App in 'src\dm_App.pas' {dmApp: TDataModule},
  f_Custom in 'src\f_Custom.pas' {frm_Custom},
  f_Main in 'src\f_Main.pas' {frm_Main},
  fr_NRI in 'src\fr_NRI.pas' {frame_NRI},
  fr_Queue in 'src\fr_Queue.pas' {frame_Queue},
  fr_Settings in 'src\fr_Settings.pas' {frame_Settings},
  fr_View_base in 'src\fr_View_base.pas' {frame_View_base},
  u_const_calc in 'src\u_const_calc.pas',
  u_const_db in 'src\u_const_db.pas',
  fr_AntennaType in 'src\Lib\fr_AntennaType.pas' {frame_AntennaTYpe},
  fr_Clutters in 'src\Lib\fr_Clutters.pas' {frame_Clutters},
  fr_Equipment in 'src\Lib\fr_Equipment.pas' {frame_Equipment},
  fr_Equipment_Pivot in 'src\Lib\fr_Equipment_Pivot.pas' {frame_Equipment_Pivot},
  fr_LinkendType in 'src\Lib\fr_LinkendType.pas' {frame_LinkendTYpe},
  fr_Relief in 'src\Lib\fr_Relief.pas' {frame_Relief},
  fr_TerminalType_WiMax in 'src\Lib\fr_TerminalType_WiMax.pas' {frame_TerminalTYpe_WiMax},
  d_Equipment_Pivot in 'src\Lib\d_Equipment_Pivot.pas' {dlg_Equipment_Pivot},
  u_log_ in 'W:\common\u_log_.pas',
  u_db_ in 'W:\common\u_db_.pas';

{$R *.res}

begin
  Application.Initialize;
  Application.MainFormOnTaskbar := True;
  Application.CreateForm(TdmApp, dmApp);
  Application.CreateForm(TdmMain, dmMain);
  Application.CreateForm(TdmRelief, dmRelief);
  Application.CreateForm(TdmQueue, dmQueue);
  Application.CreateForm(TdmTask, dmTask);
  Application.CreateForm(TdmTask_calc, dmTask_calc);
  Application.CreateForm(Tfrm_Main, frm_Main);
  Application.Run;
end.
