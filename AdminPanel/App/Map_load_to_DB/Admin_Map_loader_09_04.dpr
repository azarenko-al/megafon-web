program Admin_Map_loader_09_04;

uses
  Vcl.Forms,
  d_Map_Loader in 'd_Map_Loader.pas' {dlg_Map_Loader},
  u_Config in '..\.Shared\u_Config.pas';

{$R *.res}

begin
  Application.Initialize;
  Application.MainFormOnTaskbar := True;
  Application.CreateForm(Tdlg_Map_Loader, dlg_Map_Loader);
  Application.Run;
end.
