object dlg_Map_Loader: Tdlg_Map_Loader
  Left = 0
  Top = 0
  Caption = 'Map loader to DB'
  ClientHeight = 486
  ClientWidth = 634
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poMainFormCenter
  OnCreate = FormCreate
  OnShow = FormShow
  DesignSize = (
    634
    486)
  PixelsPerInch = 96
  TextHeight = 13
  object Label2: TLabel
    Left = 12
    Top = 13
    Width = 97
    Height = 13
    Caption = #1055#1086#1076#1082#1083#1102#1095#1077#1085#1080#1077' '#1082' '#1041#1044
  end
  object Memo_Files: TMemo
    Left = 0
    Top = 59
    Width = 634
    Height = 270
    Align = alBottom
    Anchors = [akLeft, akTop, akRight, akBottom]
    ScrollBars = ssVertical
    TabOrder = 0
  end
  object ed_Connection: TComboEdit
    Left = 12
    Top = 32
    Width = 449
    Height = 21
    Anchors = [akLeft, akTop, akRight]
    NumGlyphs = 1
    TabOrder = 1
    Text = 
      'Provider=SQLOLEDB.1;Integrated Security=SSPI;Persist Security In' +
      'fo=False;Initial Catalog=WireLess;Data Source=ASTRA'
    OnButtonClick = ed_ConnectionButtonClick
  end
  object Panel1: TPanel
    Left = 0
    Top = 449
    Width = 634
    Height = 37
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 2
    DesignSize = (
      634
      37)
    object Button1: TButton
      Left = 466
      Top = 6
      Width = 75
      Height = 25
      Anchors = [akTop, akRight]
      Caption = #1047#1072#1075#1088#1091#1079#1080#1090#1100
      Default = True
      ModalResult = 1
      TabOrder = 0
      OnClick = Button1Click
    end
    object Button2: TButton
      Left = 553
      Top = 6
      Width = 75
      Height = 25
      Anchors = [akTop, akRight]
      Cancel = True
      Caption = #1047#1072#1082#1088#1099#1090#1100
      ModalResult = 8
      TabOrder = 1
    end
    object Button3: TButton
      Left = 8
      Top = 6
      Width = 75
      Height = 25
      Action = FileOpen1
      TabOrder = 2
    end
  end
  object Memo_Log: TMemo
    Left = 0
    Top = 329
    Width = 634
    Height = 120
    Align = alBottom
    ScrollBars = ssVertical
    TabOrder = 3
  end
  object FormStorage1: TFormStorage
    Options = [fpPosition]
    UseRegistry = True
    StoredProps.Strings = (
      'ed_Connection.Text'
      'Memo_Files.Lines')
    StoredValues = <>
    Left = 488
  end
  object ADOConnection1: TADOConnection
    CommandTimeout = 30000
    ConnectionString = 
      'Provider=SQLOLEDB.1;Integrated Security=SSPI;Persist Security In' +
      'fo=False;Initial Catalog=megafon_WireLess;Data Source=ASTRA\MSSQ' +
      'LSERVER_2014'
    LoginPrompt = False
    Provider = 'SQLOLEDB.1'
    Left = 584
  end
  object ActionList1: TActionList
    Left = 536
    object FileOpen1: TFileOpen
      Category = 'File'
      Caption = #1054#1090#1082#1088#1099#1090#1100'...'
      Dialog.Filter = 'Mapinfo|*.tab'
      Dialog.Options = [ofHideReadOnly, ofAllowMultiSelect, ofEnableSizing]
      Hint = 'Open|Opens an existing file'
      ImageIndex = 7
      ShortCut = 16463
      OnAccept = FileOpen1Accept
    end
    object act_LoadFromDir: TAction
      Category = 'File'
      Caption = #1047#1072#1075#1088#1091#1079#1080#1090#1100
      OnExecute = act_LoadFromDirExecute
    end
  end
end
