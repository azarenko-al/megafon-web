unit d_Map_Loader;

interface

uses
  u_GDAL_bat,
  u_Run,

  u_db,




  System.SysUtils, System.Classes,
  Vcl.Controls, Vcl.Forms, Vcl.StdCtrls, RxPlacemnt, Vcl.Mask,
  RxToolEdit, Data.Win.ADODB, Vcl.ExtCtrls,
  Vcl.ActnList, Vcl.StdActns, Vcl.Dialogs, System.Actions, Data.DB;

type
  Tdlg_Map_Loader = class(TForm)
    FormStorage1: TFormStorage;
    Memo_Files: TMemo;
    ADOConnection1: TADOConnection;
    ed_Connection: TComboEdit;
    Label2: TLabel;
    Button1: TButton;
    Button2: TButton;
    Panel1: TPanel;
    Button3: TButton;
    ActionList1: TActionList;
    FileOpen1: TFileOpen;
    act_LoadFromDir: TAction;
    Memo_Log: TMemo;
    procedure act_LoadFromDirExecute(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure Button1Click(Sender: TObject);
//    procedure Button1Click(Sender: TObject);
    procedure Button_LoadClick(Sender: TObject);
    procedure ed_ConnectionButtonClick(Sender: TObject);
    procedure DirectoryEdit1AfterDialog(Sender: TObject; var AName: string; var
        Action: Boolean);
    procedure FileOpen1Accept(Sender: TObject);
    procedure FormShow(Sender: TObject);
//    procedure FormShow(Sender: TObject);
  private
    procedure Load;
    procedure LoadFile(aFileName: string);
    procedure Run;
    { Private declarations }
  public
    function GetGDAL_connection: string;

    class procedure ExecDlg(aConnectionString: string);

  end;

var
  dlg_Map_Loader: Tdlg_Map_Loader;

implementation

{$R *.dfm}

class procedure Tdlg_Map_Loader.ExecDlg(aConnectionString: string);
begin
  with Tdlg_Map_Loader.Create(Application) do
  begin
    ed_Connection.Text:= aConnectionString;

    ShowModal;
    Free;
  end;
end;


procedure Tdlg_Map_Loader.FormCreate(Sender: TObject);
begin
  Caption:='�������� ���� � DB';

//  FileOpen1.Dialog.InitialDir:=DirectoryEdit1.Text;

 // Memo1.Top:=230;

//  ScanDir (DirectoryEdit1.Text, '*.tab', Memo1.Lines);
end;


procedure Tdlg_Map_Loader.act_LoadFromDirExecute(Sender: TObject);
begin
//  ScanDir (DirectoryEdit1.Text, '*.tab', Memo1.Lines);
end;


procedure Tdlg_Map_Loader.Button1Click(Sender: TObject);
begin
  Run;
end;

//-------------------------------------------------------------------
procedure Tdlg_Map_Loader.Run;
//-------------------------------------------------------------------
begin

  if not db_OpenADOConnectionString(ADOConnection1, ed_Connection.Text) then
    exit;


 ////////// ADOConnection1.Execute('exec map.sp_Geometry_columns_DROP');

//    exit;


  Load();

///////////  ADOConnection1.Execute('exec map.sp_JOIN_maps');

end;

//-------------------------------------------------------------------
function Tdlg_Map_Loader.GetGDAL_connection: string;
//-------------------------------------------------------------------
var
  oSList: TStringList;
  s: string;

  str_arr : TArray<string>;

begin

  str_arr:=Trim(ed_Connection.Text).Split([';']);


  oSList:=TStringList.Create;

  for s in str_arr do
    oSList.Add (s);


//Provider=SQLOLEDB.1;Password=sa;Persist Security Info=True;User ID=sa;
  //Provider=SQLOLEDB.1;Integrated Security=SSPI;Persist Security Info=False;Initial Catalog=megafon_WireLess;Data Source=ASTRA\MSSQLSERVER_2014

//  s:=oSList.Values['Data Source'];
 //MSSQL:server=ASTRA\MSSQLSERVER_2014;database=megafon_WireLess;trusted_connection=yes

  Result:='MSSQL:server=' + oSList.Values['Data Source'] +';'+
          'database='     + oSList.Values['Initial Catalog'] + ';';
//          'trusted_connection=yes'  ;

  if oSList.Values['Persist Security Info'] = 'False' then
    Result:=Result + 'trusted_connection=yes'
  else begin
    Result:=Result + 'UID=' + oSList.Values['User ID'] + ';' +
                     'PWD=' + oSList.Values['Password'] ;

  end;
      //;Password=sa;Persist Security Info=True;User ID=sa

//    UID=SA;PWD=DummyPassw0rd"

  FreeAndNil(oSList);

 // Memo1.Text:=Result;

  // TODO -cMM: Tfrm_Main.GetGDAL_connection default body inserted
end;

//-------------------------------------------------------------------
procedure Tdlg_Map_Loader.Button_LoadClick(Sender: TObject);
//-------------------------------------------------------------------
begin
//  ScanDir (DirectoryEdit1.Text, '*.tab', Memo1.Lines);

end;


//-------------------------------------------------------------------
procedure Tdlg_Map_Loader.Load;
//-------------------------------------------------------------------
var
  I: Integer;
begin

  for I := 0 to Memo_Files.Lines.Count-1 do
    LoadFile (Memo_Files.Lines[i])

end;


//-------------------------------------------------------------------
procedure Tdlg_Map_Loader.LoadFile(aFileName: string);
//-------------------------------------------------------------------
var
 // arr: TStringDynArray;
  oGDAL_bat: TGDAL_Bat;
  s: string;
  sFile_bat: string;
 // sPath: string;
  I: Integer;

begin
  Memo_Log.Lines.Add(aFileName);

 // Memo1.Lines.Clear;

//  arr:=ScanDir (DirectoryEdit1.Text, '*.tab', Memo1.Lines);

//  Memo1.Lines.

  oGDAL_bat:=TGDAL_Bat.Create;
  oGDAL_bat.Init_Header;

//  ed_Connection.Text:=Trim(ed_Connection.Text);


  s:=Format('set params=-lco SPATIAL_INDEX=NO -skipfailures -progress  -lco SCHEMA=map  -lco OVERWRITE=YES -t_srs "EPSG:4326"  -overwrite -f MSSQLSpatial "%s" ',
         [GetGDAL_connection() ]);

  oGDAL_bat.Add(s);
  oGDAL_bat.Add('');


    s:='ogr2ogr %params% '+ Format( '"%s" ', [ExtractFileName(aFileName)]);
    oGDAL_bat.Add(s);



  sFile_bat:= ChangeFileExt(aFileName,'.bat');

 // oGDAL_bat.SaveToFile(sFile);


  oGDAL_bat.SaveToFile (sFile_bat, TEncoding.GetEncoding(866));

//  oBAT.SaveToFile(ChangeFileExt(Params.RelFileName, '.bat'), TEncoding.GetEncoding(866));

//  FreeAndNil(oBAT);

  SetCurrentDir(ExtractFilePath(sFile_bat));

  RunApp(sFile_bat,'');

end;



procedure Tdlg_Map_Loader.ed_ConnectionButtonClick(Sender: TObject);
begin
  ed_Connection.Text:=PromptDataSource(0, ed_Connection.Text);

//  ed_Connection_GDAL.Text:= GetGDAL_connection();

//  ADOConnection1.ConnectionString:= PromptDataSource(0, ADOConnection1.ConnectionString);
end;

procedure Tdlg_Map_Loader.DirectoryEdit1AfterDialog(Sender: TObject; var AName:
    string; var Action: Boolean);
begin
//  ScanDir (AName, '*.tab', Memo1.Lines);

end;

procedure Tdlg_Map_Loader.FileOpen1Accept(Sender: TObject);
var
  i: Integer;
begin
//  if FileOpen1.Open then
  Memo_Files.Lines.Clear;

  for i:=0 to FileOpen1.Dialog.Files.Count-1 do
    Memo_Files.Lines.add (FileOpen1.Dialog.Files[i])

end;

procedure Tdlg_Map_Loader.FormShow(Sender: TObject);
begin
//  ScanDir (DirectoryEdit1.Text, '*.tab', Memo1.Lines);

end;



end.


{

ogr2ogr -overwrite -f MSSQLSpatial "MSSQL:server=127.0.0.1;database=TestDB;UID=SA;PWD=DummyPassw0rd" "rivers.gpkg"

ogr2ogr -t_srs "EPSG:4326"  -f "MapInfo File" "houses_Akadem_4326.TAB" "houses_Akadem.TAB"



 ogr2ogr  -lco SCHEMA=map  -t_srs "EPSG:4326"  -overwrite -f MSSQLSpatial "MSSQL:server=ASTRA;database=megafon;trusted_connection=yes" "houses_Akadem.TAB"


ogr2ogr -t_srs "EPSG:4326"  -f "MapInfo File" "10_3-building_4326.TAB"  "10_3-building.TAB"

 ogr2ogr  -lco SCHEMA=map  -t_srs "EPSG:4326"  -overwrite -f MSSQLSpatial "MSSQL:server=ASTRA;database=megafon;trusted_connection=yes" "10_3-building.TAB"



//-----------------------------------------------------------------------
function ScanDir(aPath, aExtension: string; aFiles: TStrings): TStringDynArray;
//-----------------------------------------------------------------------
//  ScanDir1(aDir, '*.mdb', oFiles);
var
  sPath : string;
begin

  if not TDirectory.Exists(IncludeTrailingBackslash(aPath)) then
    Exit;

  Result:=TDirectory.GetFiles (IncludeTrailingBackslash(aPath) , aExtension) ;

  for sPath in Result  do
    aFiles.Add(sPath);{assuming OpenPictureDialog1 is the name you gave to your OpenPictureDialog control}

end;


ogr2ogr
 -f "MSSQLSpatial"
 "MSSQL:server=localhost\SQL2012Express;database=ProSpatial;trusted_connection=yes;"
 "tl_2010_06_zcta510.shp"
 -a_srs "ESPG:4269"
 -lco "GEOM_TYPE=geography"
 -lco "GEOM_NAME=geog4269"
 -nln "CaliforniaZCTA"
 -progress



   dt_start: TDateTime;
  iDB_ID: Integer;
begin
  dt_start:=Now;
  CodeSite.Send( 'function TdmTask.AddToDB(aTask: TTask): Integer;...' );

