program Admin_Relief_loader_10_27;

uses
  Vcl.Forms,
  d_Relief_Add in '..\src\Relief\d_Relief_Add.pas' {dlg_Relief_Add},
  u_RLF_to_ENVI_export in 'W:\common XE\Relief\u_RLF_to_ENVI_export.pas',
  u_Rel_to_clutter_map in 'W:\GIS\rel_to_bmp_3D\Project\Src\u_Rel_to_clutter_map.pas',
  u_const_db in '..\src\u_const_db.pas',
  u_Config in '..\..\.Shared\u_Config.pas',
  u_Logger in 'W:\common XE\u_Logger.pas',
  dm_Main in '..\..\.Shared\dm_Main.pas' {dmMain: TDataModule};

{$R *.res}

begin
  Application.Initialize;
  Application.MainFormOnTaskbar := True;
  Application.CreateForm(Tdlg_Relief_Add, dlg_Relief_Add);
  Application.Run;
end.
