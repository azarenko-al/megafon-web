program Admin_MDB_import_10_19;

uses
  Vcl.Forms,
  dlg_Task_MDB_import in 'dlg_Task_MDB_import.pas' {dlg_MDB_import},
  dm_MDB in 'dm_MDB.pas' {dmMDB: TDataModule},
  dm_Task_MDB_import in 'dm_Task_MDB_import.pas' {dmTask_MDB: TDataModule},
  u_const_db in '..\..\src\u_const_db.pas',
  u_Config in '..\..\..\.Shared\u_Config.pas',
  u_Logger in 'W:\common XE\u_Logger.pas';

{$R *.res}

begin
  Application.Initialize;
  Application.MainFormOnTaskbar := True;
  Application.CreateForm(Tdlg_MDB_import, dlg_MDB_import);
  Application.Run;
end.
