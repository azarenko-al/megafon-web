object dlg_MDB_import: Tdlg_MDB_import
  Left = 0
  Top = 0
  BorderStyle = bsDialog
  Caption = 'dlg_MDB_import'
  ClientHeight = 404
  ClientWidth = 585
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poMainFormCenter
  OnCreate = FormCreate
  DesignSize = (
    585
    404)
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 8
    Top = 8
    Width = 108
    Height = 13
    Caption = #1056#1072#1089#1087#1086#1083#1086#1078#1077#1085#1080#1077' '#1092#1072#1081#1083#1072
  end
  object ProgressBar1: TProgressBar
    Left = 8
    Top = 344
    Width = 569
    Height = 17
    Anchors = [akLeft, akRight]
    TabOrder = 0
  end
  object Panel1: TPanel
    Left = 0
    Top = 367
    Width = 585
    Height = 37
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 1
    object Bevel1: TBevel
      Left = 0
      Top = 0
      Width = 585
      Height = 3
      Align = alTop
      Shape = bsTopLine
      ExplicitWidth = 644
    end
    object Button_Ok: TButton
      Left = 418
      Top = 6
      Width = 75
      Height = 25
      Caption = 'OK'
      Default = True
      ModalResult = 1
      TabOrder = 0
      OnClick = Button_OkClick
    end
    object Button3: TButton
      Left = 499
      Top = 6
      Width = 75
      Height = 25
      Cancel = True
      Caption = #1047#1072#1082#1088#1099#1090#1100
      ModalResult = 2
      TabOrder = 1
    end
    object Button2: TButton
      Left = 8
      Top = 6
      Width = 75
      Height = 25
      Action = FileOpen1
      TabOrder = 2
    end
  end
  object ListBox1: TListBox
    Left = 8
    Top = 39
    Width = 569
    Height = 266
    Anchors = [akLeft, akTop, akRight, akBottom]
    ItemHeight = 13
    TabOrder = 2
  end
  object ProgressBar_files: TProgressBar
    Left = 8
    Top = 321
    Width = 569
    Height = 17
    Anchors = [akLeft, akRight]
    TabOrder = 3
  end
  object FormStorage1: TFormStorage
    StoredValues = <>
    Left = 376
  end
  object OpenDialog1: TOpenDialog
    Options = [ofHideReadOnly, ofFileMustExist, ofEnableSizing]
    Left = 232
  end
  object ActionList1: TActionList
    Left = 288
    object FileOpen1: TFileOpen
      Category = 'File'
      Caption = '&Open...'
      Dialog.DefaultExt = '*.mdb'
      Dialog.Filter = '*.mdb|*.mdb'
      Dialog.Options = [ofHideReadOnly, ofAllowMultiSelect, ofFileMustExist, ofEnableSizing]
      Hint = 'Open|Opens an existing file'
      ImageIndex = 7
      ShortCut = 16463
      OnAccept = FileOpen1Accept
    end
  end
end
