unit dlg_Task_MDB_import;

interface

uses

  dm_Task_MDB_import,

  System.SysUtils, System.Classes,
  Vcl.Controls, Vcl.Forms, RxPlacemnt, Vcl.StdCtrls,
  Vcl.ComCtrls,
  Vcl.ExtCtrls, Vcl.ActnList, Vcl.StdActns,
  Vcl.Dialogs, System.Actions;

type
  Tdlg_MDB_import = class(TForm)
    FormStorage1: TFormStorage;
    ProgressBar1: TProgressBar;
    Button_Ok: TButton;
    Button3: TButton;
    Panel1: TPanel;
    Label1: TLabel;
    Bevel1: TBevel;
    ListBox1: TListBox;
    OpenDialog1: TOpenDialog;
    ActionList1: TActionList;
    FileOpen1: TFileOpen;
    Button2: TButton;
    ProgressBar_files: TProgressBar;
    procedure Button_OkClick(Sender: TObject);
    procedure FileOpen1Accept(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    procedure Run;

    procedure DoProgress(aProgress, aMax: Integer);
    { Private declarations }
  public
    { Public declarations }
  end;

var
  dlg_MDB_import: Tdlg_MDB_import;

implementation

{$R *.dfm}

procedure Tdlg_MDB_import.Button_OkClick(Sender: TObject);
begin
  Run;
end;

procedure Tdlg_MDB_import.DoProgress(aProgress, aMax: Integer);
begin
  ProgressBar1.Max:=aMax;
  ProgressBar1.Position:=aProgress;
end;


procedure Tdlg_MDB_import.FileOpen1Accept(Sender: TObject);
var
  I: Integer;
begin
  ListBox1.Clear;

  for I := 0 to TFileOpen (Sender).Dialog.Files.Count-1 do
    ListBox1.AddItem(TFileOpen (Sender).Dialog.Files[i], nil);
//         'BODY', TFile.ReadAllText(TFileOpen (Sender).Dialog.Files[i], TEncoding.UTF8)


end;

procedure Tdlg_MDB_import.FormCreate(Sender: TObject);
begin
  Caption:='������ �� MDB';

 // ADOTable.Open;
end;

//-------------------------------------------------------------------
procedure Tdlg_MDB_import.Run;
//-------------------------------------------------------------------
var
  I: Integer;
begin
  for I := 0 to ListBox1.Count-1 do
  begin
    ProgressBar_files.Max:=ListBox1.Count;
    ProgressBar_files.Position:=i+1;

    TdmTask_MDB.Init;
    dmTask_MDB.Params.MDB_FileName:= ListBox1.Items[i];
    dmTask_MDB.OnProgress:=DoProgress;

    dmTask_MDB.Exec;

    FreeAndNil(dmTask_MDB);



  end;

//   ProgressBar1.Max:=aMax;
   ProgressBar1.Position:=0;
   ProgressBar_files.Position:=0;

//  ListBox1.Clear;




end;



end.


{



//-------------------------------------------------------------------
procedure Tfrm_Test_client.FileOpen_QueueAccept(Sender: TObject);
//-------------------------------------------------------------------
var
  I: Integer;
  k: Integer;
begin
  for I := 0 to TFileOpen (Sender).Dialog.Files.Count-1 do
  begin
    k:=dmMain.ExecStoredProc (ADOStoredProc1,  'WebService.sp_Queue_INS',
     [
       'BODY', TFile.ReadAllText(TFileOpen (Sender).Dialog.Files[i], TEncoding.UTF8)
     ]);

  end;

//    LoadFile (TFileOpen (Sender).Dialog.Files[i]);

  t_Queue.Requery([]);

  PageControl_Tasks.ActivePageIndex:=1;

end;

