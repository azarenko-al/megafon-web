unit dm_MDB;

interface

{.  $DEFINE test}


uses
  ADODB, Db,  Forms, Classes, SysUtils,








  u_db_mdb;

type
  TdmMDB = class(TDataModule)
    ADOConnection_MDB: TADOConnection;
    ADOTable1: TADOTable;
    procedure DataModuleDestroy(Sender: TObject);
    procedure DataModuleCreate(Sender: TObject);
  private
    FtempFileName : string;
//    procedure RefreshTableNames_111111111;

// TODO: BeginTransaction
//  procedure BeginTransaction;
  public
    TableNameList: TStringList;

    function CreateMDB(aFileName: string): Boolean;

    function OpenMDB(aMDBFileName: string): Boolean;



    function CloseMDB: Integer;
    function TableExists(aTableName: string): Boolean;

    class procedure Init;
    procedure OpenTable(aAdoTable: TADOTable; aTableName: string);

  end;

var
  dmMDB: TdmMDB;


implementation
  {$R *.DFM}

//const
//  TBL_PROJECT   = 'project';
//

function TdmMDB.CloseMDB: Integer;
begin
  ADOConnection_MDB.Close;

  {$IFNDEF test}
////  DeleteFile(FtempFileName);
  {$ENDIF}


end;




//------------------------------------------------------------------------------
class procedure TdmMDB.Init;
//------------------------------------------------------------------------------
begin
  if not Assigned(dmMDB) then
    Application.CreateForm(TdmMDB, dmMDB);
end;


procedure TdmMDB.DataModuleCreate(Sender: TObject);
begin
  Assert(not ADOConnection_MDB.Connected);

  TableNameList := TStringList.Create();
end;


procedure TdmMDB.DataModuleDestroy(Sender: TObject);
begin
  FreeAndNil(TableNameList);
end;


//------------------------------------------------------------------------------
function TdmMDB.CreateMDB(aFileName: string): Boolean;
//------------------------------------------------------------------------------
begin
  Assert(aFileName<>'', 'Value <=0');

  aFileName:=ChangeFileExt(aFileName, '.mdb');

  DeleteFile (aFileName);
  Result := mdb_CreateFile(aFileName);

  if Result then
    Result := mdb_OpenConnection(ADOConnection_MDB, aFileName);
end;


//------------------------------------------------------------------------------
function TdmMDB.OpenMDB(aMDBFileName: string): Boolean;
//------------------------------------------------------------------------------
var
  I: Integer;
 // oTableFieldNameList: TStringList;
  s: string;
begin
  Assert(FileExists(aMDBFileName));

  TableNameList.Clear;

//  FtempFileName := ChangeFileExt(aMDBFileName, '.temp.mdb');

//  TFile.Copy (aMDBFileName, FtempFileName);

//  Result := mdb_OpenConnection(ADOConnection_MDB, FtempFileName);
  Result := mdb_OpenConnection(ADOConnection_MDB, aMDBFileName);

  if not Result then
    Exit;


 // oTableFieldNameList:=TStringList.Create;


  ADOConnection_MDB.GetTableNames(TableNameList);



//    TableNameList


 // if TableExists(TBL_PROJECT) then
  //  db_TableOpen1(t_Project1, TBL_PROJECT);

end;

procedure TdmMDB.OpenTable(aAdoTable: TADOTable; aTableName: string);
begin
  aAdoTable.Close;
  aAdoTable.Connection:=ADOConnection_MDB;
  aAdoTable.TableName:= aTableName;
  aAdoTable.Open;

  // TODO -cMM: TdmMDB.OpenTable default body inserted
end;



function TdmMDB.TableExists(aTableName: string): Boolean;
begin
  Result := TableNameList.IndexOf(aTableName)>=0;
end;

end.

{

//------------------------------------------------------------------------------
procedure TdmMDB.RefreshTableNames_111111111;
//------------------------------------------------------------------------------
begin
  ADOConnection_MDB.GetTableNames(TableNameList);
end;

