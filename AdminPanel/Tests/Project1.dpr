program Project1;

uses
  Vcl.Forms,
  fr_WebBrowser in '..\src\fr_WebBrowser.pas' {frame_WebBrowser};

{$R *.res}

begin
  Application.Initialize;
  Application.MainFormOnTaskbar := True;
  Application.CreateForm(Tframe_WebBrowser, frame_WebBrowser);
  Application.Run;
end.
