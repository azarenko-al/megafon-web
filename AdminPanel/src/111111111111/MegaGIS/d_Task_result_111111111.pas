unit d_Task_result_111111111;

interface

uses
  u_db,
  u_Task_classes,

  u_Task_send_to_Local_ESB,




  System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.ExtCtrls, Vcl.StdCtrls,

  RxPlacemnt, Data.DB, dxmdaset,
  cxDBVGrid, cxGraphics, cxControls, cxLookAndFeels, cxLookAndFeelPainters,
  cxStyles, cxEdit, cxVGrid, cxInplaceContainer, dxGDIPlusClasses, cxCustomData,
  cxFilter, cxData, cxDataStorage, cxNavigator, dxDateRanges, cxDBData,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, cxGridLevel,
  cxClasses, cxGridCustomView, cxGrid;

type
  Tdlg_Task_result = class(TForm)
    Button2: TButton;
    Panel1: TPanel;
    cxDBVerticalGrid1: TcxDBVerticalGrid;
    dxMemData1: TdxMemData;
    dxMemData122222: TGuidField;
    dxMemData1Height: TFloatField;
    DataSource1: TDataSource;
    FormStorage1: TFormStorage;
    dxMemData1Result: TStringField;
    dxMemData1comment: TStringField;
    cxDBVerticalGrid1Result: TcxDBEditorRow;
    cxDBVerticalGrid1CalculationCode: TcxDBEditorRow;
    cxDBVerticalGrid1Latitude: TcxDBEditorRow;
    cxDBVerticalGrid1Longitude: TcxDBEditorRow;
    cxDBVerticalGrid1Height: TcxDBEditorRow;
    cxDBVerticalGrid1comment: TcxDBEditorRow;
    Button1: TButton;
    Edit1: TEdit;
    Panel2: TPanel;
    Image_profile: TImage;
    Image_Map: TImage;
    dxMemData_Places: TdxMemData;
    StringField1: TStringField;
    FloatField1: TFloatField;
    FloatField2: TFloatField;
    IntegerField1: TIntegerField;
    StringField2: TStringField;
    FloatField3: TFloatField;
    BooleanField1: TBooleanField;
    StringField3: TStringField;
    StringField4: TStringField;
    FloatField4: TFloatField;
    FloatField5: TFloatField;
    FloatField6: TFloatField;
    BooleanField2: TBooleanField;
    FloatField7: TFloatField;
    SmallintField1: TSmallintField;
    Panel3: TPanel;
    cxGrid_tasks: TcxGrid;
    cxGrid_tasksDBTableView1: TcxGridDBTableView;
    cxGrid_tasksLevel1: TcxGridLevel;
    ds_Places: TDataSource;
    cxGrid_tasksDBTableView1RecId: TcxGridDBColumn;
    cxGrid_tasksDBTableView1place_name: TcxGridDBColumn;
    cxGrid_tasksDBTableView1place_lat: TcxGridDBColumn;
    cxGrid_tasksDBTableView1place_lon: TcxGridDBColumn;
    cxGrid_tasksDBTableView1place_height: TcxGridDBColumn;
    cxGrid_tasksDBTableView1place_name_rus: TcxGridDBColumn;
    cxGrid_tasksDBTableView1place_map_building_height: TcxGridDBColumn;
    cxGrid_tasksDBTableView1__: TcxGridDBColumn;
    cxGrid_tasksDBTableView1result_msg: TcxGridDBColumn;
    cxGrid_tasksDBTableView1result_comment: TcxGridDBColumn;
    cxGrid_tasksDBTableView1result_client_antenna_lat: TcxGridDBColumn;
    cxGrid_tasksDBTableView1result_client_antenna_lon: TcxGridDBColumn;
    cxGrid_tasksDBTableView1result_client_antenna_height: TcxGridDBColumn;
    cxGrid_tasksDBTableView1___: TcxGridDBColumn;
    cxGrid_tasksDBTableView1client_map_height: TcxGridDBColumn;
    cxGrid_tasksDBTableView1clutter_code: TcxGridDBColumn;
    procedure Button1Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    FTask: TTask;
  public
    class procedure ExecDlg(aTask: TTask);

  end;


implementation

{$R *.dfm}


procedure Tdlg_Task_result.Button1Click(Sender: TObject);
begin
   Edit1.Text:= Task_Send_to_Local_MG (FTask);
end;



procedure Tdlg_Task_result.FormCreate(Sender: TObject);
begin
  Caption:='��������� �������';

  dxMemData1.Open;

  Image_Map.Picture := nil;
  Image_profile.Picture := nil;

end;

class procedure Tdlg_Task_result.ExecDlg(aTask: TTask);
begin
  with Tdlg_Task_result.Create(Application) do
  begin
    FTask:=aTask;



     db_UpdateRecord(dxMemData1,
      [
        'result',   FTask.GetResultStr(),
        'comment',  FTask.CalcResult.Description

//        'client_lat', FTask.Places[0].WGS.Lat,
//        'client_lon', FTask.Places[0].WGS.Lon
      ]);


      if FileExists(FTask.Places[0].CalcResult.FileName_Profile) then
        Image_profile.Picture.LoadFromFile(FTask.Places[0].CalcResult.FileName_Profile);


      if FileExists(FTask.Places[0].CalcResult.FileName_Map) then
        Image_Map.Picture.LoadFromFile(FTask.Places[0].CalcResult.FileName_Map);


//  oTask.CalcResult.ResultType:=rtNone;
//  oTask.CalcResult.Description:='Description';
//  oTask.GUID_str:= dxMemData1['CalculationCode'];
//
//


  //  ed_Connection.Text:= aConnectionString;

    ShowModal;
    Free;
  end;
end;

end.
