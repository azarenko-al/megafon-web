unit d_Lib_GetTableID_111111;

interface

uses
  dm_Main,
  u_db,

  System.SysUtils, System.Variants, System.Classes,
  Vcl.Controls, Vcl.Forms,
  Data.DB, Data.Win.ADODB, cxGridLevel,
  cxGridDBTableView, cxGrid, RxPlacemnt, Vcl.ExtCtrls, Vcl.StdCtrls, cxGraphics,
  cxControls, cxLookAndFeels, cxLookAndFeelPainters, cxStyles, cxCustomData,
  cxFilter, cxData, cxDataStorage, cxEdit, cxNavigator, dxDateRanges, cxDBData,
  cxGridCustomTableView, cxGridTableView, cxClasses, cxGridCustomView;

type
  Tdlg_Lib_GetTableID = class(TForm)
    Button1: TButton;
    Button2: TButton;
    Panel1: TPanel;
    FormStorage1: TFormStorage;
    cxGrid1: TcxGrid;
    cxGrid1DBTableView1: TcxGridDBTableView;
    cxGrid1DBTableView1id: TcxGridDBColumn;
    cxGrid1DBTableView1name: TcxGridDBColumn;
    cxGrid1Level1: TcxGridLevel;
    ADOTable1: TADOTable;
    DataSource1: TDataSource;
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    class function ExecDlg(aTableName: string; aID: Integer): Integer;

  end;



implementation

{$R *.dfm}


procedure Tdlg_Lib_GetTableID.FormCreate(Sender: TObject);
begin
  cxGrid1.Align:=alClient;
end;

//--------------------------------------------------------------------
class function Tdlg_Lib_GetTableID.ExecDlg(aTableName: string; aID: Integer):
    Integer;
//--------------------------------------------------------------------
begin
  with Tdlg_Lib_GetTableID.Create(Application) do
  begin
  //  FRec.Folder_ID:=aFolderID;
  //  ed_Name_.Text:=dmAntType.GetNewName ();

    dmMain.OpenTable (ADOTable1, aTableName );
    ADOTable1.Locate(FLD_ID, aID, []);


    ShowModal;

    Result:=ADOTable1[FLD_ID];

    Free;
  end;
end;

end.
