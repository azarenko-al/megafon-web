object dmData_lib: TdmData_lib
  OldCreateOrder = False
  OnCreate = DataModuleCreate
  Height = 277
  Width = 618
  object ds_TerminalType: TDataSource
    DataSet = t_TerminalType
    Left = 176
    Top = 104
  end
  object t_LinkEndType: TADOTable
    Connection = dmMain.ADOConnection1
    CursorType = ctStatic
    TableName = 'lib.LinkEndType'
    Left = 264
    Top = 48
  end
  object ds_LinkEndType: TDataSource
    DataSet = t_LinkEndType
    Left = 264
    Top = 104
  end
  object t_Equipment: TADOTable
    Connection = dmMain.ADOConnection1
    CursorType = ctStatic
    TableName = 'lib.Equipment'
    Left = 96
    Top = 48
  end
  object ds_Equipment: TDataSource
    DataSet = t_Equipment
    Left = 96
    Top = 104
  end
  object t_TerminalType: TADOTable
    Connection = dmMain.ADOConnection1
    CursorType = ctStatic
    TableName = 'lib.TerminalType'
    Left = 176
    Top = 48
  end
  object t_LinkEndType_mode: TADOTable
    Connection = dmMain.ADOConnection1
    CursorType = ctStatic
    IndexFieldNames = 'id'
    MasterFields = 'LinkEndType_id'
    MasterSource = ds_Equipment
    TableName = 'lib.LinkEndType_mode'
    Left = 368
    Top = 48
  end
  object ds_LinkEndType_mode: TDataSource
    DataSet = t_LinkEndType_mode
    Left = 368
    Top = 104
  end
  object t_ClutterModel: TADOTable
    Connection = dmMain.ADOConnection1
    CursorType = ctStatic
    TableName = 'lib.ClutterModel'
    Left = 528
    Top = 48
  end
  object ds_ClutterModel: TDataSource
    DataSet = t_ClutterModel
    Left = 528
    Top = 104
  end
end
