unit dm_Data_lib1111111111;

interface

uses
  dm_Main,

  System.Classes, Data.DB, Data.Win.ADODB;

type
  TdmData_lib = class(TDataModule)
    ds_TerminalType: TDataSource;
    t_LinkEndType: TADOTable;
    ds_LinkEndType: TDataSource;
    t_Equipment: TADOTable;
    ds_Equipment: TDataSource;
    t_TerminalType: TADOTable;
    t_LinkEndType_mode: TADOTable;
    ds_LinkEndType_mode: TDataSource;
    t_ClutterModel: TADOTable;
    ds_ClutterModel: TDataSource;
    procedure DataModuleCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  dmData_lib: TdmData_lib;

implementation

{%CLASSGROUP 'Vcl.Controls.TControl'}


{$R *.dfm}

procedure TdmData_lib.DataModuleCreate(Sender: TObject);
begin
  dmMain.OpenTable (t_Equipment);
  dmMain.OpenTable (t_TerminalType);
  dmMain.OpenTable (t_LinkEndType);


  dmMain.OpenTable(t_ClutterModel);

end;

end.
