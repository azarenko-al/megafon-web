inherited frame_Pivot: Tframe_Pivot
  Caption = 'frame_Pivot'
  ClientHeight = 502
  ClientWidth = 835
  ExplicitWidth = 851
  ExplicitHeight = 541
  PixelsPerInch = 96
  TextHeight = 13
  object cxGrid1: TcxGrid [0]
    Left = 0
    Top = 29
    Width = 505
    Height = 473
    Align = alLeft
    TabOrder = 0
    ExplicitHeight = 462
    object cxGrid1DBTableView_tasks: TcxGridDBTableView
      Navigator.Buttons.CustomButtons = <>
      FindPanel.DisplayMode = fpdmManual
      DataController.DataSource = ds_Task
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <
        item
        end>
      DataController.Summary.SummaryGroups = <>
      OptionsCustomize.ColumnsQuickCustomization = True
      OptionsSelection.MultiSelect = True
      OptionsSelection.HideSelection = True
      OptionsView.GroupByBox = False
      OptionsView.Indicator = True
      object cxGrid1DBTableView_tasksid: TcxGridDBColumn
        DataBinding.FieldName = 'id'
        SortIndex = 0
        SortOrder = soAscending
        Width = 53
      end
      object cxGrid1DBTableView_tasksDate_created: TcxGridDBColumn
        DataBinding.FieldName = 'Date_created'
      end
      object cxGrid1DBTableView_taskshost: TcxGridDBColumn
        DataBinding.FieldName = 'host'
        Width = 84
      end
      object cxGrid1DBTableView_tasksbody: TcxGridDBColumn
        DataBinding.FieldName = 'body'
        PropertiesClassName = 'TcxBlobEditProperties'
        Properties.BlobEditKind = bekMemo
        Properties.PopupHeight = 500
        Properties.PopupWidth = 500
        Width = 68
      end
      object cxGrid1DBTableView_tasksguid: TcxGridDBColumn
        DataBinding.FieldName = 'guid'
        Width = 214
      end
    end
    object cxGrid1Level1: TcxGridLevel
      GridView = cxGrid1DBTableView_tasks
    end
  end
  object ToolBar1: TToolBar [1]
    Left = 0
    Top = 0
    Width = 835
    Height = 29
    BorderWidth = 1
    ButtonHeight = 25
    ButtonWidth = 81
    Caption = 'ToolBar1'
    TabOrder = 1
    ExplicitWidth = 808
    object Button1: TButton
      Left = 0
      Top = 0
      Width = 75
      Height = 25
      Action = act_Refresh
      TabOrder = 0
    end
  end
  inherited dxBarManager1: TdxBarManager
    Left = 588
    Top = 168
    PixelsPerInch = 96
    object dxBarButton1: TdxBarButton
      Caption = 'New Button'
      Category = 0
      Hint = 'New Button'
      Visible = ivAlways
    end
  end
  inherited FormStorage1: TFormStorage
    Left = 588
    Top = 224
  end
  inherited ActionList_base: TActionList
    Left = 688
    Top = 40
    object act_Refresh: TAction
      Caption = #1054#1073#1085#1086#1074#1080#1090#1100
      OnExecute = act_RefreshExecute
    end
  end
  object t_queue: TADOTable
    Connection = dmMain.ADOConnection1
    CursorType = ctStatic
    TableName = 'WebService.queue'
    Left = 592
    Top = 40
  end
  object ds_Task: TDataSource
    DataSet = t_queue
    Left = 592
    Top = 96
  end
  object ADOStoredProc_House_points: TADOStoredProc
    DataSource = ds_Task
    ProcedureName = 'map.sp_House_points_By_LatLon'
    Parameters = <>
    Left = 65100
    Top = 384
  end
end
