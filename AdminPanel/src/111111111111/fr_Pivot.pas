unit fr_Pivot;

interface

uses
  dm_App,

  u_const_calc,

  u_cx,
 u_cx_ini,

  dm_Main,

  fr_View_base,

  u_func,

  System.SysUtils, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Variants, Messages,
  Data.DB, Data.Win.ADODB, cxGridLevel,
  cxGridCustomTableView, cxGridDBTableView,
  cxGrid, cxSplitter, Vcl.ExtCtrls,
  Vcl.ComCtrls, dxBar,
  Vcl.ActnList, Vcl.StdCtrls, Vcl.DBGrids,
  Vcl.Menus, cxGraphics, cxControls, cxLookAndFeels, cxLookAndFeelPainters,
  cxStyles, cxCustomData, cxFilter, cxData, cxDataStorage, cxEdit, cxNavigator,
  dxDateRanges, cxDBData, cxBlobEdit, System.Actions, Vcl.ToolWin,
  dxGDIPlusClasses, Vcl.Grids, cxGridTableView, cxGridCustomView, RxPlacemnt,
  cxClasses, Vcl.DBCtrls, cxContainer, cxImage, cxDBEdit;

type
  Tframe_Pivot = class(Tframe_View_base)
    cxGrid1DBTableView_tasks: TcxGridDBTableView;
    cxGrid1Level1: TcxGridLevel;
    cxGrid1: TcxGrid;
    t_queue: TADOTable;
    ds_Task: TDataSource;
    ToolBar1: TToolBar;
    dxBarButton1: TdxBarButton;
    ADOStoredProc_House_points: TADOStoredProc;
    act_Refresh: TAction;
    Button1: TButton;
    cxGrid1DBTableView_tasksid: TcxGridDBColumn;
    cxGrid1DBTableView_tasksDate_created: TcxGridDBColumn;
    cxGrid1DBTableView_taskshost: TcxGridDBColumn;
    cxGrid1DBTableView_tasksbody: TcxGridDBColumn;
    cxGrid1DBTableView_tasksguid: TcxGridDBColumn;
//    procedure FormDestroy(Sender: TObject);
    procedure act_RefreshExecute(Sender: TObject);
//    procedure act_Show_on_MapExecute(Sender: TObject);
//    procedure act_Task_Show_on_MapExecute(Sender: TObject);
//    procedure Button2Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
//    procedure ToolButton1Click(Sender: TObject);
//    procedure t_TaskAfterScroll(DataSet: TDataSet);
//    procedure cxGrid1DBTableView1StylesGetContentStyle(
//      Sender: TcxCustomGridTableView; ARecord: TcxCustomGridRecord;
//      AItem: TcxCustomGridTableItem; var AStyle: TcxStyle);
//    procedure cxGrid_tasksDBTableView1StylesGetContentStyle(
//      Sender: TcxCustomGridTableView; ARecord: TcxCustomGridRecord;
//      AItem: TcxCustomGridTableItem; var AStyle: TcxStyle);
//    procedure TabSheet1Resize(Sender: TObject);
  private
//    Fframe_Map_view: Tframe_Map_view;


 //   procedure WMCopyData(var Msg: TWMCopyData);

  public
//    procedure NewMethod;

  end;

var
  frame_Pivot: Tframe_Pivot;


implementation

{$R *.dfm}

procedure Tframe_Pivot.act_RefreshExecute(Sender: TObject);
begin
  t_queue.Requery([]);

end;


//-------------------------------------------------------------------
procedure Tframe_Pivot.FormCreate(Sender: TObject);
//-------------------------------------------------------------------
var
  s: string;
begin

  SetCaption('�������');
  cxGrid1.Align:=alClient;

  dmMain.OpenTable(t_queue);


//  s:= FormStorage1.StoredProps.Text;

 // dmMain.OpenQuery(ADOQuery_Relief);


 //  Tframe_WebBrowser.Create_(TabSheet_Map);

  cx_LoadColumnCaptions_FromIni(cxGrid1DBTableView_tasks);


 // cxGrid1DBTableView1.Navigator.Visible:=True;

  cx_Footer(cxGrid1DBTableView_tasks,   cxGrid1DBTableView_tasksDate_created);




end;




end.



{

//-------------------------------------------------------------------
procedure Tframe_Task.t_TaskAfterScroll(DataSet: TDataSet);
//-------------------------------------------------------------------
var
  sDir: string;
  sGuid: string;
begin
{
  sGuid := t_Task.FieldByName('guid').AsString;
  sDir:= g_Config.GetRequestDir_ByGUID (sGuid);

  if FileExists(sDir + 'profile.png') then
    Image_profile.Picture.LoadFromFile(sDir + 'profile.png')
  else
    Image_profile.Picture := nil;


  if FileExists(sDir + 'map.png') then
    Image_Map.Picture.LoadFromFile(sDir + 'map.png')
  else
    Image_Map.Picture := nil;
 }


end;


