inherited frame_Task: Tframe_Task
  Caption = 'frame_Task'
  ClientHeight = 871
  ClientWidth = 1050
  OnDestroy = FormDestroy
  ExplicitWidth = 1066
  ExplicitHeight = 910
  PixelsPerInch = 96
  TextHeight = 13
  object cxGrid1: TcxGrid [0]
    Left = 0
    Top = 29
    Width = 505
    Height = 324
    Align = alLeft
    TabOrder = 0
    object cxGrid1DBTableView1: TcxGridDBTableView
      PopupMenu = PopupMenu_Task
      Navigator.Buttons.CustomButtons = <>
      FindPanel.DisplayMode = fpdmManual
      DataController.DataSource = ds_Task
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <
        item
        end>
      DataController.Summary.SummaryGroups = <>
      OptionsCustomize.ColumnsQuickCustomization = True
      OptionsView.GroupByBox = False
      OptionsView.Indicator = True
      object cxGrid1DBTableView1id: TcxGridDBColumn
        DataBinding.FieldName = 'id'
        Options.Editing = False
        SortIndex = 0
        SortOrder = soDescending
        Width = 50
      end
      object cxGrid1DBTableView1Column1: TcxGridDBColumn
        DataBinding.FieldName = 'Date_created'
        Options.Editing = False
        Width = 128
      end
      object cxGrid1DBTableView1guid: TcxGridDBColumn
        DataBinding.FieldName = 'guid'
        Options.Editing = False
        Width = 100
      end
      object cxGrid1DBTableView1client_lat: TcxGridDBColumn
        DataBinding.FieldName = 'client_lat'
        Options.Editing = False
        Width = 49
      end
      object cxGrid1DBTableView1client_lon: TcxGridDBColumn
        DataBinding.FieldName = 'client_lon'
        Options.Editing = False
        Width = 51
      end
      object cxGrid1DBTableView1client_antenna_lat: TcxGridDBColumn
        DataBinding.FieldName = 'client_antenna_lat'
        Options.Editing = False
        Width = 107
      end
      object cxGrid1DBTableView1client_antenna_lon: TcxGridDBColumn
        DataBinding.FieldName = 'client_antenna_lon'
        Options.Editing = False
        Width = 97
      end
      object cxGrid1DBTableView1client_antenna_height: TcxGridDBColumn
        DataBinding.FieldName = 'client_antenna_height'
        Options.Editing = False
        Width = 113
      end
      object cxGrid1DBTableView1bitrate_Mbps: TcxGridDBColumn
        DataBinding.FieldName = 'bitrate_Mbps'
        Options.Editing = False
        Width = 69
      end
      object cxGrid1DBTableView1is_can_change_antenna_height: TcxGridDBColumn
        DataBinding.FieldName = 'is_can_change_antenna_height'
        Options.Editing = False
        Width = 20
      end
      object cxGrid1DBTableView1is_can_change_antenna_pos: TcxGridDBColumn
        DataBinding.FieldName = 'is_can_change_antenna_location'
        Options.Editing = False
        Width = 20
      end
      object col_Technology: TcxGridDBColumn
        DataBinding.FieldName = 'Technology'
        Options.Editing = False
        Width = 84
      end
      object col_Date_calc_finish: TcxGridDBColumn
        DataBinding.FieldName = 'Date_calc_finish'
        Options.Editing = False
        Width = 118
      end
      object col_result_msg: TcxGridDBColumn
        DataBinding.FieldName = 'result_msg'
        Options.Editing = False
        Width = 129
      end
      object col_result_comment: TcxGridDBColumn
        DataBinding.FieldName = 'result_comment'
        Options.Editing = False
        Width = 100
      end
      object col_log: TcxGridDBColumn
        DataBinding.FieldName = 'comment'
        PropertiesClassName = 'TcxBlobEditProperties'
        Properties.BlobEditKind = bekMemo
        Properties.PopupHeight = 300
        Properties.PopupWidth = 350
        Properties.ReadOnly = True
      end
    end
    object cxGrid1Level1: TcxGridLevel
      GridView = cxGrid1DBTableView1
    end
  end
  object PageControl1: TPageControl [1]
    Left = 0
    Top = 481
    Width = 1050
    Height = 390
    ActivePage = TabSheet1
    Align = alBottom
    TabOrder = 1
    object TabSheet_Map: TTabSheet
      Caption = 'Map'
      ExplicitLeft = 0
      ExplicitTop = 0
      ExplicitWidth = 0
      ExplicitHeight = 0
    end
    object TabSheet_Relief: TTabSheet
      Caption = 'Relief'
      ImageIndex = 1
      ExplicitLeft = 0
      ExplicitTop = 0
      ExplicitWidth = 0
      ExplicitHeight = 0
      object DBGrid_Relief: TDBGrid
        Left = 0
        Top = 0
        Width = 977
        Height = 362
        Align = alLeft
        Anchors = [akLeft, akTop, akRight, akBottom]
        DataSource = ds_Relief
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'Tahoma'
        TitleFont.Style = []
      end
    end
    object TabSheet1: TTabSheet
      Caption = 'Images DB'
      ImageIndex = 3
      object cxDBImage_profile: TcxDBImage
        Left = 0
        Top = 0
        Align = alLeft
        DataBinding.DataField = 'result_profile_image'
        DataBinding.DataSource = ds_Task_places
        Properties.GraphicClassName = 'TdxPNGImage'
        TabOrder = 0
        Height = 362
        Width = 443
      end
      object cxSplitter1: TcxSplitter
        Left = 443
        Top = 0
        Width = 8
        Height = 362
        HotZoneClassName = 'TcxSimpleStyle'
        Control = cxDBImage_profile
      end
      object cxDBImage_map: TcxDBImage
        Left = 451
        Top = 0
        Align = alLeft
        DataBinding.DataField = 'result_map_image'
        DataBinding.DataSource = ds_Task_places
        Properties.GraphicClassName = 'TdxPNGImage'
        TabOrder = 2
        Height = 362
        Width = 441
      end
    end
    object TabSheet3: TTabSheet
      Caption = 'Log'
      ImageIndex = 4
      ExplicitLeft = 0
      ExplicitTop = 0
      ExplicitWidth = 0
      ExplicitHeight = 0
      object DBMemo1: TDBMemo
        Left = 0
        Top = 0
        Width = 841
        Height = 362
        Align = alLeft
        Anchors = [akLeft, akTop, akRight, akBottom]
        DataField = 'comment'
        DataSource = ds_Task_places
        TabOrder = 0
      end
    end
  end
  object ToolBar1: TToolBar [2]
    Left = 0
    Top = 0
    Width = 1050
    Height = 29
    BorderWidth = 1
    ButtonHeight = 25
    ButtonWidth = 81
    Caption = 'ToolBar1'
    TabOrder = 2
    object Button1: TButton
      Left = 0
      Top = 0
      Width = 75
      Height = 25
      Action = act_Refresh
      TabOrder = 0
    end
  end
  object cxSplitter2: TcxSplitter [3]
    Left = 0
    Top = 473
    Width = 1050
    Height = 8
    HotZoneClassName = 'TcxSimpleStyle'
    AlignSplitter = salBottom
    Control = PageControl1
  end
  object cxGrid_tasks: TcxGrid [4]
    Left = 0
    Top = 361
    Width = 1050
    Height = 112
    Align = alBottom
    TabOrder = 4
    object cxGrid_tasksDBTableView1: TcxGridDBTableView
      PopupMenu = PopupMenu_Places
      Navigator.Buttons.CustomButtons = <>
      FindPanel.DisplayMode = fpdmManual
      DataController.DataSource = ds_Task_places
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <
        item
        end>
      DataController.Summary.SummaryGroups = <>
      OptionsBehavior.ImmediateEditor = False
      OptionsCustomize.ColumnsQuickCustomization = True
      OptionsView.GroupByBox = False
      OptionsView.Indicator = True
      object cxGrid_tasksDBTableView1id: TcxGridDBColumn
        DataBinding.FieldName = 'id'
      end
      object cxGrid_tasksDBTableView1task_id: TcxGridDBColumn
        DataBinding.FieldName = 'task_id'
        Visible = False
      end
      object cxGrid_tasksDBTableView1place_name: TcxGridDBColumn
        DataBinding.FieldName = 'place_name'
        Width = 76
      end
      object cxGrid_tasksDBTableView1place_lat: TcxGridDBColumn
        DataBinding.FieldName = 'place_lat'
      end
      object cxGrid_tasksDBTableView1place_lon: TcxGridDBColumn
        DataBinding.FieldName = 'place_lon'
      end
      object cxGrid_tasksDBTableView1place_height: TcxGridDBColumn
        DataBinding.FieldName = 'place_height'
        Width = 78
      end
      object col_ObjectID: TcxGridDBColumn
        DataBinding.FieldName = 'ObjectID'
        Width = 145
      end
      object col_SysName: TcxGridDBColumn
        DataBinding.FieldName = 'SysName'
        Width = 73
      end
      object col_map_building_height: TcxGridDBColumn
        DataBinding.FieldName = 'map_building_height'
        Width = 119
      end
      object cxGrid_tasksDBTableView1result_client_antenna_lat: TcxGridDBColumn
        DataBinding.FieldName = 'result_client_antenna_lat'
      end
      object cxGrid_tasksDBTableView1result_client_antenna_lon: TcxGridDBColumn
        DataBinding.FieldName = 'result_client_antenna_lon'
      end
      object cxGrid_tasksDBTableView1result_client_antenna_height: TcxGridDBColumn
        DataBinding.FieldName = 'result_client_antenna_height'
      end
      object col_result_ClientHWTypeID: TcxGridDBColumn
        DataBinding.FieldName = 'result_ClientHWTypeID'
        Width = 145
      end
    end
    object cxGrid_tasksLevel1: TcxGridLevel
      GridView = cxGrid_tasksDBTableView1
    end
  end
  object cxSplitter3: TcxSplitter [5]
    Left = 0
    Top = 353
    Width = 1050
    Height = 8
    HotZoneClassName = 'TcxSimpleStyle'
    AlignSplitter = salBottom
    Control = cxGrid_tasks
  end
  inherited dxBarManager1: TdxBarManager
    Left = 556
    Top = 160
    PixelsPerInch = 96
    object dxBarButton1: TdxBarButton
      Caption = 'New Button'
      Category = 0
      Hint = 'New Button'
      Visible = ivAlways
    end
  end
  inherited FormStorage1: TFormStorage
    StoredProps.Strings = (
      'PageControl1.Height'
      'cxGrid_tasks.Height'
      'PageControl1.ActivePage'
      'cxDBImage_profile.Width')
    Left = 556
    Top = 216
  end
  inherited ActionList_base: TActionList
    Left = 920
    Top = 48
    object act_Refresh: TAction
      Caption = #1054#1073#1085#1086#1074#1080#1090#1100
      OnExecute = act_RefreshExecute
    end
    object act_Show_on_Map: TAction
      Caption = #1055#1086#1082#1072#1079#1072#1090#1100' '#1085#1072' '#1082#1072#1088#1090#1077
      OnExecute = act_Show_on_MapExecute
    end
  end
  object t_Task: TADOTable
    Connection = dmMain.ADOConnection1
    CursorType = ctStatic
    AfterScroll = t_TaskAfterScroll
    TableName = 'megagis_client.view_Task'
    Left = 560
    Top = 40
  end
  object ds_Task: TDataSource
    DataSet = t_Task
    Left = 560
    Top = 96
  end
  object ds_House_points: TDataSource
    DataSet = ADOQuery_House_points
    Left = 808
    Top = 264
  end
  object ADOStoredProc_House_points: TADOStoredProc
    DataSource = ds_Task
    ProcedureName = 'map.sp_House_points_By_LatLon'
    Parameters = <>
    Left = 65100
    Top = 384
  end
  object ADOQuery_House_points: TADOQuery
    Connection = dmMain.ADOConnection1
    CursorType = ctStatic
    DataSource = ds_Task
    Parameters = <
      item
        Name = 'id'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = 497
      end>
    SQL.Strings = (
      'exec map.sp_House_points_By_Task_ID :id')
    Left = 804
    Top = 208
  end
  object ADOQuery_House: TADOQuery
    Connection = dmMain.ADOConnection1
    CursorType = ctStatic
    DataSource = ds_Task
    Parameters = <
      item
        Name = 'id'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = 497
      end>
    SQL.Strings = (
      'exec map.sp_House_points_By_Task_ID :id')
    Left = 932
    Top = 208
  end
  object ds_House: TDataSource
    DataSet = ADOQuery_House
    Left = 936
    Top = 264
  end
  object q_Relief: TADOQuery
    Connection = dmMain.ADOConnection1
    CursorType = ctStatic
    DataSource = ds_Task
    Parameters = <
      item
        Name = 'id'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = 734
      end>
    SQL.Strings = (
      'exec gis.sp_Relief_by_Task_SEL   :id')
    Left = 676
    Top = 208
  end
  object ds_Relief: TDataSource
    DataSet = q_Relief
    Left = 672
    Top = 264
  end
  object PopupMenu_Task: TPopupMenu
    Left = 32
    Top = 80
    object N1: TMenuItem
      Action = act_Show_on_Map
    end
    object N2: TMenuItem
      Caption = '-'
    end
    object actRefresh1: TMenuItem
      Action = act_Refresh
    end
  end
  object q_Task_places: TADOQuery
    Connection = dmMain.ADOConnection1
    CursorType = ctStatic
    LockType = ltReadOnly
    DataSource = ds_Task
    Parameters = <
      item
        Name = 'id'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Value = 1738
      end>
    SQL.Strings = (
      'select * '
      'from megagis_client.Task_places'
      'where task_id = :id')
    Left = 652
    Top = 40
  end
  object ds_Task_places: TDataSource
    DataSet = q_Task_places
    Left = 656
    Top = 96
  end
  object PopupMenu_Places: TPopupMenu
    Left = 48
    Top = 400
    object MenuItem1: TMenuItem
      Action = act_Show_on_Map
    end
    object MenuItem2: TMenuItem
      Caption = '-'
    end
    object MenuItem3: TMenuItem
      Action = act_Refresh
    end
  end
end
