unit fr_Task;

interface

uses
  fr_Map_view,

  u_geo,
//  dm_Task,
//  dm_Task_Profile,
 // dm_Relief,

  u_Config,

  dm_Main,

  fr_View_base,

  u_func,


  System.SysUtils, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms,


  Data.DB, Data.Win.ADODB, cxGridLevel,
  cxGridCustomTableView, cxGridDBTableView,
  cxGrid, cxSplitter, Vcl.ExtCtrls,
  Vcl.ComCtrls, dxBar,
  Vcl.ActnList, Vcl.StdCtrls, Vcl.DBGrids,
  Vcl.Menus, cxGraphics, cxControls, cxLookAndFeels, cxLookAndFeelPainters,
  cxStyles, cxCustomData, cxFilter, cxData, cxDataStorage, cxEdit, cxNavigator,
  dxDateRanges, cxDBData, cxBlobEdit, System.Actions, Vcl.ToolWin,
  dxGDIPlusClasses, Vcl.Grids, cxGridTableView, cxGridCustomView, RxPlacemnt,
  cxClasses, Vcl.DBCtrls, cxContainer, cxImage, cxDBEdit;

type
  Tframe_Task = class(Tframe_View_base)
    cxGrid1DBTableView1: TcxGridDBTableView;
    cxGrid1Level1: TcxGridLevel;
    cxGrid1: TcxGrid;
    t_Task: TADOTable;
    ds_Task: TDataSource;
    cxGrid1DBTableView1id: TcxGridDBColumn;
    cxGrid1DBTableView1guid: TcxGridDBColumn;
    cxGrid1DBTableView1client_lat: TcxGridDBColumn;
    cxGrid1DBTableView1client_lon: TcxGridDBColumn;
    cxGrid1DBTableView1client_antenna_lat: TcxGridDBColumn;
    cxGrid1DBTableView1client_antenna_lon: TcxGridDBColumn;
    cxGrid1DBTableView1client_antenna_height: TcxGridDBColumn;
    cxGrid1DBTableView1bitrate_Mbps: TcxGridDBColumn;
    cxGrid1DBTableView1is_can_change_antenna_height: TcxGridDBColumn;
    cxGrid1DBTableView1is_can_change_antenna_pos: TcxGridDBColumn;
    PageControl1: TPageControl;
    TabSheet_Map: TTabSheet;
    TabSheet_Relief: TTabSheet;
    ToolBar1: TToolBar;
    cxSplitter2: TcxSplitter;
    dxBarButton1: TdxBarButton;
    DBGrid_Relief: TDBGrid;
    ds_House_points: TDataSource;
    ADOStoredProc_House_points: TADOStoredProc;
    ADOQuery_House_points: TADOQuery;
    ADOQuery_House: TADOQuery;
    ds_House: TDataSource;
    q_Relief: TADOQuery;
    ds_Relief: TDataSource;
    cxGrid_tasks: TcxGrid;
    cxGrid_tasksDBTableView1: TcxGridDBTableView;
    cxGrid_tasksDBTableView1id: TcxGridDBColumn;
    cxGrid_tasksDBTableView1task_id: TcxGridDBColumn;
    cxGrid_tasksDBTableView1place_name: TcxGridDBColumn;
    cxGrid_tasksDBTableView1place_lat: TcxGridDBColumn;
    cxGrid_tasksDBTableView1place_lon: TcxGridDBColumn;
    cxGrid_tasksDBTableView1place_height: TcxGridDBColumn;
    cxGrid_tasksDBTableView1result_client_antenna_lat: TcxGridDBColumn;
    cxGrid_tasksDBTableView1result_client_antenna_lon: TcxGridDBColumn;
    cxGrid_tasksDBTableView1result_client_antenna_height: TcxGridDBColumn;
    cxGrid_tasksLevel1: TcxGridLevel;
    PopupMenu_Task: TPopupMenu;
    actRefresh1: TMenuItem;
    act_Refresh: TAction;
    cxGrid1DBTableView1Column1: TcxGridDBColumn;
    Button1: TButton;
    act_Show_on_Map: TAction;
    N1: TMenuItem;
    N2: TMenuItem;
    col_ObjectID: TcxGridDBColumn;
    col_map_building_height: TcxGridDBColumn;
    col_Technology: TcxGridDBColumn;
    col_result_msg: TcxGridDBColumn;
    col_result_comment: TcxGridDBColumn;
    q_Task_places: TADOQuery;
    ds_Task_places: TDataSource;
    col_log: TcxGridDBColumn;
    col_Date_calc_finish: TcxGridDBColumn;
    col_SysName: TcxGridDBColumn;
    col_result_ClientHWTypeID: TcxGridDBColumn;
    TabSheet1: TTabSheet;
    TabSheet3: TTabSheet;
    cxDBImage_profile: TcxDBImage;
    cxSplitter1: TcxSplitter;
    cxDBImage_map: TcxDBImage;
    DBMemo1: TDBMemo;
    cxSplitter3: TcxSplitter;
    PopupMenu_Places: TPopupMenu;
    MenuItem1: TMenuItem;
    MenuItem2: TMenuItem;
    MenuItem3: TMenuItem;
    procedure FormDestroy(Sender: TObject);
    procedure act_RefreshExecute(Sender: TObject);
    procedure act_Show_on_MapExecute(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure t_TaskAfterScroll(DataSet: TDataSet);
  private
//    Fframe_Map_view: Tframe_Map_view;

     FWebBrowser: Tframe_Map_view;
//    procedure View_on_Map;

  public
//    procedure NewMethod;

  end;

var
  frame_Task: Tframe_Task;


implementation

{$R *.dfm}

procedure Tframe_Task.act_RefreshExecute(Sender: TObject);
begin
  t_Task.Requery([]);

 // dmMain.OpenTable(t_Task);

  q_Task_places.Open;
end;


procedure Tframe_Task.FormCreate(Sender: TObject);
var
  s: string;
begin

  SetCaption('������-�����');
  cxGrid1.Align:=alClient;
  cxDBImage_map.Align:=alClient;

  dmMain.OpenTable(t_Task);

  q_Task_places.Open;
  q_Relief.Open;

  Resize;


//  s:= FormStorage1.StoredProps.Text;

 // dmMain.OpenQuery(ADOQuery_Relief);


 //  Tframe_WebBrowser.Create_(TabSheet_Map);


  CreateChildForm(FWebBrowser, Tframe_Map_view, TabSheet_Map);


  DBGrid_Relief.Align:=alClient;


  cxGrid1DBTableView1.Navigator.Visible:=True;



//  cx_LoadColumnCaptions_FromIni(cxGrid1DBTableView1);
//  cx_LoadColumnCaptions_FromIni(cxGrid_tasksDBTableView1);



//  TdmTask.Init;


end;

//-------------------------------------------------------------------
procedure Tframe_Task.FormDestroy(Sender: TObject);
//-------------------------------------------------------------------
begin
  FreeAndNil(FWebBrowser);

  inherited;
end;

//-------------------------------------------------------------------
procedure Tframe_Task.act_Show_on_MapExecute(Sender: TObject);
//-------------------------------------------------------------------
var
 // sJSON: string;
  ll_1,ll_2:  TLatLon;
begin
//  sJSON:=t_Task.FieldByName('GeoJSON').AsString;
//  FWebBrowser.Load_GeoJSON(sJSON);

  if not q_Task_places.IsEmpty then
  begin
    ll_1.Lat:=t_Task.FieldByName('client_Lat').AsFloat;
    ll_1.Lon:=t_Task.FieldByName('client_Lon').AsFloat;


    ll_2.Lat:=q_Task_places.FieldByName('place_Lat').AsFloat;
    ll_2.Lon:=q_Task_places.FieldByName('place_Lon').AsFloat;


    FWebBrowser.DrawLine(ll_1.Lat, ll_1.Lon, ll_2.Lat, ll_2.Lon );

  end;


end;

//-------------------------------------------------------------------
procedure Tframe_Task.t_TaskAfterScroll(DataSet: TDataSet);
//-------------------------------------------------------------------
var
  sDir: string;
  sGuid: string;
begin
{
  sGuid := t_Task.FieldByName('guid').AsString;
  sDir:= g_Config.GetRequestDir_ByGUID (sGuid);

  if FileExists(sDir + 'profile.png') then
    Image_profile.Picture.LoadFromFile(sDir + 'profile.png')
  else
    Image_profile.Picture := nil;


  if FileExists(sDir + 'map.png') then
    Image_Map.Picture.LoadFromFile(sDir + 'map.png')
  else
    Image_Map.Picture := nil;
 }


end;



end.


{

    if FileExists(FTask.Places[0].Result.ProfileFileName) then
        Image_profile.Picture.LoadFromFile(FTask.Places[0].Result.ProfileFileName);


      if FileExists(FTask.Places[0].Result.MapFileName) then
        Image_Map.Picture.LoadFromFile(FTask.Places[0].Result.MapFileName);


  cx_SetColumnCaptions(cxGrid1DBTableView1,
    [
      FLD_Name,          STR_NAME,
      FLD_Azimuth,       STR_AZIMUTH,
      FLD_Diameter,      STR_DIAMETER,
      FLD_TILT,          STR_TILT, // '�� [dB]',
      FLD_Gain,          STR_GAIN, // '�� [dB]',
      FLD_Height,        STR_HEIGHT,
      FLD_ANTENNATYPE_NAME, STR_TYPE,
      FLD_vert_width,    '������ ��� [V]',
      FLD_horz_width,    '������ ��� [H]',
      FLD_loss,        STR_LOSS,
    //  SArr(FLD_antenna_loss,  STR_LOSS),
      FLD_Polarization_STR,  STR_Polarization,
      FLD_Mast_index,   STR_Mast_index,
      FLD_BAND,         STR_BAND,

      FLD_polarization_ratio , STR_polarization_ratio // '�������� ��������� H/V [dB]'
    ]);




//  Tframe_WebBrowser.Create_(TabSheet_Map);

//
//  cx_SetColumnCaptions(cxGrid1DBTableView1,
//    [
//      FLD_Name,          STR_NAME,
//      FLD_Azimuth,       STR_AZIMUTH,
//      FLD_Diameter,      STR_DIAMETER,
//      FLD_TILT,          STR_TILT, // '�� [dB]',
//      FLD_Gain,          STR_GAIN, // '�� [dB]',
//      FLD_Height,        STR_HEIGHT,
//      FLD_ANTENNATYPE_NAME, STR_TYPE,
//      FLD_vert_width,    '������ ��� [V]',
//      FLD_horz_width,    '������ ��� [H]',
//      FLD_loss,        STR_LOSS,
//    //  SArr(FLD_antenna_loss,  STR_LOSS),
//      FLD_Polarization_STR,  STR_Polarization,
//      FLD_Mast_index,   STR_Mast_index,
//      FLD_BAND,         STR_BAND,
//
//      FLD_polarization_ratio , STR_polarization_ratio // '�������� ��������� H/V [dB]'
//    ]);
//



    if FileExists(FTask.Places[0].Result.ProfileFileName) then
        Image_profile.Picture.LoadFromFile(FTask.Places[0].Result.ProfileFileName);


      if FileExists(FTask.Places[0].Result.MapFileName) then
        Image_Map.Picture.LoadFromFile(FTask.Places[0].Result.MapFileName);


          protected
//    procedure WMEXTERNAL_LIB_IMPORT_DONE(var Message: TMessage); message WM_EXTERNAL_LIB_IMPORT_DONE;
    procedure WMEXTERNAL_PROJECT_IMPORT_DONE(var Message: TMessage); message WM_EXTERNAL_PROJECT_IMPORT_DONE;

  WM_EXTERNAL_PROJECT_IMPORT_DONE   = WM_USER + 500;
  WM_EXTERNAL_LIB_IMPORT_DONE       = WM_USER + 501;

