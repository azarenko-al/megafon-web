object dlg_MDB_import: Tdlg_MDB_import
  Left = 0
  Top = 0
  BorderStyle = bsDialog
  Caption = 'dlg_MDB_import'
  ClientHeight = 411
  ClientWidth = 580
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poMainFormCenter
  OnCreate = FormCreate
  DesignSize = (
    580
    411)
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 8
    Top = 8
    Width = 108
    Height = 13
    Caption = #1056#1072#1089#1087#1086#1083#1086#1078#1077#1085#1080#1077' '#1092#1072#1081#1083#1072
  end
  object FilenameEdit1: TFilenameEdit
    Left = 8
    Top = 27
    Width = 553
    Height = 21
    Anchors = [akLeft, akTop, akRight]
    NumGlyphs = 1
    TabOrder = 0
    Text = 'C:\OneDrive\_wsdl\_terminal.mdb'
  end
  object ProgressBar1: TProgressBar
    Left = 8
    Top = 88
    Width = 553
    Height = 17
    Anchors = [akLeft, akTop, akRight]
    TabOrder = 1
  end
  object Panel1: TPanel
    Left = 0
    Top = 374
    Width = 580
    Height = 37
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 2
    DesignSize = (
      580
      37)
    object Bevel1: TBevel
      Left = 0
      Top = 0
      Width = 580
      Height = 3
      Align = alTop
      Shape = bsTopLine
      ExplicitWidth = 644
    end
    object Button_Ok: TButton
      Left = 418
      Top = 6
      Width = 75
      Height = 25
      Anchors = [akTop, akRight]
      Caption = 'OK'
      Default = True
      ModalResult = 1
      TabOrder = 0
      OnClick = Button_OkClick
    end
    object Button3: TButton
      Left = 499
      Top = 6
      Width = 75
      Height = 25
      Anchors = [akTop, akRight]
      Cancel = True
      Caption = #1047#1072#1082#1088#1099#1090#1100
      ModalResult = 2
      TabOrder = 1
    end
  end
  object FormStorage1: TFormStorage
    StoredProps.Strings = (
      'FilenameEdit1.FileName')
    StoredValues = <>
    Left = 64
    Top = 184
  end
end
