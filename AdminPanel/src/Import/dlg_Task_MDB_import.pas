unit dlg_Task_MDB_import;

interface

uses
  dm_Main,
  dm_Task_MDB_import,

  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, RxPlacemnt, Vcl.StdCtrls, Vcl.Mask,
  RxToolEdit, Data.DB, Vcl.Grids, Vcl.DBGrids, Data.Win.ADODB, Vcl.ComCtrls,
  Vcl.ExtCtrls;

type
  Tdlg_MDB_import = class(TForm)
    FilenameEdit1: TFilenameEdit;
    FormStorage1: TFormStorage;
    ProgressBar1: TProgressBar;
    Button_Ok: TButton;
    Button3: TButton;
    Panel1: TPanel;
    Label1: TLabel;
    Bevel1: TBevel;
    procedure Button_OkClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    procedure Run;

    procedure DoProgress(aProgress, aMax: Integer);
    { Private declarations }
  public
    { Public declarations }
  end;

var
  dlg_MDB_import: Tdlg_MDB_import;

implementation

{$R *.dfm}

procedure Tdlg_MDB_import.Button_OkClick(Sender: TObject);
begin
  Run;
end;

procedure Tdlg_MDB_import.DoProgress(aProgress, aMax: Integer);
begin
  ProgressBar1.Max:=aMax;
  ProgressBar1.Position:=aProgress;
end;

procedure Tdlg_MDB_import.FormCreate(Sender: TObject);
begin
  Caption:='������ �� MDB';

 // ADOTable.Open;
end;


procedure Tdlg_MDB_import.Run;
begin
  TdmTask_MDB.Init;
  dmTask_MDB.Params.MDB_FileName:= FilenameEdit1.FileName;
  dmTask_MDB.OnProgress:=DoProgress;

  dmTask_MDB.Exec;

  FreeAndNil(dmTask_MDB);

end;



end.
