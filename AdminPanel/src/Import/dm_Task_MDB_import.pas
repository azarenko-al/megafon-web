unit dm_Task_MDB_import;

interface

uses
  dm_MDB,
  dm_Main,

  u_db,
  u_const_db,

  u_func,

  Forms, Variants, System.Generics.Collections, Dialogs,
  System.SysUtils, System.Classes, Data.DB, Data.Win.ADODB;

type
  TID_storage = class(TDictionary<string, integer>)
  public
    procedure Set_DB_ID(aTableName: string; aID, aDB_ID: Integer);
    function Get_DB_ID(aTableName: string; aID: Integer): Integer;
  end;



  TdmTask_MDB = class(TDataModule)
    ADOTable_MDB: TADOTable;
    ADOTable_SQL: TADOTable;
    procedure DataModuleCreate(Sender: TObject);
  private
    FOnProgress: TOnProgressEvent;
    FID_storage :  TID_storage; //TDictionary<integer, TConfig_Clutter>; //(<Config_Clutter>);

    { Private declarations }
  public
    Params: record
       MDB_FileName : string;

    end;

    procedure CopyTable(aTableName, aKeyColumnName: string; aParentTableName:
        string = ''; aParentColumnName: string = '');
    procedure Exec;

    class procedure Init;

    property OnProgress: TOnProgressEvent read FOnProgress write FOnProgress;
  end;

  //  TOnProgressEvent = procedure (aProgress, aMax: Integer) of object;


var
  dmTask_MDB: TdmTask_MDB;

implementation


{%CLASSGROUP 'Vcl.Controls.TControl'}

{$R *.dfm}

procedure TdmTask_MDB.DataModuleCreate(Sender: TObject);
begin
   FID_storage:=TID_storage.create; //TDictionary<integer, TConfig_Clutter>; //(<Config_Clutter>);

end;

//-------------------------------------------------------------------
procedure TdmTask_MDB.CopyTable(aTableName, aKeyColumnName: string;
    aParentTableName: string = ''; aParentColumnName: string = '');
//-------------------------------------------------------------------
var
  db_id: Integer;
  id: Integer;
  iDB_ID: Integer;
  iID: Integer;
  iParentID: Integer;
  s: string;
 // s: string;
  vKey: Variant;
label
  while_exit;

begin
 if not dmMDB.TableExists(aTableName) then
   Exit;


  dmMDB.OpenTable(ADOTable_MDB,  aTableName );
  dmMain.OpenTable(ADOTable_SQL, 'lib.'+aTableName );


  with ADOTable_MDB do
    while not EOF do
  begin
    if Assigned(FOnProgress) then
      FOnProgress (RecordCount, RecNo);
    //-------------------------------------------

    vKey:=FieldValues[aKeyColumnName];

    if aParentColumnName='' then
    begin
      if ADOTable_SQL.Locate(aKeyColumnName, vKey, []) then
      begin
        iDB_ID:=ADOTable_SQL[FLD_ID];
        iID:=FieldValues[FLD_ID];
        FID_storage.Set_DB_ID (aTableName, iID, iDB_ID);

        goto while_exit;

      end;

    end;

    if aParentColumnName<>'' then
    begin
   //   vKey:=FieldValues[aKeyColumnName];

      iParentID:=FieldByName(aParentColumnName).AsInteger;
      assert(iParentID>0);

      iDB_ID:=FID_storage.Get_DB_ID(aParentTableName, iParentID);

      if ADOTable_SQL.Locate(aParentColumnName+';'+aKeyColumnName,
                             VarArrayOf([iDB_ID, vKey]), []) then
        goto while_exit;

    end;


    //-----------------------------------------

    ADOTable_SQL.Append;
    db_CopyRecord_ExceptFields (ADOTable_MDB, ADOTable_SQL, ['id']);

    if aParentTableName<>'' then
    begin
      iParentID:=ADOTable_SQL[aParentColumnName];

      iID:=FID_storage.Get_DB_ID(aParentTableName, iParentID);
//      Inserted_records.TryGetValue(s, iID);
      ADOTable_SQL[aParentColumnName]:=iID;

    end;

    ADOTable_SQL.Post;

    db_id:=ADOTable_SQL['id'];
    id:=FieldValues[FLD_ID];

//    s:=Format('%s:%d ',[aTableName, FieldByName('id').AsInteger]);

    FID_storage.Set_DB_ID(aTableName, id, db_id );



 //   Inserted_records.TryGetValue()
while_exit:
    Next;
  end;

//  procedure db_CopyRecord_ExceptFields(aSrcDataset,aDestDataset: TDataSet;




end;

//-------------------------------------------------------------------
procedure TdmTask_MDB.Exec;
//-------------------------------------------------------------------
var
  i: Integer;
  s: string;
begin
  TdmMDB.Init;

  dmMDB.OpenMDB(Params.MDB_FileName);


  CopyTable (TBL_LinkEndType, FLD_NAME);
  CopyTable (TBL_LinkendType_mode, FLD_MODE, TBL_LinkEndType, 'LinkEndType_id');


  CopyTable (TBL_AntennaType, FLD_NAME);
  CopyTable (TBL_TerminalType, FLD_NAME);

  CopyTable (TBL_ClutterModel, FLD_NAME);
  CopyTable (TBL_ClutterModelType, FLD_NAME);
//  CopyTable (TBL_ClutterModelType, FLD_NAME);



  ShowMessage ('������ ��������.');

//  dmMDB.



  // TODO -cMM: TdmTask_MDB.Exec default body inserted
end;


//------------------------------------------------------------------------------
class procedure TdmTask_MDB.Init;
//------------------------------------------------------------------------------
begin
  if not Assigned(dmTask_MDB) then
    Application.CreateForm(TdmTask_MDB, dmTask_MDB);
end;



function TID_storage.Get_DB_ID(aTableName: string; aID: Integer): Integer;
begin
  TryGetValue(Format('%s:%d ',[aTableName, aID]), Result);
end;


procedure TID_storage.Set_DB_ID(aTableName: string; aID, aDB_ID: Integer);
begin
  Add(Format('%s:%d ',[aTableName, aID]), aDB_ID );

end;



end.

{

function TInserted_records.GetID(NewParam: Integer): Integer;
begin
  // TODO -cMM: TInserted_records.GetID default body inserted
end;


procedure TWordDict.SaveToStream(stream: TStream);
var
  posi: System.Generics.Collections.TPair<string, TFilePos>;
  i: Integer;
  pair: System.Generics.Collections.TPair<string, TFileDict>;
  writer: TWriter;
begin
  writer := TWriter.Create(stream, 4096);
  try
    writer.WriteListBegin;
    for pair in Self do
    begin
      writer.WriteString(pair.Key);
      writer.WriteListBegin;
      for posi in pair.Value do
      begin
        writer.WriteString(posi.Key);
        writer.WriteInteger(Length(posi.Value));
        for i in posi.Value do
        begin
          writer.WriteInteger(i);
        end;
      end;
      writer.WriteListEnd;
    end;
    writer.WriteListEnd;
  finally
    writer.Free;
  end;
end;
