object frame_LinkEndType_Mode: Tframe_LinkEndType_Mode
  Left = 391
  Top = 824
  Caption = 'frame_LinkEndType_Mode'
  ClientHeight = 397
  ClientWidth = 1422
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = True
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  PixelsPerInch = 96
  TextHeight = 13
  object cxGrid1: TcxGrid
    Left = 0
    Top = 0
    Width = 1422
    Height = 201
    Align = alTop
    TabOrder = 1
    LookAndFeel.Kind = lfFlat
    ExplicitWidth = 1508
    object cxGrid1DBBandedTableView1: TcxGridDBBandedTableView
      Navigator.Buttons.CustomButtons = <>
      Navigator.Visible = True
      DataController.DataSource = ds_Mode
      DataController.Filter.MaxValueListCount = 1000
      DataController.KeyFieldNames = 'id'
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <>
      DataController.Summary.SummaryGroups = <>
      OptionsBehavior.ImmediateEditor = False
      OptionsCustomize.ColumnsQuickCustomization = True
      OptionsCustomize.BandHiding = True
      OptionsCustomize.BandsQuickCustomization = True
      OptionsSelection.MultiSelect = True
      OptionsView.GroupByBox = False
      OptionsView.GroupFooters = gfVisibleWhenExpanded
      OptionsView.HeaderAutoHeight = True
      OptionsView.Indicator = True
      OptionsView.FixedBandSeparatorWidth = 1
      Preview.AutoHeight = False
      Preview.MaxLineCount = 2
      Styles.Background = cxStyle_ReadOnly
      Bands = <
        item
        end
        item
          Caption = #1064#1080#1088#1080#1085#1072' '#1087#1086#1083#1086#1089#1099' '#1080#1079#1083#1091#1095#1077#1085#1080#1103' [MHz] '#1085#1072' '#1091#1088#1086#1074#1085#1077':'
          Width = 160
        end
        item
          Caption = #1057#1080#1075#1085#1072#1090#1091#1088#1072
        end
        item
          Caption = #1055#1086#1083#1086#1089#1072' '#1087#1088#1086#1087#1091#1089#1082#1072#1085#1080#1103' '#1059#1042#1063' [MHz] '#1085#1072' '#1091#1088#1086#1074#1085#1077':'
        end
        item
          Width = 154
        end>
      object col_ID: TcxGridDBBandedColumn
        DataBinding.FieldName = 'id'
        Visible = False
        Options.Filtering = False
        VisibleForCustomization = False
        Width = 41
        Position.BandIndex = 0
        Position.ColIndex = 0
        Position.RowIndex = 0
      end
      object col__Mode: TcxGridDBBandedColumn
        Caption = #1056#1077#1078#1080#1084
        DataBinding.FieldName = 'Mode'
        Options.Filtering = False
        Width = 43
        Position.BandIndex = 0
        Position.ColIndex = 1
        Position.RowIndex = 0
      end
      object col_Bitrate_Mbps: TcxGridDBBandedColumn
        DataBinding.FieldName = 'Bitrate_Mbps'
        Options.Filtering = False
        Width = 80
        Position.BandIndex = 0
        Position.ColIndex = 2
        Position.RowIndex = 0
      end
      object col__bandwidth: TcxGridDBBandedColumn
        DataBinding.FieldName = 'bandwidth'
        Options.Filtering = False
        Width = 81
        Position.BandIndex = 0
        Position.ColIndex = 3
        Position.RowIndex = 0
      end
      object col__Modulation_type: TcxGridDBBandedColumn
        DataBinding.FieldName = 'Modulation_type'
        PropertiesClassName = 'TcxLookupComboBoxProperties'
        Properties.DropDownListStyle = lsEditList
        Properties.KeyFieldNames = 'name'
        Properties.ListColumns = <
          item
            FieldName = 'name'
          end>
        Properties.ListOptions.ShowHeader = False
        Options.Filtering = False
        Width = 107
        Position.BandIndex = 0
        Position.ColIndex = 4
        Position.RowIndex = 0
      end
      object col__traffic_type: TcxGridDBBandedColumn
        Caption = #1058#1080#1087' '#1090#1088#1072#1092#1080#1082#1072
        DataBinding.FieldName = 'traffic_type'
        Options.Filtering = False
        Width = 86
        Position.BandIndex = 0
        Position.ColIndex = 5
        Position.RowIndex = 0
      end
      object col__radiation_class: TcxGridDBBandedColumn
        Caption = #1050#1083#1072#1089#1089' '#1080#1079#1083#1091#1095#1077#1085#1080#1103
        DataBinding.FieldName = 'radiation_class'
        PropertiesClassName = 'TcxLookupComboBoxProperties'
        Properties.DropDownListStyle = lsEditList
        Properties.KeyFieldNames = 'name'
        Properties.ListColumns = <
          item
            FieldName = 'name'
          end>
        Properties.ListOptions.ShowHeader = False
        Options.Filtering = False
        Width = 95
        Position.BandIndex = 0
        Position.ColIndex = 6
        Position.RowIndex = 0
      end
      object col__Power_max: TcxGridDBBandedColumn
        DataBinding.FieldName = 'Power_max'
        Options.Filtering = False
        Width = 88
        Position.BandIndex = 0
        Position.ColIndex = 7
        Position.RowIndex = 0
      end
      object col__power_noise: TcxGridDBBandedColumn
        DataBinding.FieldName = 'power_noise'
        Options.Filtering = False
        Width = 66
        Position.BandIndex = 0
        Position.ColIndex = 8
        Position.RowIndex = 0
      end
      object col__bandwidth_tx_3: TcxGridDBBandedColumn
        Caption = '-3 [dB]'
        DataBinding.FieldName = 'bandwidth_tx_3'
        Options.Filtering = False
        Width = 48
        Position.BandIndex = 1
        Position.ColIndex = 0
        Position.RowIndex = 0
      end
      object col__bandwidth_tx_30: TcxGridDBBandedColumn
        Caption = '-30 [dB]'
        DataBinding.FieldName = 'bandwidth_tx_30'
        Options.Filtering = False
        Width = 44
        Position.BandIndex = 1
        Position.ColIndex = 1
        Position.RowIndex = 0
      end
      object col__bandwidth_tx_a: TcxGridDBBandedColumn
        Caption = 'a [dB]'
        DataBinding.FieldName = 'bandwidth_tx_a'
        Options.Filtering = False
        Width = 35
        Position.BandIndex = 1
        Position.ColIndex = 2
        Position.RowIndex = 0
      end
      object col__threshold_BER_3: TcxGridDBBandedColumn
        DataBinding.FieldName = 'threshold_BER_3'
        Options.Filtering = False
        Width = 101
        Position.BandIndex = 0
        Position.ColIndex = 9
        Position.RowIndex = 0
      end
      object col__threshold_BER_6: TcxGridDBBandedColumn
        DataBinding.FieldName = 'threshold_BER_6'
        Options.Filtering = False
        Width = 126
        Position.BandIndex = 0
        Position.ColIndex = 10
        Position.RowIndex = 0
      end
      object col__bandwidth_rx_3: TcxGridDBBandedColumn
        Caption = '-3 [dB]'
        DataBinding.FieldName = 'bandwidth_rx_3'
        Options.Filtering = False
        Width = 37
        Position.BandIndex = 3
        Position.ColIndex = 0
        Position.RowIndex = 0
      end
      object col__bandwidth_rx_30: TcxGridDBBandedColumn
        Caption = '-30 [dB]'
        DataBinding.FieldName = 'bandwidth_rx_30'
        Options.Filtering = False
        Width = 43
        Position.BandIndex = 3
        Position.ColIndex = 1
        Position.RowIndex = 0
      end
      object col__bandwidth_rx_a: TcxGridDBBandedColumn
        Caption = 'a [dB]'
        DataBinding.FieldName = 'bandwidth_rx_a'
        Options.Filtering = False
        Width = 34
        Position.BandIndex = 3
        Position.ColIndex = 2
        Position.RowIndex = 0
      end
      object col__ch_spacing: TcxGridDBBandedColumn
        DataBinding.FieldName = 'ch_spacing'
        Options.Filtering = False
        Width = 61
        Position.BandIndex = 0
        Position.ColIndex = 11
        Position.RowIndex = 0
      end
      object col__signature_width: TcxGridDBBandedColumn
        DataBinding.FieldName = 'signature_width'
        Options.Filtering = False
        Width = 80
        Position.BandIndex = 2
        Position.ColIndex = 0
        Position.RowIndex = 0
      end
      object col__signature_height: TcxGridDBBandedColumn
        DataBinding.FieldName = 'signature_height'
        Options.Filtering = False
        Width = 84
        Position.BandIndex = 2
        Position.ColIndex = 1
        Position.RowIndex = 0
      end
      object cxGrid1DBBandedTableView1Column1: TcxGridDBBandedColumn
        Caption = #1055#1088#1080#1084#1077#1095#1072#1085#1080#1077
        DataBinding.FieldName = 'comment'
        Position.BandIndex = 4
        Position.ColIndex = 0
        Position.RowIndex = 0
      end
    end
    object cxGrid1Level1: TcxGridLevel
      GridView = cxGrid1DBBandedTableView1
    end
  end
  object Panel1: TPanel
    Left = 0
    Top = 356
    Width = 1422
    Height = 41
    Align = alBottom
    Caption = 'Panel1'
    TabOrder = 0
    Visible = False
    ExplicitWidth = 1508
  end
  object ds_Mode: TDataSource
    DataSet = qry_Modes
    Left = 36
    Top = 268
  end
  object qry_Modes: TADOQuery
    BeforeEdit = qry_ModesBeforeEdit
    BeforePost = qry_ModesBeforePost
    OnNewRecord = qry_ModesNewRecord
    Parameters = <>
    SQL.Strings = (
      'SELECT top 100 * FROM    view_LINKENDTYPE_MODE')
    Left = 36
    Top = 212
    object qry_Modesid: TAutoIncField
      FieldName = 'id'
      ReadOnly = True
    end
    object qry_ModesLinkEndType_id: TIntegerField
      FieldName = 'LinkEndType_id'
    end
    object qry_Modesmode: TIntegerField
      FieldName = 'mode'
    end
    object qry_Modesradiation_class: TStringField
      FieldName = 'radiation_class'
      Size = 50
    end
    object qry_Modesbitrate_Mbps: TFloatField
      FieldName = 'bitrate_Mbps'
    end
    object qry_Modesmodulation_type: TStringField
      FieldName = 'modulation_type'
      Size = 50
    end
    object qry_Modesthreshold_BER_3: TFloatField
      FieldName = 'threshold_BER_3'
    end
    object qry_Modesthreshold_BER_6: TFloatField
      FieldName = 'threshold_BER_6'
    end
    object qry_Modespower_max: TFloatField
      FieldName = 'power_max'
    end
    object qry_Modestraffic_type: TStringField
      FieldName = 'traffic_type'
      Size = 50
    end
    object qry_Modessignature_width: TFloatField
      FieldName = 'signature_width'
    end
    object qry_Modessignature_height: TFloatField
      FieldName = 'signature_height'
    end
    object qry_Modesch_spacing: TFloatField
      FieldName = 'ch_spacing'
      MaxValue = 3000.000000000000000000
    end
    object qry_Modesbandwidth: TFloatField
      FieldName = 'bandwidth'
    end
    object qry_Modesbandwidth_tx_3: TFloatField
      FieldName = 'bandwidth_tx_3'
    end
    object qry_Modesbandwidth_tx_30: TFloatField
      FieldName = 'bandwidth_tx_30'
    end
    object qry_Modesbandwidth_tx_a: TFloatField
      FieldName = 'bandwidth_tx_a'
    end
    object qry_Modesbandwidth_rx_3: TFloatField
      FieldName = 'bandwidth_rx_3'
    end
    object qry_Modesbandwidth_rx_30: TFloatField
      FieldName = 'bandwidth_rx_30'
    end
    object qry_Modesbandwidth_rx_a: TFloatField
      FieldName = 'bandwidth_rx_a'
    end
    object qry_Modespower_noise: TFloatField
      FieldName = 'power_noise'
    end
  end
  object cxStyleRepository1: TcxStyleRepository
    Left = 704
    Top = 240
    PixelsPerInch = 96
    object cxStyle_Edit: TcxStyle
    end
    object cxStyle_ReadOnly: TcxStyle
      AssignedValues = [svColor]
      Color = clInactiveBorder
    end
    object cxStyle_row_2: TcxStyle
      AssignedValues = [svColor]
      Color = clInfoBk
    end
  end
  object FormPlacement1: TFormPlacement
    UseRegistry = True
    Left = 128
    Top = 216
  end
end
