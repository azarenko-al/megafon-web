inherited frame_LinkendTYpe_BSHPD: Tframe_LinkendTYpe_BSHPD
  Caption = 'frame_LinkendTYpe_BSHPD'
  ClientHeight = 561
  ClientWidth = 899
  ExplicitWidth = 915
  ExplicitHeight = 600
  PixelsPerInch = 96
  TextHeight = 13
  object cxGrid1: TcxGrid [0]
    Left = 0
    Top = 0
    Width = 449
    Height = 360
    Align = alLeft
    TabOrder = 0
    object cxGrid1DBTableView1: TcxGridDBTableView
      Navigator.Buttons.CustomButtons = <>
      FindPanel.DisplayMode = fpdmManual
      DataController.DataSource = ds_LinkEndType
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <
        item
        end>
      DataController.Summary.SummaryGroups = <>
      OptionsBehavior.ImmediateEditor = False
      OptionsCustomize.ColumnsQuickCustomization = True
      OptionsView.GroupByBox = False
      OptionsView.Indicator = True
      object cxGrid1DBTableView1id: TcxGridDBColumn
        DataBinding.FieldName = 'id'
        Visible = False
        Width = 51
      end
      object cxGrid1DBTableView1name: TcxGridDBColumn
        DataBinding.FieldName = 'name'
        Width = 227
      end
      object cxGrid1DBTableView1band: TcxGridDBColumn
        DataBinding.FieldName = 'band'
        Width = 63
      end
      object cxGrid1DBTableView1power_max: TcxGridDBColumn
        DataBinding.FieldName = 'power_max'
        Visible = False
        Width = 20
      end
      object cxGrid1DBTableView1freq_min: TcxGridDBColumn
        DataBinding.FieldName = 'freq_min'
        Width = 64
      end
      object cxGrid1DBTableView1freq_max: TcxGridDBColumn
        DataBinding.FieldName = 'freq_max'
        Width = 64
      end
    end
    object cxGrid1Level1: TcxGridLevel
      GridView = cxGrid1DBTableView1
    end
  end
  object cxDBVerticalGrid1: TcxDBVerticalGrid [1]
    Left = 696
    Top = 0
    Width = 203
    Height = 360
    Align = alRight
    OptionsView.ScrollBars = ssVertical
    OptionsView.RowHeaderWidth = 139
    Navigator.Buttons.CustomButtons = <>
    TabOrder = 1
    Visible = False
    DataController.DataSource = ds_LinkEndType
    Version = 1
    object cxDBVerticalGrid1id: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'id'
      ID = 0
      ParentID = -1
      Index = 0
      Version = 1
    end
    object cxDBVerticalGrid1name: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'name'
      ID = 1
      ParentID = -1
      Index = 1
      Version = 1
    end
    object cxDBVerticalGrid1folder_id: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'folder_id'
      Visible = False
      ID = 2
      ParentID = -1
      Index = 2
      Version = 1
    end
    object cxDBVerticalGrid1band: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'band'
      Visible = False
      ID = 3
      ParentID = -1
      Index = 3
      Version = 1
    end
    object cxDBVerticalGrid1power_max: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'power_max'
      Visible = False
      ID = 4
      ParentID = -1
      Index = 4
      Version = 1
    end
    object cxDBVerticalGrid1power_loss: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'power_loss'
      Visible = False
      ID = 5
      ParentID = -1
      Index = 5
      Version = 1
    end
    object cxDBVerticalGrid1freq_min: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'freq_min'
      Visible = False
      ID = 6
      ParentID = -1
      Index = 6
      Version = 1
    end
    object cxDBVerticalGrid1freq_max: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'freq_max'
      Visible = False
      ID = 7
      ParentID = -1
      Index = 7
      Version = 1
    end
    object cxDBVerticalGrid1KNG: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'KNG'
      Visible = False
      ID = 8
      ParentID = -1
      Index = 8
      Version = 1
    end
    object cxDBVerticalGrid1failure_period: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'failure_period'
      Visible = False
      ID = 9
      ParentID = -1
      Index = 9
      Version = 1
    end
    object cxDBVerticalGrid1equaliser_profit: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'equaliser_profit'
      Visible = False
      ID = 10
      ParentID = -1
      Index = 10
      Version = 1
    end
    object cxDBVerticalGrid1freq_channel_count: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'freq_channel_count'
      Visible = False
      ID = 11
      ParentID = -1
      Index = 11
      Version = 1
    end
    object cxDBVerticalGrid1kratnostByFreq: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'kratnostByFreq'
      Visible = False
      ID = 12
      ParentID = -1
      Index = 12
      Version = 1
    end
    object cxDBVerticalGrid1kratnostBySpace: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'kratnostBySpace'
      Visible = False
      ID = 13
      ParentID = -1
      Index = 13
      Version = 1
    end
    object cxDBVerticalGrid1allow_use_2_tx_antennas: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'allow_use_2_tx_antennas'
      Visible = False
      ID = 14
      ParentID = -1
      Index = 14
      Version = 1
    end
    object cxDBVerticalGrid1level_tx_a: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'level_tx_a'
      Visible = False
      ID = 15
      ParentID = -1
      Index = 15
      Version = 1
    end
    object cxDBVerticalGrid1level_rx_a: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'level_rx_a'
      Visible = False
      ID = 16
      ParentID = -1
      Index = 16
      Version = 1
    end
    object cxDBVerticalGrid1coeff_noise: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'coeff_noise'
      Visible = False
      ID = 17
      ParentID = -1
      Index = 17
      Version = 1
    end
    object cxDBVerticalGrid1vendor_id: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'vendor_id'
      Visible = False
      ID = 18
      ParentID = -1
      Index = 18
      Version = 1
    end
    object cxDBVerticalGrid1vendor_equipment_id: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'vendor_equipment_id'
      Visible = False
      ID = 19
      ParentID = -1
      Index = 19
      Version = 1
    end
    object cxDBVerticalGrid1comment: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'comment'
      Visible = False
      ID = 20
      ParentID = -1
      Index = 20
      Version = 1
    end
    object cxDBVerticalGrid1guid: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'guid'
      Visible = False
      ID = 21
      ParentID = -1
      Index = 21
      Version = 1
    end
    object cxDBVerticalGrid1date_created: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'date_created'
      Visible = False
      ID = 22
      ParentID = -1
      Index = 22
      Version = 1
    end
    object cxDBVerticalGrid1date_modify: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'date_modify'
      Visible = False
      ID = 23
      ParentID = -1
      Index = 23
      Version = 1
    end
    object cxDBVerticalGrid1user_created: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'user_created'
      Visible = False
      ID = 24
      ParentID = -1
      Index = 24
      Version = 1
    end
    object cxDBVerticalGrid1user_modify: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'user_modify'
      Visible = False
      ID = 25
      ParentID = -1
      Index = 25
      Version = 1
    end
  end
  object cxSplitter1: TcxSplitter [2]
    Left = 688
    Top = 0
    Width = 8
    Height = 360
    HotZoneClassName = 'TcxSimpleStyle'
    AlignSplitter = salRight
    Control = cxDBVerticalGrid1
    Visible = False
  end
  object cxSplitter2: TcxSplitter [3]
    Left = 0
    Top = 360
    Width = 899
    Height = 8
    HotZoneClassName = 'TcxSimpleStyle'
    AlignSplitter = salBottom
    Control = PageControl1
    Visible = False
  end
  object PageControl1: TPageControl [4]
    Left = 0
    Top = 368
    Width = 899
    Height = 193
    ActivePage = TabSheet1
    Align = alBottom
    TabOrder = 4
    Visible = False
    object TabSheet1: TTabSheet
      Caption = 'TabSheet1'
    end
  end
  inherited dxBarManager1: TdxBarManager
    Left = 564
    Top = 184
    PixelsPerInch = 96
  end
  inherited FormStorage1: TFormStorage
    Left = 564
    Top = 240
  end
  object t_LinkEndType: TADOTable
    Connection = dmMain.ADOConnection1
    CursorType = ctStatic
    TableName = 'lib.LinkEndType'
    Left = 568
    Top = 24
  end
  object ds_LinkEndType: TDataSource
    DataSet = t_LinkEndType
    Left = 568
    Top = 80
  end
end
