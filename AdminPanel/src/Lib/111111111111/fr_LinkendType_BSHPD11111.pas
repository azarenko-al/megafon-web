unit fr_LinkendType_BSHPD11111;

interface

uses
  dm_Main,
  fr_View_base,


  u_cx_ini,






  System.Classes,
  Vcl.Controls, Vcl.Forms,


  Data.DB, Data.Win.ADODB, cxGridLevel,
  cxGridCustomTableView, cxGridDBTableView,
  cxGrid, cxDBVGrid, cxSplitter,
  Vcl.ComCtrls, cxGraphics, cxControls, cxLookAndFeels, cxLookAndFeelPainters,
  cxStyles, cxCustomData, cxFilter, cxData, cxDataStorage, cxEdit, cxNavigator,
  dxDateRanges, cxDBData, cxVGrid, cxInplaceContainer, cxGridTableView,
  cxGridCustomView, System.Actions, Vcl.ActnList, RxPlacemnt, cxClasses, dxBar
      ;

type
  Tframe_LinkendTYpe_BSHPD = class(Tframe_View_base)
    cxGrid1DBTableView1: TcxGridDBTableView;
    cxGrid1Level1: TcxGridLevel;
    cxGrid1: TcxGrid;
    t_LinkEndType: TADOTable;
    ds_LinkEndType: TDataSource;
    cxGrid1DBTableView1id: TcxGridDBColumn;
    cxGrid1DBTableView1name: TcxGridDBColumn;
    cxGrid1DBTableView1band: TcxGridDBColumn;
    cxGrid1DBTableView1power_max: TcxGridDBColumn;
    cxGrid1DBTableView1freq_min: TcxGridDBColumn;
    cxGrid1DBTableView1freq_max: TcxGridDBColumn;
    cxDBVerticalGrid1: TcxDBVerticalGrid;
    cxSplitter1: TcxSplitter;
    cxDBVerticalGrid1id: TcxDBEditorRow;
    cxDBVerticalGrid1name: TcxDBEditorRow;
    cxDBVerticalGrid1folder_id: TcxDBEditorRow;
    cxDBVerticalGrid1band: TcxDBEditorRow;
    cxDBVerticalGrid1power_max: TcxDBEditorRow;
    cxDBVerticalGrid1power_loss: TcxDBEditorRow;
    cxDBVerticalGrid1freq_min: TcxDBEditorRow;
    cxDBVerticalGrid1freq_max: TcxDBEditorRow;
    cxDBVerticalGrid1KNG: TcxDBEditorRow;
    cxDBVerticalGrid1failure_period: TcxDBEditorRow;
    cxDBVerticalGrid1equaliser_profit: TcxDBEditorRow;
    cxDBVerticalGrid1freq_channel_count: TcxDBEditorRow;
    cxDBVerticalGrid1kratnostByFreq: TcxDBEditorRow;
    cxDBVerticalGrid1kratnostBySpace: TcxDBEditorRow;
    cxDBVerticalGrid1allow_use_2_tx_antennas: TcxDBEditorRow;
    cxDBVerticalGrid1level_tx_a: TcxDBEditorRow;
    cxDBVerticalGrid1level_rx_a: TcxDBEditorRow;
    cxDBVerticalGrid1coeff_noise: TcxDBEditorRow;
    cxDBVerticalGrid1vendor_id: TcxDBEditorRow;
    cxDBVerticalGrid1vendor_equipment_id: TcxDBEditorRow;
    cxDBVerticalGrid1comment: TcxDBEditorRow;
    cxDBVerticalGrid1guid: TcxDBEditorRow;
    cxDBVerticalGrid1date_created: TcxDBEditorRow;
    cxDBVerticalGrid1date_modify: TcxDBEditorRow;
    cxDBVerticalGrid1user_created: TcxDBEditorRow;
    cxDBVerticalGrid1user_modify: TcxDBEditorRow;
    cxSplitter2: TcxSplitter;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    procedure FormCreate(Sender: TObject);
  private

  public

  end;

//var
//  fra_LinkendTYpe: Tfra_LinkendTYpe;

implementation

{$R *.dfm}

procedure Tframe_LinkendTYpe_BSHPD.FormCreate(Sender: TObject);
begin
  SetCaption('������ ���');
  cxGrid1.Align:=alClient;

  dmMain.OpenTable (t_LinkEndType);



//  cxGrid1DBTableView1.DataController.DataSource:=dmData_lib.ds_ClutterModel;


  dmMain.OpenTable(t_LinkEndType);


  cx_LoadColumnCaptions_FromIni(cxGrid1DBTableView1);
  cx_LoadColumnCaptions_FromIni(cxDBVerticalGrid1);


  cxGrid1DBTableView1.Navigator.Visible:=True;

//  exit;
//
//  cx_Grid_SetColumnCaptions(cxGrid1DBTableView1,
//   [
//     'NAME', '��������' ,
//     'band', '��������, GHz' ,
//     'freq_min', ' ������� min, MHz' ,
//     'freq_max', '������� max, MHz'
//
//   ]);

//
//   ��������� - ����������� ���������
//�������� ��������:     ��������/ ������ �������, m/ ��������, W
//
//
//
//��� ������ - ������������
//�������� ��������:  ��������/ ��������, GHz / ������� min, MHz / ������� max, MHz
//
//
//  cx_DBVerticalGrid_SetCaptions(cxDBVerticalGrid1,
//  [
//   'NAME', '��������'
//
////    'iteration_count', '���-�� ��������',
////    'iteration_antenna_h','������.. ��������',
////    'dir_Data',   '������� � �������',
////    'dir_Relief', '������� � ��������� �����',
////
////    'clutter_model_id',    '������ ����� ��������� (�� ���������)',
////    'link_calc_method_id', '�������� ������� (�� ���������)',
////    'equipment_id',        '������ ������������ (�� ���������)',
////
////    'rest_service_http', 'HTTP �������',
////
////    'IMAGE_map_WIDTH',  '����� : ������',
////    'IMAGE_map_HEIGHT', '����� : ������',
////    'IMAGE_PROFILE_HEIGHT', '������� : ������',
////    'IMAGE_PROFILE_WIDTH',  '������� : ������'
////
//
//  ]);
//

end;


end.
