unit d_Equipment_Pivot;

interface

uses
  u_cx,
  dm_Main,

  fr_View_base,

  System.SysUtils, System.Classes, Vcl.Controls, Vcl.Forms, Variants, Messages,
  Data.DB, Data.Win.ADODB, cxGridLevel, cxGridCustomTableView, cxGridDBTableView,
  cxGrid, cxSplitter, dxBar, Vcl.ActnList,  cxEdit,
  cxGridTableView, cxGridCustomView,  cxTextEdit,
  cxGridBandedTableView, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, cxStyles, cxCustomData, cxFilter, cxData,
  cxDataStorage, cxNavigator, dxDateRanges, cxDBData, cxDBLookupComboBox,
  System.Actions, RxPlacemnt, cxClasses;

type
  Tdlg_Equipment_Pivot = class(Tframe_View_base)
    dxBarButton1: TdxBarButton;
    ADOStoredProc_House_points: TADOStoredProc;
    act_Refresh: TAction;
    ADOStoredProc1: TADOStoredProc;
    ds_Task_full11111: TDataSource;
    cxGrid_Pivot: TcxGrid;
    cxGrid_PivotBandedTableView1: TcxGridBandedTableView;
    cxGrid_PivotDBTableView1: TcxGridDBTableView;
    cxGrid_PivotDBTableView1bitrate: TcxGridDBColumn;
    cxGrid_PivotDBTableView1DBColumn1: TcxGridDBColumn;
    cxGrid_PivotDBTableView1DBColumn2: TcxGridDBColumn;
    cxGrid_PivotDBTableView1DBColumn3: TcxGridDBColumn;
    cxGrid_PivotDBTableView1DBColumn4: TcxGridDBColumn;
    cxGrid_PivotDBTableView1DBColumn5: TcxGridDBColumn;
    cxGrid_PivotDBTableView1DBColumn6: TcxGridDBColumn;
    cxGrid_PivotDBTableView1DBColumn7: TcxGridDBColumn;
    cxGrid_PivotDBTableView1DBColumn8: TcxGridDBColumn;
    cxGrid_PivotDBTableView1DBColumn9: TcxGridDBColumn;
    cxGrid_PivotDBTableView1DBColumn10: TcxGridDBColumn;
    cxGrid_PivotDBTableView1DBColumn11: TcxGridDBColumn;
    cxGrid_PivotDBTableView1DBColumn12: TcxGridDBColumn;
    cxGrid_PivotDBTableView1DBColumn13: TcxGridDBColumn;
    cxGrid_PivotDBTableView1DBColumn15: TcxGridDBColumn;
    cxGrid_PivotDBTableView1DBColumn17: TcxGridDBColumn;
    cxGrid_PivotDBTableView1DBColumn20: TcxGridDBColumn;
    cxGrid_PivotDBTableView1DBColumn21: TcxGridDBColumn;
    cxGrid_PivotDBTableView1DBColumn22: TcxGridDBColumn;
    cxGrid_PivotDBTableView1DBColumn25: TcxGridDBColumn;
    cxGrid_PivotDBTableView1DBColumn30: TcxGridDBColumn;
    cxGrid_PivotDBTableView1DBColumn35: TcxGridDBColumn;
    cxGrid_PivotLevel1: TcxGridLevel;
    t_Equipment_bitrate_distance_technology_xref: TADOTable;
    ds_t_Equipment_bitrate_distance_technology_xref: TDataSource;
    cxGrid2: TcxGrid;
    cxGridBandedTableView1: TcxGridBandedTableView;
    cxGridDBTableView1: TcxGridDBTableView;
    cxGridLevel1: TcxGridLevel;
    t_Equipment: TADOTable;
    ds_Equipment: TDataSource;
    cxGridDBTableView1id: TcxGridDBColumn;
    cxGridDBTableView1bitrate: TcxGridDBColumn;
    cxGridDBTableView1distance_km: TcxGridDBColumn;
    cxGridDBTableView1technology: TcxGridDBColumn;
    cxGridDBTableView1Equipment_id: TcxGridDBColumn;
    cxGridDBTableView1priority: TcxGridDBColumn;
    cxGridDBTableView1tech: TcxGridDBColumn;
    cxSplitter1: TcxSplitter;

//    procedure act_RefreshExecute(Sender: TObject);
    procedure FormCreate(Sender: TObject);

  private
    procedure RefreshData;

  public
    class procedure ExecDlg;
//    procedure NewMethod;

  end;

//var
//  dlg_Equipment_Pivot: Tdlg_Equipment_Pivot;


implementation

{$R *.dfm}


//-------------------------------------------------------------------
class procedure Tdlg_Equipment_Pivot.ExecDlg;
//-------------------------------------------------------------------
begin
  with Tdlg_Equipment_Pivot.Create(Application) do
  begin
 //   dmMain.OpenQuery(q_Task, 'select  * from megagis_client.view_Task where guid=:guid',
//      ['guid', aGUID]);
 //   FTask:=aTask;


 //   q_Task_places.Open;
  //  q_Relief.Open;

  //  ed_Connection.Text:= aConnectionString;

    ShowModal;
    Free;
  end;
end;



//-------------------------------------------------------------------
procedure Tdlg_Equipment_Pivot.FormCreate(Sender: TObject);
//-------------------------------------------------------------------

begin

  Caption:='������ ������������';
//  cxGrid1.Align:=alClient;

  Resize;
 // dmMain.OpenTable(t_queue);


  cxGrid2.Align:=alClient;

//  s:= FormStorage1.StoredProps.Text;

 // dmMain.OpenQuery(ADOQuery_Relief);


 //  Tframe_WebBrowser.Create_(TabSheet_Map);

 // cx_LoadColumnCaptions_FromIni(cxGrid1DBTableView_tasks);


 // cxGrid1DBTableView1.Navigator.Visible:=True;

  cx_Footer(cxGrid_PivotDBTableView1,   cxGrid_PivotDBTableView1bitrate);
  cx_Footer(cxGridDBTableView1,  cxGridDBTableView1bitrate);

  RefreshData;

end;



//-------------------------------------------------------------------
procedure Tdlg_Equipment_Pivot.RefreshData;
//-------------------------------------------------------------------
var
  I: Integer;
begin
  dmMain.OpenTable(t_Equipment);
  dmMain.OpenTable(t_Equipment_bitrate_distance_technology_xref);

  dmMain.StoredProc_Open(ADOStoredProc1, 'lib.sp_Equipment_Pivot', []);


  cxGrid_PivotDBTableView1.BeginUpdate();

  cxGrid_PivotDBTableView1.Columns[0].Width:=50;

  while cxGrid_PivotDBTableView1.ColumnCount>1 do
    cxGrid_PivotDBTableView1.Columns[1].Free;

  cxGrid_PivotDBTableView1.DataController.CreateAllItems(true);


  for I := 1 to cxGrid_PivotDBTableView1.ColumnCount-1 do
  begin
    cxGrid_PivotDBTableView1.Columns[i].Width:=70;
    cxGrid_PivotDBTableView1.Columns[i].PropertiesClass:= TcxTextEditProperties;

    cxGrid_PivotDBTableView1.Columns[i].Caption:=
      Format('<= %s km',[cxGrid_PivotDBTableView1.Columns[i].DataBinding.FieldName]);

    with TcxTextEditProperties(cxGrid_PivotDBTableView1.Columns[i].Properties) do
      Alignment.Vert := taTopJustify;

    //TextEdit

  end;

  cxGrid_PivotDBTableView1.EndUpdate;


//  cxGrid1DBTableView1.OptionsView. ColumnAutoWidth = false;
//  cxGrid1DBTableView1.BestFitColumns();


//To disable auditing at the SQL Server:

  //cxGrid1DBTableView1DBColumn3.hea

end;



end.



{

//-------------------------------------------------------------------
procedure Tframe_Task.t_TaskAfterScroll(DataSet: TDataSet);
//-------------------------------------------------------------------
var
  sDir: string;
  sGuid: string;
begin
{
  sGuid := t_Task.FieldByName('guid').AsString;
  sDir:= g_Config.GetRequestDir_ByGUID (sGuid);

  if FileExists(sDir + 'profile.png') then
    Image_profile.Picture.LoadFromFile(sDir + 'profile.png')
  else
    Image_profile.Picture := nil;


  if FileExists(sDir + 'map.png') then
    Image_Map.Picture.LoadFromFile(sDir + 'map.png')
  else
    Image_Map.Picture := nil;
 }


end;



procedure Tdlg_Equipment_Pivot.act_RefreshExecute(Sender: TObject);
begin
 // t_queue.Requery([]);

end;
