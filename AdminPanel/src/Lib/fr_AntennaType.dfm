inherited frame_AntennaTYpe: Tframe_AntennaTYpe
  Caption = 'frame_AntennaTYpe'
  ClientHeight = 398
  ClientWidth = 1131
  ExplicitWidth = 1147
  ExplicitHeight = 437
  PixelsPerInch = 96
  TextHeight = 13
  object cxGrid1: TcxGrid [0]
    Left = 0
    Top = 30
    Width = 593
    Height = 368
    Align = alLeft
    TabOrder = 0
    object cxGrid1DBTableView1: TcxGridDBTableView
      Navigator.Buttons.CustomButtons = <>
      Navigator.Visible = True
      DataController.DataSource = ds_AntennaType
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <>
      DataController.Summary.SummaryGroups = <>
      OptionsBehavior.ImmediateEditor = False
      OptionsView.GroupByBox = False
      OptionsView.Indicator = True
      object cxGrid1DBTableView1id: TcxGridDBColumn
        DataBinding.FieldName = 'id'
        Visible = False
        Width = 88
      end
      object col_name: TcxGridDBColumn
        DataBinding.FieldName = 'name'
        SortIndex = 0
        SortOrder = soAscending
        Width = 169
      end
      object cxGrid1DBTableView1gain: TcxGridDBColumn
        DataBinding.FieldName = 'gain'
      end
      object cxGrid1DBTableView1freq: TcxGridDBColumn
        DataBinding.FieldName = 'freq'
      end
      object cxGrid1DBTableView1diameter: TcxGridDBColumn
        DataBinding.FieldName = 'diameter'
      end
      object cxGrid1DBTableView1vert_width: TcxGridDBColumn
        DataBinding.FieldName = 'vert_width'
      end
      object cxGrid1DBTableView1horz_width: TcxGridDBColumn
        DataBinding.FieldName = 'horz_width'
      end
      object cxGrid1DBTableView1polarization_str: TcxGridDBColumn
        DataBinding.FieldName = 'polarization_str'
        Visible = False
      end
      object cxGrid1DBTableView1band: TcxGridDBColumn
        DataBinding.FieldName = 'band'
        Width = 80
      end
    end
    object cxGrid1Level1: TcxGridLevel
      GridView = cxGrid1DBTableView1
    end
  end
  object cxDBVerticalGrid1: TcxDBVerticalGrid [1]
    Left = 826
    Top = 30
    Width = 305
    Height = 368
    Align = alRight
    OptionsView.ScrollBars = ssVertical
    OptionsView.RowHeaderWidth = 115
    Navigator.Buttons.CustomButtons = <>
    TabOrder = 1
    Visible = False
    DataController.DataSource = ds_AntennaType
    Version = 1
    object cxDBVerticalGrid1id: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'id'
      ID = 0
      ParentID = -1
      Index = 0
      Version = 1
    end
    object cxDBVerticalGrid1name: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'name'
      ID = 1
      ParentID = -1
      Index = 1
      Version = 1
    end
    object cxDBVerticalGrid1gain: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'gain'
      ID = 2
      ParentID = -1
      Index = 2
      Version = 1
    end
    object cxDBVerticalGrid1freq: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'freq'
      ID = 3
      ParentID = -1
      Index = 3
      Version = 1
    end
    object cxDBVerticalGrid1diameter: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'diameter'
      ID = 4
      ParentID = -1
      Index = 4
      Version = 1
    end
    object cxDBVerticalGrid1omni: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'omni'
      Visible = False
      ID = 5
      ParentID = -1
      Index = 5
      Version = 1
    end
    object cxDBVerticalGrid1vert_width: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'vert_width'
      Visible = False
      ID = 6
      ParentID = -1
      Index = 6
      Version = 1
    end
    object cxDBVerticalGrid1horz_width: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'horz_width'
      Visible = False
      ID = 7
      ParentID = -1
      Index = 7
      Version = 1
    end
    object cxDBVerticalGrid1fronttobackratio: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'fronttobackratio'
      Visible = False
      ID = 8
      ParentID = -1
      Index = 8
      Version = 1
    end
    object cxDBVerticalGrid1polarization_ratio: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'polarization_ratio'
      Visible = False
      ID = 9
      ParentID = -1
      Index = 9
      Version = 1
    end
    object cxDBVerticalGrid1polarization_str: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'polarization_str'
      Visible = False
      ID = 10
      ParentID = -1
      Index = 10
      Version = 1
    end
    object cxDBVerticalGrid1weight: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'weight'
      Visible = False
      ID = 11
      ParentID = -1
      Index = 11
      Version = 1
    end
    object cxDBVerticalGrid1band: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'band'
      Visible = False
      ID = 12
      ParentID = -1
      Index = 12
      Version = 1
    end
    object cxDBVerticalGrid1electrical_tilt: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'electrical_tilt'
      Visible = False
      ID = 13
      ParentID = -1
      Index = 13
      Version = 1
    end
    object cxDBVerticalGrid1comment: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'comment'
      Visible = False
      ID = 14
      ParentID = -1
      Index = 14
      Version = 1
    end
    object cxDBVerticalGrid1guid: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'guid'
      Visible = False
      ID = 15
      ParentID = -1
      Index = 15
      Version = 1
    end
    object cxDBVerticalGrid1vendor_id: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'vendor_id'
      Visible = False
      ID = 16
      ParentID = -1
      Index = 16
      Version = 1
    end
    object cxDBVerticalGrid1band_id: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'band_id'
      Visible = False
      ID = 17
      ParentID = -1
      Index = 17
      Version = 1
    end
  end
  object cxSplitter1: TcxSplitter [2]
    Left = 818
    Top = 30
    Width = 8
    Height = 368
    HotZoneClassName = 'TcxSimpleStyle'
    AlignSplitter = salRight
    Control = cxDBVerticalGrid1
    Visible = False
  end
  inherited dxBarManager1: TdxBarManager
    Left = 692
    Top = 160
    PixelsPerInch = 96
    DockControlHeights = (
      0
      0
      30
      0)
    inherited dxBarManager1Bar1: TdxBar
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarButton1'
        end>
      Visible = True
    end
    object dxBarButton1: TdxBarButton
      Action = act_Refresh
      Category = 0
    end
    object dxBarButton2: TdxBarButton
      Caption = 'New Item'
      Category = 0
      Hint = 'New Item'
      Visible = ivAlways
    end
    object dxBarButton3: TdxBarButton
      Caption = 'New Item'
      Category = 0
      Hint = 'New Item'
      Visible = ivAlways
    end
    object dxBarContainerItem1: TdxBarContainerItem
      Caption = 'New Item'
      Category = 0
      Visible = ivAlways
      ItemLinks = <>
    end
  end
  inherited FormStorage1: TFormStorage
    StoredProps.Strings = (
      'cxDBVerticalGrid1.Width')
    Left = 692
    Top = 224
  end
  inherited ActionList_base: TActionList
    Left = 696
    Top = 296
    object act_Import_MDB: TAction
      Caption = #1048#1084#1087#1086#1088#1090' '#1080#1079' MDB'
    end
    object act_Refresh: TAction
      Caption = 'Refresh'
      OnExecute = act_RefreshExecute
    end
  end
  object t_AntennaType: TADOTable
    Connection = dmMain.ADOConnection1
    CursorType = ctStatic
    TableName = 'lib.AntennaType'
    Left = 688
    Top = 24
  end
  object ds_AntennaType: TDataSource
    DataSet = t_AntennaType
    Left = 688
    Top = 80
  end
end
