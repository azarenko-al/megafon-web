﻿unit fr_AntennaType;

interface

uses
  dm_Main,

  u_cx_ini,

  u_cx,
  fr_View_base,

  System.Classes,
  Vcl.Controls, Vcl.Forms,


  Data.DB, Data.Win.ADODB, cxGridLevel,
  cxGridCustomTableView, cxGridDBTableView,
  cxGrid, cxDBVGrid, cxSplitter, dxBar,



  Vcl.ActnList, cxGraphics, cxControls, cxLookAndFeels, cxLookAndFeelPainters,
  cxStyles, cxCustomData, cxFilter, cxData, cxDataStorage, cxEdit, cxNavigator,
  dxDateRanges, cxDBData, System.Actions, cxVGrid, cxInplaceContainer,
  cxGridTableView, cxGridCustomView, RxPlacemnt, cxClasses;

type
  Tframe_AntennaTYpe = class(Tframe_View_base)
    cxGrid1DBTableView1: TcxGridDBTableView;
    cxGrid1Level1: TcxGridLevel;
    cxGrid1: TcxGrid;
    t_AntennaType: TADOTable;
    ds_AntennaType: TDataSource;
    cxGrid1DBTableView1id: TcxGridDBColumn;
    col_name: TcxGridDBColumn;
    cxGrid1DBTableView1gain: TcxGridDBColumn;
    cxGrid1DBTableView1freq: TcxGridDBColumn;
    cxGrid1DBTableView1diameter: TcxGridDBColumn;
    cxGrid1DBTableView1vert_width: TcxGridDBColumn;
    cxGrid1DBTableView1horz_width: TcxGridDBColumn;
    cxGrid1DBTableView1polarization_str: TcxGridDBColumn;
    cxGrid1DBTableView1band: TcxGridDBColumn;
    cxDBVerticalGrid1: TcxDBVerticalGrid;
    cxSplitter1: TcxSplitter;
    cxDBVerticalGrid1id: TcxDBEditorRow;
    cxDBVerticalGrid1name: TcxDBEditorRow;
    cxDBVerticalGrid1gain: TcxDBEditorRow;
    cxDBVerticalGrid1freq: TcxDBEditorRow;
    cxDBVerticalGrid1diameter: TcxDBEditorRow;
    cxDBVerticalGrid1omni: TcxDBEditorRow;
    cxDBVerticalGrid1vert_width: TcxDBEditorRow;
    cxDBVerticalGrid1horz_width: TcxDBEditorRow;
    cxDBVerticalGrid1fronttobackratio: TcxDBEditorRow;
    cxDBVerticalGrid1polarization_ratio: TcxDBEditorRow;
    cxDBVerticalGrid1polarization_str: TcxDBEditorRow;
    cxDBVerticalGrid1weight: TcxDBEditorRow;
    cxDBVerticalGrid1band: TcxDBEditorRow;
    cxDBVerticalGrid1electrical_tilt: TcxDBEditorRow;
    cxDBVerticalGrid1comment: TcxDBEditorRow;
    cxDBVerticalGrid1guid: TcxDBEditorRow;
    cxDBVerticalGrid1vendor_id: TcxDBEditorRow;
    cxDBVerticalGrid1band_id: TcxDBEditorRow;
    dxBarButton1: TdxBarButton;
    dxBarButton2: TdxBarButton;
    dxBarButton3: TdxBarButton;
    dxBarContainerItem1: TdxBarContainerItem;
    act_Import_MDB: TAction;
    act_Refresh: TAction;
    procedure act_RefreshExecute(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

//var
//  frame_AntennaTYpe: Tframe_AntennaTYpe;

implementation

{$R *.dfm}


procedure Tframe_AntennaTYpe.act_RefreshExecute(Sender: TObject);
begin
  t_AntennaType.Requery([]);
end;


procedure Tframe_AntennaTYpe.FormCreate(Sender: TObject);
begin
  SetCaption('Антенны');

  cxGrid1.Align:=alClient;

  dmMain.OpenTable(t_AntennaType);

  cx_LoadColumnCaptions_FromIni(cxGrid1DBTableView1);

  cxGrid1DBTableView1.Navigator.Visible:=True;

  cx_Footer(cxGrid1DBTableView1, col_name);



end;

end.
