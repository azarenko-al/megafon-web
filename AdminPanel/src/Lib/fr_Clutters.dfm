inherited frame_Clutters: Tframe_Clutters
  Caption = 'frame_Clutters'
  ClientHeight = 778
  ClientWidth = 952
  ExplicitWidth = 968
  ExplicitHeight = 817
  PixelsPerInch = 96
  TextHeight = 13
  object cxGrid1: TcxGrid [0]
    Left = 0
    Top = 0
    Width = 337
    Height = 533
    Align = alLeft
    TabOrder = 0
    object cxGrid1DBTableView1: TcxGridDBTableView
      Navigator.Buttons.CustomButtons = <>
      DataController.DataSource = ds_ClutterModel
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <>
      DataController.Summary.SummaryGroups = <>
      OptionsBehavior.ImmediateEditor = False
      OptionsCustomize.ColumnsQuickCustomization = True
      OptionsView.GroupByBox = False
      object cxGrid1DBTableView1id: TcxGridDBColumn
        DataBinding.FieldName = 'id'
      end
      object cxGrid1DBTableView1name: TcxGridDBColumn
        DataBinding.FieldName = 'name'
      end
    end
    object cxGrid1Level1: TcxGridLevel
      GridView = cxGrid1DBTableView1
    end
  end
  object cxDBVerticalGrid1: TcxDBVerticalGrid [1]
    Left = 681
    Top = 0
    Width = 271
    Height = 533
    Align = alRight
    OptionsView.ScrollBars = ssVertical
    OptionsView.RowHeaderWidth = 115
    Navigator.Buttons.CustomButtons = <>
    TabOrder = 1
    Visible = False
    DataController.DataSource = ds_ClutterModel
    Version = 1
    object cxDBVerticalGrid1id: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'id'
      ID = 0
      ParentID = -1
      Index = 0
      Version = 1
    end
    object cxDBVerticalGrid1name: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'name'
      ID = 1
      ParentID = -1
      Index = 1
      Version = 1
    end
  end
  object cxSplitter1: TcxSplitter [2]
    Left = 673
    Top = 0
    Width = 8
    Height = 533
    HotZoneClassName = 'TcxSimpleStyle'
    AlignSplitter = salRight
    Control = cxDBVerticalGrid1
    Visible = False
  end
  object cxSplitter2: TcxSplitter [3]
    Left = 0
    Top = 533
    Width = 952
    Height = 8
    HotZoneClassName = 'TcxSimpleStyle'
    AlignSplitter = salBottom
    Control = PageControl1
    Visible = False
  end
  object PageControl1: TPageControl [4]
    Left = 0
    Top = 541
    Width = 952
    Height = 237
    ActivePage = TabSheet1
    Align = alBottom
    TabOrder = 4
    object TabSheet1: TTabSheet
      Caption = #1042#1099#1089#1086#1090#1099
      object cxGrid2: TcxGrid
        Left = 0
        Top = 0
        Width = 401
        Height = 209
        Align = alLeft
        TabOrder = 0
        LookAndFeel.Kind = lfFlat
        object cxGridDBTableView1: TcxGridDBTableView
          Navigator.Buttons.CustomButtons = <>
          DataController.DataSource = ds_Clutters
          DataController.Filter.MaxValueListCount = 1000
          DataController.Summary.DefaultGroupSummaryItems = <>
          DataController.Summary.FooterSummaryItems = <>
          DataController.Summary.SummaryGroups = <>
          Filtering.ColumnPopup.MaxDropDownItemCount = 12
          OptionsSelection.HideFocusRectOnExit = False
          OptionsSelection.InvertSelect = False
          OptionsView.GroupByBox = False
          OptionsView.GroupFooters = gfVisibleWhenExpanded
          OptionsView.Indicator = True
          Preview.AutoHeight = False
          Preview.MaxLineCount = 2
          object col__code: TcxGridDBColumn
            DataBinding.FieldName = 'code'
            PropertiesClassName = 'TcxTextEditProperties'
            Properties.Alignment.Horz = taLeftJustify
            Properties.MaxLength = 0
            Properties.ReadOnly = False
            Options.Editing = False
            Options.Filtering = False
            Width = 47
          end
          object col__name: TcxGridDBColumn
            DataBinding.FieldName = 'name'
            PropertiesClassName = 'TcxTextEditProperties'
            Properties.Alignment.Horz = taLeftJustify
            Properties.MaxLength = 0
            Properties.ReadOnly = False
            Options.Editing = False
            Options.Filtering = False
            Width = 149
          end
          object col__height: TcxGridDBColumn
            Caption = ' [m]'
            DataBinding.FieldName = 'height'
            PropertiesClassName = 'TcxSpinEditProperties'
            Properties.Alignment.Horz = taLeftJustify
            Properties.AssignedValues.MaxValue = True
            Properties.AssignedValues.MinValue = True
            Properties.ReadOnly = False
            Options.Filtering = False
            Width = 68
          end
          object col__color: TcxGridDBColumn
            DataBinding.FieldName = 'color'
            PropertiesClassName = 'TcxColorComboBoxProperties'
            Properties.CustomColors = <>
            Properties.ShowDescriptions = False
            Options.Filtering = False
            Width = 48
          end
        end
        object cxGridLevel1: TcxGridLevel
          GridView = cxGridDBTableView1
        end
      end
    end
  end
  inherited dxBarManager1: TdxBarManager
    Left = 404
    Top = 136
    PixelsPerInch = 96
  end
  inherited FormStorage1: TFormStorage
    Left = 404
    Top = 200
  end
  object t_ClutterModel: TADOTable
    Connection = dmMain.ADOConnection1
    CursorType = ctStatic
    TableName = 'lib.ClutterModel'
    Left = 400
    Top = 16
  end
  object ds_ClutterModel: TDataSource
    DataSet = t_ClutterModel
    Left = 400
    Top = 72
  end
  object q_Clutters: TADOQuery
    Connection = dmMain.ADOConnection1
    CursorType = ctStatic
    DataSource = ds_ClutterModel
    Parameters = <
      item
        Name = 'id'
        DataType = ftInteger
        Precision = 10
        Value = 1
      end>
    SQL.Strings = (
      'SELECT  * FROM  lib.view_Clutters  WHERE (clutter_model_id=:id)')
    Left = 508
    Top = 20
  end
  object ds_Clutters: TDataSource
    DataSet = q_Clutters
    Left = 512
    Top = 84
  end
end
