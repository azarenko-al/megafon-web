﻿unit fr_Clutters;

interface

uses
  dm_Main ,
  u_cx_ini,

  fr_View_base,

  System.Classes,
  Vcl.Controls, Vcl.Forms,


  Data.DB, Data.Win.ADODB, cxGridLevel,
  cxGridCustomTableView, cxGridDBTableView,
  cxGrid, cxSplitter, cxDBVGrid,
  Vcl.ComCtrls, cxGraphics, cxControls, cxLookAndFeels, cxLookAndFeelPainters,
  cxStyles, cxCustomData, cxFilter, cxData, cxDataStorage, cxEdit, cxNavigator,
  dxDateRanges, cxDBData, cxTextEdit, cxSpinEdit, cxColorComboBox, cxVGrid,
  cxInplaceContainer, cxGridTableView, cxGridCustomView, System.Actions,
  Vcl.ActnList, RxPlacemnt, cxClasses, dxBar, Vcl.Grids, Vcl.DBGrids;

type
  Tframe_Clutters = class(Tframe_View_base)
    cxGrid1DBTableView1: TcxGridDBTableView;
    cxGrid1Level1: TcxGridLevel;
    cxGrid1: TcxGrid;
    cxGrid1DBTableView1id: TcxGridDBColumn;
    cxGrid1DBTableView1name: TcxGridDBColumn;
    cxDBVerticalGrid1: TcxDBVerticalGrid;
    cxDBVerticalGrid1id: TcxDBEditorRow;
    cxDBVerticalGrid1name: TcxDBEditorRow;
    cxSplitter1: TcxSplitter;
    t_ClutterModel: TADOTable;
    ds_ClutterModel: TDataSource;
    cxSplitter2: TcxSplitter;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    q_Clutters: TADOQuery;
    ds_Clutters: TDataSource;
    cxGrid2: TcxGrid;
    cxGridDBTableView1: TcxGridDBTableView;
    col__code: TcxGridDBColumn;
    col__name: TcxGridDBColumn;
    col__height: TcxGridDBColumn;
    col__color: TcxGridDBColumn;
    cxGridLevel1: TcxGridLevel;
    procedure FormCreate(Sender: TObject);
  private

    { Private declarations }
  public
    { Public declarations }
  end;

//var
//  frame_Clutters: Tframe_Clutters;

implementation

{$R *.dfm}


procedure Tframe_Clutters.FormCreate(Sender: TObject);
begin
  SetCaption('Типы местности');

  cxGrid1.Align:=alClient;


//  cxGrid1DBTableView1.DataController.DataSource:=dmData_lib.ds_ClutterModel;


  dmMain.OpenTable(t_ClutterModel);

  q_Clutters.Open;



//  dmMain.OpenQuery( q_Clutters ,
  //    'SELECT  * FROM  lib.view_Clutters  WHERE (clutter_model_id=:id)');
//      SELECT  * FROM  lib.view_Clutters  WHERE (clutter_model_id=:id)


  cx_LoadColumnCaptions_FromIni(cxGrid1DBTableView1);
  cx_LoadColumnCaptions_FromIni(cxGridDBTableView1);

  cxGrid1DBTableView1.Navigator.Visible:=True;


//  dmMain.OpenTable (t_Task );

//  ADOQuery1.Open;

//  Admin_Panel.view_Task

  {
  cx_Grid_SetColumnCaptions(cxGrid1DBTableView1,
   [
     'NAME', 'Íàçâàíèå'
   ]);
   }

end;





end.

