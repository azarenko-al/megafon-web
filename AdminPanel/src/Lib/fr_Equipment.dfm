inherited frame_Equipment: Tframe_Equipment
  Caption = 'frame_Equipment'
  ClientHeight = 584
  ClientWidth = 1115
  ExplicitWidth = 1131
  ExplicitHeight = 623
  PixelsPerInch = 96
  TextHeight = 13
  object cxGrid1: TcxGrid [0]
    Left = 0
    Top = 30
    Width = 321
    Height = 352
    Align = alLeft
    TabOrder = 0
    object cxGrid1DBTableView1: TcxGridDBTableView
      Navigator.Buttons.CustomButtons = <>
      FindPanel.DisplayMode = fpdmManual
      DataController.DataSource = ds_Equipment
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <
        item
        end>
      DataController.Summary.SummaryGroups = <>
      OptionsCustomize.ColumnsQuickCustomization = True
      OptionsView.GroupByBox = False
      OptionsView.Indicator = True
      object cxGrid1DBTableView1id: TcxGridDBColumn
        DataBinding.FieldName = 'id'
        Visible = False
      end
      object cxGrid1DBTableView1name: TcxGridDBColumn
        DataBinding.FieldName = 'name'
        Width = 148
      end
      object cxGrid1DBTableView1loss: TcxGridDBColumn
        DataBinding.FieldName = 'loss'
        Visible = False
      end
      object cxGrid1DBTableView1antennagain: TcxGridDBColumn
        DataBinding.FieldName = 'antennagain'
        Visible = False
      end
      object cxGrid1DBTableView1height: TcxGridDBColumn
        DataBinding.FieldName = 'height'
        Visible = False
      end
      object cxGrid1DBTableView1power: TcxGridDBColumn
        DataBinding.FieldName = 'power'
        Visible = False
      end
      object cxGrid1DBTableView1sense: TcxGridDBColumn
        DataBinding.FieldName = 'sense'
        Visible = False
      end
      object cxGrid1DBTableView1DNA_width: TcxGridDBColumn
        DataBinding.FieldName = 'DNA_width'
        Visible = False
      end
      object cxGrid1DBTableView1noise: TcxGridDBColumn
        DataBinding.FieldName = 'noise'
        Visible = False
      end
      object cxGrid1DBTableView1suppression: TcxGridDBColumn
        DataBinding.FieldName = 'suppression'
        Visible = False
      end
      object cxGrid1DBTableView1comments: TcxGridDBColumn
        DataBinding.FieldName = 'comments'
        Visible = False
      end
      object cxGrid1DBTableView1NetStandard_id: TcxGridDBColumn
        DataBinding.FieldName = 'NetStandard_id'
        Visible = False
      end
      object cxGrid1DBTableView1SignalNoise: TcxGridDBColumn
        DataBinding.FieldName = 'SignalNoise'
        Visible = False
      end
      object col_LinkEndType_id: TcxGridDBColumn
        DataBinding.FieldName = 'LinkEndType_id'
        PropertiesClassName = 'TcxLookupComboBoxProperties'
        Properties.DropDownRows = 30
        Properties.KeyFieldNames = 'id'
        Properties.ListColumns = <
          item
            FieldName = 'name'
          end>
        Properties.ListSource = ds_LinkEndType
        Width = 118
      end
      object cxGrid1DBTableView1Column1: TcxGridDBColumn
        DataBinding.FieldName = 'AntennaType_id'
        PropertiesClassName = 'TcxLookupComboBoxProperties'
        Properties.DropDownRows = 30
        Properties.KeyFieldNames = 'id'
        Properties.ListColumns = <
          item
            FieldName = 'name'
          end>
        Properties.ListSource = ds_AntennaType
        Width = 143
      end
    end
    object cxGrid1Level1: TcxGridLevel
      GridView = cxGrid1DBTableView1
    end
  end
  object cxDBVerticalGrid1: TcxDBVerticalGrid [1]
    Left = 800
    Top = 30
    Width = 315
    Height = 352
    Align = alRight
    OptionsView.ScrollBars = ssVertical
    OptionsView.RowHeaderWidth = 185
    Navigator.Buttons.CustomButtons = <>
    TabOrder = 1
    DataController.DataSource = ds_Equipment
    Version = 1
    object cxDBVerticalGrid1id: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'id'
      Visible = False
      ID = 0
      ParentID = -1
      Index = 0
      Version = 1
    end
    object cxDBVerticalGrid1name: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'name'
      ID = 1
      ParentID = -1
      Index = 1
      Version = 1
    end
    object cxDBVerticalGrid1LinkEndType_id: TcxDBEditorRow
      Properties.EditPropertiesClassName = 'TcxLookupComboBoxProperties'
      Properties.EditProperties.DropDownRows = 30
      Properties.EditProperties.KeyFieldNames = 'id'
      Properties.EditProperties.ListColumns = <
        item
          FieldName = 'name'
        end>
      Properties.EditProperties.ListSource = ds_LinkEndType
      Properties.DataBinding.FieldName = 'LinkEndType_id'
      ID = 2
      ParentID = -1
      Index = 2
      Version = 1
    end
    object cxDBVerticalGrid1comments: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'comments'
      Visible = False
      ID = 3
      ParentID = -1
      Index = 3
      Version = 1
    end
    object cxDBVerticalGrid1CategoryRow1: TcxCategoryRow
      Visible = False
      ID = 4
      ParentID = -1
      Index = 4
      Version = 1
    end
    object cxDBVerticalGrid1terminal_id: TcxDBEditorRow
      Properties.EditPropertiesClassName = 'TcxLookupComboBoxProperties'
      Properties.EditProperties.ImmediatePost = True
      Properties.EditProperties.KeyFieldNames = 'id'
      Properties.EditProperties.ListColumns = <
        item
          FieldName = 'name'
        end>
      Properties.EditProperties.ListSource = ds_TerminalType
      Properties.EditProperties.OnCloseUp = cxDBVerticalGrid1terminal_idEditPropertiesCloseUp
      Properties.DataBinding.FieldName = 'terminal_id'
      Visible = False
      ID = 5
      ParentID = -1
      Index = 5
      Version = 1
    end
    object cxDBVerticalGrid1terminal_power: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'terminal_power'
      Visible = False
      ID = 6
      ParentID = -1
      Index = 6
      Version = 1
    end
    object cxDBVerticalGrid1terminal_sensitivity: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'terminal_sensitivity'
      Visible = False
      ID = 7
      ParentID = -1
      Index = 7
      Version = 1
    end
    object cxDBVerticalGrid1terminal_loss: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'terminal_loss'
      Visible = False
      ID = 8
      ParentID = -1
      Index = 8
      Version = 1
    end
    object cxDBVerticalGrid1terminal_antenna_heightgain: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'terminal_antenna_gain'
      Visible = False
      ID = 9
      ParentID = -1
      Index = 9
      Version = 1
    end
    object cxDBVerticalGrid1terminal_dna_width: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'terminal_antenna_dna_width'
      Visible = False
      ID = 10
      ParentID = -1
      Index = 10
      Version = 1
    end
    object cxDBVerticalGrid1terminal_antenna_height: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'terminal_antenna_height'
      Visible = False
      ID = 11
      ParentID = -1
      Index = 11
      Version = 1
    end
    object cxDBVerticalGrid1terminal_antenna_gain: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'terminal_antenna_gain'
      Visible = False
      ID = 12
      ParentID = -1
      Index = 12
      Version = 1
    end
    object cxDBVerticalGrid1terminal_antenna_diameter: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'terminal_antenna_diameter'
      Visible = False
      ID = 13
      ParentID = -1
      Index = 13
      Version = 1
    end
    object cxDBVerticalGrid1terminal_antenna_dna_width: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'terminal_antenna_dna_width'
      Visible = False
      ID = 14
      ParentID = -1
      Index = 14
      Version = 1
    end
    object row_AntennaType_id: TcxDBEditorRow
      Properties.EditPropertiesClassName = 'TcxLookupComboBoxProperties'
      Properties.EditProperties.DropDownRows = 30
      Properties.EditProperties.KeyFieldNames = 'id'
      Properties.EditProperties.ListColumns = <
        item
          FieldName = 'name'
        end>
      Properties.EditProperties.ListSource = ds_AntennaType
      Properties.DataBinding.FieldName = 'AntennaType_id'
      ID = 15
      ParentID = -1
      Index = 15
      Version = 1
    end
    object cxDBVerticalGrid1DBEditorRow1: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'LinkEndType_mode_id'
      Visible = False
      ID = 16
      ParentID = -1
      Index = 16
      Version = 1
    end
  end
  object cxSplitter1: TcxSplitter [2]
    Left = 792
    Top = 30
    Width = 8
    Height = 352
    HotZoneClassName = 'TcxSimpleStyle'
    AlignSplitter = salRight
    Control = cxDBVerticalGrid1
  end
  object PageControl1: TPageControl [3]
    Left = 0
    Top = 382
    Width = 1115
    Height = 202
    ActivePage = TabSheet1
    Align = alBottom
    TabOrder = 3
    Visible = False
    object TabSheet1: TTabSheet
      Caption = 'TabSheet1'
      object DBGrid1: TDBGrid
        Left = 0
        Top = 0
        Width = 377
        Height = 174
        Align = alLeft
        DataSource = ds_TerminalType
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'Tahoma'
        TitleFont.Style = []
      end
      object DBGrid2: TDBGrid
        Left = 377
        Top = 0
        Width = 344
        Height = 174
        Align = alLeft
        DataSource = ds_LinkEndType
        TabOrder = 1
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'Tahoma'
        TitleFont.Style = []
      end
    end
  end
  inherited dxBarManager1: TdxBarManager
    Left = 364
    Top = 160
    PixelsPerInch = 96
    DockControlHeights = (
      0
      0
      30
      0)
    inherited dxBarManager1Bar1: TdxBar
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarButton1'
        end>
      Visible = True
    end
    object dxBarButton1: TdxBarButton
      Action = act_Refresh
      Category = 0
    end
  end
  inherited FormStorage1: TFormStorage
    Left = 364
    Top = 208
  end
  inherited ActionList_base: TActionList
    Left = 472
    Top = 160
    object act_Refresh: TAction
      Caption = 'Refresh'
      OnExecute = act_RefreshExecute
    end
  end
  object ds_TerminalType: TDataSource
    DataSet = t_TerminalType
    Left = 480
    Top = 96
  end
  object t_LinkEndType: TADOTable
    Connection = dmMain.ADOConnection1
    CursorType = ctStatic
    TableName = 'lib.LinkEndType'
    Left = 568
    Top = 40
  end
  object ds_LinkEndType: TDataSource
    DataSet = t_LinkEndType
    Left = 568
    Top = 96
  end
  object t_Equipment: TADOTable
    Connection = dmMain.ADOConnection1
    CursorType = ctStatic
    TableName = 'lib.Equipment'
    Left = 368
    Top = 40
  end
  object ds_Equipment: TDataSource
    DataSet = t_Equipment
    Left = 368
    Top = 96
  end
  object t_TerminalType: TADOTable
    Connection = dmMain.ADOConnection1
    CursorType = ctStatic
    TableName = 'lib.TerminalType'
    Left = 480
    Top = 40
  end
  object t_AntennaType: TADOTable
    Connection = dmMain.ADOConnection1
    CursorType = ctStatic
    TableName = 'lib.AntennaType'
    Left = 680
    Top = 40
  end
  object ds_AntennaType: TDataSource
    DataSet = t_AntennaType
    Left = 680
    Top = 88
  end
  object q_LinkEndType_mode: TADOQuery
    Connection = dmMain.ADOConnection1
    CursorType = ctStatic
    DataSource = ds_LinkEndType
    Parameters = <
      item
        Name = 'id'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Value = 9533
      end>
    Prepared = True
    SQL.Strings = (
      'select * from   lib.LinkEndType_mode'
      'where '
      '  LinkEndType_id=:id')
    Left = 680
    Top = 160
  end
  object ds_LinkEndType_mode: TDataSource
    DataSet = q_LinkEndType_mode
    Left = 680
    Top = 208
  end
end
