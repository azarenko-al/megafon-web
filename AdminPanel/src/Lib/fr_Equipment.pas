﻿unit fr_Equipment;

interface

uses
  dm_Main,
  fr_View_base,

  u_cx_ini,


  u_db,
  u_const_db,

  CodeSiteLogging,

  System.Variants, System.Classes,
  Vcl.Controls, Vcl.Forms,


  Data.DB, Data.Win.ADODB, cxGridLevel,
  cxGridCustomTableView, cxGridDBTableView,
  cxGrid, cxVGrid, cxDBVGrid, cxSplitter, Vcl.ComCtrls,
  Vcl.DBGrids,


  Vcl.ActnList,
  dxBar, cxGraphics, cxControls, cxLookAndFeels, cxLookAndFeelPainters,
  cxStyles, cxCustomData, cxFilter, cxData, cxDataStorage, cxEdit, cxNavigator,
  dxDateRanges, cxDBData, cxDBLookupComboBox, System.Actions, Vcl.Grids,
  cxInplaceContainer, cxGridTableView, cxGridCustomView, RxPlacemnt, cxClasses;

type
  Tframe_Equipment = class(Tframe_View_base)
    cxGrid1DBTableView1: TcxGridDBTableView;
    cxGrid1Level1: TcxGridLevel;
    cxGrid1: TcxGrid;
    t_Equipment: TADOTable;
    ds_Equipment: TDataSource;
    cxDBVerticalGrid1: TcxDBVerticalGrid;
    cxSplitter1: TcxSplitter;
    cxGrid1DBTableView1id: TcxGridDBColumn;
    cxGrid1DBTableView1name: TcxGridDBColumn;
    cxGrid1DBTableView1loss: TcxGridDBColumn;
    cxGrid1DBTableView1antennagain: TcxGridDBColumn;
    cxGrid1DBTableView1height: TcxGridDBColumn;
    cxGrid1DBTableView1power: TcxGridDBColumn;
    cxGrid1DBTableView1sense: TcxGridDBColumn;
    cxGrid1DBTableView1DNA_width: TcxGridDBColumn;
    cxGrid1DBTableView1noise: TcxGridDBColumn;
    cxGrid1DBTableView1suppression: TcxGridDBColumn;
    cxGrid1DBTableView1comments: TcxGridDBColumn;
    cxGrid1DBTableView1NetStandard_id: TcxGridDBColumn;
    cxGrid1DBTableView1SignalNoise: TcxGridDBColumn;
    cxDBVerticalGrid1id: TcxDBEditorRow;
    cxDBVerticalGrid1name: TcxDBEditorRow;
    cxDBVerticalGrid1LinkEndType_id: TcxDBEditorRow;
    cxDBVerticalGrid1comments: TcxDBEditorRow;
    cxDBVerticalGrid1terminal_id: TcxDBEditorRow;
    cxDBVerticalGrid1terminal_power: TcxDBEditorRow;
    cxDBVerticalGrid1terminal_sensitivity: TcxDBEditorRow;
    cxDBVerticalGrid1terminal_loss: TcxDBEditorRow;
    cxDBVerticalGrid1terminal_antenna_heightgain: TcxDBEditorRow;
    cxDBVerticalGrid1terminal_dna_width: TcxDBEditorRow;
    t_TerminalType: TADOTable;
    ds_TerminalType: TDataSource;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    t_LinkEndType: TADOTable;
    ds_LinkEndType: TDataSource;
    cxDBVerticalGrid1terminal_antenna_height: TcxDBEditorRow;
    cxDBVerticalGrid1terminal_antenna_gain: TcxDBEditorRow;
    cxDBVerticalGrid1terminal_antenna_diameter: TcxDBEditorRow;
    cxDBVerticalGrid1terminal_antenna_dna_width: TcxDBEditorRow;
    cxDBVerticalGrid1CategoryRow1: TcxCategoryRow;
    DBGrid1: TDBGrid;
    row_AntennaType_id: TcxDBEditorRow;
    t_AntennaType: TADOTable;
    ds_AntennaType: TDataSource;
    q_LinkEndType_mode: TADOQuery;
    ds_LinkEndType_mode: TDataSource;
    DBGrid2: TDBGrid;
    cxGrid1DBTableView1Column1: TcxGridDBColumn;
    cxDBVerticalGrid1DBEditorRow1: TcxDBEditorRow;
    col_LinkEndType_id: TcxGridDBColumn;
    act_Refresh: TAction;
    dxBarButton1: TdxBarButton;
    procedure act_RefreshExecute(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure cxDBVerticalGrid1terminal_idEditPropertiesCloseUp(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

//var
//  fra_TerminalTYpe: Tfra_TerminalTYpe;

implementation

{$R *.dfm}

procedure Tframe_Equipment.act_RefreshExecute(Sender: TObject);
begin
  t_Equipment.Requery([]);
  t_TerminalType.Requery([]);
  t_LinkEndType.Requery([]);

  t_AntennaType.Requery([]);

end;


procedure Tframe_Equipment.cxDBVerticalGrid1terminal_idEditPropertiesCloseUp(Sender: TObject);
begin
  inherited;

//  cxDBVerticalGrid1terminal_id.Properties.EditProperties.

  CodeSite.Send('cxDBVerticalGrid1terminal_idEditPropertiesCloseUp');

//  CodeSite.Send(ADOTable1['terminal_id']);
  CodeSite.Send(cxDBVerticalGrid1terminal_id.Properties.Value);

  if t_TerminalType.Locate(FLD_Id,  cxDBVerticalGrid1terminal_id.Properties.Value, []) then


  db_UpdateRecord(t_Equipment,
    [
      FLD_terminal_height, t_TerminalType[FLD_height],
      FLD_terminal_power,
      FLD_terminal_gain,

      10
    ]);


end;

//-----------------------------------------------------------------
procedure Tframe_Equipment.FormCreate(Sender: TObject);
//-----------------------------------------------------------------
begin
  SetCaption('Шаблоны оборудования');

  cxGrid1.Align:=alClient;

  dmMain.OpenTable (t_Equipment);
  dmMain.OpenTable (t_TerminalType);
  dmMain.OpenTable (t_LinkEndType);

  dmMain.OpenTable (t_AntennaType);

  q_LinkEndType_mode.Open;


//    dmMain.OpenTable(t_TerminalType);

  cx_LoadColumnCaptions_FromIni(cxGrid1DBTableView1);
//  cx_LoadColumnCaptions_FromIni(cxGrid1DBTableView1);

  cxGrid1DBTableView1.Navigator.Visible:=True;


//  cx_LoadColumnCaptions_FromIni(cxGrid1DBTableView1);
  cx_LoadColumnCaptions_FromIni(cxDBVerticalGrid1);

//
//  cx_Grid_SetColumnCaptions(cxGrid1DBTableView1,
//   [
//     'NAME', 'Íàçâàíèå'
//   ]);
//
//
//  cx_Grid_SetColumnCaptions(cxGrid1DBTableView1,
//   [
//     'NAME', 'Íàçâàíèå'
//   ]);


end;

end.


