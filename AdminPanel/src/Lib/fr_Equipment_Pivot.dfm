inherited frame_Equipment_Pivot: Tframe_Equipment_Pivot
  Caption = 'frame_Equipment_Pivot'
  ClientHeight = 639
  ClientWidth = 1146
  ExplicitWidth = 1162
  ExplicitHeight = 678
  PixelsPerInch = 96
  TextHeight = 13
  object cxGrid_Pivot: TcxGrid [0]
    Left = 0
    Top = 0
    Width = 1146
    Height = 233
    Align = alTop
    TabOrder = 0
    object cxGrid_PivotBandedTableView1: TcxGridBandedTableView
      Navigator.Buttons.CustomButtons = <>
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <>
      DataController.Summary.SummaryGroups = <>
      Bands = <
        item
        end>
    end
    object cxGrid_PivotDBTableView1: TcxGridDBTableView
      Navigator.Buttons.CustomButtons = <>
      DataController.DataSource = ds_Task_full11111
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <>
      DataController.Summary.SummaryGroups = <>
      OptionsData.Deleting = False
      OptionsData.Editing = False
      OptionsData.Inserting = False
      OptionsView.CellAutoHeight = True
      OptionsView.GroupByBox = False
      OptionsView.Indicator = True
      object cxGrid_PivotDBTableView1bitrate: TcxGridDBColumn
        DataBinding.FieldName = 'bitrate'
        PropertiesClassName = 'TcxTextEditProperties'
        Properties.Alignment.Horz = taRightJustify
        Properties.Alignment.Vert = taTopJustify
      end
      object cxGrid_PivotDBTableView1DBColumn1: TcxGridDBColumn
        DataBinding.FieldName = '1'
        Width = 68
      end
      object cxGrid_PivotDBTableView1DBColumn2: TcxGridDBColumn
        DataBinding.FieldName = '2'
        Width = 68
      end
      object cxGrid_PivotDBTableView1DBColumn3: TcxGridDBColumn
        DataBinding.FieldName = '3'
        Width = 68
      end
      object cxGrid_PivotDBTableView1DBColumn4: TcxGridDBColumn
        DataBinding.FieldName = '4'
        Width = 68
      end
      object cxGrid_PivotDBTableView1DBColumn5: TcxGridDBColumn
        DataBinding.FieldName = '5'
        Width = 68
      end
      object cxGrid_PivotDBTableView1DBColumn6: TcxGridDBColumn
        DataBinding.FieldName = '6'
        Width = 68
      end
      object cxGrid_PivotDBTableView1DBColumn7: TcxGridDBColumn
        DataBinding.FieldName = '7'
        Width = 68
      end
      object cxGrid_PivotDBTableView1DBColumn8: TcxGridDBColumn
        DataBinding.FieldName = '8'
        Width = 68
      end
      object cxGrid_PivotDBTableView1DBColumn9: TcxGridDBColumn
        DataBinding.FieldName = '9'
        Width = 68
      end
      object cxGrid_PivotDBTableView1DBColumn10: TcxGridDBColumn
        DataBinding.FieldName = '10'
        Width = 68
      end
      object cxGrid_PivotDBTableView1DBColumn11: TcxGridDBColumn
        DataBinding.FieldName = '11'
        Width = 68
      end
      object cxGrid_PivotDBTableView1DBColumn12: TcxGridDBColumn
        DataBinding.FieldName = '12'
        Width = 68
      end
      object cxGrid_PivotDBTableView1DBColumn13: TcxGridDBColumn
        DataBinding.FieldName = '13'
        Width = 68
      end
      object cxGrid_PivotDBTableView1DBColumn15: TcxGridDBColumn
        DataBinding.FieldName = '15'
        Width = 68
      end
      object cxGrid_PivotDBTableView1DBColumn17: TcxGridDBColumn
        DataBinding.FieldName = '17'
        Width = 68
      end
      object cxGrid_PivotDBTableView1DBColumn20: TcxGridDBColumn
        DataBinding.FieldName = '20'
        Width = 68
      end
      object cxGrid_PivotDBTableView1DBColumn21: TcxGridDBColumn
        DataBinding.FieldName = '21'
      end
      object cxGrid_PivotDBTableView1DBColumn22: TcxGridDBColumn
        DataBinding.FieldName = '22'
      end
      object cxGrid_PivotDBTableView1DBColumn25: TcxGridDBColumn
        DataBinding.FieldName = '25'
      end
      object cxGrid_PivotDBTableView1DBColumn30: TcxGridDBColumn
        DataBinding.FieldName = '30'
      end
      object cxGrid_PivotDBTableView1DBColumn35: TcxGridDBColumn
        DataBinding.FieldName = '35'
      end
    end
    object cxGrid_PivotLevel1: TcxGridLevel
      GridView = cxGrid_PivotDBTableView1
    end
  end
  object cxGrid2: TcxGrid [1]
    Left = 608
    Top = 241
    Width = 538
    Height = 398
    Align = alRight
    TabOrder = 1
    object cxGridBandedTableView1: TcxGridBandedTableView
      Navigator.Buttons.CustomButtons = <>
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <>
      DataController.Summary.SummaryGroups = <>
      Bands = <
        item
        end>
    end
    object cxGridDBTableView1: TcxGridDBTableView
      Navigator.Buttons.CustomButtons = <>
      DataController.DataSource = ds_t_Equipment_bitrate_distance_technology_xref
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <>
      DataController.Summary.SummaryGroups = <>
      OptionsData.Deleting = False
      OptionsData.Editing = False
      OptionsData.Inserting = False
      OptionsView.CellAutoHeight = True
      OptionsView.GroupByBox = False
      OptionsView.Indicator = True
      object cxGridDBTableView1id: TcxGridDBColumn
        DataBinding.FieldName = 'id'
        Visible = False
      end
      object cxGridDBTableView1bitrate: TcxGridDBColumn
        DataBinding.FieldName = 'bitrate'
      end
      object cxGridDBTableView1distance_km: TcxGridDBColumn
        DataBinding.FieldName = 'distance_km'
      end
      object cxGridDBTableView1technology: TcxGridDBColumn
        DataBinding.FieldName = 'technology'
        Visible = False
        Width = 82
      end
      object cxGridDBTableView1Equipment_id: TcxGridDBColumn
        DataBinding.FieldName = 'Equipment_id'
        PropertiesClassName = 'TcxLookupComboBoxProperties'
        Properties.KeyFieldNames = 'id'
        Properties.ListColumns = <
          item
            FieldName = 'name'
          end>
        Properties.ListSource = ds_Equipment
        Width = 90
      end
      object cxGridDBTableView1priority: TcxGridDBColumn
        DataBinding.FieldName = 'priority'
      end
      object cxGridDBTableView1tech: TcxGridDBColumn
        DataBinding.FieldName = 'tech'
        Width = 102
      end
      object cxGridDBTableView1Equipment_name: TcxGridDBColumn
        DataBinding.FieldName = 'Equipment_name'
        Width = 100
      end
      object cxGridDBTableView1LinkEndType_name: TcxGridDBColumn
        DataBinding.FieldName = 'LinkEndType_name'
        Width = 98
      end
      object cxGridDBTableView1AntennaType_name: TcxGridDBColumn
        DataBinding.FieldName = 'AntennaType_name'
        Width = 267
      end
      object cxGridDBTableView1freq_max: TcxGridDBColumn
        DataBinding.FieldName = 'freq_max'
      end
      object cxGridDBTableView1freq_min: TcxGridDBColumn
        DataBinding.FieldName = 'freq_min'
      end
      object cxGridDBTableView1MegaGIS_name: TcxGridDBColumn
        DataBinding.FieldName = 'MegaGIS_name'
        Width = 421
      end
    end
    object cxGridLevel1: TcxGridLevel
      GridView = cxGridDBTableView1
    end
  end
  object cxSplitter1: TcxSplitter [2]
    Left = 0
    Top = 233
    Width = 1146
    Height = 8
    HotZoneClassName = 'TcxSimpleStyle'
    AlignSplitter = salTop
    Control = cxGrid_Pivot
  end
  inherited dxBarManager1: TdxBarManager
    Left = 124
    Top = 288
    PixelsPerInch = 96
    object dxBarButton1: TdxBarButton
      Caption = 'New Button'
      Category = 0
      Hint = 'New Button'
      Visible = ivAlways
    end
  end
  inherited FormStorage1: TFormStorage
    Left = 124
    Top = 344
  end
  inherited ActionList_base: TActionList
    Left = 336
    Top = 296
    object act_Refresh: TAction
      Caption = #1054#1073#1085#1086#1074#1080#1090#1100
    end
  end
  object ADOStoredProc_House_points: TADOStoredProc
    ProcedureName = 'map.sp_House_points_By_LatLon'
    Parameters = <>
    Left = 65100
    Top = 384
  end
  object ADOStoredProc1: TADOStoredProc
    Connection = dmMain.ADOConnection1
    CursorType = ctStatic
    LockType = ltReadOnly
    ProcedureName = 'lib.sp_Equipment_Pivot'
    Parameters = <>
    Left = 232
    Top = 296
  end
  object ds_Task_full11111: TDataSource
    DataSet = ADOStoredProc1
    Left = 232
    Top = 352
  end
  object t_Equipment_bitrate_distance_technology_xref: TADOTable
    Connection = dmMain.ADOConnection1
    CursorType = ctStatic
    TableName = 'lib.view_Equipment_bitrate_distance_technology'
    Left = 176
    Top = 472
  end
  object ds_t_Equipment_bitrate_distance_technology_xref: TDataSource
    DataSet = t_Equipment_bitrate_distance_technology_xref
    Left = 176
    Top = 536
  end
  object t_Equipment: TADOTable
    Connection = dmMain.ADOConnection1
    CursorType = ctStatic
    TableName = 'lib.Equipment'
    Left = 408
    Top = 472
  end
  object ds_Equipment: TDataSource
    DataSet = t_Equipment
    Left = 408
    Top = 536
  end
end
