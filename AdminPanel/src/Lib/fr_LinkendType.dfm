inherited frame_LinkendTYpe: Tframe_LinkendTYpe
  Caption = 'frame_LinkendTYpe'
  ClientHeight = 561
  ClientWidth = 1036
  ExplicitWidth = 1052
  ExplicitHeight = 600
  PixelsPerInch = 96
  TextHeight = 13
  object cxGrid1: TcxGrid [0]
    Left = 0
    Top = 30
    Width = 497
    Height = 330
    Align = alLeft
    TabOrder = 0
    object cxGrid1DBTableView1: TcxGridDBTableView
      Navigator.Buttons.CustomButtons = <>
      FindPanel.DisplayMode = fpdmManual
      DataController.DataSource = ds_LinkEndType
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <
        item
        end>
      DataController.Summary.SummaryGroups = <>
      OptionsBehavior.ImmediateEditor = False
      OptionsCustomize.ColumnsQuickCustomization = True
      OptionsView.GroupByBox = False
      OptionsView.Indicator = True
      object cxGrid1DBTableView1id: TcxGridDBColumn
        DataBinding.FieldName = 'id'
        Visible = False
        Width = 51
      end
      object cxGrid1DBTableView1name: TcxGridDBColumn
        DataBinding.FieldName = 'name'
        Width = 252
      end
      object cxGrid1DBTableView1band: TcxGridDBColumn
        DataBinding.FieldName = 'band'
        Width = 63
      end
      object cxGrid1DBTableView1power_max: TcxGridDBColumn
        DataBinding.FieldName = 'power_max'
        Visible = False
        Width = 20
      end
      object cxGrid1DBTableView1freq_min: TcxGridDBColumn
        DataBinding.FieldName = 'freq_min'
        Width = 64
      end
      object cxGrid1DBTableView1freq_max: TcxGridDBColumn
        DataBinding.FieldName = 'freq_max'
        Width = 64
      end
    end
    object cxGrid1Level1: TcxGridLevel
      GridView = cxGrid1DBTableView1
    end
  end
  object cxDBVerticalGrid1: TcxDBVerticalGrid [1]
    Left = 833
    Top = 30
    Width = 203
    Height = 330
    Align = alRight
    OptionsView.ScrollBars = ssVertical
    OptionsView.RowHeaderWidth = 139
    Navigator.Buttons.CustomButtons = <>
    TabOrder = 1
    Visible = False
    DataController.DataSource = ds_LinkEndType
    Version = 1
    object cxDBVerticalGrid1id: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'id'
      ID = 0
      ParentID = -1
      Index = 0
      Version = 1
    end
    object cxDBVerticalGrid1name: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'name'
      ID = 1
      ParentID = -1
      Index = 1
      Version = 1
    end
    object cxDBVerticalGrid1folder_id: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'folder_id'
      Visible = False
      ID = 2
      ParentID = -1
      Index = 2
      Version = 1
    end
    object cxDBVerticalGrid1band: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'band'
      Visible = False
      ID = 3
      ParentID = -1
      Index = 3
      Version = 1
    end
    object cxDBVerticalGrid1power_max: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'power_max'
      Visible = False
      ID = 4
      ParentID = -1
      Index = 4
      Version = 1
    end
    object cxDBVerticalGrid1power_loss: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'power_loss'
      Visible = False
      ID = 5
      ParentID = -1
      Index = 5
      Version = 1
    end
    object cxDBVerticalGrid1freq_min: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'freq_min'
      Visible = False
      ID = 6
      ParentID = -1
      Index = 6
      Version = 1
    end
    object cxDBVerticalGrid1freq_max: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'freq_max'
      Visible = False
      ID = 7
      ParentID = -1
      Index = 7
      Version = 1
    end
    object cxDBVerticalGrid1KNG: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'KNG'
      Visible = False
      ID = 8
      ParentID = -1
      Index = 8
      Version = 1
    end
    object cxDBVerticalGrid1failure_period: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'failure_period'
      Visible = False
      ID = 9
      ParentID = -1
      Index = 9
      Version = 1
    end
    object cxDBVerticalGrid1equaliser_profit: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'equaliser_profit'
      Visible = False
      ID = 10
      ParentID = -1
      Index = 10
      Version = 1
    end
    object cxDBVerticalGrid1freq_channel_count: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'freq_channel_count'
      Visible = False
      ID = 11
      ParentID = -1
      Index = 11
      Version = 1
    end
    object cxDBVerticalGrid1kratnostByFreq: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'kratnostByFreq'
      Visible = False
      ID = 12
      ParentID = -1
      Index = 12
      Version = 1
    end
    object cxDBVerticalGrid1kratnostBySpace: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'kratnostBySpace'
      Visible = False
      ID = 13
      ParentID = -1
      Index = 13
      Version = 1
    end
    object cxDBVerticalGrid1allow_use_2_tx_antennas: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'allow_use_2_tx_antennas'
      Visible = False
      ID = 14
      ParentID = -1
      Index = 14
      Version = 1
    end
    object cxDBVerticalGrid1level_tx_a: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'level_tx_a'
      Visible = False
      ID = 15
      ParentID = -1
      Index = 15
      Version = 1
    end
    object cxDBVerticalGrid1level_rx_a: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'level_rx_a'
      Visible = False
      ID = 16
      ParentID = -1
      Index = 16
      Version = 1
    end
    object cxDBVerticalGrid1coeff_noise: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'coeff_noise'
      Visible = False
      ID = 17
      ParentID = -1
      Index = 17
      Version = 1
    end
    object cxDBVerticalGrid1vendor_id: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'vendor_id'
      Visible = False
      ID = 18
      ParentID = -1
      Index = 18
      Version = 1
    end
    object cxDBVerticalGrid1vendor_equipment_id: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'vendor_equipment_id'
      Visible = False
      ID = 19
      ParentID = -1
      Index = 19
      Version = 1
    end
    object cxDBVerticalGrid1comment: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'comment'
      Visible = False
      ID = 20
      ParentID = -1
      Index = 20
      Version = 1
    end
    object cxDBVerticalGrid1guid: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'guid'
      Visible = False
      ID = 21
      ParentID = -1
      Index = 21
      Version = 1
    end
    object cxDBVerticalGrid1date_created: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'date_created'
      Visible = False
      ID = 22
      ParentID = -1
      Index = 22
      Version = 1
    end
    object cxDBVerticalGrid1date_modify: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'date_modify'
      Visible = False
      ID = 23
      ParentID = -1
      Index = 23
      Version = 1
    end
    object cxDBVerticalGrid1user_created: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'user_created'
      Visible = False
      ID = 24
      ParentID = -1
      Index = 24
      Version = 1
    end
    object cxDBVerticalGrid1user_modify: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'user_modify'
      Visible = False
      ID = 25
      ParentID = -1
      Index = 25
      Version = 1
    end
  end
  object cxSplitter1: TcxSplitter [2]
    Left = 825
    Top = 30
    Width = 8
    Height = 330
    HotZoneClassName = 'TcxSimpleStyle'
    AlignSplitter = salRight
    Control = cxDBVerticalGrid1
    Visible = False
  end
  object cxSplitter2: TcxSplitter [3]
    Left = 0
    Top = 360
    Width = 1036
    Height = 8
    HotZoneClassName = 'TcxSimpleStyle'
    AlignSplitter = salBottom
    Control = PageControl1
  end
  object PageControl1: TPageControl [4]
    Left = 0
    Top = 368
    Width = 1036
    Height = 193
    ActivePage = TabSheet1
    Align = alBottom
    TabOrder = 4
    object TabSheet1: TTabSheet
      Caption = #1056#1077#1078#1080#1084#1099
      object cxGrid2: TcxGrid
        Left = 0
        Top = 0
        Width = 673
        Height = 165
        Align = alLeft
        TabOrder = 0
        object cxGridDBTableView_modes: TcxGridDBTableView
          Navigator.Buttons.CustomButtons = <>
          FindPanel.DisplayMode = fpdmManual
          DataController.DataSource = ds_LinkEndType_mode
          DataController.Summary.DefaultGroupSummaryItems = <>
          DataController.Summary.FooterSummaryItems = <
            item
            end>
          DataController.Summary.SummaryGroups = <>
          OptionsBehavior.ImmediateEditor = False
          OptionsCustomize.ColumnsQuickCustomization = True
          OptionsView.GroupByBox = False
          OptionsView.Indicator = True
          object cxGridDBTableView_modesid: TcxGridDBColumn
            DataBinding.FieldName = 'id'
            Visible = False
          end
          object cxGridDBTableView_modesLinkEndType_id: TcxGridDBColumn
            DataBinding.FieldName = 'LinkEndType_id'
            Visible = False
          end
          object cxGridDBTableView_modesmode: TcxGridDBColumn
            DataBinding.FieldName = 'mode'
          end
          object cxGridDBTableView_modesradiation_class: TcxGridDBColumn
            DataBinding.FieldName = 'radiation_class'
            Width = 88
          end
          object cxGridDBTableView_modesbitrate_Mbps: TcxGridDBColumn
            DataBinding.FieldName = 'bitrate_Mbps'
          end
          object cxGridDBTableView_modesmodulation_type: TcxGridDBColumn
            DataBinding.FieldName = 'modulation_type'
            Width = 94
          end
          object cxGridDBTableView_modesthreshold_BER_3: TcxGridDBColumn
            DataBinding.FieldName = 'threshold_BER_3'
          end
          object cxGridDBTableView_modesthreshold_BER_6: TcxGridDBColumn
            DataBinding.FieldName = 'threshold_BER_6'
          end
          object cxGridDBTableView_modespower_max: TcxGridDBColumn
            DataBinding.FieldName = 'power_max'
          end
          object cxGridDBTableView_modestraffic_type: TcxGridDBColumn
            DataBinding.FieldName = 'traffic_type'
            Width = 63
          end
          object cxGridDBTableView_modessignature_width: TcxGridDBColumn
            DataBinding.FieldName = 'signature_width'
          end
          object cxGridDBTableView_modessignature_height: TcxGridDBColumn
            DataBinding.FieldName = 'signature_height'
          end
          object cxGridDBTableView_modesch_spacing: TcxGridDBColumn
            DataBinding.FieldName = 'ch_spacing'
          end
          object cxGridDBTableView_modesbandwidth: TcxGridDBColumn
            DataBinding.FieldName = 'bandwidth'
          end
          object cxGridDBTableView_modesbandwidth_tx_3: TcxGridDBColumn
            DataBinding.FieldName = 'bandwidth_tx_3'
          end
          object cxGridDBTableView_modesbandwidth_tx_30: TcxGridDBColumn
            DataBinding.FieldName = 'bandwidth_tx_30'
          end
          object cxGridDBTableView_modesbandwidth_tx_a: TcxGridDBColumn
            DataBinding.FieldName = 'bandwidth_tx_a'
          end
          object cxGridDBTableView_modesbandwidth_rx_3: TcxGridDBColumn
            DataBinding.FieldName = 'bandwidth_rx_3'
          end
          object cxGridDBTableView_modesbandwidth_rx_30: TcxGridDBColumn
            DataBinding.FieldName = 'bandwidth_rx_30'
          end
          object cxGridDBTableView_modesbandwidth_rx_a: TcxGridDBColumn
            DataBinding.FieldName = 'bandwidth_rx_a'
          end
          object cxGridDBTableView_modespower_noise: TcxGridDBColumn
            DataBinding.FieldName = 'power_noise'
          end
          object cxGridDBTableView_modesmodulation_id__11111111: TcxGridDBColumn
            DataBinding.FieldName = 'modulation_id__11111111'
          end
          object cxGridDBTableView_modescomment: TcxGridDBColumn
            DataBinding.FieldName = 'comment'
            Width = 49
          end
          object cxGridDBTableView_modespower_min: TcxGridDBColumn
            DataBinding.FieldName = 'power_min'
          end
        end
        object cxGridLevel1: TcxGridLevel
          GridView = cxGridDBTableView_modes
        end
      end
    end
  end
  inherited dxBarManager1: TdxBarManager
    Left = 620
    Top = 224
    PixelsPerInch = 96
    DockControlHeights = (
      0
      0
      30
      0)
    inherited dxBarManager1Bar1: TdxBar
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarButton1'
        end>
      Visible = True
    end
    object dxBarButton1: TdxBarButton
      Action = act_Refresh
      Category = 0
    end
  end
  inherited FormStorage1: TFormStorage
    StoredProps.Strings = (
      'PageControl1.Height')
    Left = 620
    Top = 280
  end
  inherited ActionList_base: TActionList
    Left = 536
    Top = 64
    object act_Refresh: TAction
      Caption = 'Refresh'
      OnExecute = act_RefreshExecute
    end
  end
  object t_LinkEndType: TADOTable
    Connection = dmMain.ADOConnection1
    CursorType = ctStatic
    TableName = 'lib.LinkEndType'
    Left = 624
    Top = 64
  end
  object ds_LinkEndType: TDataSource
    DataSet = t_LinkEndType
    Left = 624
    Top = 128
  end
  object q_LinkEndType_mode: TADOQuery
    Connection = dmMain.ADOConnection1
    CursorType = ctStatic
    DataSource = ds_LinkEndType
    Parameters = <
      item
        Name = 'id'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Value = 9533
      end>
    Prepared = True
    SQL.Strings = (
      'select * from   lib.LinkEndType_mode'
      'where '
      '  LinkEndType_id=:id')
    Left = 736
    Top = 64
  end
  object ds_LinkEndType_mode: TDataSource
    DataSet = q_LinkEndType_mode
    Left = 736
    Top = 120
  end
end
