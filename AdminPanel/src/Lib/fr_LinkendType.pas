﻿unit fr_LinkendType;

interface

uses
  dm_Main,
  fr_View_base,

  u_cx_ini,

  System.Classes,
  Vcl.Controls, Vcl.Forms,


  Data.DB, Data.Win.ADODB, cxGridLevel,
  cxGridCustomTableView, cxGridDBTableView,
  cxGrid, cxDBVGrid, cxSplitter,
  Vcl.ComCtrls,


  Vcl.ActnList, dxBar,
  Vcl.DBGrids, cxGraphics, cxControls, cxLookAndFeels, cxLookAndFeelPainters,
  cxStyles, cxCustomData, cxFilter, cxData, cxDataStorage, cxEdit, cxNavigator,
  dxDateRanges, cxDBData, System.Actions, Vcl.Grids, cxVGrid,
  cxInplaceContainer, cxGridTableView, cxGridCustomView, RxPlacemnt, cxClasses
      ;

type
  Tframe_LinkendTYpe = class(Tframe_View_base)
    cxGrid1DBTableView1: TcxGridDBTableView;
    cxGrid1Level1: TcxGridLevel;
    cxGrid1: TcxGrid;
    t_LinkEndType: TADOTable;
    ds_LinkEndType: TDataSource;
    cxGrid1DBTableView1id: TcxGridDBColumn;
    cxGrid1DBTableView1name: TcxGridDBColumn;
    cxGrid1DBTableView1band: TcxGridDBColumn;
    cxGrid1DBTableView1power_max: TcxGridDBColumn;
    cxGrid1DBTableView1freq_min: TcxGridDBColumn;
    cxGrid1DBTableView1freq_max: TcxGridDBColumn;
    cxDBVerticalGrid1: TcxDBVerticalGrid;
    cxSplitter1: TcxSplitter;
    cxDBVerticalGrid1id: TcxDBEditorRow;
    cxDBVerticalGrid1name: TcxDBEditorRow;
    cxDBVerticalGrid1folder_id: TcxDBEditorRow;
    cxDBVerticalGrid1band: TcxDBEditorRow;
    cxDBVerticalGrid1power_max: TcxDBEditorRow;
    cxDBVerticalGrid1power_loss: TcxDBEditorRow;
    cxDBVerticalGrid1freq_min: TcxDBEditorRow;
    cxDBVerticalGrid1freq_max: TcxDBEditorRow;
    cxDBVerticalGrid1KNG: TcxDBEditorRow;
    cxDBVerticalGrid1failure_period: TcxDBEditorRow;
    cxDBVerticalGrid1equaliser_profit: TcxDBEditorRow;
    cxDBVerticalGrid1freq_channel_count: TcxDBEditorRow;
    cxDBVerticalGrid1kratnostByFreq: TcxDBEditorRow;
    cxDBVerticalGrid1kratnostBySpace: TcxDBEditorRow;
    cxDBVerticalGrid1allow_use_2_tx_antennas: TcxDBEditorRow;
    cxDBVerticalGrid1level_tx_a: TcxDBEditorRow;
    cxDBVerticalGrid1level_rx_a: TcxDBEditorRow;
    cxDBVerticalGrid1coeff_noise: TcxDBEditorRow;
    cxDBVerticalGrid1vendor_id: TcxDBEditorRow;
    cxDBVerticalGrid1vendor_equipment_id: TcxDBEditorRow;
    cxDBVerticalGrid1comment: TcxDBEditorRow;
    cxDBVerticalGrid1guid: TcxDBEditorRow;
    cxDBVerticalGrid1date_created: TcxDBEditorRow;
    cxDBVerticalGrid1date_modify: TcxDBEditorRow;
    cxDBVerticalGrid1user_created: TcxDBEditorRow;
    cxDBVerticalGrid1user_modify: TcxDBEditorRow;
    cxSplitter2: TcxSplitter;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    cxGrid2: TcxGrid;
    cxGridDBTableView_modes: TcxGridDBTableView;
    cxGridLevel1: TcxGridLevel;
    q_LinkEndType_mode: TADOQuery;
    ds_LinkEndType_mode: TDataSource;
    cxGridDBTableView_modesid: TcxGridDBColumn;
    cxGridDBTableView_modesLinkEndType_id: TcxGridDBColumn;
    cxGridDBTableView_modesmode: TcxGridDBColumn;
    cxGridDBTableView_modesradiation_class: TcxGridDBColumn;
    cxGridDBTableView_modesbitrate_Mbps: TcxGridDBColumn;
    cxGridDBTableView_modesmodulation_type: TcxGridDBColumn;
    cxGridDBTableView_modesthreshold_BER_3: TcxGridDBColumn;
    cxGridDBTableView_modesthreshold_BER_6: TcxGridDBColumn;
    cxGridDBTableView_modespower_max: TcxGridDBColumn;
    cxGridDBTableView_modestraffic_type: TcxGridDBColumn;
    cxGridDBTableView_modessignature_width: TcxGridDBColumn;
    cxGridDBTableView_modessignature_height: TcxGridDBColumn;
    cxGridDBTableView_modesch_spacing: TcxGridDBColumn;
    cxGridDBTableView_modesbandwidth: TcxGridDBColumn;
    cxGridDBTableView_modesbandwidth_tx_3: TcxGridDBColumn;
    cxGridDBTableView_modesbandwidth_tx_30: TcxGridDBColumn;
    cxGridDBTableView_modesbandwidth_tx_a: TcxGridDBColumn;
    cxGridDBTableView_modesbandwidth_rx_3: TcxGridDBColumn;
    cxGridDBTableView_modesbandwidth_rx_30: TcxGridDBColumn;
    cxGridDBTableView_modesbandwidth_rx_a: TcxGridDBColumn;
    cxGridDBTableView_modespower_noise: TcxGridDBColumn;
    cxGridDBTableView_modesmodulation_id__11111111: TcxGridDBColumn;
    cxGridDBTableView_modescomment: TcxGridDBColumn;
    cxGridDBTableView_modespower_min: TcxGridDBColumn;
    dxBarButton1: TdxBarButton;
    act_Refresh: TAction;
    procedure act_RefreshExecute(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private

  public

  end;

//var
//  fra_LinkendTYpe: Tfra_LinkendTYpe;

implementation

{$R *.dfm}

procedure Tframe_LinkendTYpe.act_RefreshExecute(Sender: TObject);
begin
  t_LinkEndType.Requery([]);
end;

//-------------------------------------------------------------------
procedure Tframe_LinkendTYpe.FormCreate(Sender: TObject);
//-------------------------------------------------------------------
begin
  SetCaption('Модели РРС');

  cxGrid1.Align:=alClient;

  dmMain.OpenTable (t_LinkEndType);
  q_LinkEndType_mode.Open;



//  cxGrid1DBTableView1.DataController.DataSource:=dmData_lib.ds_ClutterModel;


  dmMain.OpenTable(t_LinkEndType);


  cx_LoadColumnCaptions_FromIni(cxGrid1DBTableView1);
  cx_LoadColumnCaptions_FromIni(cxDBVerticalGrid1);


  cxGrid1DBTableView1.Navigator.Visible:=True;

//  exit;
//
//  cx_Grid_SetColumnCaptions(cxGrid1DBTableView1,
//   [
//     'NAME', 'Íàçâàíèå' ,
//     'band', 'Äèàïàçîí, GHz' ,
//     'freq_min', ' ×àñòîòà min, MHz' ,
//     'freq_max', '×àñòîòà max, MHz'
//
//   ]);

//
//   Òåðìèíàëû - Àáîíåíòñêèå òåðìèíàëû
//Íàçâàíèÿ ñòîëáöîâ:     Íàçâàíèå/ Âûñîòà àíòåííû, m/ Ìîùíîñòü, W
//
//
//
//ÐÐÑ ìîäåëè - Îáîðóäîâàíèå
//Íàçâàíèÿ ñòîëáöîâ:  Íàçâàíèå/ Äèàïàçîí, GHz / ×àñòîòà min, MHz / ×àñòîòà max, MHz
//
//
//  cx_DBVerticalGrid_SetCaptions(cxDBVerticalGrid1,
//  [
//   'NAME', 'Íàçâàíèå'
//
////    'iteration_count', 'Êîë-âî èòåðàöèé',
////    'iteration_antenna_h','Âûñîòà.. èòåðàöèé',
////    'dir_Data',   'Êàòàëîã ñ äàííûìè',
////    'dir_Relief', 'Êàòàëîã ñ ìàòðèöàìè âûñîò',
////
////    'clutter_model_id',    'Ìîäåëü òèïîâ ìåñòíîñòè (ïî óìîë÷àíèþ)',
////    'link_calc_method_id', 'Ìåòîäèêà ðàñ÷åòà (ïî óìîë÷àíèþ)',
////    'equipment_id',        'Ìîäåëü îáîðóäîâàíèÿ (ïî óìîë÷àíèþ)',
////
////    'rest_service_http', 'HTTP ïðåôèêñ',
////
////    'IMAGE_map_WIDTH',  'Êàðòà : øèðèíà',
////    'IMAGE_map_HEIGHT', 'Êàðòà : âûñîòà',
////    'IMAGE_PROFILE_HEIGHT', 'Ïðîôèëü : øèðèíà',
////    'IMAGE_PROFILE_WIDTH',  'Ïðîôèëü : âûñîòà'
////
//
//  ]);
//

end;


end.
