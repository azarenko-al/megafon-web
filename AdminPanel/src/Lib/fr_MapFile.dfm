inherited frame_MapFile: Tframe_MapFile
  Caption = 'frame_MapFile'
  ClientHeight = 613
  ClientWidth = 722
  OnDestroy = FormDestroy
  ExplicitWidth = 738
  ExplicitHeight = 652
  PixelsPerInch = 96
  TextHeight = 13
  object cxGrid1: TcxGrid [0]
    Left = 0
    Top = 29
    Width = 465
    Height = 339
    Align = alLeft
    TabOrder = 0
    object cxGrid1DBTableView1: TcxGridDBTableView
      PopupMenu = PopupMenu1
      Navigator.Buttons.CustomButtons = <>
      FindPanel.DisplayMode = fpdmManual
      DataController.DataSource = DataSource1
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <
        item
        end>
      DataController.Summary.SummaryGroups = <>
      OptionsBehavior.ImmediateEditor = False
      OptionsView.GroupByBox = False
      OptionsView.Indicator = True
      object col_table_name: TcxGridDBColumn
        DataBinding.FieldName = 'table_name'
        Width = 284
      end
      object cxGrid1DBTableView1rec_count: TcxGridDBColumn
        DataBinding.FieldName = 'rec_count'
        Width = 85
      end
      object cxGrid1DBTableView1Column1: TcxGridDBColumn
        DataBinding.FieldName = 'GeoJSON'
        Visible = False
      end
    end
    object cxGrid1Level1: TcxGridLevel
      GridView = cxGrid1DBTableView1
    end
  end
  object cxSplitter1: TcxSplitter [1]
    Left = 0
    Top = 368
    Width = 722
    Height = 8
    HotZoneClassName = 'TcxSimpleStyle'
    AlignSplitter = salBottom
    Control = PageControl1
    ExplicitWidth = 8
  end
  object PageControl1: TPageControl [2]
    Left = 0
    Top = 376
    Width = 722
    Height = 237
    ActivePage = TabSheet1
    Align = alBottom
    TabOrder = 2
    object TabSheet1: TTabSheet
      Caption = #1050#1072#1088#1090#1072
      ExplicitLeft = 0
      ExplicitTop = 0
      ExplicitWidth = 0
      ExplicitHeight = 0
    end
  end
  object ToolBar1: TToolBar [3]
    Left = 0
    Top = 0
    Width = 722
    Height = 29
    BorderWidth = 1
    ButtonHeight = 21
    Caption = 'ToolBar1'
    TabOrder = 3
    object Button2: TButton
      Left = 0
      Top = 0
      Width = 75
      Height = 21
      Caption = #1047#1072#1075#1088#1091#1079#1080#1090#1100
      TabOrder = 1
    end
    object Button1: TButton
      Left = 75
      Top = 0
      Width = 118
      Height = 21
      Action = act_View_on_Map
      TabOrder = 0
    end
  end
  inherited dxBarManager1: TdxBarManager
    Left = 652
    Top = 80
    PixelsPerInch = 96
  end
  inherited FormStorage1: TFormStorage
    StoredProps.Strings = (
      'PageControl1.Height')
    Left = 652
    Top = 136
  end
  inherited ActionList_base: TActionList
    Left = 648
    Top = 288
  end
  object DataSource1: TDataSource
    DataSet = ADOStoredProc1
    Left = 494
    Top = 152
  end
  object ADOStoredProc1: TADOStoredProc
    Connection = dmMain.ADOConnection1
    CursorType = ctStatic
    LockType = ltReadOnly
    ProcedureName = 'map.sp_Map_SEL'
    Parameters = <>
    Left = 496
    Top = 80
  end
  object ActionList1: TActionList
    OnUpdate = ActionList1Update
    Left = 640
    Top = 224
    object act_Del: TAction
      Caption = #1059#1076#1072#1083#1080#1090#1100
      OnExecute = act_DelExecute
    end
    object act_View_on_Map: TAction
      Caption = #1055#1086#1082#1072#1079#1072#1090#1100' '#1085#1072' '#1082#1072#1088#1090#1077
      OnExecute = act_View_on_MapExecute
    end
  end
  object PopupMenu1: TPopupMenu
    Left = 488
    Top = 248
    object N2: TMenuItem
      Action = act_View_on_Map
    end
    object N3: TMenuItem
      Caption = '-'
    end
    object N1: TMenuItem
      Action = act_Del
    end
  end
end
