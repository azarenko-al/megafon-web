﻿unit fr_MapFile;

interface

uses
  u_func,
  u_cx_ini,

  fr_View_base,
  fr_Map_view,

//  d_Map_Loader,


  u_cx,

  u_db,
  dm_Main,

  System.SysUtils, System.Variants, System.Classes,
  Vcl.Controls, Vcl.Forms,
  Data.DB, Data.Win.ADODB, cxGridLevel, cxGridCustomTableView,
  cxGridDBTableView, cxGrid, cxSplitter,
  Vcl.ComCtrls, Vcl.StdCtrls, Vcl.Menus,
  System.Actions, Vcl.ActnList, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, cxStyles, cxCustomData, cxFilter, cxData,
  cxDataStorage, cxEdit, cxNavigator, dxDateRanges, cxDBData, Vcl.ToolWin,
  cxGridTableView, cxGridCustomView, RxPlacemnt, cxClasses, dxBar;

type
  Tframe_MapFile = class(Tframe_View_base)
    cxGrid1: TcxGrid;
    cxGrid1DBTableView1: TcxGridDBTableView;
    cxGrid1Level1: TcxGridLevel;
    DataSource1: TDataSource;
    cxSplitter1: TcxSplitter;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    ADOStoredProc1: TADOStoredProc;
    ToolBar1: TToolBar;
    Button1: TButton;
    ActionList1: TActionList;
    act_Del: TAction;
    PopupMenu1: TPopupMenu;
    N1: TMenuItem;
    col_table_name: TcxGridDBColumn;
    cxGrid1DBTableView1rec_count: TcxGridDBColumn;
    cxGrid1DBTableView1Column1: TcxGridDBColumn;
    act_View_on_Map: TAction;
    N2: TMenuItem;
    N3: TMenuItem;
    Button2: TButton;
    procedure FormDestroy(Sender: TObject);
    procedure ActionList1Update(Action: TBasicAction; var Handled: Boolean);
    procedure act_DelExecute(Sender: TObject);
    procedure act_View_on_MapExecute(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    FWebBrowser: Tframe_Map_view;
  public
    { Public declarations }
  end;

var
  frame_MapFile: Tframe_MapFile;

implementation

{$R *.dfm}

 //-------------------------------------------------------------------
procedure Tframe_MapFile.FormCreate(Sender: TObject);
//-------------------------------------------------------------------
begin
  SetCaption('Карты строений');

  cxGrid1.Align:=alClient;

//    dmMain.OpenTable (t_Settings);

  CreateChildForm(FWebBrowser, Tframe_Map_view, TabSheet1);

//  Tframe_WebBrowser.Create_(pn_Map);

  dmMain.StoredProc_Open(ADOStoredProc1, 'map.sp_Map_SEL', []);

  Resize;

  cx_LoadColumnCaptions_FromIni(cxGrid1DBTableView1);

  cxGrid1DBTableView1.Navigator.Visible:=True;

  cx_Footer(cxGrid1DBTableView1, col_table_name);


end;

procedure Tframe_MapFile.FormDestroy(Sender: TObject);
begin
  inherited;

  FreeAndNil(FWebBrowser);

end;

procedure Tframe_MapFile.ActionList1Update(Action: TBasicAction; var Handled:
    Boolean);
begin
  inherited;

  act_Del.Enabled:=not ADOStoredProc1.IsEmpty;
  act_View_on_Map.Enabled:=not ADOStoredProc1.IsEmpty;

end;

procedure Tframe_MapFile.act_DelExecute(Sender: TObject);
begin
  inherited;

  dmMain.StoredProc_Exec(dmMain.ADOStoredProc1, 'map.sp_Map_del',
      [FLD_ID, ADOStoredProc1[FLD_ID] ]);
end;

//-------------------------------------------------------------------
procedure Tframe_MapFile.act_View_on_MapExecute(Sender: TObject);
//-------------------------------------------------------------------
var
  sJSON: string;
begin
  sJSON:=ADOStoredProc1.FieldByName('GeoJSON').AsString;

  FWebBrowser.Load_GeoJSON(sJSON, true);

end;


end.
