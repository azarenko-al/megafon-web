inherited frame_Relief: Tframe_Relief
  Caption = 'frame_Relief'
  ClientHeight = 477
  ClientWidth = 989
  OnDestroy = FormDestroy
  ExplicitWidth = 1005
  ExplicitHeight = 516
  PixelsPerInch = 96
  TextHeight = 13
  object cxGrid1: TcxGrid [0]
    Left = 0
    Top = 29
    Width = 585
    Height = 313
    Align = alLeft
    TabOrder = 0
    object cxGrid1DBTableView1: TcxGridDBTableView
      PopupMenu = PopupMenu1
      Navigator.Buttons.CustomButtons = <>
      DataController.DataSource = DataSource1
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <>
      DataController.Summary.SummaryGroups = <>
      OptionsBehavior.ImmediateEditor = False
      OptionsCustomize.ColumnsQuickCustomization = True
      OptionsSelection.MultiSelect = True
      OptionsView.GroupByBox = False
      OptionsView.Indicator = True
      object col_filename: TcxGridDBColumn
        DataBinding.FieldName = 'filename'
        Width = 348
      end
      object cxGrid1DBTableView1Column1_step: TcxGridDBColumn
        DataBinding.FieldName = 'step'
        Width = 76
      end
      object cxGrid1DBTableView1row_count: TcxGridDBColumn
        DataBinding.FieldName = 'row_count'
      end
      object cxGrid1DBTableView1col_count: TcxGridDBColumn
        DataBinding.FieldName = 'col_count'
      end
      object col_GeoJSON: TcxGridDBColumn
        DataBinding.FieldName = 'GeoJSON'
        Visible = False
      end
      object cxGrid1DBTableView1id: TcxGridDBColumn
        DataBinding.FieldName = 'id'
        Visible = False
        Width = 56
      end
    end
    object cxGrid1Level1: TcxGridLevel
      GridView = cxGrid1DBTableView1
    end
  end
  object ToolBar2: TToolBar [1]
    Left = 0
    Top = 0
    Width = 989
    Height = 29
    BorderWidth = 1
    ButtonHeight = 21
    Caption = 'ToolBar2'
    TabOrder = 1
    object Button2: TButton
      Left = 0
      Top = 0
      Width = 121
      Height = 21
      Action = act_Add_file
      TabOrder = 0
    end
  end
  object cxSplitter1: TcxSplitter [2]
    Left = 0
    Top = 342
    Width = 989
    Height = 8
    HotZoneClassName = 'TcxSimpleStyle'
    AlignSplitter = salBottom
    Control = PageControl1
    ExplicitWidth = 8
  end
  object PageControl1: TPageControl [3]
    Left = 0
    Top = 350
    Width = 989
    Height = 127
    ActivePage = TabSheet1
    Align = alBottom
    TabOrder = 3
    object TabSheet1: TTabSheet
      Caption = #1050#1072#1088#1090#1072
      ExplicitLeft = 0
      ExplicitTop = 0
      ExplicitWidth = 0
      ExplicitHeight = 0
    end
  end
  inherited dxBarManager1: TdxBarManager
    Left = 780
    Top = 40
    PixelsPerInch = 96
    inherited dxBarManager1Bar1: TdxBar
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarButton1'
        end>
    end
    object dxBarButton1: TdxBarButton
      Action = act_Add_file
      Category = 0
    end
  end
  inherited FormStorage1: TFormStorage
    StoredProps.Strings = (
      'PageControl1.Height')
    Left = 780
    Top = 96
  end
  inherited ActionList_base: TActionList
    OnUpdate = ActionList_baseUpdate
    Left = 680
    Top = 160
    object act_View_on_Map: TAction
      Caption = #1055#1086#1082#1072#1079#1072#1090#1100' '#1085#1072' '#1082#1072#1088#1090#1077
      OnExecute = act_View_on_MapExecute
    end
  end
  object view_Relief: TADOTable
    Connection = dmMain.ADOConnection1
    CursorType = ctStatic
    TableName = 'gis.view_Relief'
    Left = 680
    Top = 40
  end
  object DataSource1: TDataSource
    DataSet = view_Relief
    Left = 680
    Top = 96
  end
  object PopupMenu1: TPopupMenu
    Left = 776
    Top = 160
    object actView1: TMenuItem
      Action = act_View_on_Map
    end
    object N2: TMenuItem
      Caption = '-'
    end
    object N1: TMenuItem
      Action = act_Add_file
    end
  end
  object ActionList1: TActionList
    OnUpdate = ActionList_baseUpdate
    Left = 680
    Top = 216
    object act_Add_file: TFileOpen
      Category = 'File'
      Caption = #1044#1086#1073#1072#1074#1080#1090#1100' '#1092#1072#1081#1083'...'
      Dialog.Options = [ofHideReadOnly, ofAllowMultiSelect, ofFileMustExist, ofEnableSizing]
      Hint = 'Open|Opens an existing file'
      ImageIndex = 7
      ShortCut = 16463
      OnAccept = act_Add_fileAccept
    end
  end
  object ADOStoredProc1: TADOStoredProc
    Parameters = <>
    Left = 896
    Top = 40
  end
end
