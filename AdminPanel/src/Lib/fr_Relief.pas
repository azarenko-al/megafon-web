﻿unit fr_Relief;

interface

uses
  u_func,

  u_cx,
  u_cx_ini,
  u_files,
  u_db,


//  d_Relief_Add,
  u_const_db,

  dm_Main,
  fr_View_base,
  fr_Map_view,

  u_rel_Matrix_base,
  u_rel_engine,

//  dm_Relief,

  System.Types,
  System.SysUtils, System.Classes,  Vcl.Controls, Vcl.Forms,
  Data.DB, Data.Win.ADODB, cxGridLevel,   cxGridCustomTableView, cxGridDBTableView,
  cxGrid, cxSplitter, Vcl.ComCtrls,
  Vcl.StdCtrls, System.Actions, Vcl.ActnList,
  Vcl.Menus, cxGraphics, cxControls, cxLookAndFeels, cxLookAndFeelPainters,
  cxStyles, cxCustomData, cxFilter, cxData, cxDataStorage, cxEdit, cxNavigator,
  dxDateRanges, cxDBData, Vcl.ToolWin, cxGridTableView, cxGridCustomView,
  RxPlacemnt, cxClasses, dxBar, Vcl.StdActns;

type
  Tframe_Relief = class(Tframe_View_base)
    cxGrid1DBTableView1: TcxGridDBTableView;
    cxGrid1Level1: TcxGridLevel;
    cxGrid1: TcxGrid;
    view_Relief: TADOTable;
    DataSource1: TDataSource;
    cxGrid1DBTableView1id: TcxGridDBColumn;
    col_filename: TcxGridDBColumn;
    cxGrid1DBTableView1row_count: TcxGridDBColumn;
    cxGrid1DBTableView1col_count: TcxGridDBColumn;
    ToolBar2: TToolBar;
    cxGrid1DBTableView1Column1_step: TcxGridDBColumn;
    cxSplitter1: TcxSplitter;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    col_GeoJSON: TcxGridDBColumn;
    act_View_on_Map: TAction;
    PopupMenu1: TPopupMenu;
    actView1: TMenuItem;
    ActionList1: TActionList;
    act_Add_file: TFileOpen;
    Button2: TButton;
    ADOStoredProc1: TADOStoredProc;
    dxBarButton1: TdxBarButton;
    N1: TMenuItem;
    N2: TMenuItem;
    procedure FormDestroy(Sender: TObject);
    procedure ActionList_baseUpdate(Action: TBasicAction; var Handled: Boolean);
    procedure act_Add_fileAccept(Sender: TObject);
    procedure act_View_on_MapExecute(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
     FFrame_Map_view: Tframe_Map_view;
    procedure Load_RLF_files_from_dir(aDir : string);

    procedure Add_file(aFileName : string);


  public

  end;

//
//var
//  fra_Relief: Tfra_Relief;

implementation

{$R *.dfm}



procedure Tframe_Relief.FormDestroy(Sender: TObject);
begin
  inherited;

  FreeAndNil(FFrame_Map_view);

end;

procedure Tframe_Relief.ActionList_baseUpdate(Action: TBasicAction; var
    Handled: Boolean);
begin
  act_View_on_Map.Enabled:=not view_Relief.IsEmpty;
end;

procedure Tframe_Relief.act_Add_fileAccept(Sender: TObject);

var
  i: Integer;
begin
  with TFileOpen(Sender) do
    for i:=0 to Dialog.Files.Count-1 do    // Iterate
        Add_file (Dialog.Files[i]);

  view_Relief.Requery([]);

      //  UpdateFileLists();
  //    end;
end;


procedure Tframe_Relief.Button1Click(Sender: TObject);
begin
  inherited;

//   Tdlg_Relief_Add.ExecDlg ();

 // dmRelief.Load_RLF_files_from_dir(DirectoryEdit1.Text);
//  dmMain.OpenTable (t_Relief);

end;

procedure Tframe_Relief.FormCreate(Sender: TObject);
begin
  SetCaption('Матрицы высот');
  cxGrid1.Align:=alClient;


 // dmMain.OpenStoredProc(ADOStoredProc1, ADOStoredProc1.ProcedureName, []);


  dmMain.OpenTable (view_Relief);


//
//  cx_Grid_SetColumnCaptions(cxGrid1DBTableView1,
//    [
//      'filename', 'Èìÿ ôàéëà',
//      'step', 'Øàã ìàòðèöû, m',
//      'row_count', 'Âûñîòà',
//      'col_count', 'Øèðèíà'
//
//    ]);

  cxGrid1DBTableView1.Navigator.Visible:=True;


  cx_LoadColumnCaptions_FromIni(cxGrid1DBTableView1);



  cx_Footer(cxGrid1DBTableView1, col_filename);

//  cx_LoadColumnCaptions_FromIni(cxDBVerticalGrid1);


  CreateChildForm(FFrame_Map_view, Tframe_Map_view, TabSheet1);

  Resize;
//  Invalidate;

//
//  1.      Êàðòû - Êàðòû ñòðîåíèé
//
//Èìÿ ôàéëà íå îòîáðàæàåò ôîðìàò
//
//2.      Ðåëüåô - Ìàòðèöû âûñîò
//
//Íàçâàíèÿ ñòîëáöîâ: Èìÿ ôàéëà, Øàã ìàòðèöû, m,
//
//Row_count / Col-count - ÷òî çíà÷èò è äëÿ ÷åãî?
//

//  Tframe_WebBrowser.Create_(pn_Map);

end;

procedure Tframe_Relief.act_View_on_MapExecute(Sender: TObject);
var
  sJSON: string;
begin
  sJSON:=view_Relief.FieldByName('GeoJSON').AsString;

  FFrame_Map_view.Load_GeoJSON(sJSON, true);

end;

//-----------------------------------------------------------------
procedure Tframe_Relief.Add_file(aFileName : string);
//-----------------------------------------------------------------
var
  i: Integer;
//  oFiles: TStringList;
//  sFileName: string;

 // bl_arr: TBLPointArray;
  k: Integer;
//  oRel: TrelMatrixBase;

  iFileSize: int64;
  dtFileDate: TDate;
  e: Double;
  iStep: word;

//  oRelEngine: TRelEngine;
  oRelMatrix: TrelMatrixBase;
  s: string;
  sFileName_clu: string;

//  strArr: TStringDynArray;
begin

 // Log(ClassName  + ': GetListInRect = '+ IntToStr(i));

// Assert (oFiles.Count>0);

 // for sFileName in strArr do

     //Memo_Log.Lines.Add(sFileName);

//     sFileName:=oFiles[i];

   //  CodeSite.Send('add - '+ sFileName);

     oRelMatrix:=rel_CreateRelMatrixByFileName (aFileName);
     if not Assigned(oRelMatrix) then
       Exit;
//
//  Add (obj);
     oRelMatrix.OpenFile (oRelMatrix.FileName);

//    oRelMatrix.sa

    iFileSize  := GetFileSize_(oRelMatrix.FileName);
    dtFileDate := GetFileDate (oRelMatrix.FileName);


///    iFileSize  := GetFileSize_(oRel.FileName);
//    dtFileDate := GetFileDate(oRel.FileName);


   iStep:=Round ((oRelMatrix.XYBounds.TopLeft.X  - oRelMatrix.XYBounds.BottomRight.X) /  oRelMatrix.RowCount);

assert (oRelMatrix.XYBounds.TopLeft.X  > oRelMatrix.XYBounds.BottomRight.X );

s:=oRelMatrix.ToGeometry();

//    k:=db_ExecStoredProc_v1( ADOStoredProc1, SP_RELIEF_ADD,
    k:=dmMain.StoredProc_Exec( ADOStoredProc1, SP_RELIEF_ADD,

//    k:=db_ExecStoredProc_( dmMain_web.ADOConnection1, SP_RELIEF_ADD,
        [
          FLD_FILENAME, oRelMatrix.FileName,

          FLD_FileSize, iFileSize,

       //   FLD_FileDate, dtFileDate,
     //db_DateTimeToSqlDateTime(dtFileDate), //: WideString;


//         'lat_max_str', IntToStr(round(oRelMatrix.XYBounds.TopLeft.X)),
//         'lat_min_str', IntToStr(round(oRelMatrix.XYBounds.BottomRight.X)),
//
//         'lon_min_str', IntToStr(round(oRelMatrix.XYBounds.TopLeft.y)),
//         'lon_max_str', IntToStr(round(oRelMatrix.XYBounds.BottomRight.Y)),

//
         'lat_max', round(oRelMatrix.XYBounds.TopLeft.X),
         'lat_min', round(oRelMatrix.XYBounds.BottomRight.X),

         'lon_min', round(oRelMatrix.XYBounds.TopLeft.y),
         'lon_max', round(oRelMatrix.XYBounds.BottomRight.Y),


         'geom_str',  oRelMatrix.ToGeometry(),

         'COL_COUNT', oRelMatrix.ColCount,
         'ROW_COUNT', oRelMatrix.RowCount,

         'step', iStep

         ]);


//     if oRelEngine.AddFile (sFileName) then
//     ;

  //   bl_arr := GetReliefEngine.Items[i].GetPoints_WGS();

{
    if CheckBox_GeoTif.Checked then
    begin
      sFileName_clu:=ChangeFileExt (sFileName, '_clu.envi');
      if not FileExists(sFileName_clu) then
        TRLF_to_ENVI_export.SaveCluttersToEnvi_ByFile(sFileName, sFileName_clu);


      Assert (DirectoryEdit_GeoTIF.Text <> '');

//      T_Rel_to_Clutter_map.Exec_CLU(sFileName_clu, oRelMatrix.ZoneNum_GK, Round(oRelMatrix.stepX), DirectoryEdit_GeoTIF.Text);

    end;
 }

    FreeAndNil(oRelMatrix);


end;



// ---------------------------------------------------------------
procedure Tframe_Relief.Load_RLF_files_from_dir(aDir : string);
// ---------------------------------------------------------------
var
  sFileName: string;

  strArr: TStringDynArray;
begin
  //--------------------------------------------
  aDir:= IncludeTrailingBackslash(aDir);

 // oFiles:=TStringList.Create;

  strArr:=ScanDir(aDir, '*.rlf');//  , Memo1.Lines);

  for sFileName in strArr do
    Add_file (sFileName);


end;




end.

    {

//-------------------------------------------------------------------
procedure Tframe_MapFile.act_ViewExecute(Sender: TObject);
//-------------------------------------------------------------------
var
  sJSON: string;
begin
  sJSON:=ADOStoredProc1.FieldByName('GeoJSON').AsString;

  FWebBrowser.Load_GeoJSON(sJSON);

end;



  System.Types, SysUtils,    System.Variants,
  Vcl.Controls, Vcl.Forms, Vcl.StdCtrls, Data.Win.ADODB,
