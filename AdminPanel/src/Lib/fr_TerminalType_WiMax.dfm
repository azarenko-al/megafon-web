inherited frame_TerminalTYpe_WiMax: Tframe_TerminalTYpe_WiMax
  Caption = 'frame_TerminalTYpe_WiMax'
  ClientHeight = 444
  ClientWidth = 971
  ExplicitWidth = 987
  ExplicitHeight = 483
  PixelsPerInch = 96
  TextHeight = 13
  object cxGrid1: TcxGrid [0]
    Left = 0
    Top = 0
    Width = 569
    Height = 444
    Align = alLeft
    TabOrder = 0
    object cxGrid1DBTableView1: TcxGridDBTableView
      Navigator.Buttons.CustomButtons = <>
      FindPanel.DisplayMode = fpdmManual
      DataController.DataSource = ds_TerminalType
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <
        item
        end>
      DataController.Summary.SummaryGroups = <>
      OptionsBehavior.ImmediateEditor = False
      OptionsCustomize.ColumnsQuickCustomization = True
      OptionsView.GroupByBox = False
      OptionsView.Indicator = True
      object cxGrid1DBTableView1id: TcxGridDBColumn
        DataBinding.FieldName = 'id'
        Visible = False
        Width = 31
      end
      object cxGrid1DBTableView1name: TcxGridDBColumn
        DataBinding.FieldName = 'name'
        Width = 248
      end
      object cxGrid1DBTableView1loss: TcxGridDBColumn
        DataBinding.FieldName = 'loss'
        Visible = False
      end
      object cxGrid1DBTableView1antennagain: TcxGridDBColumn
        DataBinding.FieldName = 'antennagain'
        Width = 76
      end
      object cxGrid1DBTableView1height: TcxGridDBColumn
        DataBinding.FieldName = 'height'
      end
      object cxGrid1DBTableView1power: TcxGridDBColumn
        DataBinding.FieldName = 'power'
        Width = 68
      end
      object cxGrid1DBTableView1sense: TcxGridDBColumn
        DataBinding.FieldName = 'sense'
      end
    end
    object cxGrid1Level1: TcxGridLevel
      GridView = cxGrid1DBTableView1
    end
  end
  object cxDBVerticalGrid1: TcxDBVerticalGrid [1]
    Left = 720
    Top = 0
    Width = 251
    Height = 444
    Align = alRight
    OptionsView.ScrollBars = ssVertical
    OptionsView.RowHeaderWidth = 134
    Navigator.Buttons.CustomButtons = <>
    TabOrder = 1
    Visible = False
    DataController.DataSource = ds_TerminalType
    Version = 1
    object cxDBVerticalGrid1id: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'id'
      Visible = False
      ID = 0
      ParentID = -1
      Index = 0
      Version = 1
    end
    object cxDBVerticalGrid1name: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'name'
      ID = 1
      ParentID = -1
      Index = 1
      Version = 1
    end
    object cxDBVerticalGrid1loss: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'loss'
      Visible = False
      ID = 2
      ParentID = -1
      Index = 2
      Version = 1
    end
    object cxDBVerticalGrid1antennagain: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'antennagain'
      ID = 3
      ParentID = -1
      Index = 3
      Version = 1
    end
    object cxDBVerticalGrid1height: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'height'
      ID = 4
      ParentID = -1
      Index = 4
      Version = 1
    end
    object cxDBVerticalGrid1power: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'power'
      ID = 5
      ParentID = -1
      Index = 5
      Version = 1
    end
    object cxDBVerticalGrid1sense: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'sense'
      ID = 6
      ParentID = -1
      Index = 6
      Version = 1
    end
    object cxDBVerticalGrid1DNA_width: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'DNA_width'
      Visible = False
      ID = 7
      ParentID = -1
      Index = 7
      Version = 1
    end
    object cxDBVerticalGrid1noise: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'noise'
      Visible = False
      ID = 8
      ParentID = -1
      Index = 8
      Version = 1
    end
    object cxDBVerticalGrid1suppression: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'suppression'
      Visible = False
      ID = 9
      ParentID = -1
      Index = 9
      Version = 1
    end
    object cxDBVerticalGrid1net_access_level: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'net_access_level'
      Visible = False
      ID = 10
      ParentID = -1
      Index = 10
      Version = 1
    end
    object cxDBVerticalGrid1weight: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'weight'
      Visible = False
      ID = 11
      ParentID = -1
      Index = 11
      Version = 1
    end
    object cxDBVerticalGrid1comments: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'comments'
      Visible = False
      ID = 12
      ParentID = -1
      Index = 12
      Version = 1
    end
    object cxDBVerticalGrid1NetStandard_id: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'NetStandard_id'
      Visible = False
      ID = 13
      ParentID = -1
      Index = 13
      Version = 1
    end
    object cxDBVerticalGrid1SignalNoise: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'SignalNoise'
      Visible = False
      ID = 14
      ParentID = -1
      Index = 14
      Version = 1
    end
    object cxDBVerticalGrid1guid: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'guid'
      Visible = False
      ID = 15
      ParentID = -1
      Index = 15
      Version = 1
    end
    object cxDBVerticalGrid1date_created: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'date_created'
      Visible = False
      ID = 16
      ParentID = -1
      Index = 16
      Version = 1
    end
    object cxDBVerticalGrid1date_modify: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'date_modify'
      Visible = False
      ID = 17
      ParentID = -1
      Index = 17
      Version = 1
    end
    object cxDBVerticalGrid1user_created: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'user_created'
      Visible = False
      ID = 18
      ParentID = -1
      Index = 18
      Version = 1
    end
    object cxDBVerticalGrid1user_modify: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'user_modify'
      Visible = False
      ID = 19
      ParentID = -1
      Index = 19
      Version = 1
    end
  end
  object cxSplitter1: TcxSplitter [2]
    Left = 712
    Top = 0
    Width = 8
    Height = 444
    HotZoneClassName = 'TcxSimpleStyle'
    AlignSplitter = salRight
    Control = cxDBVerticalGrid1
    Visible = False
  end
  inherited dxBarManager1: TdxBarManager
    Left = 644
    Top = 192
    PixelsPerInch = 96
  end
  inherited FormStorage1: TFormStorage
    Left = 636
    Top = 256
  end
  inherited ActionList_base: TActionList
    Left = 632
    Top = 320
  end
  object t_TerminalType: TADOTable
    Connection = dmMain.ADOConnection1
    CursorType = ctStatic
    TableName = 'lib.TerminalType'
    Left = 640
    Top = 56
  end
  object ds_TerminalType: TDataSource
    DataSet = t_TerminalType
    Left = 640
    Top = 104
  end
end
