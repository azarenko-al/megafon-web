﻿unit fr_TerminalType_WiMax;

interface

uses
  dm_Main,
  u_cx_ini,


  fr_View_base,

  System.Classes,
  Vcl.Controls, Vcl.Forms,


  Data.DB, Data.Win.ADODB, cxGridLevel,
  cxGridCustomTableView, cxGridDBTableView,
  cxGrid, cxDBVGrid, cxSplitter, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, cxStyles, cxCustomData, cxFilter, cxData,
  cxDataStorage, cxEdit, cxNavigator, dxDateRanges, cxDBData, cxVGrid,
  cxInplaceContainer, cxGridTableView, cxGridCustomView, System.Actions,
  Vcl.ActnList, RxPlacemnt, cxClasses, dxBar;

type
  Tframe_TerminalTYpe_WiMax = class(Tframe_View_base)
    cxGrid1DBTableView1: TcxGridDBTableView;
    cxGrid1Level1: TcxGridLevel;
    cxGrid1: TcxGrid;
    t_TerminalType: TADOTable;
    ds_TerminalType: TDataSource;
    cxDBVerticalGrid1: TcxDBVerticalGrid;
    cxSplitter1: TcxSplitter;
    cxGrid1DBTableView1id: TcxGridDBColumn;
    cxGrid1DBTableView1name: TcxGridDBColumn;
    cxGrid1DBTableView1loss: TcxGridDBColumn;
    cxGrid1DBTableView1antennagain: TcxGridDBColumn;
    cxGrid1DBTableView1height: TcxGridDBColumn;
    cxGrid1DBTableView1power: TcxGridDBColumn;
    cxGrid1DBTableView1sense: TcxGridDBColumn;
    cxDBVerticalGrid1id: TcxDBEditorRow;
    cxDBVerticalGrid1name: TcxDBEditorRow;
    cxDBVerticalGrid1loss: TcxDBEditorRow;
    cxDBVerticalGrid1antennagain: TcxDBEditorRow;
    cxDBVerticalGrid1height: TcxDBEditorRow;
    cxDBVerticalGrid1power: TcxDBEditorRow;
    cxDBVerticalGrid1sense: TcxDBEditorRow;
    cxDBVerticalGrid1DNA_width: TcxDBEditorRow;
    cxDBVerticalGrid1noise: TcxDBEditorRow;
    cxDBVerticalGrid1suppression: TcxDBEditorRow;
    cxDBVerticalGrid1net_access_level: TcxDBEditorRow;
    cxDBVerticalGrid1weight: TcxDBEditorRow;
    cxDBVerticalGrid1comments: TcxDBEditorRow;
    cxDBVerticalGrid1NetStandard_id: TcxDBEditorRow;
    cxDBVerticalGrid1SignalNoise: TcxDBEditorRow;
    cxDBVerticalGrid1guid: TcxDBEditorRow;
    cxDBVerticalGrid1date_created: TcxDBEditorRow;
    cxDBVerticalGrid1date_modify: TcxDBEditorRow;
    cxDBVerticalGrid1user_created: TcxDBEditorRow;
    cxDBVerticalGrid1user_modify: TcxDBEditorRow;
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

//var
//  fra_TerminalTYpe: Tfra_TerminalTYpe;

implementation

{$R *.dfm}

procedure Tframe_TerminalTYpe_WiMax.FormCreate(Sender: TObject);
begin
  Caption:='WiMAX абонентские терминалы (WiMAX АТ)';

//  Caption:='WiMAX àáîíåíòñêèå òåðìèíàëû (WiMAX ÀÒ)';
  cxGrid1.Align:=alClient;

  dmMain.OpenTable(t_TerminalType);


  cx_LoadColumnCaptions_FromIni(cxGrid1DBTableView1);
  cx_LoadColumnCaptions_FromIni(cxDBVerticalGrid1);

  cxGrid1DBTableView1.Navigator.Visible:=True;


  {
  cx_Grid_SetColumnCaptions(cxGrid1DBTableView1,
   [
     'NAME', 'Íàçâàíèå',
     'height', 'Âûñîòà àíòåííû, m',
     'power', 'Ìîùíîñòü, W',
     'sense', '×óâñòâèòåëüíîñòü, dB'
   ]);



  cx_DBVerticalGrid_SetCaptions(cxDBVerticalGrid1,
   [
     'NAME', 'Íàçâàíèå',
     'height', 'Âûñîòà àíòåííû, m',
     'power', 'Ìîùíîñòü, W',
     'sense', '×óâñòâèòåëüíîñòü, dB'
   ]);
   }


//   Òåðìèíàëû - Àáîíåíòñêèå òåðìèíàëû
//Íàçâàíèÿ ñòîëáöîâ:     Íàçâàíèå/ Âûñîòà àíòåííû, m/ Ìîùíîñòü, W

end;

end.
