inherited frame_Map: Tframe_Map
  Caption = 'frame_Map'
  ClientHeight = 803
  ClientWidth = 1079
  OnDestroy = FormDestroy
  OnShow = FormShow
  ExplicitWidth = 1095
  ExplicitHeight = 842
  PixelsPerInch = 96
  TextHeight = 13
  object Splitter3: TSplitter [0]
    Left = 289
    Top = 56
    Height = 480
    ExplicitLeft = 353
    ExplicitTop = 23
    ExplicitHeight = 585
  end
  object Panel1: TPanel [1]
    Left = 0
    Top = 56
    Width = 289
    Height = 480
    Align = alLeft
    BevelOuter = bvLowered
    TabOrder = 0
    object Splitter1: TSplitter
      Left = 1
      Top = 317
      Width = 287
      Height = 3
      Cursor = crVSplit
      Align = alBottom
      ExplicitTop = 352
    end
    object cxDBVerticalGrid1: TcxDBVerticalGrid
      Left = 1
      Top = 225
      Width = 287
      Height = 120
      Align = alTop
      OptionsView.RowHeaderWidth = 198
      Navigator.Buttons.CustomButtons = <>
      TabOrder = 0
      DataController.DataSource = ds_Map
      Version = 1
      object cxDBVerticalGrid1CategoryRow3: TcxCategoryRow
        Properties.Caption = #1043#1048#1057
        ID = 0
        ParentID = -1
        Index = 0
        Version = 1
      end
      object cxDBVerticalGrid1DBEditorRow2: TcxDBEditorRow
        Properties.DataBinding.FieldName = 'rel_height'
        ID = 1
        ParentID = 0
        Index = 0
        Version = 1
      end
      object row_clutter_code: TcxDBEditorRow
        Properties.DataBinding.FieldName = 'clutter_code'
        ID = 2
        ParentID = 0
        Index = 1
        Version = 1
      end
      object row_clutter_height: TcxDBEditorRow
        Properties.DataBinding.FieldName = 'clutter_height'
        ID = 3
        ParentID = 0
        Index = 2
        Version = 1
      end
      object row_clutter_name: TcxDBEditorRow
        Properties.DataBinding.FieldName = 'clutter_name'
        Visible = False
        ID = 4
        ParentID = 0
        Index = 3
        Version = 1
      end
      object row_map_building_height: TcxDBEditorRow
        Properties.DataBinding.FieldName = 'map_building_height'
        ID = 5
        ParentID = 0
        Index = 4
        Version = 1
      end
    end
    object PageControl_Building: TPageControl
      Left = 1
      Top = 320
      Width = 287
      Height = 159
      ActivePage = TabSheet1
      Align = alBottom
      TabOrder = 1
      object TabSheet1: TTabSheet
        Caption = #1058#1086#1095#1082#1080' '#1079#1076#1072#1085#1080#1103
        object cxGrid_: TcxGrid
          Left = 0
          Top = 0
          Width = 279
          Height = 131
          Align = alClient
          TabOrder = 0
          object cxGrid_DBTableView1: TcxGridDBTableView
            Navigator.Buttons.CustomButtons = <>
            DataController.DataSource = ds_DataSource_House_Points
            DataController.Summary.DefaultGroupSummaryItems = <>
            DataController.Summary.FooterSummaryItems = <
              item
                Kind = skCount
                Column = cxGrid_DBTableView1lat
              end>
            DataController.Summary.SummaryGroups = <>
            OptionsView.Footer = True
            OptionsView.GroupByBox = False
            object cxGrid_DBTableView1lat: TcxGridDBColumn
              DataBinding.FieldName = 'lat'
              Width = 79
            end
            object cxGrid_DBTableView1lon: TcxGridDBColumn
              DataBinding.FieldName = 'lon'
              Width = 80
            end
            object cxGrid_DBTableView1height: TcxGridDBColumn
              DataBinding.FieldName = 'height'
              Width = 45
            end
          end
          object cxGrid_Level1: TcxGridLevel
            GridView = cxGrid_DBTableView1
          end
        end
      end
    end
    object cxDBVerticalGrid2: TcxDBVerticalGrid
      Left = 1
      Top = 1
      Width = 287
      Height = 224
      Align = alTop
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      LookAndFeel.ScrollbarMode = sbmClassic
      OptionsView.RowHeaderWidth = 168
      Navigator.Buttons.CustomButtons = <>
      ParentFont = False
      TabOrder = 2
      DataController.DataSource = ds_Task
      Version = 1
      object cxCategoryRow1: TcxCategoryRow
        Properties.Caption = #1050#1083#1080#1077#1085#1090
        ID = 0
        ParentID = -1
        Index = 0
        Version = 1
      end
      object cxDBVerticalGrid2DBEditorRow1: TcxDBEditorRow
        Properties.DataBinding.FieldName = 'name'
        ID = 1
        ParentID = 0
        Index = 0
        Version = 1
      end
      object cxDBEditorRow1: TcxDBEditorRow
        Properties.DataBinding.FieldName = 'client_lat'
        ID = 2
        ParentID = 0
        Index = 1
        Version = 1
      end
      object cxDBEditorRow2: TcxDBEditorRow
        Properties.DataBinding.FieldName = 'client_lon'
        ID = 3
        ParentID = 0
        Index = 2
        Version = 1
      end
      object cxDBEditorRow3: TcxDBEditorRow
        Properties.DataBinding.FieldName = 'client_antenna_lat'
        ID = 4
        ParentID = 0
        Index = 3
        Version = 1
      end
      object cxDBEditorRow4: TcxDBEditorRow
        Height = 21
        Properties.DataBinding.FieldName = 'client_antenna_lon'
        ID = 5
        ParentID = 0
        Index = 4
        Version = 1
      end
      object cxDBEditorRow5: TcxDBEditorRow
        Properties.DataBinding.FieldName = 'client_antenna_height'
        ID = 6
        ParentID = 0
        Index = 5
        Version = 1
      end
      object cxDBEditorRow6: TcxDBEditorRow
        Properties.EditPropertiesClassName = 'TcxCheckBoxProperties'
        Properties.EditProperties.NullStyle = nssUnchecked
        Properties.EditProperties.UseAlignmentWhenInplace = True
        Properties.DataBinding.FieldName = 'is_can_change_antenna_height'
        ID = 7
        ParentID = 0
        Index = 6
        Version = 1
      end
      object cxDBEditorRow7: TcxDBEditorRow
        Properties.EditPropertiesClassName = 'TcxCheckBoxProperties'
        Properties.EditProperties.NullStyle = nssUnchecked
        Properties.EditProperties.UseAlignmentWhenInplace = True
        Properties.DataBinding.FieldName = 'is_can_change_antenna_location'
        ID = 8
        ParentID = 0
        Index = 7
        Version = 1
      end
      object cxDBEditorRow8: TcxDBEditorRow
        Properties.DataBinding.FieldName = 'bitrate_Mbps'
        ID = 9
        ParentID = 0
        Index = 8
        Version = 1
      end
      object cxDBEditorRow9: TcxDBEditorRow
        Properties.EditPropertiesClassName = 'TcxComboBoxProperties'
        Properties.EditProperties.Items.Strings = (
          'WiMax'
          #1056#1056#1051)
        Properties.DataBinding.FieldName = 'Technology'
        ID = 10
        ParentID = 0
        Index = 9
        Version = 1
      end
      object cxDBVerticalGrid2DBEditorRow2: TcxDBEditorRow
        Properties.DataBinding.FieldName = 'id'
        Visible = False
        ID = 11
        ParentID = -1
        Index = 1
        Version = 1
      end
    end
  end
  object ToolBar1: TToolBar [2]
    Left = 0
    Top = 0
    Width = 1079
    Height = 29
    BorderWidth = 1
    ButtonHeight = 21
    Caption = 'ToolBar1'
    TabOrder = 1
    object Button8: TButton
      Left = 0
      Top = 0
      Width = 153
      Height = 21
      Action = act_Tool_select_site_custom
      TabOrder = 8
    end
    object Button1: TButton
      Left = 153
      Top = 0
      Width = 105
      Height = 21
      Action = act_Tool_select_site
      TabOrder = 2
    end
    object Button5: TButton
      Left = 258
      Top = 0
      Width = 80
      Height = 21
      Action = act_Tool_select_client
      TabOrder = 6
    end
    object Button2: TButton
      Left = 338
      Top = 0
      Width = 80
      Height = 21
      Action = act_Tool_select_client_ant
      TabOrder = 1
    end
    object Button6: TButton
      Left = 418
      Top = 0
      Width = 80
      Height = 21
      Action = act_Tool_Point
      TabOrder = 7
    end
    object Button_Profile: TButton
      Left = 498
      Top = 0
      Width = 80
      Height = 21
      Action = act_Tool_Profile_111
      TabOrder = 0
      Visible = False
    end
    object Button3: TButton
      Left = 578
      Top = 0
      Width = 80
      Height = 21
      Caption = 'Calc'
      TabOrder = 3
      OnClick = Button3Click
    end
    object Button4: TButton
      Left = 658
      Top = 0
      Width = 120
      Height = 21
      Action = act_Calc_to_ApiService
      TabOrder = 4
    end
    object ed_Response: TEdit
      Left = 778
      Top = 0
      Width = 121
      Height = 21
      TabOrder = 5
    end
    object Button9: TButton
      Left = 899
      Top = 0
      Width = 75
      Height = 21
      Caption = 'Button9'
      TabOrder = 9
      OnClick = Button9Click
    end
  end
  object PageControl_PLaces: TPageControl [3]
    Left = 0
    Top = 544
    Width = 1079
    Height = 259
    ActivePage = Places
    Align = alBottom
    TabOrder = 2
    object Places: TTabSheet
      Caption = 'Places'
      object ToolBar2: TToolBar
        Left = 0
        Top = 0
        Width = 1071
        Height = 33
        BorderWidth = 1
        ButtonHeight = 21
        Caption = 'ToolBar2'
        TabOrder = 0
        object Clear: TButton
          AlignWithMargins = True
          Left = 0
          Top = 0
          Width = 73
          Height = 21
          Action = act_Place_del
          TabOrder = 0
        end
        object btn_Draw: TButton
          Left = 73
          Top = 0
          Width = 52
          Height = 21
          Caption = 'Draw'
          TabOrder = 1
          OnClick = btn_DrawClick
        end
        object ed_Radius: TEdit
          Left = 125
          Top = 0
          Width = 60
          Height = 21
          TabOrder = 2
          Text = '2'
        end
        object Button7: TButton
          Left = 185
          Top = 0
          Width = 100
          Height = 21
          Action = act_select_by_radius
          TabOrder = 3
        end
        object Button10: TButton
          Left = 285
          Top = 0
          Width = 75
          Height = 21
          Caption = 'DrawMap'
          TabOrder = 4
          OnClick = Button10Click
        end
      end
      object cxGrid_places: TcxGrid
        Left = 0
        Top = 33
        Width = 1017
        Height = 198
        Align = alLeft
        TabOrder = 1
        LookAndFeel.ScrollbarMode = sbmClassic
        object cxGrid_placesDBTableView1: TcxGridDBTableView
          PopupMenu = PopupMenu_Places
          Navigator.Buttons.CustomButtons = <>
          DataController.DataSource = ds_Task_place
          DataController.KeyFieldNames = 'id'
          DataController.Summary.DefaultGroupSummaryItems = <>
          DataController.Summary.FooterSummaryItems = <
            item
              Kind = skCount
              Column = col_place_name
            end>
          DataController.Summary.SummaryGroups = <>
          OptionsCustomize.ColumnsQuickCustomization = True
          OptionsCustomize.ColumnsQuickCustomizationShowCommands = False
          OptionsData.Editing = False
          OptionsData.Inserting = False
          OptionsSelection.MultiSelect = True
          OptionsView.Footer = True
          OptionsView.GroupByBox = False
          OptionsView.HeaderAutoHeight = True
          OptionsView.Indicator = True
          object cxGrid_placesDBTableView1Column5: TcxGridDBColumn
            DataBinding.FieldName = 'id'
            Width = 50
          end
          object col_place_name: TcxGridDBColumn
            DataBinding.FieldName = 'place_name'
            Width = 87
          end
          object cxGrid_placesDBTableView1place_name_rus: TcxGridDBColumn
            DataBinding.FieldName = 'place_name_rus'
            Width = 87
          end
          object cxGrid_placesDBTableView1place_lat: TcxGridDBColumn
            DataBinding.FieldName = 'place_lat'
          end
          object cxGrid_placesDBTableView1place_lon: TcxGridDBColumn
            DataBinding.FieldName = 'place_lon'
          end
          object cxGrid_placesDBTableView1place_height: TcxGridDBColumn
            DataBinding.FieldName = 'place_height'
            Visible = False
          end
          object cxGrid_placesDBTableView1map_building_height: TcxGridDBColumn
            DataBinding.FieldName = 'map_building_height'
            Visible = False
          end
          object cxGrid_placesDBTableView1result_client_antenna_lat: TcxGridDBColumn
            DataBinding.FieldName = 'result_client_antenna_lat'
            Visible = False
          end
          object cxGrid_placesDBTableView1result_client_antenna_lon: TcxGridDBColumn
            DataBinding.FieldName = 'result_client_antenna_lon'
            Visible = False
          end
          object cxGrid_placesDBTableView1result_client_antenna_height: TcxGridDBColumn
            DataBinding.FieldName = 'result_client_antenna_height'
            Visible = False
          end
          object cxGrid_placesDBTableView1result_ClientHWTypeID: TcxGridDBColumn
            DataBinding.FieldName = 'result_ClientHWTypeID'
            Visible = False
            Width = 131
          end
          object cxGrid_placesDBTableView1comment: TcxGridDBColumn
            DataBinding.FieldName = 'comment'
            Visible = False
          end
          object col_length_to_client_km: TcxGridDBColumn
            DataBinding.FieldName = 'length_to_client_km'
            Visible = False
          end
          object col_result_msg: TcxGridDBColumn
            DataBinding.FieldName = 'result_msg'
            Visible = False
            Width = 119
          end
          object col_result_code: TcxGridDBColumn
            DataBinding.FieldName = 'result_code'
            Visible = False
          end
          object ol_wm_Azimuth: TcxGridDBColumn
            Caption = 'wm Azimuth'
            DataBinding.FieldName = 'wm_Azimuth'
          end
          object cxGrid_placesDBTableView1Column1: TcxGridDBColumn
            Caption = 'azimuth WiMax -> client'
            DataBinding.FieldName = 'Azimuth_Site_to_Client'
            Width = 69
          end
          object cxGrid_placesDBTableView1Column2: TcxGridDBColumn
            Caption = 'Azimuth diff'
            DataBinding.FieldName = 'Azimuth_diff'
          end
          object cxGrid_placesDBTableView1Column3: TcxGridDBColumn
            Caption = 'Azimuth Client -> WiMax'
            DataBinding.FieldName = 'Azimuth_Client_to_Site'
          end
          object cxGrid_placesDBTableView1Column4: TcxGridDBColumn
            AlternateCaption = 'wm_Is_ready'
            Caption = 'wm Is ready'
            DataBinding.FieldName = 'wm_Is_ready'
            PropertiesClassName = 'TcxCheckBoxProperties'
            Properties.NullStyle = nssUnchecked
          end
        end
        object cxGrid_placesLevel1: TcxGridLevel
          GridView = cxGrid_placesDBTableView1
        end
      end
    end
    object TabSheet2: TTabSheet
      Caption = 'Tasks'
      ImageIndex = 1
      object cxGrid_task: TcxGrid
        Left = 0
        Top = 0
        Width = 913
        Height = 231
        Align = alLeft
        TabOrder = 0
        object cxGridDBTableView1: TcxGridDBTableView
          Navigator.Buttons.CustomButtons = <>
          DataController.DataSource = ds_Task
          DataController.Summary.DefaultGroupSummaryItems = <>
          DataController.Summary.FooterSummaryItems = <>
          DataController.Summary.SummaryGroups = <>
          OptionsView.GroupByBox = False
          OptionsView.Indicator = True
          object cxGridDBTableView1id: TcxGridDBColumn
            DataBinding.FieldName = 'id'
            Width = 47
          end
          object col_name: TcxGridDBColumn
            DataBinding.FieldName = 'name'
            Width = 97
          end
          object cxGridDBTableView1client_lat: TcxGridDBColumn
            DataBinding.FieldName = 'client_lat'
          end
          object cxGridDBTableView1client_lon: TcxGridDBColumn
            DataBinding.FieldName = 'client_lon'
          end
          object cxGridDBTableView1client_antenna_lat: TcxGridDBColumn
            DataBinding.FieldName = 'client_antenna_lat'
          end
          object cxGridDBTableView1client_antenna_lon: TcxGridDBColumn
            DataBinding.FieldName = 'client_antenna_lon'
          end
          object cxGridDBTableView1client_antenna_height: TcxGridDBColumn
            DataBinding.FieldName = 'client_antenna_height'
          end
          object cxGridDBTableView1bitrate_Mbps: TcxGridDBColumn
            DataBinding.FieldName = 'bitrate_Mbps'
          end
          object cxGridDBTableView1is_can_change_antenna_height: TcxGridDBColumn
            DataBinding.FieldName = 'is_can_change_antenna_height'
          end
          object cxGridDBTableView1is_can_change_antenna_location: TcxGridDBColumn
            DataBinding.FieldName = 'is_can_change_antenna_location'
          end
          object cxGridDBTableView1ClientHWTypeID: TcxGridDBColumn
            DataBinding.FieldName = 'ClientHWTypeID'
          end
          object cxGridDBTableView1Technology: TcxGridDBColumn
            DataBinding.FieldName = 'Technology'
          end
          object cxGridDBTableView1_: TcxGridDBColumn
            DataBinding.FieldName = '_'
          end
          object cxGridDBTableView1user_created: TcxGridDBColumn
            DataBinding.FieldName = 'user_created'
          end
        end
        object cxGridLevel1: TcxGridLevel
          GridView = cxGridDBTableView1
        end
      end
    end
  end
  object cxSplitter1: TcxSplitter [4]
    Left = 0
    Top = 536
    Width = 1079
    Height = 8
    HotZoneClassName = 'TcxSimpleStyle'
    AlignSplitter = salBottom
    Control = PageControl_PLaces
  end
  inherited dxBarManager1: TdxBarManager
    Left = 788
    Top = 80
    PixelsPerInch = 96
    DockControlHeights = (
      0
      0
      27
      0)
    inherited dxBarManager1Bar1: TdxBar
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarSubItem2'
        end>
      Visible = True
    end
    object dxBarSubItem2: TdxBarSubItem
      Caption = #1042#1099#1073#1088#1090#1100' '#1085#1072' '#1082#1072#1088#1090#1077
      Category = 0
      Visible = ivAlways
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarButton1'
        end
        item
          Visible = True
          ItemName = 'dxBarButton2'
        end
        item
          BeginGroup = True
          Visible = True
          ItemName = 'dxBarButton3'
        end
        item
          Visible = True
          ItemName = 'dxBarButton4'
        end
        item
          BeginGroup = True
          Visible = True
          ItemName = 'dxBarButton5'
        end>
    end
    object dxBarButton1: TdxBarButton
      Action = act_Tool_select_site
      Category = 0
    end
    object dxBarButton2: TdxBarButton
      Action = act_Tool_select_site_custom
      Category = 0
    end
    object dxBarButton3: TdxBarButton
      Action = act_Tool_select_client
      Category = 0
    end
    object dxBarButton4: TdxBarButton
      Action = act_Tool_select_client_ant
      Category = 0
    end
    object dxBarButton5: TdxBarButton
      Action = act_Tool_Point
      Category = 0
    end
  end
  inherited FormStorage1: TFormStorage
    Options = [fpPosition]
    StoredProps.Strings = (
      'PageControl_Building.Height'
      'Panel1.Left'
      'PageControl_PLaces.Height'
      'ed_Radius.Text'
      'PageControl_PLaces.ActivePage')
    Left = 788
    Top = 136
  end
  inherited ActionList_base: TActionList
    OnUpdate = ActionList_baseUpdate
    Left = 368
    Top = 264
    object act_Tool_Profile_111: TAction
      Category = 'Tools'
      Caption = #1055#1088#1086#1092#1080#1083#1100
      OnExecute = act_Tool_Profile_111Execute
    end
    object act_Tool_select_site: TAction
      Category = 'Tools'
      Caption = #1057#1090#1072#1085#1094#1080#1103' '#1052#1077#1075#1072#1092#1086#1085
      OnExecute = act_Tool_Profile_111Execute
    end
    object act_Tool_select_site_custom: TAction
      Category = 'Tools'
      Caption = #1057#1090#1072#1085#1094#1080#1103' '#1052#1077#1075#1072#1092#1086#1085' custom'
      OnExecute = act_Tool_Profile_111Execute
    end
    object act_Tool_select_client: TAction
      Category = 'Tools'
      Caption = #1050#1083#1080#1077#1085#1090
      OnExecute = act_Tool_Profile_111Execute
    end
    object act_Tool_select_client_ant: TAction
      Category = 'Tools'
      Caption = #1050#1083#1080#1077#1085#1090' '#1072#1085#1090
      OnExecute = act_Tool_Profile_111Execute
    end
    object act_Tool_Point: TAction
      Category = 'Tools'
      Caption = #1058#1086#1095#1082#1072
      OnExecute = act_Tool_Profile_111Execute
    end
    object act_Calc_to_ApiService: TAction
      Caption = 'Calc_to_ApiService'
      OnExecute = act_Calc_to_ApiServiceExecute
    end
    object act_Places_clear: TAction
      Category = 'Places'
      Caption = 'Places_clear'
    end
    object act_Client_Ant_clear: TAction
      Caption = 'act_Client_Ant_clear'
      OnExecute = act_Client_Ant_clearExecute
    end
    object act_select_by_radius: TAction
      Category = 'Places'
      Caption = 'select_by_radius'
      OnExecute = act_select_by_radiusExecute
    end
    object act_Calc: TAction
      Caption = 'act_Calc'
    end
    object act_Place_del: TAction
      Category = 'Places'
      Caption = 'Place_del'
      OnExecute = act_Place_delExecute
    end
  end
  object dxMemData_map: TdxMemData
    Indexes = <>
    Persistent.Data = {
      5665728FC2F5285C8FFE3F080000000800000006001200636C69656E745F6D61
      705F686569676874000200000002000D00636C75747465725F636F6465000200
      000002000F00636C75747465725F686569676874001400000001000D00636C75
      747465725F6E616D650001000000010008005F5F5F5F5F5F5F00020000000200
      0D006C6F6769635F6865696768740002000000050018006C6F6769635F69735F
      7465726D696E6174655F63616C630008000000060014006D61705F6275696C64
      696E675F68656967687400000000000000000000000000000000000000000000
      0000000000000000000000}
    SortOptions = []
    Left = 360
    Top = 80
    object dxMemData_mapclient_map_height: TFloatField
      FieldName = 'client_map_height'
    end
    object dxMemData_mapclutter_code: TSmallintField
      FieldName = 'clutter_code'
    end
    object dxMemData_mapclutter_height: TSmallintField
      FieldName = 'clutter_height'
    end
    object dxMemData_mapclutter_name: TStringField
      FieldName = 'clutter_name'
    end
    object dxMemData_map_______: TStringField
      FieldName = '_______'
      Size = 1
    end
    object dxMemData_maplogic_height: TSmallintField
      FieldName = 'logic_height'
    end
    object dxMemData_maplogic_is_terminate_calc: TBooleanField
      FieldName = 'logic_is_terminate_calc'
    end
    object dxMemData_mapmap_building_height: TFloatField
      FieldName = 'map_building_height'
    end
    object dxMemData_maprel_height: TIntegerField
      FieldName = 'rel_height'
    end
  end
  object ds_Map: TDataSource
    DataSet = dxMemData_map
    Left = 360
    Top = 136
  end
  object t_Task: TADOTable
    Connection = dmMain.ADOConnection1
    CursorType = ctStatic
    TableName = 'Admin_Panel.view_Task'
    Left = 501
    Top = 80
  end
  object ds_Task: TDataSource
    DataSet = t_Task
    Left = 501
    Top = 136
  end
  object v_Task_places: TADOTable
    Connection = dmMain.ADOConnection1
    CursorType = ctStatic
    IndexFieldNames = 'task_id'
    MasterFields = 'id'
    MasterSource = ds_Task
    TableName = 'Admin_Panel.view_Task_places'
    Left = 597
    Top = 80
  end
  object ds_Task_place: TDataSource
    DataSet = v_Task_places
    Left = 597
    Top = 136
  end
  object ADOStoredProc1: TADOStoredProc
    Parameters = <>
    Left = 360
    Top = 392
  end
  object sp_House_points_By_LatLon: TADOStoredProc
    Connection = dmMain.ADOConnection1
    CursorType = ctStatic
    LockType = ltReadOnly
    ProcedureName = 'map.sp_House_points_By_LatLon'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = 4
      end
      item
        Name = '@LAT'
        Attributes = [paNullable]
        DataType = ftFloat
        Precision = 15
        Value = Null
      end
      item
        Name = '@LON'
        Attributes = [paNullable]
        DataType = ftFloat
        Precision = 15
        Value = Null
      end>
    Left = 824
    Top = 280
  end
  object ds_DataSource_House_Points: TDataSource
    DataSet = sp_House_points_By_LatLon
    Left = 824
    Top = 336
  end
  object PopupMenu_Places: TPopupMenu
    Left = 328
    Top = 696
    object actPlacedel1: TMenuItem
      Action = act_Place_del
    end
  end
end
