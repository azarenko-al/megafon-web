﻿unit fr_Map;

interface

uses
  u_log_,
  dm_Main,

  u_Config,
  dm_Map,
  u_geo,
  fr_Map_view,
  u_func,

  u_cx,
  d_Task,

//  u_Task_send_to_Remote_MG,
  u_Task_send_to_Local_MG,
  u_Task_send_to_local_ApiService,

  dm_Task_Calc,
  u_Task_classes,

  u_db,
  dm_Relief,

  I_rel_Matrix1,

  fr_View_base,

  System.SysUtils, System.Variants, System.Classes,  Vcl.Controls, Vcl.Forms,
  Vcl.ActnList, Vcl.ExtCtrls, Data.DB, dxmdaset,  cxVGrid, cxDBVGrid,
  Vcl.ComCtrls, Vcl.StdCtrls,  RxPlacemnt, dxBar,  cxSplitter, Data.Win.ADODB,
  cxGridDBTableView, cxGridLevel, cxGrid, cxGraphics, cxControls,
  cxLookAndFeels, cxLookAndFeelPainters, cxStyles, cxEdit, cxCheckBox,
  cxDropDownEdit, cxCustomData, cxFilter, cxData, cxDataStorage, cxNavigator,
  dxDateRanges, cxDBData, System.Actions, Vcl.ToolWin, cxGridCustomTableView,
  cxGridTableView, cxGridCustomView, cxInplaceContainer, cxClasses, Vcl.Menus;

type
  Tframe_Map = class(Tframe_View_base)
    Panel1: TPanel;
    dxMemData_map: TdxMemData;
    dxMemData_mapclient_map_height: TFloatField;
    ds_Map: TDataSource;
    cxDBVerticalGrid1: TcxDBVerticalGrid;
    PageControl_Building: TPageControl;
    TabSheet1: TTabSheet;
    Splitter1: TSplitter;
    ToolBar1: TToolBar;
    Button_Profile: TButton;
    dxMemData_mapclutter_code: TSmallintField;
    dxMemData_mapclutter_height: TSmallintField;
    row_clutter_code: TcxDBEditorRow;
    cxDBVerticalGrid1CategoryRow3: TcxCategoryRow;
    row_clutter_height: TcxDBEditorRow;
    cxDBVerticalGrid1DBEditorRow2: TcxDBEditorRow;
    act_Tool_Profile_111: TAction;
    Button2: TButton;
    act_Tool_select_site: TAction;
    act_Tool_select_client: TAction;
    act_Tool_Point: TAction;
    dxMemData_mapclutter_name: TStringField;
    row_clutter_name: TcxDBEditorRow;
    Button1: TButton;
    dxMemData_maplogic_height: TSmallintField;
    dxMemData_maplogic_is_terminate_calc: TBooleanField;
    dxMemData_map_______: TStringField;
    Button3: TButton;
    dxMemData_mapmap_building_height: TFloatField;
    row_map_building_height: TcxDBEditorRow;
    Button4: TButton;
    act_Calc_to_ApiService: TAction;
    ed_Response: TEdit;
    act_Places_clear: TAction;
    Button5: TButton;
    act_Tool_select_client_ant: TAction;
    Button6: TButton;
    PageControl_PLaces: TPageControl;
    Places: TTabSheet;
    ToolBar2: TToolBar;
    Clear: TButton;
    Splitter3: TSplitter;
    act_Client_Ant_clear: TAction;
    btn_Draw: TButton;
    ed_Radius: TEdit;
    Button7: TButton;
    act_select_by_radius: TAction;
    cxSplitter1: TcxSplitter;
    TabSheet2: TTabSheet;
    t_Task: TADOTable;
    ds_Task: TDataSource;
    v_Task_places: TADOTable;
    ds_Task_place: TDataSource;
    cxDBVerticalGrid2: TcxDBVerticalGrid;
    cxCategoryRow1: TcxCategoryRow;
    cxDBEditorRow1: TcxDBEditorRow;
    cxDBEditorRow2: TcxDBEditorRow;
    cxDBEditorRow3: TcxDBEditorRow;
    cxDBEditorRow4: TcxDBEditorRow;
    cxDBEditorRow5: TcxDBEditorRow;
    cxDBEditorRow6: TcxDBEditorRow;
    cxDBEditorRow7: TcxDBEditorRow;
    cxDBEditorRow8: TcxDBEditorRow;
    cxDBEditorRow9: TcxDBEditorRow;
    cxDBVerticalGrid2DBEditorRow1: TcxDBEditorRow;
    act_Tool_select_site_custom: TAction;
    Button8: TButton;
    dxBarSubItem2: TdxBarSubItem;
    dxBarButton1: TdxBarButton;
    dxBarButton2: TdxBarButton;
    dxBarButton3: TdxBarButton;
    dxBarButton4: TdxBarButton;
    dxBarButton5: TdxBarButton;
    Button9: TButton;
    dxMemData_maprel_height: TIntegerField;
    ADOStoredProc1: TADOStoredProc;
    sp_House_points_By_LatLon: TADOStoredProc;
    ds_DataSource_House_Points: TDataSource;
    cxGrid_placesDBTableView1: TcxGridDBTableView;
    cxGrid_placesLevel1: TcxGridLevel;
    cxGrid_places: TcxGrid;
    cxGrid_placesDBTableView1place_name_rus: TcxGridDBColumn;
    col_place_name: TcxGridDBColumn;
    cxGrid_placesDBTableView1place_lat: TcxGridDBColumn;
    cxGrid_placesDBTableView1place_lon: TcxGridDBColumn;
    cxGrid_placesDBTableView1place_height: TcxGridDBColumn;
    cxGrid_placesDBTableView1map_building_height: TcxGridDBColumn;
    cxGrid_placesDBTableView1result_client_antenna_lat: TcxGridDBColumn;
    cxGrid_placesDBTableView1result_client_antenna_lon: TcxGridDBColumn;
    cxGrid_placesDBTableView1result_client_antenna_height: TcxGridDBColumn;
    cxGrid_placesDBTableView1result_ClientHWTypeID: TcxGridDBColumn;
    cxGrid_placesDBTableView1comment: TcxGridDBColumn;
    cxGrid_task: TcxGrid;
    cxGridDBTableView1: TcxGridDBTableView;
    cxGridLevel1: TcxGridLevel;
    cxGridDBTableView1id: TcxGridDBColumn;
    col_name: TcxGridDBColumn;
    cxGridDBTableView1client_lat: TcxGridDBColumn;
    cxGridDBTableView1client_lon: TcxGridDBColumn;
    cxGridDBTableView1client_antenna_lat: TcxGridDBColumn;
    cxGridDBTableView1client_antenna_lon: TcxGridDBColumn;
    cxGridDBTableView1client_antenna_height: TcxGridDBColumn;
    cxGridDBTableView1bitrate_Mbps: TcxGridDBColumn;
    cxGridDBTableView1is_can_change_antenna_height: TcxGridDBColumn;
    cxGridDBTableView1is_can_change_antenna_location: TcxGridDBColumn;
    cxGridDBTableView1ClientHWTypeID: TcxGridDBColumn;
    cxGridDBTableView1Technology: TcxGridDBColumn;
    cxGridDBTableView1_: TcxGridDBColumn;
    cxGridDBTableView1user_created: TcxGridDBColumn;
    Button10: TButton;
    cxGrid_: TcxGrid;
    cxGrid_DBTableView1: TcxGridDBTableView;
    cxGrid_DBTableView1lat: TcxGridDBColumn;
    cxGrid_DBTableView1lon: TcxGridDBColumn;
    cxGrid_DBTableView1height: TcxGridDBColumn;
    cxGrid_Level1: TcxGridLevel;
    col_length_to_client_km: TcxGridDBColumn;
    col_result_msg: TcxGridDBColumn;
    col_result_code: TcxGridDBColumn;
    act_Calc: TAction;
    PopupMenu_Places: TPopupMenu;
    act_Place_del: TAction;
    actPlacedel1: TMenuItem;
    cxGrid_placesDBTableView1Column1: TcxGridDBColumn;
    cxGrid_placesDBTableView1Column2: TcxGridDBColumn;
    cxGrid_placesDBTableView1Column3: TcxGridDBColumn;
    cxGrid_placesDBTableView1Column4: TcxGridDBColumn;
    cxGrid_placesDBTableView1Column5: TcxGridDBColumn;
    cxDBVerticalGrid2DBEditorRow2: TcxDBEditorRow;
    ol_wm_Azimuth: TcxGridDBColumn;
    procedure ActionList_baseUpdate(Action: TBasicAction; var Handled: Boolean);
    procedure act_Calc_to_ApiServiceExecute(Sender: TObject);
    procedure act_Client_Ant_clearExecute(Sender: TObject);
    procedure act_select_by_radiusExecute(Sender: TObject);
//    procedure act_Places_clearExecute(Sender: TObject);
    procedure act_Place_delExecute(Sender: TObject);
    procedure act_Tool_Profile_111Execute(Sender: TObject);
    procedure Button3Click(Sender: TObject);
    procedure btn_DrawClick(Sender: TObject);
    procedure Button10Click(Sender: TObject);
    procedure Button9Click(Sender: TObject);
    procedure Button_ProfileClick(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
//    procedure Timer1Timer(Sender: TObject);
  type
    TToolType =  (ttNone,
                  ttSelectPoint,

                  ttSelectClient,
                  ttSelectClientAnt,

                  ttSelectSite,
                  ttSelectSite_Custom,

                  ttProfile_Point_1,
                  ttProfile_Point_2
                  ); //, ttProfile);

  private
    FProfile: record
      Point1:  TLatLon;
      Point2:  TLatLon;

    end;

  private
 //   FTask: TTask;


//    FFileName_task: string;
//    FFileName_places: string;

    FMap_view: Tframe_Map_view;

    FProfilePointNumber : integer;
    FProfilePoint1: TLatLon;

    FToolType: TToolType;

    procedure Calc;
//    procedure CalcToLocalApiService;
    procedure SetTool(aTool: TToolType);
    function DB_to_Task: TTask;

    procedure DrawMap(aFit: Boolean = false);

  public
    procedure DoOnMapClick(aLat, aLon: double);
//    procedure Task_to_DB(oTask: TTask);

  end;

//var
//  frame_Map: Tframe_Map;

implementation

{$R *.dfm}

procedure Tframe_Map.ActionList_baseUpdate(Action: TBasicAction; var Handled:
    Boolean);
begin
  act_Place_del.Enabled:=v_Task_places.RecordCount>0;

end;

//uses dm_Main;


//-------------------------------------------------------------------
procedure Tframe_Map.FormCreate(Sender: TObject);
//-------------------------------------------------------------------

var
  s: string;
begin
  SetCaption('Карта');

  PageControl_PLaces.ActivePageIndex:=0;
//
//  t_Task.Open; //active:=true;
//  t_Task_places.Open;
//
//  exit;

  cxGrid_places.Align:=alClient;


  TdmMap.Init;
  TdmRelief.Init;


  s:=FormStorage1.StoredProps.Text;

//  s:=FormStorage1.StoredValues.Create// Store dProps.Text;

//  dxMemData_task.Open;

  cxGrid_task.align:=alClient;
  cxGrid_places.align:=alClient;

  CreateChildForm(FMap_view, Tframe_Map_view, Self);

  FMap_view.OnMapClick_LatLon:= DoOnMapClick;




  cxDBVerticalGrid1.Align:=alClient;

  dxMemData_map.Open;

  cx_Footer(cxGrid_placesDBTableView1, col_place_name);
  cx_Footer(cxGrid_DBTableView1, cxGrid_DBTableView1lat);


  //cxGrid_DBTableView1lat

//  dxMemData_task.Append;
//  dxMemData_task.Post;

//  dxMemData_Places.Close;
//  dxMemData_Places.Open;


//  t_Task.TableName:='Admin_Panel.view_Task';

//  t_Task.Open; //active:=true;
//  t_Task_places.Open;


  dmMain.OpenTable (t_Task );

  if t_Task.IsEmpty then
    db_AddRecord_(t_Task,
      [
        'name', 'my task'
      ]);


  dmMain.OpenTable (v_Task_places);

//  DrawMap (True);



end;

//-------------------------------------------------------------------
procedure Tframe_Map.FormDestroy(Sender: TObject);
//-------------------------------------------------------------------
var
  s: string;
begin
  inherited;

//  s:=FMap_view.GetMapState;

//  FormStorage1.StoredValues.Values['MapState'].Value:=s;

  FreeAndNil(FMap_view);

  //dxMemData_task.SaveToBinaryFile ( FFileName_task );
//  dxMemData_Places.SaveToBinaryFile( FFileName_places );

end;


procedure Tframe_Map.act_Tool_Profile_111Execute(Sender: TObject);
begin
//  if Sender=act_Tool_Profile     then SetTool(ttProfile) else
  if Sender=act_Tool_select_client then SetTool(ttSelectClient) else
  if Sender=act_Tool_select_client_ant then SetTool(ttSelectClientAnt) else

  if Sender=act_Tool_Point then SetTool(ttSelectPoint) else


  if Sender=act_Tool_select_site          then SetTool(ttSelectSite) else
  if Sender=act_Tool_select_site_custom   then SetTool(ttSelectSite_Custom) else

  if Sender=act_Tool_Point         then SetTool(ttNone) else

//  if Sender=act_         then SetTool(ttNone) else


//if Sender=act_Tool_Point         then SetTool(ttNone) else

end;

procedure Tframe_Map.Button3Click(Sender: TObject);
begin
  Calc;
end;

procedure Tframe_Map.Button_ProfileClick(Sender: TObject);
begin
 // SetTool (ttProfile);
end;

//-------------------------------------------------------------------
procedure Tframe_Map.DoOnMapClick(aLat, aLon: double);
//-------------------------------------------------------------------
var
  b: Boolean;
  eHeight: Double;
  k: Integer;
  rec:  Trel_Record;
  rNRI_Site: TdmMap.TNRI_Site;

  ll:  TLatLon;

  rClutter_LOgic: TConfig_Clutter_Logic_rec;
begin

  case FToolType of
{
    ttProfile:   begin
                   if FProfilePointNumber=0 then
                   begin
                     FProfilePointNumber:=1;

                     FProfilePoint1.Lat:=aLat;
                     FProfilePoint1.Lon:=aLon;
                   end else begin
                      FProfilePointNumber:=0;
                      FMap_view.DrawLineProfile_WGS(FProfilePoint1.Lat, FProfilePoint1.Lon, aLat, aLon);
                   end;

                   Exit;
                 end;
 }

    ttSelectClient: begin
                    db_UpdateRecord(t_Task,
                      [
                        'client_lat', aLat,
                        'client_lon', aLon,

                        'client_antenna_lat', null,
                        'client_antenna_lon', null
                      ]);

//                      if t_Task.FieldByName('client_antenna_height').AsFloat=0 then
//                        db_UpdateRecord(t_Task,
//                          [
//                            'client_antenna_height', 10
//                          ]);

                      DrawMap;

                  end;

    ttSelectClientAnt: begin
                    db_UpdateRecord(t_Task,
                      [
                        'client_antenna_lat', aLat,
                        'client_antenna_lon', aLon
                      ]);

                      DrawMap;

                  end;

    ttSelectSite_Custom: begin

      b:= dmMap.FindHousePoints_WGS (MakeBLPoint(aLat, aLon), eHeight) ;

//  db_UpdateRecord(dxMemData_map,
//      [
//        'map_building_height', IIF(b, eHeight, null)
//      ]);
//

         db_AddRecord_(v_Task_places,
              [

                'task_id',    t_Task['id'],
                'place_Lat',  aLat,
                'place_Lon',  aLon,

                'map_building_height', IIF(b, eHeight, null)

              ]);

          DrawMap;
    end;

    ttSelectSite: begin

                    k:=dmMain.StoredProc_Open(ADOStoredProc1, '[Admin_Panel].sp_Place_by_pos_INS',
                      [
                        'task_id', t_Task['id'],
                        'LAT', aLat,
                        'LON', aLon
                      ]);


//                    t_Task_places.Close;
//                    t_Task_places.Open;

                    v_Task_places.Requery([]);
                    DrawMap;

                  end;

  end;


//  if FToolType = ttSelectClient then

{
  k:=dmMap.House_points_By_LatLon(sp_House_points_By_LatLon, aLat, aLon);

  b:= dmMap.FindHousePoints_WGS (MakeBLPoint(aLat, aLon), eHeight) ;

  db_UpdateRecord(dxMemData_map,
      [
        'map_building_height', IIF(b, eHeight, null)
      ]);
 }

  if dmRelief.FindPointByPos_WGS(MakeBLPoint(aLat, aLon), rec) then
  begin
    b:=g_Config.ClutterLogic.TryGetValue(rec.Clutter_Code, rClutter_LOgic);

//
//     Clutter_code : byte;
//    height : double;
//    is_terminate_calc : Boolean;


    db_UpdateRecord(dxMemData_map,
      [
        'rel_height',     rec.Rel_H,
        'clutter_code',   rec.Clutter_Code,
        'clutter_height', rec.Clutter_H
      ]);

  end;

end;

procedure Tframe_Map.SetTool(aTool: TToolType);
begin
  FToolType:=aTool;

//  case aTool of
//    ttProfile: FProfilePointNumber:=0;

//  end;

end;

//-------------------------------------------------------------------
procedure Tframe_Map.act_Calc_to_ApiServiceExecute(Sender: TObject);
//-------------------------------------------------------------------
var
  oTask: TTask;
begin
  oTask:=DB_to_Task();

  ed_Response.Text:= Task_Send_to_Local_ApiService(oTask);
  FreeAndNil(oTask);
end;


procedure Tframe_Map.act_Client_Ant_clearExecute(Sender: TObject);
begin
  inherited;
  //
  if Sender=act_Client_Ant_clear then
  begin
     db_UpdateRecord(t_Task,
                      [
                        'client_antenna_lat', null,
                        'client_antenna_lon', null
                      ]);
    DrawMap;

  end;



end;


procedure Tframe_Map.act_select_by_radiusExecute(Sender: TObject);
var
  k: Integer;
begin
  if Sender = act_select_by_radius then
  begin
//
//    k:=dmMain.OpenStoredProc(ADOStoredProc1, '[Admin_Panel].sp_Place_by_Radius_SEL',
//      [
//        'task_id', t_Task['id'],
//        'LAT', t_Task['client_lat'],
//        'LON', t_Task['client_lon'],
//
//        'radius_km', StrToInt(ed_radius.Text)
//      ]);
//
//    db_View (ADOStoredProc1);



    k:=dmMain.StoredProc_Open(ADOStoredProc1, '[Admin_Panel].sp_Place_by_Radius_INS',
      [
        'task_id', t_Task.FieldByName('id').AsInteger,
        'LAT', t_Task.FieldByName('client_lat').AsFloat,
        'LON', t_Task.FieldByName('client_lon').AsFloat,

        'radius_km', StrToInt(ed_radius.Text)
      ]);

   // t_Task_places.close;
   // t_Task_places.open;
    v_Task_places.Requery([]);

    DrawMap;
  end;

end;


//
////-------------------------------------------------------------------
//procedure Tframe_Map.act_Places_clearExecute(Sender: TObject);
////-------------------------------------------------------------------
//begin
////  dxMemData_Sites.dele
//  if Sender=act_Places_clear then
//  begin
//    t_Task_places.DisableControls;
//
//    with t_Task_places do
//      while not eof do Delete;
//
//    t_Task_places.EnableControls;
//
////    FMap_view.Clear;;
//    DrawMap;
//  end;
//
//
////  dxMemData_Places.Close;
////  dxMemData_Places.Open;
//end;



procedure Tframe_Map.act_Place_delExecute(Sender: TObject);
begin
   dmMain.StoredProc_Exec(ADOStoredProc1, '[Admin_Panel].sp_Task_place_DEL',
      ['id', v_Task_places['id'] ]);

   v_Task_places.Requery([]);
end;

procedure Tframe_Map.btn_DrawClick(Sender: TObject);
begin
  DrawMap;
end;

procedure Tframe_Map.Button10Click(Sender: TObject);
begin
  DrawMap(true);
end;

procedure Tframe_Map.Button9Click(Sender: TObject);
begin
  Task_Send_to_Local_MG_Echo ('12312312') ;

//  FMap_view.GetMapState;

end;

//-------------------------------------------------------------------
procedure Tframe_Map.Calc;
//-------------------------------------------------------------------
var
  oTask: TTask;
  s: string;

begin
//  Task_Send_to_Local_MG_Echo ('12312312') ;
 // exit;


  oTask:=DB_to_Task();

//  Assert(Assigned(dmTask_Profile));

  dmMain.Config_Load_;

  TdmTask_Calc.Init;
//  dmTask_Calc.Pa
  dmTask_Calc.Calc (oTask);

  Tdlg_Task.ExecDlg(oTask.DB_ID);




 // dmTask_calc.Calc(oTask);

//  if Copy (oTask.GUID_str,1,1)='-' then
//  begin
 //   Log ('Send_to_Local_MG');

 //!!!!!!!!!!!!!!!!!
//  if Task_Send_to_Local_MG(oTask, s) then
//    ed_Response.Text:=s;


  FreeAndNil(oTask);

//  Log (sResponse);


end;



//-------------------------------------------------------------------
procedure Tframe_Map.DrawMap(aFit: Boolean = false);
//-------------------------------------------------------------------
var
  k: Integer;
 // oTask: TTask;
  s: string;
 // oPlace:  TTaskPlace;

begin
  TLog.Send ('procedure Tframe_Map.DrawMap');

  db_PostDataset(t_Task);
//  db_PostDatasets(Self);

{
  oTask:=DB_to_Task();

  FMap_view.DrawTask(oTask);

  FreeAndNil(oTask);
 }

  k:=dmMain.StoredProc_Open(ADOStoredProc1, 'Admin_Panel.sp_geoJson_SEL', [ ]);
  if ADOStoredProc1.RecordCount>0 then
  begin
    s:=ADOStoredProc1.Fields[0].Value;
    FMap_view.Load_GeoJSON(s, aFit);

  end;




  {
  Clear;

  FMap_view.DrawMarker_Client (oTask.WGS.client_lat, oTask.WGS.client_lon);

  if oTask.WGS.client_antenna_lat<>0 then
  begin
    FMap_view.DrawMarker_Client_ant (oTask.WGS.client_antenna_lat, oTask.WGS.client_antenna_lon);

    oTask.WGS.client_lat:=oTask.WGS.client_antenna_lat;
    oTask.WGS.client_lon:=oTask.WGS.client_antenna_lon;

  end;


  for oPlace in oTask.Places do
    if oPlace.Pos_WGS.Lat<>0 then
  begin
    FMap_view.DrawLineAndSite
      (oTask.WGS.client_lat,
       oTask.WGS.client_lon,

       oPlace.Pos_WGS.Lat,
       oPlace.Pos_WGS.Lon,

       DEF_ResultType_COLOR [oPlace.CalcResult.ResultType]

       );
  end;
   }

end;


//-------------------------------------------------------------------
function Tframe_Map.DB_to_Task: TTask;
//-------------------------------------------------------------------
var
  oTask: TTask;
  oTaskPlace: TTaskPlace;


begin
  oTask:=TTask.Create;
  Result:=oTask;

  db_PostDataset(t_Task);


  with t_Task do
  begin
    oTask.GUID_str                :='--'+GenerateGUID();

    oTask.wgs.client_lat          := FieldByName('client_lat').AsFloat;
    oTask.wgs.client_lon          := FieldByName('client_lon').AsFloat;

//    Assert(FieldByName('client_lat').AsFloat <> 0);
//    Assert(FieldByName('client_lon').AsFloat <> 0);


    if FieldByName('client_antenna_lat').AsFloat<>0 then
    begin
      oTask.wgs.client_antenna_lat  := FieldByName('client_antenna_lat').AsFloat;
      oTask.wgs.client_antenna_lon  := FieldByName('client_antenna_lon').AsFloat;
    end;

    oTask.client_antenna_height     := FieldByName('client_antenna_height').AsFloat;


    oTask.is_can_change_antenna_height   := FieldByName('is_can_change_antenna_height').AsBoolean;
    oTask.is_can_change_antenna_location := FieldByName('is_can_change_antenna_location').AsBoolean;

    oTask.bitrate_Mbps      := FieldByName('bitrate_Mbps').AsFloat;
    oTask.Technology        := FieldByName('Technology').AsString;
  end;

  //-------------------------------------------------------------------
  v_Task_places.First;

  oTask.Places.Clear;

  with v_Task_places do
    while not EOF do
  begin
    oTaskPlace:=oTask.Places.AddItem;
//    oTaskPlace.ObjectID:= StrToInt64 (oPoint.ObjectID);
//    oTaskPlace.SysName :=oPoint.ObjectTypeSysname;

    oTaskPlace.AdminPanel_DB_ID:=FieldByName('id').AsInteger;




    oTaskPlace.place_name        :=FieldByName('place_name').AsString;
    oTaskPlace.place_name_display:=FieldByName('place_name_rus').AsString + ' ' + FieldByName('place_name').AsString;

   {$IFDEF beeline}
    oTaskPlace.place_name        :=FieldByName('id').AsString;
    oTaskPlace.place_name_display:=FieldByName('id').AsString;

   {$ENDIF}


    oTaskPlace.Pos_wgs.lat:=FieldByName('place_lat').AsFloat;;
    oTaskPlace.Pos_wgs.lon:=FieldByName('place_lon').AsFloat;
    oTaskPlace.height :=FieldByName('place_height').AsFloat;

    oTaskPlace.SetResultStr(FieldByName('result_msg').AsString);


    //aTask.Technology
    oTaskPlace.WM_Is_WiMax    :=Eq(oTask.Technology,'WiMax') and (not FieldByName('WM_Azimuth').IsNull);
    oTaskPlace.WM_Azimuth     :=FieldByName('WM_Azimuth').AsFloat;;
    oTaskPlace.WM_Is_Ready    :=FieldByName('WM_Is_Ready').AsBoolean;

    oTaskPlace.Azimuth_Place_to_Client:=
        geo_Azimuth (MakeBLPoint (oTaskPlace.Pos_wgs.Lat, oTaskPlace.Pos_wgs.Lon),
                     MakeBLPoint (oTask.wgs.client_lat, oTask.wgs.client_lon));

    oTaskPlace.Azimuth_Client_to_Place:=
        geo_Azimuth (MakeBLPoint (oTask.wgs.client_lat, oTask.wgs.client_lon),
                     MakeBLPoint (oTaskPlace.Pos_wgs.Lat, oTaskPlace.Pos_wgs.Lon));


   // oTaskPlace.AdminPanel_DB_ID:=FieldByName('id').AsInteger;

    Assert(FieldByName('place_lat').AsFloat <> 0);
    Assert(FieldByName('place_lon').AsFloat <> 0);


    Next;
  end;

  v_Task_places.First;


 // oCalcRequest.ClientHWTypeID:= '-';
//    oCalcRequest.ClientHWTypeID:= FieldByName('bitrate_Mbps').AsFloat;

//  oCalcRequest.Technology  := FieldByName('Technology').AsString;


//
//
//  Log ('CalcResponse -----------------------------');
//  Log (Format('CalculationCode: %s', [oCalcResponse.CalculationCode]));
//  Log (Format('Message_: %s',        [oCalcResponse.Message_]));
//  Log (Format('Type_: %s',           [oCalcResponse.Type_]));
//

end;

procedure Tframe_Map.FormShow(Sender: TObject);
begin
//  DrawMap (True);
end;




end.


{
procedure Tframe_Map.Task_to_DB(oTask: TTask);
begin

end;


//procedure Tframe_Map.Timer1Timer(Sender: TObject);
//begin
//  TLog.Send ('procedure Tframe_Map.Timer1Timer(Sender: TObject);');
//
//  Timer1.Enabled:=False;
////  DrawMap(true);
//end;
//

