object frame_Map_view: Tframe_Map_view
  Left = 0
  Top = 0
  Caption = 'frame_Map_view'
  ClientHeight = 308
  ClientWidth = 789
  Color = clBtnFace
  UseDockManager = True
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Panel_Test: TPanel
    Left = 0
    Top = 29
    Width = 789
    Height = 44
    Align = alTop
    TabOrder = 0
    Visible = False
    object Button2: TButton
      Left = 133
      Top = 6
      Width = 50
      Height = 25
      Caption = 'go'
      TabOrder = 0
    end
    object Button3: TButton
      Left = 189
      Top = 6
      Width = 75
      Height = 25
      Caption = 'setview'
      TabOrder = 1
      OnClick = Button3Click
    end
    object Button4_DrawLine: TButton
      Left = 270
      Top = 6
      Width = 75
      Height = 25
      Caption = 'DrawLine'
      TabOrder = 2
      OnClick = Button4_DrawLineClick
    end
    object Button1: TButton
      Left = 14
      Top = 6
      Width = 75
      Height = 25
      Caption = 'Test'
      TabOrder = 3
      OnClick = Button1Click
    end
    object Button4: TButton
      Left = 400
      Top = 6
      Width = 75
      Height = 25
      Caption = 'resize'
      TabOrder = 4
      OnClick = Button4Click
    end
    object Button5: TButton
      Left = 496
      Top = 6
      Width = 75
      Height = 25
      Caption = 'Invalidate'
      TabOrder = 5
      OnClick = Button5Click
    end
    object Button6: TButton
      Left = 577
      Top = 6
      Width = 75
      Height = 25
      Caption = 'Button6'
      TabOrder = 6
      OnClick = Button6Click
    end
    object Button7: TButton
      Left = 658
      Top = 6
      Width = 75
      Height = 25
      Caption = 'WM_SIZE'
      TabOrder = 7
      OnClick = Button7Click
    end
  end
  object StatusBar1: TStatusBar
    Left = 0
    Top = 289
    Width = 789
    Height = 19
    Panels = <>
    SimplePanel = True
  end
  object ToolBar1: TToolBar
    Left = 0
    Top = 0
    Width = 789
    Height = 29
    BorderWidth = 1
    ButtonHeight = 21
    Caption = 'ToolBar1'
    TabOrder = 2
    Visible = False
    Wrapable = False
    object FilenameEdit12: TFilenameEdit
      Left = 0
      Top = 0
      Width = 401
      Height = 21
      NumGlyphs = 1
      TabOrder = 0
      Text = 'W:\WEB\_HTML\leaflet.html'
    end
  end
  object WebBrowser1: TWebBrowser
    Left = 0
    Top = 73
    Width = 379
    Height = 216
    Align = alLeft
    TabOrder = 3
    OnBeforeNavigate2 = WebBrowser1BeforeNavigate2
    OnNavigateComplete2 = WebBrowser1NavigateComplete2
    OnDocumentComplete = WebBrowser1DocumentComplete
    ExplicitHeight = 194
    ControlData = {
      4C0000002C270000531600000000000000000000000000000000000000000000
      000000004C000000000000000000000001000000E0D057007335CF11AE690800
      2B2E126204000000000000004C0000000114020000000000C000000000000046
      8000000000000000000000000000000000000000000000000000000000000000
      00000000000000000100000000000000000000000000000000000000}
  end
  object FormStorage1: TFormStorage
    UseRegistry = True
    StoredValues = <>
    Left = 656
    Top = 96
  end
end
