unit fr_Map_View;

interface

uses
  u_Log_,
  u_func,
  u_Config,

  u_Task_classes,

  u_geo,

  System.JSON.Builders,
  System.JSON.Writers,

  CodeSiteLogging,

  windows,   ActiveX,   MSHTML_TLB, Messages,
  System.SysUtils, System.Variants, System.Classes,
  Vcl.Controls, Vcl.Forms, Vcl.OleCtrls, SHDocVw, Vcl.ExtCtrls, Vcl.StdCtrls, RxPlacemnt,
  Vcl.ComCtrls, RxToolEdit, Vcl.Mask, Vcl.ToolWin;

type
  Tframe_Map_view = class(TForm)
    Panel_Test: TPanel;
    Button2: TButton;
    Button3: TButton;
    Button4_DrawLine: TButton;
    FormStorage1: TFormStorage;
    StatusBar1: TStatusBar;
    Button1: TButton;
    ToolBar1: TToolBar;
    FilenameEdit12: TFilenameEdit;
    Button4: TButton;
    Button5: TButton;
    Button6: TButton;
    Button7: TButton;
    WebBrowser1: TWebBrowser;
    procedure FormDestroy(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure Button3Click(Sender: TObject);
    procedure Button4Click(Sender: TObject);
    procedure Button4_DrawLineClick(Sender: TObject);
    procedure Button5Click(Sender: TObject);
    procedure Button6Click(Sender: TObject);
    procedure Button7Click(Sender: TObject);

    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure WebBrowser1BeforeNavigate2(ASender: TObject; const pDisp: IDispatch;
        const URL, Flags, TargetFrameName, PostData, Headers: OLEVariant; var
        Cancel: WordBool);
    procedure WebBrowser1DocumentComplete(ASender: TObject; const pDisp: IDispatch;
        const URL: OLEVariant);
    procedure WebBrowser1NavigateComplete2(ASender: TObject; const pDisp:
        IDispatch; const URL: OLEVariant);

  type
    TWebCallbackResponse = record
       Lat, Lon : double;
    end;

    TOnMapClickEvent = procedure (aLat, aLon: double) of object;

  private
    FWebCallbackResponse: TWebCallbackResponse;

    FIDoc: IHTMLDocument2;
    FScript: Variant;

    FOnMapClick_LatLon: TOnMapClickEvent;

//    FDocumentComplete: boolean;

//    procedure Exec(aParam: string);
    class procedure ExecDlg;

  public
    procedure DrawTask(aTask: TTask);

    procedure Load_GeoJSON(aGeoJSON: string; aFit: boolean = false);

//    procedure DrawLine(aLat1, aLon1, aLat2, aLon2: double; aLabel, aColor: string);

//  public
//    procedure Clear;

//    procedure DrawLineProfile_WGS(aLat1, aLon1, aLat2, aLon2: double);
//    procedure DrawMarkerSite(aLat, aLon: double; aLabel: string);

    function ExtractMapCallback(aValue: string): TWebCallbackResponse;

//    procedure Load_GeoJSON(aJSON: string);
//    procedure DrawLineAndSite(aLat, aLon, aLat_site, aLon_site: double; aColorStr:
//        string);

//    procedure DrawMarker_Client(aLat, aLon: double);
//    procedure DrawMarker_Client_Ant(aLat, aLon: double);


    procedure SetCenterLatLon(aLat, aLon: double);

//    function GetMapState: string;

//    procedure Navigate(aFileName: string);

    procedure SetView(aLat, aLon: double);

    property OnMapClick_LatLon: TOnMapClickEvent read FOnMapClick_LatLon write  FOnMapClick_LatLon;

  //  class function Create_(AOwner: TWinControl): Tframe_WebBrowser;
  end;

//var
//  frame_Map_view: Tframe_Map_view;

implementation

{$R *.dfm}


procedure Tframe_Map_view.FormDestroy(Sender: TObject);
begin
  TLog.Send('Tframe_Map_view.FormDestroy(Sender: TObject);');

//  WebBrowser1.Navigate('about:blank');

end;

procedure Tframe_Map_view.Button1Click(Sender: TObject);
begin
   FScript.Test();
end;

//-------------------------------------------------------------------
class procedure Tframe_Map_view.ExecDlg;
//-------------------------------------------------------------------

begin
  with Tframe_Map_view.Create(Application) do
  begin

    //DrawLine(60.02, 30, 60.4, 30.1);

    ShowModal;

    Free;
  end;


end;


//-------------------------------------------------------------------
procedure Tframe_Map_view.FormCreate(Sender: TObject);
//-------------------------------------------------------------------
begin
  WebBrowser1.Align:=alClient;


//  WebBrowser1.Navigate('file://W:\WEB\AdminPanel\bin\html\simple.html');
//  WebBrowser1.Navigate('file://'+  FilenameEdit1.FileName);

//  FilenameEdit1.FileName:=g_Config.Map_print_HTML_template;
   Assert(FileExists(g_Config.Map_HTML_template));

   WebBrowser1.Navigate('file://' + g_Config.Map_HTML_template);
 // WebBrowser1.Navigate('file://W:\WEB\_html\leaflet.html');

//  while WebBrowser1.ReadyState <> READYSTATE_COMPLETE do
//    Application.ProcessMessages;

  WebBrowser1.Tag:=1;

   while WebBrowser1.Tag <> 0  do
   begin
     Application.ProcessMessages;
     Sleep(200);
   end;

//
//    while aWB.busy do
////    while aWB.ReadyState <> READYSTATE_COMPLETE do
//    begin
//TLog.Send('while WB.ReadyState <> READYSTATE_COMPLETE do');
//
//      Sleep(200);
//      Application.ProcessMessages;
////      Delay()
//    end;

//function TFrameMap.WaitLoadInternetMap(): Boolean;
//var
// TickCount: Cardinal;
//begin
// Result := False;
// WebBrowser.Tag := 1;
// TickCount := GetTickCount;
// while (WebBrowser.Tag = 1) and
//   ((GetTickCount - TickCount) / 1000 < DEF_MapWaitSpan) do
// begin
//  Sleep(200);
//  Application.ProcessMessages;
// end; // while
// if WebBrowser.Tag = 0 then
//  Result := True
// else
//  WebBrowser.Tag := 0;
// FMapLoaded := Result;
//end;
//
//procedure TFrameMap.WebBrowserDocumentComplete(Sender: TObject;
//  const pDisp: IDispatch; const URL: OleVariant);
//begin
// if pDisp = (Sender as TWebBrowser).Application then
//  WebBrowser.Tag := 0;
//end;


//   WebBrowser1.Busy

//g_Config.Map_HTML_template

//  WebBrowser1.Navigate('file://W:\WEB\_html\leaflet.html');
//  WebBrowser1.Navigate('http://localhost/leaflet.html');
  try
    WebBrowser1.Document.QueryInterface(IHTMLDocument2, FIDoc);
    FScript := FIDoc.Script;

  except

  end;
end;

procedure Tframe_Map_view.WebBrowser1NavigateComplete2(ASender: TObject; const
    pDisp: IDispatch; const URL: OLEVariant);
begin
  WebBrowser1.Tag:=0;

end;

procedure Tframe_Map_view.WebBrowser1DocumentComplete(ASender: TObject; const
    pDisp: IDispatch; const URL: OLEVariant);
begin
//  WebBrowser1.Tag:=0;
end;


procedure Tframe_Map_view.Button3Click(Sender: TObject);
begin
  SetView ( 61, 40 );
end;

procedure Tframe_Map_view.Button4Click(Sender: TObject);
begin
//  pn_Map.Realign;

  WebBrowser1.Realign;
  WebBrowser1.Width:=Width;
  WebBrowser1.Align:=alClient;

//  WebBrowser1.Invalidate;

//  pn_Map.Realign;
//  pn_Map.resize;
end;

procedure Tframe_Map_view.Button4_DrawLineClick(Sender: TObject);
begin
//  Load_GeoJSON (Memo1.Text);

  //DrawLine(60.02, 30, 60.4, 30.1);

end;

procedure Tframe_Map_view.Button5Click(Sender: TObject);
begin
//  if Visible and (WebBrowser1 <> nil) then
//  begin
//    FocusControl(nil);
//    FocusControl(WebBrowser1);
//  end;

//  WebBrowser1.Invalidate;

end;

procedure Tframe_Map_view.Button6Click(Sender: TObject);
begin
//  pn_Map.Realign;

//  WebBrowser1.Realign;
  WebBrowser1.Width:=Width;
  WebBrowser1.Align:=alClient;

//  WebBrowser1.Invalidate;
end;

procedure Tframe_Map_view.Button7Click(Sender: TObject);
begin
//  PostMessage(Handle, WM_PAINT, 0,0);
//  PostMessage(WebBrowser1.Handle, WM_PAINT, 0,0);

  PostMessage(Handle, WM_SIZE, 0,0);
  PostMessage(Handle, WM_SIZE, 0,0);
//  PostMessage(WebBrowser1.Handle, WM_SIZE, 0,0);

end;



procedure Tframe_Map_view.SetView(aLat, aLon: double);
begin
  FScript.SetView (aLat, aLon);

 // Exec (Format('SetView (%1.6f, %1.6f)',[aLat, aLon]));

end;



//procedure Tframe_Map_view.DrawMarkerSite(aLat, aLon: double; aLabel: string);
//begin
//  FScript.DrawMarkerSite (aLat, aLon, aLabel);
//end;
//
//
//
//procedure Tframe_Map_view.Load_GeoJSON(aJSON: string);
//begin
//  FScript.Load_GeoJSON(aJSON);
//end;


//-------------------------------------------------------------------
function Tframe_Map_view.ExtractMapCallback(aValue: string):   TWebCallbackResponse;
//-------------------------------------------------------------------
var
  oSList: TStringList;
  s: string;

  str_arr : TArray<string>;
begin

  str_arr:=Trim(aValue).Split(['&']);

  oSList:=TStringList.Create;

  for s in str_arr do
    oSList.Add (s);



  Result.Lat:= AsFloat(oSList.Values['lat']);
  Result.Lon:= AsFloat(oSList.Values['lng']);


  FreeAndNil(oSList);

end;



//procedure Tframe_Map_view.DrawLine(aLat1, aLon1, aLat2, aLon2: double; aLabel, aColor: string);
//begin
//  FScript.Clear;
//  FScript.DrawLineAndSite (aLat1, aLon1, aLat2, aLon2, aLabel, aColor);
//  FScript.Fit;
//end;


//-------------------------------------------------------------------
procedure Tframe_Map_view.DrawTask(aTask: TTask);
//-------------------------------------------------------------------
var
//System.JSON.

  oBuilder: TJSONObjectBuilder;
  oPlace:  TTaskPlace;

  oWriter: TJsonWriter;

begin
 {
  oWriter:=TJsonWriter.create;
//  oWriter.f

oBuilder := TJSONObjectBuilder.Create(oWriter);
  oBuilder
    .BeginObject
      .BeginArray('colors')
        .BeginObject
          .Add('name', 'red')
          .Add('hex', '#f00')
        .EndObject
    .EndArray
  .EndObject;

  }

  //aTask:=DB_to_Task();

//  FScript.Clear;

  {
  FWebBrowser.DrawMarker_Client (aTask.WGS.client_lat, aTask.WGS.client_lon);

  if aTask.WGS.client_antenna_lat<>0 then
  begin
    FWebBrowser.DrawMarker_Client_ant (aTask.WGS.client_antenna_lat, aTask.WGS.client_antenna_lon);

    aTask.WGS.client_lat:=aTask.WGS.client_antenna_lat;
    aTask.WGS.client_lon:=aTask.WGS.client_antenna_lon;

  end;
   }
//
//  for oPlace in aTask.Places do
//    if (oPlace.Pos_WGS.Lat<>0) and (aTask.WGS.client_lat<>0) then
//  begin
//    FScript.DrawLineAndSite
//      (
//      aTask.WGS.client_lat,
//      aTask.WGS.client_lon,
//
////       oPlace.CalcResult.WGS.client_antenna.lat,
////       oPlace.CalcResult.WGS.client_antenna.lon,
//
//       oPlace.Pos_WGS.Lat,
//       oPlace.Pos_WGS.Lon,
//
//       oPlace.place_name,
//
////       'red'
//       DEF_ResultType_COLOR [oPlace.CalcResult.ResultType]
//      );
//
//  end;
//
//  Assert (aTask.WGS.client_lat <> 0);
////  SetCenterLatLon(aTask.WGS.client_lat, aTask.WGS.client_lon);
//
//  FScript.Fit;


end;

procedure Tframe_Map_view.FormShow(Sender: TObject);
begin
  PostMessage(Handle, WM_SIZE, 0,0);
 PostMessage(Handle, WM_SIZE, 0,0);

end;


procedure Tframe_Map_view.Load_GeoJSON(aGeoJSON: string; aFit: boolean = false);
begin
  try
    FScript.Load_GeoJSON(aGeoJSON, aFit);
  except on E: Exception do
  end;
end;

procedure Tframe_Map_view.SetCenterLatLon(aLat, aLon: double);
begin
  FScript.SetCenterLatLon (aLat, aLon);
end;


procedure Tframe_Map_view.WebBrowser1BeforeNavigate2(ASender: TObject; const
    pDisp: IDispatch; const URL, Flags, TargetFrameName, PostData, Headers:
    OLEVariant; var Cancel: WordBool);
var
  s: string;
  r: TWebCallbackResponse;
begin

  StatusBar1.SimpleText:=URL;

  if Pos('##',URL)>0 then
  begin
    s:=Copy(URL, Pos('##',URL)+2, 1000);


    r:=ExtractMapCallback(s);

    if Assigned(FOnMapClick_LatLon) then
      FOnMapClick_LatLon(r.Lat, r.Lon);

//        property OnMapClick_LatLon: TOnMapClickEvent read FOnMapClick_LatLon write FOnMapClick_LatLon;

  end;

end;




end.


(*

  oForm:=Create(cxTabSheet1);
  oForm.ManualDock(cxTabSheet1);
  oForm.Show;


WB.Navigate('file://|' + ExtractFilePath(Application.ExeName) +

uses
  MSHTML_TLB, SHDocVw, ShellAPI;

function ExecuteScript(doc: IHTMLDocument2; script: string; language: string): Boolean;
var
  win: IHTMLWindow2;
  Olelanguage: Olevariant;
begin
  if doc <> nil then
  begin
    try
      win := doc.parentWindow;
      if win <> nil then
      begin
        try
          Olelanguage := language;
          win.ExecScript(script, Olelanguage);
        finally
          win := nil;
        end;
      end;
    finally
      doc := nil;
    end;
  end;
end;
Sample usage:

IDoc: IHTMLDocument2;
Webbrowser1.Document.QueryInterface(IHTMLDocument2, iDoc);
ExecuteScript(iDoc, 'document.login.submit()', 'JavaScript');






procedure Tframe_Map.WebBrowser1DocumentComplete(ASender: TObject;  const pDisp: IDispatch; const URL: OLEVariant);
begin
//  ShowMessage ('WebBrowser1DocumentComplete');
end;

procedure Tframe_Map.WebBrowser1DownloadBegin(Sender: TObject);
begin
 CodeSite.Send( 'WebBrowser1DownloadBegin' );


end;

procedure Tframe_Map.WebBrowser1DownloadComplete(Sender: TObject);
begin
  CodeSite.Send( 'WebBrowser1DownloadComplete' );

// ShowMessage ('WebBrowser1DownloadComplete');
end;

procedure Tframe_Map.WebBrowser1NavigateComplete2(ASender: TObject; const pDisp: IDispatch; const URL: OLEVariant);
begin
//ShowMessage ('WebBrowser1NavigateComplete2');
end;



To get _ANYINT variable from your html sample use this code:

function Read_ANYINT: integer;
var
  v: OleVariant;
begin
  v := Browser.Document;
  Result := v.parentWindow._ANYINT;
end;

r := Read_ANYINT;
With this html code:

<html>
<head>
<script>
var myVariable={name: "LA LA LA", x: 3, y: 5, mul: function () {return this.x * this.y}};
var myArray=["Yello", "Green"];
</script>
</head>
<body>
</body>
</html>


//-------------------------------------------------------------------
procedure WebBrowserScreenShot_PNG(const aWB: TWebBrowser; const aFileName:
    TFileName);
//-------------------------------------------------------------------
 var
   viewObject : IViewObject;
   r : TRect;
   bitmap : TBitmap;
 begin
   if aWB.Document <> nil then
   begin
     aWB.Document.QueryInterface(IViewObject, viewObject) ;
     if Assigned(viewObject) then
     try
       bitmap := TBitmap.Create;
       try
         r := Rect(0, 0, aWB.Width, aWB.Height) ;

         bitmap.Height := aWB.Height;
         bitmap.Width := aWB.Width;

         viewObject.Draw(DVASPECT_CONTENT, 1, nil, nil, Application.Handle, bitmap.Canvas.Handle, @r, nil, nil, 0) ;



         with TPNGObject.Create do
         try
           Assign(bitmap) ;
           SaveToFile(aFileName) ;
         finally
           Free;
         end;
       finally
         bitmap.Free;
       end;
     finally
       viewObject._Release;
     end;
   end;
 end;

//-----------------------------------------------------------------------
function ExecuteScript(aWebbrowser: TWebbrowser; script: string): Boolean;
//-----------------------------------------------------------------------
var
  win: IHTMLWindow2;
  Olelanguage: Olevariant;

  vIDoc: IHTMLDocument2;
begin
  aWebbrowser.Document.QueryInterface(IHTMLDocument2, vIDoc);


  if vIDoc <> nil then
  begin
    try
      win := vIDoc.parentWindow;
      if win <> nil then
      begin
        try
          Olelanguage := 'JavaScript';
          win.ExecScript(script, Olelanguage);
        finally
          win := nil;
        end;
      end;
    finally
      vIDoc := nil;
    end;
  end;
end;

procedure Tframe_Map.SaveToPNG(aFileName: string);
begin
  WebBrowserScreenShot_PNG(WebBrowser1, aFileName);
end;

procedure Tframe_Map.Button1Click(Sender: TObject);
//var
//  IDoc: IHTMLDocument2;
begin
//  Webbrowser1.Document.QueryInterface(IHTMLDocument2, iDoc);
  ExecuteScript(Webbrowser1, 'Test()');

end;


procedure Tframe_Map.Button7Click(Sender: TObject);
begin
  WebBrowser1.Align:=alNone;
  WebBrowser1.Width :=600;
  WebBrowser1.Height:=400;

  Application.ProcessMessages;

  SaveToPNG('d:\WebBrowserImage.jpg');

end;

//-------------------------------------------------------------------
procedure Tframe_Map.SetWebBrowserSize(aWidth, aHeight: Integer);
//-------------------------------------------------------------------
begin
  WebBrowser1.Align:=alNone;
  WebBrowser1.Width :=aWidth;
  WebBrowser1.Height:=aHeight;

 // WebBrowser1.Refresh;

  Application.ProcessMessages;

end;



procedure Tframe_Map_view.Load_GeoJSON(aGeoJSON: string);
begin
//   Exec ( Format( 'Load_GeoJSON (''%s'')', [aGeoJSON] ));

//   Exec (  'Load_GeoJSON (''[{"type": "LineString",	"coordinates": [[29.39, 60], [29.3, 60.3], [29.56, 60.5]]}]'')' ) ;


////[{"type": "LineString",	"coordinates": [[29.39, 60], [29.3, 60.3], [29.56, 60.5]]}]


//
// function Load_GeoJSON ( p_value )
// {
//	layerGroup.clearLayers();
//
//	L.geoJSON(p_value).addTo(map);
//  //alert ('test 111111111');
// }
//
//
// function SetView ( p_lat, p_lon )
// {
//	map.setView([p_lat, p_lon], 10);
//
//	// layerGroup.clearLayers();



  // TODO -cMM: Tframe_WebBrowser.Load_GeoJSON default body inserted
end;



procedure Tframe_Map_view.Button5Click(Sender: TObject);

begin
//    WebBrowserScreenShot_PNG(WebBrowser1,'d:\WebBrowserImage.jpg') ;
end;



//    procedure Load_GeoJSON(aGeoJSON: string);

//    procedure SaveToPNG(aFileName: string);

//    procedure SetWebBrowserSize(aWidth, aHeight: Integer);




procedure Tframe_Map_view.Exec(aParam: string);
begin
  ExecuteScript(Webbrowser1, aParam);

end;



procedure Tframe_Map_view.Navigate(aFileName: string);
begin
//  WebBrowser1.Navigate('file://'+  FilenameEdit1.FileName);

//  Webb Navigate('file://'+  aFileName);
end;






function SetCenterLatLon (p_lat, p_lon )



procedure Tframe_Map_view.DrawLineAndSite(aLat, aLon, aLat_site, aLon_site:  double; aColorStr: string);
begin
	FScript.DrawLineAndSite (aLat, aLon, aLat_site, aLon_site, aColorStr);
  //, 'test', 'red' );
//	DrawLineAndSite ( 60.1, 36.29, 60.1, 37.0);//, 'test', 'red' );


  // TODO -cMM: Tframe_Map_view.DrawLineAndSite default body inserted
end;



procedure Tframe_Map_view.DrawLineProfile_WGS(aLat1, aLon1, aLat2, aLon2:  double);
begin
  FScript.DrawLineProfile (aLat1, aLon1, aLat2, aLon2);
end;





procedure Tframe_Map_view.Clear;
begin
  FScript.Clear;
end;

