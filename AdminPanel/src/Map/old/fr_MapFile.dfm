inherited frame_MapFile: Tframe_MapFile
  Caption = 'frame_MapFile'
  ClientHeight = 613
  ClientWidth = 722
  OnCreate = FormCreate
  ExplicitWidth = 738
  ExplicitHeight = 652
  PixelsPerInch = 96
  TextHeight = 13
  object cxGrid1: TcxGrid [0]
    Left = 0
    Top = 62
    Width = 393
    Height = 306
    Align = alLeft
    TabOrder = 0
    ExplicitTop = 29
    ExplicitHeight = 339
    object cxGrid1DBTableView1: TcxGridDBTableView
      PopupMenu = PopupMenu1
      Navigator.Buttons.CustomButtons = <>
      FindPanel.DisplayMode = fpdmManual
      DataController.DataSource = DataSource1
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <
        item
        end>
      DataController.Summary.SummaryGroups = <>
      OptionsBehavior.ImmediateEditor = False
      OptionsView.GroupByBox = False
      OptionsView.Indicator = True
      object cxGrid1DBTableView1table_name: TcxGridDBColumn
        DataBinding.FieldName = 'table_name'
        Width = 284
      end
      object cxGrid1DBTableView1rec_count: TcxGridDBColumn
        DataBinding.FieldName = 'rec_count'
        Width = 85
      end
    end
    object cxGrid1Level1: TcxGridLevel
      GridView = cxGrid1DBTableView1
    end
  end
  object cxSplitter1: TcxSplitter [1]
    Left = 0
    Top = 368
    Width = 722
    Height = 8
    HotZoneClassName = 'TcxSimpleStyle'
    AlignSplitter = salBottom
    Control = PageControl1
    Visible = False
  end
  object PageControl1: TPageControl [2]
    Left = 0
    Top = 376
    Width = 722
    Height = 237
    ActivePage = TabSheet1
    Align = alBottom
    TabOrder = 2
    Visible = False
    object TabSheet1: TTabSheet
      Caption = #1050#1072#1088#1090#1072
    end
  end
  object ToolBar1: TToolBar [3]
    Left = 0
    Top = 33
    Width = 722
    Height = 29
    BorderWidth = 1
    ButtonHeight = 21
    Caption = 'ToolBar1'
    TabOrder = 3
    ExplicitTop = 0
    object Button1: TButton
      Left = 0
      Top = 0
      Width = 75
      Height = 21
      Caption = #1047#1072#1075#1088#1091#1079#1080#1090#1100
      TabOrder = 0
      OnClick = Button1Click
    end
  end
  inherited pn_Header: TPanel
    Width = 722
    TabOrder = 8
  end
  inherited dxBarManager1: TdxBarManager
    Left = 604
    Top = 88
    PixelsPerInch = 96
  end
  inherited FormStorage1: TFormStorage
    Left = 604
    Top = 144
  end
  object DataSource1: TDataSource
    DataSet = ADOStoredProc1
    Left = 446
    Top = 160
  end
  object ADOStoredProc1: TADOStoredProc
    Active = True
    Connection = dmMain.ADOConnection1
    CursorType = ctStatic
    LockType = ltReadOnly
    ProcedureName = 'map.sp_Map_SEL'
    Parameters = <>
    Left = 440
    Top = 88
  end
  object ActionList1: TActionList
    OnUpdate = ActionList1Update
    Left = 616
    Top = 240
    object act_Del: TAction
      Caption = #1059#1076#1072#1083#1080#1090#1100
      OnExecute = act_DelExecute
    end
  end
  object PopupMenu1: TPopupMenu
    Left = 208
    Top = 104
    object N1: TMenuItem
      Action = act_Del
    end
  end
end
