unit fr_MapFile;

interface

uses
  u_func,
  u_cx_ini,
  u_cx,
  fr_View_base,
  fr_Map_view,

  d_Map_Loader,
  u_config,

  u_db,
  dm_Main,

  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, cxGraphics, cxControls, cxLookAndFeels, cxLookAndFeelPainters, cxStyles, cxCustomData,
  cxFilter, cxData, cxDataStorage, cxEdit, cxNavigator, dxDateRanges, Data.DB, cxDBData, Data.Win.ADODB, cxGridLevel, cxGridCustomTableView,
  cxGridTableView, cxGridDBTableView, cxClasses, cxGridCustomView, cxGrid, cxSplitter, Vcl.OleCtrls, SHDocVw, Vcl.ExtCtrls, dxBar,
  RxPlacemnt, Vcl.ComCtrls, Vcl.StdCtrls, Vcl.ToolWin, Vcl.Menus,
  System.Actions, Vcl.ActnList;

type
  Tframe_MapFile = class(Tframe_View_base)
    cxGrid1: TcxGrid;
    cxGrid1DBTableView1: TcxGridDBTableView;
    cxGrid1Level1: TcxGridLevel;
    DataSource1: TDataSource;
    cxSplitter1: TcxSplitter;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    ADOStoredProc1: TADOStoredProc;
    ToolBar1: TToolBar;
    Button1: TButton;
    ActionList1: TActionList;
    act_Del: TAction;
    PopupMenu1: TPopupMenu;
    N1: TMenuItem;
    cxGrid1DBTableView1table_name: TcxGridDBColumn;
    cxGrid1DBTableView1rec_count: TcxGridDBColumn;
    procedure ActionList1Update(Action: TBasicAction; var Handled: Boolean);
    procedure act_DelExecute(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
   FWebBrowser: Tframe_Map_view;
  public
    { Public declarations }
  end;

var
  frame_MapFile: Tframe_MapFile;

implementation

{$R *.dfm}



procedure Tframe_MapFile.ActionList1Update(Action: TBasicAction; var Handled:
    Boolean);
begin
  inherited;

  act_Del.Enabled:=ADOStoredProc1.RecordCount>0;
end;

procedure Tframe_MapFile.act_DelExecute(Sender: TObject);
begin
  inherited;

  dmMain.ExecStoredProc(dmMain.ADOStoredProc1, 'map.sp_Map_del',
      [FLD_ID, ADOStoredProc1[FLD_ID] ]);
end;

procedure Tframe_MapFile.Button1Click(Sender: TObject);
begin
  inherited;

  Tdlg_Map_Loader.ExecDlg (g_Config.ConnectionStr) ;

 end;

procedure Tframe_MapFile.FormCreate(Sender: TObject);
begin
  SetCaption('����� ��������');
  cxGrid1.Align:=alClient;

//    dmMain.OpenTable (t_Settings);

 // CreateChildForm(FWebBrowser, Tframe_Map_view, TabSheet1);

//  Tframe_WebBrowser.Create_(pn_Map);


  dmMain.OpenStoredProc(ADOStoredProc1, ADOStoredProc1.ProcedureName, []);


  cx_LoadColumnCaptions_FromIni(cxGrid1DBTableView1);

  cxGrid1DBTableView1.Navigator.Visible:=True;


//  cx_LoadColumnCaptions_FromIni(cxDBVerticalGrid1);

//  cx_Grid_SetColumnCaptions(cxGrid1DBTableView1,
//   [
//     'table_name', '��� �����'
//   ]);
//


end;

end.
