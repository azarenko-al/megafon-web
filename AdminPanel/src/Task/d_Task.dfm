inherited dlg_Task: Tdlg_Task
  Align = alNone
  Caption = 'dlg_Task'
  ClientHeight = 871
  ClientWidth = 1051
  Position = poDefault
  OnDestroy = FormDestroy
  OnShow = FormShow
  ExplicitWidth = 1067
  ExplicitHeight = 910
  PixelsPerInch = 96
  TextHeight = 13
  object cxGrid_Task: TcxGrid [0]
    Left = 0
    Top = 0
    Width = 1051
    Height = 113
    Align = alTop
    TabOrder = 0
    object cxGrid_TaskDBTableView1: TcxGridDBTableView
      PopupMenu = PopupMenu1
      Navigator.Buttons.CustomButtons = <>
      FindPanel.DisplayMode = fpdmManual
      DataController.DataSource = ds_Task
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <
        item
        end>
      DataController.Summary.SummaryGroups = <>
      OptionsCustomize.ColumnsQuickCustomization = True
      OptionsSelection.HideSelection = True
      OptionsView.GroupByBox = False
      OptionsView.Indicator = True
      Styles.OnGetContentStyle = cxGrid_TaskDBTableView1StylesGetContentStyle
      object cxGrid_TaskDBTableView1id: TcxGridDBColumn
        DataBinding.FieldName = 'id'
        Options.Editing = False
        SortIndex = 0
        SortOrder = soDescending
        Width = 50
      end
      object cxGrid_TaskDBTableView1Column1: TcxGridDBColumn
        DataBinding.FieldName = 'Date_created'
        Options.Editing = False
        Width = 128
      end
      object cxGrid_TaskDBTableView1guid: TcxGridDBColumn
        DataBinding.FieldName = 'guid'
        Options.Editing = False
        Width = 100
      end
      object cxGrid_TaskDBTableView1client_lat: TcxGridDBColumn
        DataBinding.FieldName = 'client_lat'
        Options.Editing = False
        Width = 49
      end
      object cxGrid_TaskDBTableView1client_lon: TcxGridDBColumn
        DataBinding.FieldName = 'client_lon'
        Options.Editing = False
        Width = 51
      end
      object cxGrid_TaskDBTableView1client_antenna_lat: TcxGridDBColumn
        DataBinding.FieldName = 'client_antenna_lat'
        Options.Editing = False
        Width = 107
      end
      object cxGrid_TaskDBTableView1client_antenna_lon: TcxGridDBColumn
        DataBinding.FieldName = 'client_antenna_lon'
        Options.Editing = False
        Width = 97
      end
      object cxGrid_TaskDBTableView1client_antenna_height: TcxGridDBColumn
        DataBinding.FieldName = 'client_antenna_height'
        Options.Editing = False
        Width = 113
      end
      object cxGrid_TaskDBTableView1bitrate_Mbps: TcxGridDBColumn
        DataBinding.FieldName = 'bitrate_Mbps'
        Options.Editing = False
        Width = 69
      end
      object cxGrid_TaskDBTableView1is_can_change_antenna_height: TcxGridDBColumn
        DataBinding.FieldName = 'is_can_change_antenna_height'
        Options.Editing = False
        Width = 20
      end
      object cxGrid_TaskDBTableView1is_can_change_antenna_pos: TcxGridDBColumn
        DataBinding.FieldName = 'is_can_change_antenna_location'
        Options.Editing = False
        Width = 20
      end
      object col_Technology: TcxGridDBColumn
        DataBinding.FieldName = 'Technology'
        Options.Editing = False
        Width = 84
      end
      object col_Date_calc_finish: TcxGridDBColumn
        DataBinding.FieldName = 'Date_calc_finish'
        Options.Editing = False
        Width = 118
      end
      object col_result_msg: TcxGridDBColumn
        DataBinding.FieldName = 'result_msg'
        Options.Editing = False
        Width = 129
      end
      object col_result_comment: TcxGridDBColumn
        DataBinding.FieldName = 'result_comment'
        Options.Editing = False
        Width = 100
      end
      object col_log: TcxGridDBColumn
        DataBinding.FieldName = 'comment'
        PropertiesClassName = 'TcxBlobEditProperties'
        Properties.BlobEditKind = bekMemo
        Properties.PopupHeight = 300
        Properties.PopupWidth = 350
        Properties.ReadOnly = True
      end
      object col_Date_calc_diff: TcxGridDBColumn
        DataBinding.FieldName = 'Date_calc_diff'
      end
      object col_child_count: TcxGridDBColumn
        DataBinding.FieldName = 'child_count'
      end
    end
    object cxGrid_TaskLevel1: TcxGridLevel
      GridView = cxGrid_TaskDBTableView1
    end
  end
  object PageControl1: TPageControl [1]
    Left = 0
    Top = 444
    Width = 1051
    Height = 390
    ActivePage = TabSheet_Map
    Align = alBottom
    TabOrder = 1
    object TabSheet_Map: TTabSheet
      Caption = 'Map'
    end
    object TabSheet_Relief: TTabSheet
      Caption = 'Relief'
      ImageIndex = 1
      object DBGrid_Relief: TDBGrid
        Left = 0
        Top = 0
        Width = 978
        Height = 362
        Align = alLeft
        Anchors = [akLeft, akTop, akRight, akBottom]
        DataSource = ds_Relief
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'Tahoma'
        TitleFont.Style = []
      end
    end
    object TabSheet1: TTabSheet
      Caption = 'Images DB'
      ImageIndex = 3
      OnResize = TabSheet1Resize
      object cxDBImage_profile: TcxDBImage
        Left = 0
        Top = 0
        Align = alLeft
        DataBinding.DataField = 'result_profile_image'
        DataBinding.DataSource = ds_Task_places
        Properties.GraphicClassName = 'TdxPNGImage'
        TabOrder = 0
        Height = 362
        Width = 443
      end
      object cxDBImage_map: TcxDBImage
        Left = 443
        Top = 0
        Align = alLeft
        DataBinding.DataField = 'result_map_image'
        DataBinding.DataSource = ds_Task_places
        Properties.GraphicClassName = 'TdxPNGImage'
        TabOrder = 1
        Height = 362
        Width = 441
      end
    end
    object TabSheet3: TTabSheet
      Caption = 'Log'
      ImageIndex = 4
      object DBMemo1: TDBMemo
        Left = 0
        Top = 0
        Width = 842
        Height = 362
        Align = alLeft
        Anchors = [akLeft, akTop, akRight, akBottom]
        DataField = 'comment'
        DataSource = ds_Task_places
        TabOrder = 0
        ExplicitTop = -2
      end
    end
    object TabSheet2: TTabSheet
      Caption = 'Zone'
      ImageIndex = 4
      object cxDBImage_Zone: TcxDBImage
        Left = 0
        Top = 0
        Align = alLeft
        DataBinding.DataField = 'result_zone_image'
        DataBinding.DataSource = ds_Task_places
        Properties.GraphicClassName = 'TdxPNGImage'
        TabOrder = 0
        Height = 362
        Width = 905
      end
    end
  end
  object cxSplitter2: TcxSplitter [2]
    Left = 0
    Top = 249
    Width = 1051
    Height = 8
    HotZoneClassName = 'TcxSimpleStyle'
    AlignSplitter = salTop
    Control = cxGrid_Places
  end
  object cxGrid_Places: TcxGrid [3]
    Left = 0
    Top = 113
    Width = 1051
    Height = 136
    Align = alTop
    TabOrder = 3
    object cxGrid_PlacesDBTableView1: TcxGridDBTableView
      PopupMenu = PopupMenu_Places
      Navigator.Buttons.CustomButtons = <>
      FindPanel.DisplayMode = fpdmManual
      DataController.DataSource = ds_Task_places
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <
        item
        end>
      DataController.Summary.SummaryGroups = <>
      OptionsBehavior.ImmediateEditor = False
      OptionsCustomize.ColumnsQuickCustomization = True
      OptionsSelection.HideSelection = True
      OptionsView.GroupByBox = False
      OptionsView.Indicator = True
      Styles.OnGetContentStyle = cxGrid_PlacesDBTableView1StylesGetContentStyle
      object cxGrid_PlacesDBTableView1id: TcxGridDBColumn
        DataBinding.FieldName = 'id'
        Visible = False
      end
      object cxGrid_PlacesDBTableView1task_id: TcxGridDBColumn
        DataBinding.FieldName = 'task_id'
        Visible = False
      end
      object col_place_name: TcxGridDBColumn
        DataBinding.FieldName = 'place_name'
        Width = 76
      end
      object cxGrid_PlacesDBTableView1place_lat: TcxGridDBColumn
        DataBinding.FieldName = 'place_lat'
      end
      object cxGrid_PlacesDBTableView1place_lon: TcxGridDBColumn
        DataBinding.FieldName = 'place_lon'
      end
      object cxGrid_PlacesDBTableView1place_height: TcxGridDBColumn
        DataBinding.FieldName = 'place_height'
        Width = 78
      end
      object col_ObjectID: TcxGridDBColumn
        DataBinding.FieldName = 'ObjectID'
        Width = 145
      end
      object col_SysName: TcxGridDBColumn
        DataBinding.FieldName = 'SysName'
        Width = 73
      end
      object col_map_building_height: TcxGridDBColumn
        DataBinding.FieldName = 'map_building_height'
        Width = 119
      end
      object cxGrid_PlacesDBTableView1result_client_antenna_lat: TcxGridDBColumn
        DataBinding.FieldName = 'result_client_antenna_lat'
      end
      object cxGrid_PlacesDBTableView1result_client_antenna_lon: TcxGridDBColumn
        DataBinding.FieldName = 'result_client_antenna_lon'
      end
      object cxGrid_PlacesDBTableView1result_client_antenna_height: TcxGridDBColumn
        DataBinding.FieldName = 'result_client_antenna_height'
      end
      object col_result_ClientHWTypeID: TcxGridDBColumn
        DataBinding.FieldName = 'result_ClientHWTypeID'
        Width = 133
      end
      object cxGrid_PlacesDBTableView1result_freq_Mhz: TcxGridDBColumn
        DataBinding.FieldName = 'result_freq_Mhz'
      end
      object col_result_msg_: TcxGridDBColumn
        DataBinding.FieldName = 'result_msg'
      end
      object col_length_to_client_km: TcxGridDBColumn
        DataBinding.FieldName = 'length_to_client_km'
      end
      object col_result_code: TcxGridDBColumn
        DataBinding.FieldName = 'result_code'
      end
    end
    object cxGrid_PlacesLevel1: TcxGridLevel
      GridView = cxGrid_PlacesDBTableView1
    end
  end
  object Panel1: TPanel [4]
    Left = 0
    Top = 834
    Width = 1051
    Height = 37
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 4
    object Button2: TButton
      Left = 4
      Top = 6
      Width = 75
      Height = 25
      Cancel = True
      Caption = 'Cancel'
      ModalResult = 2
      TabOrder = 0
    end
  end
  inherited dxBarManager1: TdxBarManager
    Left = 796
    Top = 280
    PixelsPerInch = 96
    object dxBarButton1: TdxBarButton
      Caption = 'New Button'
      Category = 0
      Hint = 'New Button'
      Visible = ivAlways
    end
  end
  inherited FormStorage1: TFormStorage
    Options = [fpState, fpPosition]
    StoredProps.Strings = (
      'PageControl1.Height'
      'cxGrid_Places.Height'
      'PageControl1.ActivePage'
      'cxDBImage_profile.Width')
    Left = 788
    Top = 344
  end
  inherited ActionList_base: TActionList
    Left = 656
    Top = 280
    object act_Show_on_Map: TAction
      Caption = #1055#1086#1082#1072#1079#1072#1090#1100' '#1085#1072' '#1082#1072#1088#1090#1077
      OnExecute = act_Show_on_MapExecute
    end
    object act_Show_on_Map_main: TAction
      Caption = #1055#1086#1082#1072#1079#1072#1090#1100' '#1085#1072' '#1082#1072#1088#1090#1077
      OnExecute = act_Show_on_Map_mainExecute
    end
    object act_Equipment_Pivot: TAction
      Caption = 'act_Equipment_Pivot'
      OnExecute = act_Equipment_PivotExecute
    end
  end
  object ds_Task: TDataSource
    DataSet = q_Task
    Left = 320
    Top = 344
  end
  object ADOStoredProc_House_points: TADOStoredProc
    DataSource = ds_Task
    ProcedureName = 'map.sp_House_points_By_LatLon'
    Parameters = <>
    Left = 65100
    Top = 384
  end
  object q_Relief: TADOQuery
    Connection = dmMain.ADOConnection1
    CursorType = ctStatic
    LockType = ltReadOnly
    DataSource = ds_Task
    Parameters = <
      item
        Name = 'id'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = 734
      end>
    SQL.Strings = (
      'exec gis.sp_Relief_by_Task_SEL   :id')
    Left = 516
    Top = 288
  end
  object ds_Relief: TDataSource
    DataSet = q_Relief
    Left = 512
    Top = 344
  end
  object ds_Task_places: TDataSource
    DataSet = q_Task_places
    Left = 416
    Top = 344
  end
  object PopupMenu_Places: TPopupMenu
    Left = 56
    Top = 160
    object MenuItem1: TMenuItem
      Action = act_Show_on_Map
    end
    object actEquipmentPivot1: TMenuItem
      Action = act_Equipment_Pivot
    end
  end
  object q_Task: TADOQuery
    Connection = dmMain.ADOConnection1
    CursorType = ctStatic
    LockType = ltReadOnly
    Parameters = <>
    SQL.Strings = (
      'select top 10 * '
      'from megagis_client.view_Task')
    Left = 324
    Top = 288
  end
  object cxStyleRepository1: TcxStyleRepository
    Left = 936
    Top = 280
    PixelsPerInch = 96
    object cxStyle_Red: TcxStyle
      AssignedValues = [svColor]
      Color = clRed
    end
    object cxStyle_green: TcxStyle
    end
    object cxStyle_Yellow: TcxStyle
      AssignedValues = [svColor]
      Color = clYellow
    end
  end
  object PopupMenu1: TPopupMenu
    Left = 56
    Top = 32
    object MenuItem4: TMenuItem
      Action = act_Show_on_Map_main
    end
  end
  object q_Task_places: TADOQuery
    Connection = dmMain.ADOConnection1
    CursorType = ctStatic
    LockType = ltReadOnly
    DataSource = ds_Task
    Parameters = <
      item
        Name = 'id'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Value = 14161
      end>
    SQL.Strings = (
      'exec MegaGIS_client.sp_Task_place_SEL    :id')
    Left = 412
    Top = 296
  end
  object ADOStoredProc1: TADOStoredProc
    Parameters = <>
    Left = 167
    Top = 296
  end
end
