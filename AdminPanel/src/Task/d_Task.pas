﻿unit d_Task;

interface

uses
  dm_App,

  fr_Map_view,
  d_Equipment_Pivot,

  u_geo,
//  dm_Task,
//  dm_Task_Profile,
 // dm_Relief,



  u_cx,
  dm_Main,

  fr_View_base,

  u_func,


  System.SysUtils, System.Classes,
  Vcl.Controls, Vcl.Forms, Variants,


  Data.DB, Data.Win.ADODB, cxGridLevel,
  cxGridCustomTableView, cxGridDBTableView,
  cxGrid, cxSplitter, Vcl.ExtCtrls,
  Vcl.ComCtrls, dxBar,
  Vcl.ActnList, Vcl.StdCtrls, Vcl.DBGrids,
  Vcl.Menus,
  cxStyles, cxEdit,


  Vcl.DBCtrls, cxDBEdit, u_Task_classes, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, cxCustomData, cxFilter, cxData, cxDataStorage,
  cxNavigator, dxDateRanges, cxDBData, cxBlobEdit, cxContainer, System.Actions,
  cxImage, Vcl.Grids, cxGridTableView, cxGridCustomView, RxPlacemnt, cxClasses;

type
  Tdlg_Task = class(Tframe_View_base)
    cxGrid_TaskDBTableView1: TcxGridDBTableView;
    cxGrid_TaskLevel1: TcxGridLevel;
    cxGrid_Task: TcxGrid;
    ds_Task: TDataSource;
    cxGrid_TaskDBTableView1id: TcxGridDBColumn;
    cxGrid_TaskDBTableView1guid: TcxGridDBColumn;
    cxGrid_TaskDBTableView1client_lat: TcxGridDBColumn;
    cxGrid_TaskDBTableView1client_lon: TcxGridDBColumn;
    cxGrid_TaskDBTableView1client_antenna_lat: TcxGridDBColumn;
    cxGrid_TaskDBTableView1client_antenna_lon: TcxGridDBColumn;
    cxGrid_TaskDBTableView1client_antenna_height: TcxGridDBColumn;
    cxGrid_TaskDBTableView1bitrate_Mbps: TcxGridDBColumn;
    cxGrid_TaskDBTableView1is_can_change_antenna_height: TcxGridDBColumn;
    cxGrid_TaskDBTableView1is_can_change_antenna_pos: TcxGridDBColumn;
    PageControl1: TPageControl;
    TabSheet_Map: TTabSheet;
    TabSheet_Relief: TTabSheet;
    cxSplitter2: TcxSplitter;
    dxBarButton1: TdxBarButton;
    DBGrid_Relief: TDBGrid;
    ADOStoredProc_House_points: TADOStoredProc;
    q_Relief: TADOQuery;
    ds_Relief: TDataSource;
    cxGrid_Places: TcxGrid;
    cxGrid_PlacesDBTableView1: TcxGridDBTableView;
    cxGrid_PlacesDBTableView1id: TcxGridDBColumn;
    cxGrid_PlacesDBTableView1task_id: TcxGridDBColumn;
    col_place_name: TcxGridDBColumn;
    cxGrid_PlacesDBTableView1place_lat: TcxGridDBColumn;
    cxGrid_PlacesDBTableView1place_lon: TcxGridDBColumn;
    cxGrid_PlacesDBTableView1place_height: TcxGridDBColumn;
    cxGrid_PlacesDBTableView1result_client_antenna_lat: TcxGridDBColumn;
    cxGrid_PlacesDBTableView1result_client_antenna_lon: TcxGridDBColumn;
    cxGrid_PlacesDBTableView1result_client_antenna_height: TcxGridDBColumn;
    cxGrid_PlacesLevel1: TcxGridLevel;
    cxGrid_TaskDBTableView1Column1: TcxGridDBColumn;
    act_Show_on_Map: TAction;
    col_ObjectID: TcxGridDBColumn;
    col_map_building_height: TcxGridDBColumn;
    col_Technology: TcxGridDBColumn;
    col_result_msg: TcxGridDBColumn;
    col_result_comment: TcxGridDBColumn;
    ds_Task_places: TDataSource;
    col_log: TcxGridDBColumn;
    col_Date_calc_finish: TcxGridDBColumn;
    col_SysName: TcxGridDBColumn;
    col_result_ClientHWTypeID: TcxGridDBColumn;
    TabSheet1: TTabSheet;
    TabSheet3: TTabSheet;
    cxDBImage_profile: TcxDBImage;
    cxDBImage_map: TcxDBImage;
    DBMemo1: TDBMemo;
    PopupMenu_Places: TPopupMenu;
    MenuItem1: TMenuItem;
    q_Task: TADOQuery;
    Button2: TButton;
    Panel1: TPanel;
    col_result_msg_: TcxGridDBColumn;
    cxStyleRepository1: TcxStyleRepository;
    cxStyle_Red: TcxStyle;
    cxStyle_green: TcxStyle;
    cxStyle_Yellow: TcxStyle;
    col_length_to_client_km: TcxGridDBColumn;
    col_result_code: TcxGridDBColumn;
    act_Show_on_Map_main: TAction;
    PopupMenu1: TPopupMenu;
    MenuItem4: TMenuItem;
    col_Date_calc_diff: TcxGridDBColumn;
    col_child_count: TcxGridDBColumn;
    q_Task_places: TADOQuery;
    act_Equipment_Pivot: TAction;
    actEquipmentPivot1: TMenuItem;
    cxGrid_PlacesDBTableView1result_freq_Mhz: TcxGridDBColumn;
    TabSheet2: TTabSheet;
    cxDBImage_Zone: TcxDBImage;
    ADOStoredProc1: TADOStoredProc;
    procedure act_Equipment_PivotExecute(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
//    procedure act_RefreshExecute(Sender: TObject);
    procedure act_Show_on_MapExecute(Sender: TObject);
    procedure act_Show_on_Map_mainExecute(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure t_TaskAfterScroll(DataSet: TDataSet);
    procedure cxGrid_TaskDBTableView1StylesGetContentStyle(
      Sender: TcxCustomGridTableView; ARecord: TcxCustomGridRecord;
      AItem: TcxCustomGridTableItem; var AStyle: TcxStyle);
    procedure cxGrid_PlacesDBTableView1StylesGetContentStyle(
      Sender: TcxCustomGridTableView; ARecord: TcxCustomGridRecord;
      AItem: TcxCustomGridTableItem; var AStyle: TcxStyle);
    procedure TabSheet1Resize(Sender: TObject);
  private
    FID : integer;
//    Fframe_Map_view: Tframe_Map_view;

     FMap_view: Tframe_Map_view;

    function DB_to_Task: TTask;
    procedure DrawMap;
//    procedure View_on_Map;

  public

//    class procedure ExecDlg(aGUID: string);
    class procedure ExecDlg(aID : integer);


//    procedure NewMethod;

  end;

//var
//  dlg_Task: Tdlg_Task;


implementation

{$R *.dfm}

//uses dm_App;


procedure Tdlg_Task.act_Equipment_PivotExecute(Sender: TObject);
begin
   Tdlg_Equipment_Pivot.ExecDlg;;
end;

//procedure Tdlg_Task.act_RefreshExecute(Sender: TObject);
//begin
//  t_Task.Requery([]);
//
// // dmMain.OpenTable(t_Task);
//
//  q_Task_places.Open;
//end;


procedure Tdlg_Task.cxGrid_PlacesDBTableView1StylesGetContentStyle(
  Sender: TcxCustomGridTableView; ARecord: TcxCustomGridRecord;
  AItem: TcxCustomGridTableItem; var AStyle: TcxStyle);
var
  s: string;

begin
  inherited;

  s:=varToStr( ARecord.Values[col_result_msg_.Index]) ;
  dmApp.CalcResult_UpdateContentStyle(s,AStyle);


end;


procedure Tdlg_Task.cxGrid_TaskDBTableView1StylesGetContentStyle(
  Sender: TcxCustomGridTableView; ARecord: TcxCustomGridRecord;
  AItem: TcxCustomGridTableItem; var AStyle: TcxStyle);
var
  s: string;
begin
  inherited;

  s:=varToStr( ARecord.Values[col_result_msg.Index]) ;
  dmApp.CalcResult_UpdateContentStyle(s,AStyle);

end;

//-------------------------------------------------------------------
class procedure Tdlg_Task.ExecDlg(aID : integer);
//-------------------------------------------------------------------
begin
  with Tdlg_Task.Create(Application) do
  begin
    FID:=aID;

    dmMain.OpenQuery(q_Task, 'select  * from megagis_client.view_Task where id=:id',
      ['id', aID]);
 //   FTask:=aTask;


    q_Task_places.Open;
    q_Relief.Open;

  //  ed_Connection.Text:= aConnectionString;

    ShowModal;
    Free;
  end;
end;


//-------------------------------------------------------------------
procedure Tdlg_Task.FormCreate(Sender: TObject);
//-------------------------------------------------------------------
//var
//  s: string;
begin

  SetCaption('Задача');

  cxGrid_Task.Height:=60;

  PageControl1.Align:=alClient;

  cxDBImage_map.Align:=alClient;
  cxDBImage_Zone.Align:=alClient;

  q_Task_places.close;

//  dmMain.OpenTable(t_Task);

//  q_Task_places.Open;
//  q_Relief.Open;

//  Resize;


//  s:= FormStorage1.StoredProps.Text;

 // dmMain.OpenQuery(ADOQuery_Relief);


 //  Tframe_WebBrowser.Create_(TabSheet_Map);


  CreateChildForm(FMap_view, Tframe_Map_view, TabSheet_Map);
  //FMap_view.
  //FMap_view.Resize;


  DBGrid_Relief.Align:=alClient;


 // cx_Footer(cxGrid_TaskDBTableView1);
  cx_Footer(cxGrid_PlacesDBTableView1, col_place_name);


//  cxGrid1DBTableView1.Navigator.Visible:=True;



//  cx_LoadColumnCaptions_FromIni(cxGrid1DBTableView1);
//  cx_LoadColumnCaptions_FromIni(cxGrid_tasksDBTableView1);



//  TdmTask.Init;


end;

//-------------------------------------------------------------------
procedure Tdlg_Task.FormDestroy(Sender: TObject);
//-------------------------------------------------------------------
begin
  FreeAndNil(FMap_view);

  inherited;
end;

//-------------------------------------------------------------------
procedure Tdlg_Task.act_Show_on_MapExecute(Sender: TObject);
//-------------------------------------------------------------------
var
 // sJSON: string;
  ll_1,ll_2:  TLatLon;
  sName: string;
begin
//  sJSON:=t_Task.FieldByName('GeoJSON').AsString;
//  FMap_view.Load_GeoJSON(sJSON);

  if not q_Task_places.IsEmpty then
  begin
    ll_1.Lat:=q_Task.FieldByName('client_Lat').AsFloat;
    ll_1.Lon:=q_Task.FieldByName('client_Lon').AsFloat;


    ll_2.Lat:=q_Task_places.FieldByName('place_Lat').AsFloat;
    ll_2.Lon:=q_Task_places.FieldByName('place_Lon').AsFloat;

    sName:=q_Task_places.FieldByName('place_name').AsString;

//    FMap_view.DrawLine(ll_1.Lat, ll_1.Lon, ll_2.Lat, ll_2.Lon , sName, 'blue');

  end;
end;


procedure Tdlg_Task.act_Show_on_Map_mainExecute(Sender: TObject);
begin
  DrawMap

end;


//-------------------------------------------------------------------
procedure Tdlg_Task.DrawMap;
//-------------------------------------------------------------------
var
  k: Integer;
//  oTask: TTask;
  s: string;
 // oPlace:  TTaskPlace;

begin
  k:=dmMain.StoredProc_Open(ADOStoredProc1, 'MegaGIS_client.sp_Task_geoJson_SEL',
    ['Task_ID', FID ]);

  if ADOStoredProc1.RecordCount>0 then
  begin
    s:=ADOStoredProc1.Fields[0].Value;
    FMap_view.Load_GeoJSON(s, True);

  end;



//  k:=dmMain.StoredProc_Open(ADOStoredProc1, 'Admin_Panel.sp_geoJson_SEL', [ ]);
//  if ADOStoredProc1.RecordCount>0 then
//  begin
//    s:=ADOStoredProc1.Fields[0].Value;
//    FMap_view.Load_GeoJSON(s, aFit);
//
//  end;



 // oTask:=DB_to_Task();


//  FMap_view.Clear;

  {
  FMap_view.DrawMarker_Client (oTask.WGS.client_lat, oTask.WGS.client_lon);

  if oTask.WGS.client_antenna_lat<>0 then
  begin
    FMap_view.DrawMarker_Client_ant (oTask.WGS.client_antenna_lat, oTask.WGS.client_antenna_lon);

    oTask.WGS.client_lat:=oTask.WGS.client_antenna_lat;
    oTask.WGS.client_lon:=oTask.WGS.client_antenna_lon;

  end;
   }
//
//   FMap_view.DrawTask(oTask);
//
//   FreeAndNil(oTask);

//  for oPlace in oTask.Places do
//  begin
//    FMap_view.DrawLineAndSite
//      (oPlace.CalcResult.WGS.client_antenna.lat,
//       oPlace.CalcResult.WGS.client_antenna.lon,
//
//       oPlace.Pos_WGS.Lat,
//       oPlace.Pos_WGS.Lon,
//
//       DEF_ResultType_COLOR [oPlace.CalcResult.ResultType]
//
//       );
//
//  end;

end;


procedure Tdlg_Task.FormShow(Sender: TObject);
begin
 // inherited;
  Resize;

end;


//-------------------------------------------------------------------
function Tdlg_Task.DB_to_Task: TTask;
//-------------------------------------------------------------------
var
  oTask: TTask;
  oTaskPlace: TTaskPlace;

begin
  oTask:=TTask.Create;
  Result:=oTask;


  //-------------------------------------------------------------------
  q_Task_places.First;

//  oTask.Places.Clear;

  with q_Task_places do
    while not EOF do
  begin
    oTaskPlace:=oTask.Places.AddItem;

    oTaskPlace.Pos_wgs.lat:=FieldByName('place_lat').AsFloat;;
    oTaskPlace.Pos_wgs.lon:=FieldByName('place_lon').AsFloat;

    oTaskPlace.CalcResult.WGS.client_antenna.Lat:=FieldByName('result_client_antenna_lat').AsFloat;;
    oTaskPlace.CalcResult.WGS.client_antenna.Lon:=FieldByName('result_client_antenna_lon').AsFloat;;

//    oTaskPlace.CalcResult.WGS.client_antenna.Lat:=FieldByName('client_antenna_lat').AsFloat;;
//    oTaskPlace.CalcResult.WGS.client_antenna.Lon:=FieldByName('client_antenna_lon').AsFloat;;

    oTaskPlace.SetResultStr(FieldByName('result_msg').AsString);

  //  oTaskPlace.height :=FieldByName('place_height').AsFloat;

//    oTaskPlace.SetResultStr(FieldByName('result_msg').AsString);

   // oTaskPlace.AdminPanel_DB_ID:=FieldByName('id').AsInteger;

//    Assert(FieldByName('place_lat').AsFloat <> 0);
//    Assert(FieldByName('place_lon').AsFloat <> 0);


    Next;
  end;

  q_Task_places.First;


 // oCalcRequest.ClientHWTypeID:= '-';
//    oCalcRequest.ClientHWTypeID:= FieldByName('bitrate_Mbps').AsFloat;

//  oCalcRequest.Technology  := FieldByName('Technology').AsString;


//
//
//  Log ('CalcResponse -----------------------------');
//  Log (Format('CalculationCode: %s', [oCalcResponse.CalculationCode]));
//  Log (Format('Message_: %s',        [oCalcResponse.Message_]));
//  Log (Format('Type_: %s',           [oCalcResponse.Type_]));
//

end;

procedure Tdlg_Task.TabSheet1Resize(Sender: TObject);
begin
  cxDBImage_profile.Width:=TabSheet1.Width div 2;
end;


//-------------------------------------------------------------------
procedure Tdlg_Task.t_TaskAfterScroll(DataSet: TDataSet);
//-------------------------------------------------------------------
//var
//  sDir: string;
//  sGuid: string;

begin
{
  sGuid := t_Task.FieldByName('guid').AsString;
  sDir:= g_Config.GetRequestDir_ByGUID (sGuid);

  if FileExists(sDir + 'profile.png') then
    Image_profile.Picture.LoadFromFile(sDir + 'profile.png')
  else
    Image_profile.Picture := nil;


  if FileExists(sDir + 'map.png') then
    Image_Map.Picture.LoadFromFile(sDir + 'map.png')
  else
    Image_Map.Picture := nil;
 }


end;



end.


{

    if FileExists(FTask.Places[0].Result.ProfileFileName) then
        Image_profile.Picture.LoadFromFile(FTask.Places[0].Result.ProfileFileName);


      if FileExists(FTask.Places[0].Result.MapFileName) then
        Image_Map.Picture.LoadFromFile(FTask.Places[0].Result.MapFileName);


  cx_SetColumnCaptions(cxGrid1DBTableView1,
    [
      FLD_Name,          STR_NAME,
      FLD_Azimuth,       STR_AZIMUTH,
      FLD_Diameter,      STR_DIAMETER,
      FLD_TILT,          STR_TILT, // 'ÊÓ [dB]',
      FLD_Gain,          STR_GAIN, // 'ÊÓ [dB]',
      FLD_Height,        STR_HEIGHT,
      FLD_ANTENNATYPE_NAME, STR_TYPE,
      FLD_vert_width,    'Øèðèíà ÄÍÀ [V]',
      FLD_horz_width,    'Øèðèíà ÄÍÀ [H]',
      FLD_loss,        STR_LOSS,
    //  SArr(FLD_antenna_loss,  STR_LOSS),
      FLD_Polarization_STR,  STR_Polarization,
      FLD_Mast_index,   STR_Mast_index,
      FLD_BAND,         STR_BAND,

      FLD_polarization_ratio , STR_polarization_ratio // 'Çàùèòíîå îòíîøåíèå H/V [dB]'
    ]);




//  Tframe_WebBrowser.Create_(TabSheet_Map);

//
//  cx_SetColumnCaptions(cxGrid1DBTableView1,
//    [
//      FLD_Name,          STR_NAME,
//      FLD_Azimuth,       STR_AZIMUTH,
//      FLD_Diameter,      STR_DIAMETER,
//      FLD_TILT,          STR_TILT, // 'ÊÓ [dB]',
//      FLD_Gain,          STR_GAIN, // 'ÊÓ [dB]',
//      FLD_Height,        STR_HEIGHT,
//      FLD_ANTENNATYPE_NAME, STR_TYPE,
//      FLD_vert_width,    'Øèðèíà ÄÍÀ [V]',
//      FLD_horz_width,    'Øèðèíà ÄÍÀ [H]',
//      FLD_loss,        STR_LOSS,
//    //  SArr(FLD_antenna_loss,  STR_LOSS),
//      FLD_Polarization_STR,  STR_Polarization,
//      FLD_Mast_index,   STR_Mast_index,
//      FLD_BAND,         STR_BAND,
//
//      FLD_polarization_ratio , STR_polarization_ratio // 'Çàùèòíîå îòíîøåíèå H/V [dB]'
//    ]);
//



    if FileExists(FTask.Places[0].Result.ProfileFileName) then
        Image_profile.Picture.LoadFromFile(FTask.Places[0].Result.ProfileFileName);


      if FileExists(FTask.Places[0].Result.MapFileName) then
        Image_Map.Picture.LoadFromFile(FTask.Places[0].Result.MapFileName);


          protected
//    procedure WMEXTERNAL_LIB_IMPORT_DONE(var Message: TMessage); message WM_EXTERNAL_LIB_IMPORT_DONE;
    procedure WMEXTERNAL_PROJECT_IMPORT_DONE(var Message: TMessage); message WM_EXTERNAL_PROJECT_IMPORT_DONE;

  WM_EXTERNAL_PROJECT_IMPORT_DONE   = WM_USER + 500;
  WM_EXTERNAL_LIB_IMPORT_DONE       = WM_USER + 501;


    k:=dmMain.StoredProc_Open(ADOStoredProc1, 'Admin_Panel.sp_geoJson_SEL', [ ]);
  if ADOStoredProc1.RecordCount>0 then
  begin
    s:=ADOStoredProc1.Fields[0].Value;
    FMap_view.Load_GeoJSON(s, aFit);

  end;

