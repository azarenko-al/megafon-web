inherited frame_Task: Tframe_Task
  Caption = 'frame_Task'
  ClientHeight = 837
  ClientWidth = 1460
  OnDestroy = FormDestroy
  ExplicitWidth = 1476
  ExplicitHeight = 876
  PixelsPerInch = 96
  TextHeight = 13
  object cxGrid1: TcxGrid [0]
    Left = 0
    Top = 59
    Width = 505
    Height = 230
    Align = alLeft
    TabOrder = 0
    ExplicitTop = 29
    ExplicitHeight = 260
    object cxGrid1DBTableView_tasks: TcxGridDBTableView
      PopupMenu = PopupMenu_Task
      Navigator.Buttons.CustomButtons = <>
      FindPanel.DisplayMode = fpdmAlways
      DataController.DataSource = ds_Task
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <
        item
          Kind = skCount
          Column = cxGrid1DBTableView_tasksid
        end>
      DataController.Summary.SummaryGroups = <>
      OptionsBehavior.ImmediateEditor = False
      OptionsCustomize.ColumnsQuickCustomization = True
      OptionsData.Deleting = False
      OptionsData.Editing = False
      OptionsData.Inserting = False
      OptionsView.Footer = True
      OptionsView.GroupByBox = False
      OptionsView.Indicator = True
      Styles.OnGetContentStyle = cxGrid1DBTableView1StylesGetContentStyle
      object cxGrid1DBTableView_tasksid: TcxGridDBColumn
        DataBinding.FieldName = 'id'
        SortIndex = 0
        SortOrder = soDescending
        Width = 50
      end
      object col_Date_created: TcxGridDBColumn
        DataBinding.FieldName = 'Date_created'
        Width = 128
      end
      object col_Date_calc_finish: TcxGridDBColumn
        DataBinding.FieldName = 'Date_calc_finish'
        Width = 118
      end
      object cxGrid1DBTableView_tasksguid: TcxGridDBColumn
        DataBinding.FieldName = 'guid'
        Width = 100
      end
      object cxGrid1DBTableView_tasksclient_lat: TcxGridDBColumn
        DataBinding.FieldName = 'client_lat'
        Width = 49
      end
      object cxGrid1DBTableView_tasksclient_lon: TcxGridDBColumn
        DataBinding.FieldName = 'client_lon'
        Width = 51
      end
      object cxGrid1DBTableView_tasksclient_antenna_lat: TcxGridDBColumn
        DataBinding.FieldName = 'client_antenna_lat'
        Width = 107
      end
      object cxGrid1DBTableView_tasksclient_antenna_lon: TcxGridDBColumn
        DataBinding.FieldName = 'client_antenna_lon'
        Width = 97
      end
      object cxGrid1DBTableView_tasksclient_antenna_height: TcxGridDBColumn
        DataBinding.FieldName = 'client_antenna_height'
        Width = 113
      end
      object cxGrid1DBTableView_tasksbitrate_Mbps: TcxGridDBColumn
        DataBinding.FieldName = 'bitrate_Mbps'
        Width = 69
      end
      object cxGrid1DBTableView_tasksis_can_change_antenna_height: TcxGridDBColumn
        DataBinding.FieldName = 'is_can_change_antenna_height'
        Width = 20
      end
      object cxGrid1DBTableView_tasksis_can_change_antenna_pos: TcxGridDBColumn
        DataBinding.FieldName = 'is_can_change_antenna_location'
        Width = 20
      end
      object col_Technology: TcxGridDBColumn
        DataBinding.FieldName = 'Technology'
        Width = 84
      end
      object col_result_msg: TcxGridDBColumn
        DataBinding.FieldName = 'result_msg'
        Width = 129
      end
      object col_result_comment: TcxGridDBColumn
        DataBinding.FieldName = 'result_comment'
        Width = 100
      end
      object col_log: TcxGridDBColumn
        DataBinding.FieldName = 'comment'
        PropertiesClassName = 'TcxBlobEditProperties'
        Properties.BlobEditKind = bekMemo
        Properties.PopupHeight = 300
        Properties.PopupWidth = 350
        Properties.ReadOnly = True
      end
      object col_child_count: TcxGridDBColumn
        DataBinding.FieldName = 'child_count'
      end
      object col_Date_calc_diff: TcxGridDBColumn
        DataBinding.FieldName = 'Date_calc_diff'
      end
      object cxGrid1DBTableView_tasksColumn1: TcxGridDBColumn
        DataBinding.FieldName = 'Date_calc_diff_str'
      end
      object cxGrid1DBTableView_tasksColumn2: TcxGridDBColumn
        DataBinding.FieldName = 'request_source'
      end
      object col_ProcessID: TcxGridDBColumn
        DataBinding.FieldName = 'ProcessID'
      end
      object col_host: TcxGridDBColumn
        DataBinding.FieldName = 'host'
      end
    end
    object cxGrid1Level1: TcxGridLevel
      GridView = cxGrid1DBTableView_tasks
    end
  end
  object ToolBar1: TToolBar [1]
    Left = 0
    Top = 0
    Width = 1460
    Height = 29
    BorderWidth = 1
    ButtonHeight = 25
    ButtonWidth = 81
    Caption = 'ToolBar1'
    TabOrder = 1
    ExplicitTop = -6
    object Button1: TButton
      Left = 0
      Top = 0
      Width = 75
      Height = 25
      Action = act_Refresh
      TabOrder = 0
    end
    object Button2: TButton
      Left = 75
      Top = 0
      Width = 75
      Height = 25
      Action = act_Task_view
      TabOrder = 1
    end
  end
  object cxSplitter3: TcxSplitter [2]
    Left = 0
    Top = 289
    Width = 1460
    Height = 8
    HotZoneClassName = 'TcxSimpleStyle'
    AlignSplitter = salBottom
    Control = cxPageControl_bottom
  end
  object cxPageControl_bottom: TcxPageControl [3]
    Left = 0
    Top = 297
    Width = 1460
    Height = 540
    Align = alBottom
    TabOrder = 3
    Properties.ActivePage = cxTabSheet_places
    Properties.CustomButtons.Buttons = <>
    Properties.NavigatorPosition = npLeftTop
    Properties.Rotate = True
    Properties.TabPosition = tpLeft
    ClientRectBottom = 536
    ClientRectLeft = 98
    ClientRectRight = 1456
    ClientRectTop = 4
    object cxTabSheet_places: TcxTabSheet
      Caption = #1040#1073#1086#1085#1077#1085#1090#1099
      ImageIndex = 0
      object cxGrid_tasks: TcxGrid
        Left = 0
        Top = 0
        Width = 1358
        Height = 161
        Align = alTop
        TabOrder = 0
        object cxGrid_tasksDBTableView1: TcxGridDBTableView
          PopupMenu = PopupMenu_Places
          Navigator.Buttons.CustomButtons = <>
          FindPanel.DisplayMode = fpdmManual
          DataController.DataSource = ds_Task_places
          DataController.Summary.DefaultGroupSummaryItems = <>
          DataController.Summary.FooterSummaryItems = <
            item
              Kind = skCount
              FieldName = 'place_name'
            end>
          DataController.Summary.SummaryGroups = <>
          OptionsBehavior.ImmediateEditor = False
          OptionsCustomize.ColumnsQuickCustomization = True
          OptionsSelection.HideSelection = True
          OptionsView.Footer = True
          OptionsView.GroupByBox = False
          OptionsView.Indicator = True
          Styles.OnGetContentStyle = cxGrid_tasksDBTableView1StylesGetContentStyle
          object cxGrid_tasksDBTableView1id: TcxGridDBColumn
            DataBinding.FieldName = 'id'
            Visible = False
          end
          object cxGrid_tasksDBTableView1task_id: TcxGridDBColumn
            DataBinding.FieldName = 'task_id'
            Visible = False
          end
          object col_row_number: TcxGridDBColumn
            DataBinding.FieldName = 'row_number'
            Visible = False
            Width = 54
          end
          object col_place_name: TcxGridDBColumn
            DataBinding.FieldName = 'place_name'
            Width = 76
          end
          object cxGrid_tasksDBTableView1place_lat: TcxGridDBColumn
            DataBinding.FieldName = 'place_lat'
          end
          object cxGrid_tasksDBTableView1place_lon: TcxGridDBColumn
            DataBinding.FieldName = 'place_lon'
          end
          object cxGrid_tasksDBTableView1place_height: TcxGridDBColumn
            DataBinding.FieldName = 'place_height'
            Width = 78
          end
          object col_ObjectID: TcxGridDBColumn
            DataBinding.FieldName = 'ObjectID'
            Width = 145
          end
          object col_SysName: TcxGridDBColumn
            DataBinding.FieldName = 'SysName'
            Width = 73
          end
          object col_map_building_height: TcxGridDBColumn
            DataBinding.FieldName = 'map_building_height'
            Width = 119
          end
          object cxGrid_tasksDBTableView1result_client_antenna_lat: TcxGridDBColumn
            DataBinding.FieldName = 'result_client_antenna_lat'
          end
          object cxGrid_tasksDBTableView1result_client_antenna_lon: TcxGridDBColumn
            DataBinding.FieldName = 'result_client_antenna_lon'
          end
          object cxGrid_tasksDBTableView1result_client_antenna_height: TcxGridDBColumn
            DataBinding.FieldName = 'result_client_antenna_height'
          end
          object col_result_ClientHWTypeID: TcxGridDBColumn
            DataBinding.FieldName = 'result_ClientHWTypeID'
            Width = 145
          end
          object cxGrid_tasksDBTableView1result_freq_Mhz: TcxGridDBColumn
            DataBinding.FieldName = 'result_freq_Mhz'
            Width = 74
          end
          object col_Tasks_result_msg_: TcxGridDBColumn
            DataBinding.FieldName = 'result_msg'
            Width = 103
          end
          object col_length_to_client_km: TcxGridDBColumn
            DataBinding.FieldName = 'length_to_client_km'
          end
          object col_result_code: TcxGridDBColumn
            DataBinding.FieldName = 'result_code'
          end
        end
        object cxGrid_tasksLevel1: TcxGridLevel
          GridView = cxGrid_tasksDBTableView1
        end
      end
      object cxPageControl_place_bottom: TcxPageControl
        Left = 0
        Top = 291
        Width = 1358
        Height = 241
        Align = alBottom
        TabOrder = 1
        Properties.ActivePage = TabSheet_Map
        Properties.CustomButtons.Buttons = <>
        Properties.Rotate = True
        Properties.TabPosition = tpLeft
        ClientRectBottom = 237
        ClientRectLeft = 69
        ClientRectRight = 1354
        ClientRectTop = 4
        object TabSheet_Map: TcxTabSheet
          Caption = #1050#1072#1088#1090#1072
          ImageIndex = 0
        end
        object cxTabSheet2: TcxTabSheet
          Caption = #1046#1091#1088#1085#1072#1083
          ImageIndex = 1
          object DBMemo1: TDBMemo
            Left = 0
            Top = 0
            Width = 1207
            Height = 233
            Align = alLeft
            Anchors = [akLeft, akTop, akRight, akBottom]
            DataField = 'comment'
            DataSource = ds_Task_places
            TabOrder = 0
          end
        end
        object cxTabSheet_images: TcxTabSheet
          Caption = 'Images DB'
          ImageIndex = 2
          OnResize = cxTabSheet_imagesResize
          object cxDBImage_profile: TcxDBImage
            Left = 0
            Top = 0
            Align = alLeft
            DataBinding.DataField = 'result_profile_image'
            DataBinding.DataSource = ds_Task_places
            Properties.GraphicClassName = 'TdxPNGImage'
            TabOrder = 0
            Height = 233
            Width = 443
          end
          object cxDBImage_map: TcxDBImage
            Left = 443
            Top = 0
            Align = alLeft
            DataBinding.DataField = 'result_map_image'
            DataBinding.DataSource = ds_Task_places
            Properties.GraphicClassName = 'TdxPNGImage'
            TabOrder = 1
            Height = 233
            Width = 441
          end
        end
      end
      object cxSplitter1: TcxSplitter
        Left = 0
        Top = 283
        Width = 1358
        Height = 8
        HotZoneClassName = 'TcxMediaPlayer9Style'
        AlignSplitter = salBottom
        Control = cxPageControl_place_bottom
      end
    end
    object cxTabSheet_map: TcxTabSheet
      Caption = #1050#1072#1088#1090#1072
      ImageIndex = 1
    end
    object cxTabSheet4: TcxTabSheet
      Caption = #1052#1072#1090#1088#1080#1094#1099' '#1074#1099#1089#1086#1090
      ImageIndex = 2
      object DBGrid_Relief: TDBGrid
        Left = 0
        Top = 0
        Width = 1358
        Height = 177
        Align = alTop
        DataSource = ds_Relief
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'Tahoma'
        TitleFont.Style = []
      end
    end
    object cxTabSheet5: TcxTabSheet
      Caption = #1046#1091#1088#1085#1072#1083
      ImageIndex = 3
      object DBMemo_log_task: TDBMemo
        Left = 0
        Top = 0
        Width = 1358
        Height = 145
        Align = alTop
        DataField = 'comment'
        DataSource = ds_Task
        TabOrder = 0
      end
    end
    object cxTabSheet6: TcxTabSheet
      Caption = 'Image Zone'
      ImageIndex = 4
      object cxDBImage1: TcxDBImage
        Left = 0
        Top = 0
        Align = alTop
        DataBinding.DataField = 'result_zone_image'
        DataBinding.DataSource = ds_Task_places
        Properties.GraphicClassName = 'TdxPNGImage'
        TabOrder = 0
        Height = 80
        Width = 1358
      end
    end
    object cxTabSheet7: TcxTabSheet
      Caption = 'XML request'
      ImageIndex = 5
      object DBMemo_xml_request: TDBMemo
        Left = 0
        Top = 0
        Width = 1358
        Height = 80
        Align = alTop
        DataField = 'xml_request'
        DataSource = ds_Task
        ScrollBars = ssBoth
        TabOrder = 0
      end
    end
  end
  inherited dxBarManager1: TdxBarManager
    Left = 668
    Top = 176
    PixelsPerInch = 96
    DockControlHeights = (
      0
      0
      30
      0)
    inherited dxBarManager1Bar1: TdxBar
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarButton2'
        end
        item
          BeginGroup = True
          Visible = True
          ItemName = 'dxBarButton3'
        end
        item
          BeginGroup = True
          UserDefine = [udWidth]
          UserWidth = 67
          Visible = True
          ItemName = 'dxBarSpinEdit1'
        end>
      Visible = True
    end
    object dxBarButton1: TdxBarButton
      Caption = 'New Button'
      Category = 0
      Hint = 'New Button'
      Visible = ivAlways
    end
    object dxBarButton2: TdxBarButton
      Action = act_Refresh
      Category = 0
    end
    object dxBarButton3: TdxBarButton
      Action = act_Task_view
      Category = 0
    end
    object dxBarSpinEdit1: TdxBarSpinEdit
      Caption = 'New Item'
      Category = 0
      Hint = 'New Item'
      Visible = ivAlways
      Value = 100.000000000000000000
    end
  end
  inherited FormStorage1: TFormStorage
    StoredProps.Strings = (
      'cxPageControl_place_bottom.Height'
      'cxPageControl_bottom.Height')
    Left = 1028
    Top = 128
  end
  inherited ActionList_base: TActionList
    Left = 560
    Top = 176
    object act_Refresh: TAction
      Caption = #1054#1073#1085#1086#1074#1080#1090#1100
      OnExecute = act_RefreshExecute
    end
    object act_Show_on_Map: TAction
      Caption = #1055#1086#1082#1072#1079#1072#1090#1100' '#1085#1072' '#1082#1072#1088#1090#1077
      OnExecute = act_Show_on_MapExecute
    end
    object act_Task_Show_on_Map: TAction
      Category = 'task'
      Caption = #1055#1086#1082#1072#1079#1072#1090#1100' '#1079#1072#1076#1072#1095#1091' '#1085#1072' '#1082#1072#1088#1090#1077
      OnExecute = act_Task_Show_on_MapExecute
    end
    object act_task_Send_to_Queue: TAction
      Caption = 'Send to Queue'
      OnExecute = act_task_Send_to_QueueExecute
    end
    object FileSaveAs1: TFileSaveAs
      Category = 'File'
      Caption = 'Save &As...'
      Hint = 'Save As|Saves the active file with a new name'
      ImageIndex = 30
    end
    object act_Task_del_all: TAction
      Category = 'task'
      Caption = 'act_Task_del_all'
      OnExecute = act_Task_del_allExecute
    end
    object act_Equipment_Pivot: TAction
      Caption = 'act_Equipment_Pivot'
      OnExecute = act_Equipment_PivotExecute
    end
    object act_Task_view: TAction
      Category = 'task'
      Caption = 'act_Task_view'
      OnExecute = act_Task_viewExecute
    end
  end
  object t_Task: TADOTable
    Connection = dmMain.ADOConnection1
    CursorType = ctStatic
    TableName = 'megagis_client.view_Task'
    Left = 664
    Top = 56
  end
  object ds_Task: TDataSource
    DataSet = t_Task
    Left = 664
    Top = 112
  end
  object ADOStoredProc_House_points: TADOStoredProc
    DataSource = ds_Task
    ProcedureName = 'map.sp_House_points_By_LatLon'
    Parameters = <>
    Left = 65100
    Top = 384
  end
  object q_Task_Relief: TADOQuery
    Connection = dmMain.ADOConnection1
    CursorType = ctStatic
    LockType = ltReadOnly
    DataSource = ds_Task
    Parameters = <
      item
        Name = 'id'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = 5834
      end>
    SQL.Strings = (
      'exec gis.sp_Relief_by_Task_SEL   :id')
    Left = 908
    Top = 56
  end
  object ds_Relief: TDataSource
    DataSet = q_Task_Relief
    Left = 904
    Top = 112
  end
  object PopupMenu_Task: TPopupMenu
    Left = 560
    Top = 48
    object N1: TMenuItem
      Action = act_Task_Show_on_Map
    end
    object N2: TMenuItem
      Caption = '-'
    end
    object SendtoQueue1: TMenuItem
      Action = act_task_Send_to_Queue
    end
    object actTaskdelall1: TMenuItem
      Action = act_Task_del_all
    end
    object N3: TMenuItem
      Caption = '-'
    end
    object actRefresh1: TMenuItem
      Action = act_Refresh
    end
  end
  object q_Task_places: TADOQuery
    Connection = dmMain.ADOConnection1
    CursorType = ctStatic
    LockType = ltBatchOptimistic
    DataSource = ds_Task
    Parameters = <
      item
        Name = 'id'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Value = 9675
      end>
    SQL.Strings = (
      'exec MegaGIS_client.sp_Task_place_SEL    :id')
    Left = 812
    Top = 56
  end
  object ds_Task_places: TDataSource
    DataSet = q_Task_places
    Left = 816
    Top = 112
  end
  object PopupMenu_Places: TPopupMenu
    Left = 560
    Top = 112
    object MenuItem1: TMenuItem
      Action = act_Show_on_Map
    end
    object MenuItem2: TMenuItem
      Caption = '-'
    end
    object MenuItem3: TMenuItem
      Action = act_Refresh
    end
    object N4: TMenuItem
      Caption = '-'
    end
    object actEquipmentPivot1: TMenuItem
      Action = act_Equipment_Pivot
    end
  end
  object ADOStoredProc1: TADOStoredProc
    Parameters = <>
    Left = 1032
    Top = 64
  end
  object q_Task: TADOQuery
    Connection = dmMain.ADOConnection1
    CursorType = ctStatic
    LockType = ltBatchOptimistic
    DataSource = ds_Task
    Parameters = <>
    SQL.Strings = (
      'select * from  megagis_client.view_Task')
    Left = 732
    Top = 56
  end
end
