﻿unit fr_Task;

interface

uses
  fr_Map_view,
  d_Equipment_Pivot,

  u_Task_classes,
  d_Task,
  dm_Task,

  dm_App,



  u_geo,
//  dm_Task,
//  dm_Task_Profile,
 // dm_Relief,


  u_cx,

  dm_Main,

  fr_View_base,

  u_func,


  System.SysUtils, System.Classes,
  Vcl.Controls, Vcl.Forms, Variants, Messages,


  Data.DB, Data.Win.ADODB, cxGridLevel,
  cxGridCustomTableView, cxGridDBTableView,
  cxGrid, cxSplitter,
  Vcl.ComCtrls, dxBar,
  Vcl.ActnList, Vcl.StdCtrls, Vcl.DBGrids,
  Vcl.Menus,
  cxStyles, cxEdit,


  Vcl.DBCtrls, cxDBEdit, Vcl.StdActns,
  cxPC, cxGraphics, cxControls, cxLookAndFeels, cxLookAndFeelPainters,
  cxCustomData, cxFilter, cxData, cxDataStorage, cxNavigator, dxDateRanges,
  cxDBData, cxBlobEdit, dxBarBuiltInMenu, cxContainer, System.Actions,
  Vcl.Grids, cxImage, Vcl.ToolWin, cxGridTableView, cxGridCustomView,
  RxPlacemnt, cxClasses, dxBarExtItems;

type
  Tframe_Task = class(Tframe_View_base)
    cxGrid1DBTableView_tasks: TcxGridDBTableView;
    cxGrid1Level1: TcxGridLevel;
    cxGrid1: TcxGrid;
    t_Task: TADOTable;
    ds_Task: TDataSource;
    cxGrid1DBTableView_tasksid: TcxGridDBColumn;
    cxGrid1DBTableView_tasksguid: TcxGridDBColumn;
    cxGrid1DBTableView_tasksclient_lat: TcxGridDBColumn;
    cxGrid1DBTableView_tasksclient_lon: TcxGridDBColumn;
    cxGrid1DBTableView_tasksclient_antenna_lat: TcxGridDBColumn;
    cxGrid1DBTableView_tasksclient_antenna_lon: TcxGridDBColumn;
    cxGrid1DBTableView_tasksclient_antenna_height: TcxGridDBColumn;
    cxGrid1DBTableView_tasksbitrate_Mbps: TcxGridDBColumn;
    cxGrid1DBTableView_tasksis_can_change_antenna_height: TcxGridDBColumn;
    cxGrid1DBTableView_tasksis_can_change_antenna_pos: TcxGridDBColumn;
    ToolBar1: TToolBar;
    dxBarButton1: TdxBarButton;
    ADOStoredProc_House_points: TADOStoredProc;
    q_Task_Relief: TADOQuery;
    ds_Relief: TDataSource;
    PopupMenu_Task: TPopupMenu;
    actRefresh1: TMenuItem;
    act_Refresh: TAction;
    col_Date_created: TcxGridDBColumn;
    Button1: TButton;
    act_Show_on_Map: TAction;
    N1: TMenuItem;
    N2: TMenuItem;
    col_Technology: TcxGridDBColumn;
    col_result_msg: TcxGridDBColumn;
    col_result_comment: TcxGridDBColumn;
    q_Task_places: TADOQuery;
    ds_Task_places: TDataSource;
    col_log: TcxGridDBColumn;
    col_Date_calc_finish: TcxGridDBColumn;
    cxSplitter3: TcxSplitter;
    PopupMenu_Places: TPopupMenu;
    MenuItem1: TMenuItem;
    MenuItem2: TMenuItem;
    MenuItem3: TMenuItem;
    Button2: TButton;
    act_Task_Show_on_Map: TAction;
    col_Date_calc_diff: TcxGridDBColumn;
    col_child_count: TcxGridDBColumn;
    cxGrid1DBTableView_tasksColumn1: TcxGridDBColumn;
    act_task_Send_to_Queue: TAction;
    SendtoQueue1: TMenuItem;
    N3: TMenuItem;
    FileSaveAs1: TFileSaveAs;
    cxGrid1DBTableView_tasksColumn2: TcxGridDBColumn;
    act_Task_del_all: TAction;
    actTaskdelall1: TMenuItem;
    act_Equipment_Pivot: TAction;
    N4: TMenuItem;
    actEquipmentPivot1: TMenuItem;
    col_ProcessID: TcxGridDBColumn;
    col_host: TcxGridDBColumn;
    cxPageControl_bottom: TcxPageControl;
    cxTabSheet_places: TcxTabSheet;
    cxTabSheet_map: TcxTabSheet;
    cxTabSheet4: TcxTabSheet;
    cxTabSheet5: TcxTabSheet;
    cxTabSheet6: TcxTabSheet;
    cxTabSheet7: TcxTabSheet;
    cxGrid_tasks: TcxGrid;
    cxGrid_tasksDBTableView1: TcxGridDBTableView;
    cxGrid_tasksDBTableView1id: TcxGridDBColumn;
    cxGrid_tasksDBTableView1task_id: TcxGridDBColumn;
    col_row_number: TcxGridDBColumn;
    col_place_name: TcxGridDBColumn;
    cxGrid_tasksDBTableView1place_lat: TcxGridDBColumn;
    cxGrid_tasksDBTableView1place_lon: TcxGridDBColumn;
    cxGrid_tasksDBTableView1place_height: TcxGridDBColumn;
    col_ObjectID: TcxGridDBColumn;
    col_SysName: TcxGridDBColumn;
    col_map_building_height: TcxGridDBColumn;
    cxGrid_tasksDBTableView1result_client_antenna_lat: TcxGridDBColumn;
    cxGrid_tasksDBTableView1result_client_antenna_lon: TcxGridDBColumn;
    cxGrid_tasksDBTableView1result_client_antenna_height: TcxGridDBColumn;
    col_result_ClientHWTypeID: TcxGridDBColumn;
    cxGrid_tasksDBTableView1result_freq_Mhz: TcxGridDBColumn;
    col_Tasks_result_msg_: TcxGridDBColumn;
    col_length_to_client_km: TcxGridDBColumn;
    col_result_code: TcxGridDBColumn;
    cxGrid_tasksLevel1: TcxGridLevel;
    DBGrid_Relief: TDBGrid;
    DBMemo_log_task: TDBMemo;
    cxDBImage1: TcxDBImage;
    DBMemo_xml_request: TDBMemo;
    cxPageControl_place_bottom: TcxPageControl;
    TabSheet_Map: TcxTabSheet;
    cxTabSheet2: TcxTabSheet;
    DBMemo1: TDBMemo;
    cxTabSheet_images: TcxTabSheet;
    cxDBImage_profile: TcxDBImage;
    cxDBImage_map: TcxDBImage;
    cxSplitter1: TcxSplitter;
    act_Task_view: TAction;
    ADOStoredProc1: TADOStoredProc;
    q_Task: TADOQuery;
    dxBarButton2: TdxBarButton;
    dxBarButton3: TdxBarButton;
    dxBarSpinEdit1: TdxBarSpinEdit;
    procedure act_Equipment_PivotExecute(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure act_RefreshExecute(Sender: TObject);
    procedure act_Show_on_MapExecute(Sender: TObject);
    procedure act_Task_del_allExecute(Sender: TObject);
    procedure act_task_Send_to_QueueExecute(Sender: TObject);
    procedure act_Task_Show_on_MapExecute(Sender: TObject);
    procedure act_Task_viewExecute(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure ToolButton1Click(Sender: TObject);
//    procedure t_TaskAfterScroll(DataSet: TDataSet);
    procedure cxGrid1DBTableView1StylesGetContentStyle(
      Sender: TcxCustomGridTableView; ARecord: TcxCustomGridRecord;
      AItem: TcxCustomGridTableItem; var AStyle: TcxStyle);
    procedure cxGrid_tasksDBTableView1StylesGetContentStyle(
      Sender: TcxCustomGridTableView; ARecord: TcxCustomGridRecord;
      AItem: TcxCustomGridTableItem; var AStyle: TcxStyle);
    procedure cxTabSheet_imagesResize(Sender: TObject);
  private
//    Fframe_Map_view: Tframe_Map_view;

     FWebBrowser: Tframe_Map_view;
     FWebBrowser_Main: Tframe_Map_view;

    function DB_to_Task: TTask;
    procedure DrawMap;
//    procedure View_on_Map;

    procedure WMCopyData(var Msg: TWMCopyData);

  public
//    procedure NewMethod;

  end;

var
  frame_Task: Tframe_Task;


implementation

{$R *.dfm}

procedure Tframe_Task.act_Equipment_PivotExecute(Sender: TObject);
begin
  Tdlg_Equipment_Pivot.ExecDlg;

end;

procedure Tframe_Task.act_RefreshExecute(Sender: TObject);
begin
  t_Task.Requery([]);

 // dmMain.OpenTable(t_Task);

  q_Task_places.Open;
end;


//-------------------------------------------------------------------
procedure Tframe_Task.FormCreate(Sender: TObject);
//-------------------------------------------------------------------
var
  s: string;
begin

  SetCaption('Запрос-Ответ');


  q_Task_places.close;
  q_Task_Relief.close;


  //select * from  megagis_client.view_Task

  dmMain.OpenTable(t_Task, 'megagis_client.view_Task');

  q_Task_places.Open;
  q_Task_Relief.Open;


//  q_Task_full.SQL.Text:='select * from MegaGIS_client.view_Task_full where id=:id';
//  q_Task_full.Open;



  cxGrid_tasks.Align:=alClient;
  cxGrid1.Align:=alClient;
  cxDBImage_map.Align:=alClient;

  cxDBImage1.Align:=alClient;

  DBMemo_log_task.Align:=alClient;
  DBMemo_xml_request.Align:=alClient;

  Resize;


//  s:= FormStorage1.StoredProps.Text;

 // dmMain.OpenQuery(ADOQuery_Relief);


 //  Tframe_WebBrowser.Create_(TabSheet_Map);


  CreateChildForm(FWebBrowser,      Tframe_Map_view, TabSheet_Map);
  CreateChildForm(FWebBrowser_Main, Tframe_Map_view, cxTabSheet_Map);


  DBGrid_Relief.Align:=alClient;


 // cxGrid1DBTableView1.Navigator.Visible:=True;

  cx_Footer(cxGrid1DBTableView_tasks,   col_Date_created);
  cx_Footer(cxGrid_tasksDBTableView1,  col_place_name);




//  cx_LoadColumnCaptions_FromIni(cxGrid1DBTableView1);
//  cx_LoadColumnCaptions_FromIni(cxGrid_tasksDBTableView1);



//  TdmTask.Init;


end;

//-------------------------------------------------------------------
procedure Tframe_Task.FormDestroy(Sender: TObject);
//-------------------------------------------------------------------
begin
  FreeAndNil(FWebBrowser);
  FreeAndNil(FWebBrowser_Main);

  inherited;
end;

//-------------------------------------------------------------------
procedure Tframe_Task.act_Show_on_MapExecute(Sender: TObject);
//-------------------------------------------------------------------
var
 // sJSON: string;
  ll_1,ll_2:  TLatLon;
//var
  oTask: TTask;
  sName: string;
 // oPlace:  TTaskPlace;

begin
//  oTask:=DB_to_Task();


//  sJSON:=t_Task.FieldByName('GeoJSON').AsString;
//  FWebBrowser.Load_GeoJSON(sJSON);

  if not q_Task_places.IsEmpty then
  begin
//    ll_1.Lat:=t_Task.FieldByName('client_Lat').AsFloat;
//    ll_1.Lon:=t_Task.FieldByName('client_Lon').AsFloat;

    ll_1.Lat:=q_Task_places.FieldByName('result_client_antenna_lat').AsFloat;;
    ll_1.Lon:=q_Task_places.FieldByName('result_client_antenna_lon').AsFloat;;


    ll_2.Lat:=q_Task_places.FieldByName('place_Lat').AsFloat;
    ll_2.Lon:=q_Task_places.FieldByName('place_Lon').AsFloat;

    sName:=q_Task_places.FieldByName('place_name').AsString;

  //.....  FWebBrowser.DrawLine(ll_1.Lat, ll_1.Lon, ll_2.Lat, ll_2.Lon, sName, 'blue' );

  end;


end;


procedure Tframe_Task.act_Task_del_allExecute(Sender: TObject);
begin
  dmTask.Clear_All_Dlg;
  t_Task.Requery([]);
  q_Task_places.Requery([]);
end;


procedure Tframe_Task.act_task_Send_to_QueueExecute(Sender: TObject);
begin
  Assert (Assigned (dmTask));

  dmTask.SendToQueue(t_Task['id']);
end;


procedure Tframe_Task.act_Task_Show_on_MapExecute(Sender: TObject);
begin
  DrawMap;
end;

procedure Tframe_Task.act_Task_viewExecute(Sender: TObject);
begin
  Tdlg_Task.ExecDlg(t_Task['id']);
end;


procedure Tframe_Task.cxGrid1DBTableView1StylesGetContentStyle(
  Sender: TcxCustomGridTableView; ARecord: TcxCustomGridRecord;
  AItem: TcxCustomGridTableItem; var AStyle: TcxStyle);
var
  s: string;
begin
  inherited;

  s:= varToStr( ARecord.Values[col_result_msg.Index]);

  //ConnectionPossible
  //ConnectionPossible
  //ConnectionImpossible

  dmApp.CalcResult_UpdateContentStyle(s,AStyle);

//
//  if Eq(s,DEF_ConnectionPossible) then
//    AStyle:=cxStyle_green else
//
//  if Eq(s,DEF_CalculationImpossible) then
//    AStyle:=cxStyle_red;

//ConnectionPossible

end;


procedure Tframe_Task.cxGrid_tasksDBTableView1StylesGetContentStyle(
  Sender: TcxCustomGridTableView; ARecord: TcxCustomGridRecord;
  AItem: TcxCustomGridTableItem; var AStyle: TcxStyle);
var
  s: string;
begin
  inherited;

  s:= varToStr( ARecord.Values[col_Tasks_result_msg_.Index] );
  dmApp.CalcResult_UpdateContentStyle(s,AStyle);

//
//  if Eq(s,'ConnectionPossible') then
//    AStyle:=cxStyle_green else
//
//  if Eq(s,'CalculationImpossible') then
//    AStyle:=cxStyle_red;

//col_result_msg_
end;

procedure Tframe_Task.cxTabSheet_imagesResize(Sender: TObject);
begin
  cxDBImage_profile.Width:=cxTabSheet_images.Width div 2;

end;

procedure Tframe_Task.ToolButton1Click(Sender: TObject);
begin
  inherited;
end;



//-------------------------------------------------------------------
function Tframe_Task.DB_to_Task: TTask;
//-------------------------------------------------------------------
var
  oTask: TTask;
  oTaskPlace: TTaskPlace;

begin
  oTask:=TTask.Create;
  Result:=oTask;

  oTask.wgs.client_lat:=t_Task.FieldByName('client_lat').AsFloat;;
  oTask.wgs.client_lon:=t_Task.FieldByName('client_lon').AsFloat;


  //-------------------------------------------------------------------
  q_Task_places.First;

//  oTask.Places.Clear;

  with q_Task_places do
    while not EOF do
  begin
    oTaskPlace:=oTask.Places.AddItem;

    oTaskPlace.place_name:=FieldByName('place_name').AsString;

    oTaskPlace.Pos_wgs.lat:=FieldByName('place_lat').AsFloat;;
    oTaskPlace.Pos_wgs.lon:=FieldByName('place_lon').AsFloat;

    oTaskPlace.CalcResult.WGS.client_antenna.Lat:=FieldByName('result_client_antenna_lat').AsFloat;;
    oTaskPlace.CalcResult.WGS.client_antenna.Lon:=FieldByName('result_client_antenna_lon').AsFloat;;

    oTaskPlace.SetResultStr(FieldByName('result_msg').AsString);

  //  oTaskPlace.height :=FieldByName('place_height').AsFloat;

//    oTaskPlace.SetResultStr(FieldByName('result_msg').AsString);

   // oTaskPlace.AdminPanel_DB_ID:=FieldByName('id').AsInteger;

//    Assert(FieldByName('place_lat').AsFloat <> 0);
//    Assert(FieldByName('place_lon').AsFloat <> 0);


    Next;
  end;

  q_Task_places.First;


 // oCalcRequest.ClientHWTypeID:= '-';
//    oCalcRequest.ClientHWTypeID:= FieldByName('bitrate_Mbps').AsFloat;

//  oCalcRequest.Technology  := FieldByName('Technology').AsString;


//
//
//  Log ('CalcResponse -----------------------------');
//  Log (Format('CalculationCode: %s', [oCalcResponse.CalculationCode]));
//  Log (Format('Message_: %s',        [oCalcResponse.Message_]));
//  Log (Format('Type_: %s',           [oCalcResponse.Type_]));
//

end;


procedure Tframe_Task.WMCopyData(var Msg: TWMCopyData);
begin
 // ShowMessage ('procedure Tfrm_BSHPD_Server_monitor.WMCopyData(var Msg: TWMCopyData);');


 // s := pchar(Msg.CopyDataStruct.lpData) ;

 // Memo_Log.Lines.Add( s);
end;


//-------------------------------------------------------------------
procedure Tframe_Task.DrawMap;
//-------------------------------------------------------------------
var
  k: Integer;
//  oTask: TTask;
//  oPlace:  TTaskPlace;
  s: string;

begin
  cxPageControl_bottom.ActivePage:=cxTabSheet_places;

  k:=dmMain.StoredProc_Open(ADOStoredProc1, 'MegaGIS_client.sp_Task_geoJson_SEL',
    ['Task_ID', t_Task['id'] ]);

  if ADOStoredProc1.RecordCount>0 then
  begin
    s:=ADOStoredProc1.Fields[0].Value;


    FWebBrowser_Main.Load_GeoJSON(s, True);

  end;


//  oTask:=DB_to_Task();
//
//  FWebBrowser_Main.DrawTask(oTask);

//  Clear;

  {
  FWebBrowser.DrawMarker_Client (oTask.WGS.client_lat, oTask.WGS.client_lon);

  if oTask.WGS.client_antenna_lat<>0 then
  begin
    FWebBrowser.DrawMarker_Client_ant (oTask.WGS.client_antenna_lat, oTask.WGS.client_antenna_lon);

    oTask.WGS.client_lat:=oTask.WGS.client_antenna_lat;
    oTask.WGS.client_lon:=oTask.WGS.client_antenna_lon;

  end;
   }

//  for oPlace in oTask.Places do
//  begin
//    FWebBrowser_Main.DrawLineAndSite
//      (oPlace.CalcResult.WGS.client_antenna.lat,
//       oPlace.CalcResult.WGS.client_antenna.lon,
//
//       oPlace.WGS.Lat,
//       oPlace.WGS.Lon,
//
//       DEF_ResultType_COLOR [oPlace.CalcResult.ResultType]
//      );
//
//  end;

end;

end.



{

//-------------------------------------------------------------------
procedure Tframe_Task.t_TaskAfterScroll(DataSet: TDataSet);
//-------------------------------------------------------------------
var
  sDir: string;
  sGuid: string;
begin
{
  sGuid := t_Task.FieldByName('guid').AsString;
  sDir:= g_Config.GetRequestDir_ByGUID (sGuid);

  if FileExists(sDir + 'profile.png') then
    Image_profile.Picture.LoadFromFile(sDir + 'profile.png')
  else
    Image_profile.Picture := nil;


  if FileExists(sDir + 'map.png') then
    Image_Map.Picture.LoadFromFile(sDir + 'map.png')
  else
    Image_Map.Picture := nil;
 }


end;


