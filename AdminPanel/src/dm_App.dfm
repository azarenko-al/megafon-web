object dmApp: TdmApp
  OldCreateOrder = False
  OnCreate = DataModuleCreate
  Height = 190
  Width = 238
  object cxLookAndFeelController1: TcxLookAndFeelController
    Kind = lfFlat
    NativeStyle = False
    Left = 64
    Top = 24
  end
  object cxStyleRepository1: TcxStyleRepository
    Left = 64
    Top = 88
    PixelsPerInch = 96
    object cxStyle_Red: TcxStyle
      AssignedValues = [svColor]
      Color = clRed
    end
    object cxStyle_green: TcxStyle
      AssignedValues = [svColor]
      Color = clLime
    end
    object cxStyle_Yellow: TcxStyle
      AssignedValues = [svColor]
      Color = clYellow
    end
  end
end
