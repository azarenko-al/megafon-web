unit dm_App;

interface

uses

  u_func,
//  u_Task_send_to_Local_MG,

  System.Classes,

  cxLookAndFeels,
  cxStyles, cxClasses;

type
  TdmApp = class(TDataModule)
    cxLookAndFeelController1: TcxLookAndFeelController;
    cxStyleRepository1: TcxStyleRepository;
    cxStyle_Red: TcxStyle;
    cxStyle_green: TcxStyle;
    cxStyle_Yellow: TcxStyle;
    procedure DataModuleCreate(Sender: TObject);
  private
    { Private declarations }
  public
    procedure CalcResult_UpdateContentStyle(aValue: string; var AStyle: TcxStyle);

    { Public declarations }
  end;

var
  dmApp: TdmApp;

implementation

{%CLASSGROUP 'Vcl.Controls.TControl'}

uses dm_Main;



{$R *.dfm}

procedure TdmApp.DataModuleCreate(Sender: TObject);
begin
//  TdmMain.Init;
//
//  dmMain.Open;


//  Task_Send_to_Local_MG_Echo ('12312312') ;


//  Application.CreateForm(TdmTask_Relief, dmTask_Relief);
//  Application.CreateForm(TdmTask, dmTask);
//  Application.CreateForm(TdmMain, dmMain);


end;

//-------------------------------------------------------------------
procedure TdmApp.CalcResult_UpdateContentStyle(aValue: string; var AStyle:  TcxStyle);
//-------------------------------------------------------------------
const
  DEF_ConnectionPossible    ='ConnectionPossible';
  DEF_ConnectionImpossible  ='ConnectionImpossible';
  DEF_CalculationImpossible ='CalculationImpossible';

begin

  //ConnectionPossible
  //ConnectionPossible
  //ConnectionImpossible

  if Eq(aValue,DEF_CalculationImpossible) then
    AStyle:=cxStyle_Yellow;

  if Eq(aValue,DEF_ConnectionPossible) then
    AStyle:=cxStyle_green else

  if Eq(aValue,DEF_ConnectionImpossible) then
    AStyle:=cxStyle_Red;

//ConnectionPossible

end;

end.


{

procedure Tframe_Task.cxGrid1DBTableView1StylesGetContentStyle(
  Sender: TcxCustomGridTableView; ARecord: TcxCustomGridRecord;
  AItem: TcxCustomGridTableItem; var AStyle: TcxStyle);
var
  s: string;
begin
  inherited;

  s:= varToStr( ARecord.Values[col_result_msg.Index]);

  //ConnectionPossible
  //ConnectionPossible
  //ConnectionImpossible

  if Eq(s,DEF_ConnectionPossible) then
    AStyle:=cxStyle_green else

  if Eq(s,DEF_CalculationImpossible) then
    AStyle:=cxStyle_red;

//ConnectionPossible

end;
