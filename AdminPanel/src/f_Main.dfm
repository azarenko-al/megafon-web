object frm_Main: Tfrm_Main
  Left = 0
  Top = 0
  Caption = 'frm_Main'
  ClientHeight = 697
  ClientWidth = 791
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  PixelsPerInch = 96
  TextHeight = 13
  object dxNavBar1: TdxNavBar
    Left = 0
    Top = 0
    Width = 329
    Height = 678
    Align = alLeft
    ActiveGroupIndex = 0
    TabOrder = 0
    View = 10
    OptionsView.NavigationPane.ShowActiveGroupCaptionWhenCollapsed = True
    object dxNavBar1Group1: TdxNavBarGroup
      Caption = #1057#1087#1088#1072#1074#1086#1095#1085#1080#1082#1080
      SelectedLinkIndex = -1
      TopVisibleLinkIndex = 0
      Links = <
        item
          Item = dxNavBar1Item_LinkendTYpe
        end
        item
          Item = dxNavBar1Item_BS_WiMax
        end
        item
          Item = dxNavBar1Item_Terminal_WiMAX
        end
        item
          Item = dxNavBar1Item_Antenna
        end
        item
          Item = dxNavBar1Item_Clutter
        end>
    end
    object dxNavBar1Group3: TdxNavBarGroup
      Caption = #1054#1073#1086#1088#1091#1076#1086#1074#1072#1085#1080#1077
      SelectedLinkIndex = -1
      TopVisibleLinkIndex = 0
      Links = <
        item
          Item = dxNavBar1Item_Equipment
        end
        item
          Item = dxNavBar1Item_Pivot
        end>
    end
    object dxNavBar1Group2: TdxNavBarGroup
      Caption = 'NRI'
      SelectedLinkIndex = -1
      TopVisibleLinkIndex = 0
      Links = <
        item
          Item = dxNavBar1Item_NRI
        end>
    end
    object dxNavBar1Group4: TdxNavBarGroup
      Caption = 'MegaGIS '#1082#1083#1080#1077#1085#1090
      SelectedLinkIndex = -1
      TopVisibleLinkIndex = 0
      Links = <
        item
          Item = dxNavBar1Item_Settings
        end
        item
          Item = dxNavBar1Item_Queue
        end
        item
          Item = dxNavBar1Item_MegaGIS_Task
        end
        item
        end>
    end
    object dxNavBar1Group5: TdxNavBarGroup
      Caption = #1043#1048#1057
      SelectedLinkIndex = -1
      TopVisibleLinkIndex = 0
      Links = <
        item
          Item = dxNavBar1Item_Map
        end
        item
          Item = dxNavBar1Item_MapList
        end
        item
          Item = dxNavBar1Item_Relief
        end>
    end
    object dxNavBar1Item_NRI: TdxNavBarItem
      Caption = #1055#1086#1079#1080#1094#1080#1080
      OnClick = dxNavBar1Item_NRIClick
    end
    object dxNavBar1Item_Antenna: TdxNavBarItem
      Caption = #1040#1085#1090#1077#1085#1085#1099
      OnClick = dxNavBar1Item_NRIClick
    end
    object dxNavBar1Item_LinkendTYpe: TdxNavBarItem
      Caption = #1056#1072#1076#1080#1086#1088#1077#1083#1077#1081#1085#1099#1077' '#1089#1090#1072#1085#1094#1080#1080' ('#1056#1056#1057')'
      OnClick = dxNavBar1Item_NRIClick
    end
    object dxNavBar1Item_Clutter: TdxNavBarItem
      Caption = #1058#1080#1087#1099' '#1084#1077#1089#1090#1085#1086#1089#1090#1080
      OnClick = dxNavBar1Item_NRIClick
    end
    object dxNavBar1Item_Relief: TdxNavBarItem
      Caption = #1052#1072#1090#1088#1080#1094#1099' '#1074#1099#1089#1086#1090
      OnClick = dxNavBar1Item_NRIClick
    end
    object dxNavBar1Item_MapList: TdxNavBarItem
      Caption = #1050#1072#1088#1090#1072' '#1089#1090#1088#1086#1077#1085#1080#1081
      OnClick = dxNavBar1Item_NRIClick
    end
    object dxNavBar1Item_MegaGIS_Task: TdxNavBarItem
      Caption = #1047#1072#1087#1088#1086#1089'-'#1054#1090#1074#1077#1090
      OnClick = dxNavBar1Item_NRIClick
    end
    object dxNavBar1Item_Equipment: TdxNavBarItem
      Caption = #1064#1072#1073#1083#1086#1085#1099' '#1086#1073#1086#1088#1091#1076#1086#1074#1072#1085#1080#1103
      OnClick = dxNavBar1Item_NRIClick
    end
    object dxNavBar1Item_Settings: TdxNavBarItem
      Caption = #1053#1072#1089#1090#1088#1086#1081#1082#1072
      OnClick = dxNavBar1Item_NRIClick
    end
    object dxNavBar1Item_Terminal_WiMAX: TdxNavBarItem
      Caption = 'WiMAX '#1072#1073#1086#1085#1077#1085#1090#1089#1082#1080#1077' '#1090#1077#1088#1084#1080#1085#1072#1083#1099' (WiMAX '#1040#1058')'
      OnClick = dxNavBar1Item_NRIClick
    end
    object dxNavBar1Item_BS_WiMax: TdxNavBarItem
      Caption = 'WiMAX '#1073#1072#1079#1086#1074#1099#1077' '#1089#1090#1072#1085#1094#1080#1080' (WiMAX '#1041#1057')'
      OnClick = dxNavBar1Item_NRIClick
    end
    object dxNavBar1Item_Map: TdxNavBarItem
      Caption = #1050#1072#1088#1090#1072
      OnClick = dxNavBar1Item_NRIClick
    end
    object dxNavBar1Item_Queue: TdxNavBarItem
      Caption = #1054#1095#1077#1088#1077#1076#1100
      OnClick = dxNavBar1Item_NRIClick
    end
    object dxNavBar1Item_Pivot: TdxNavBarItem
      Caption = #1056#1077#1078#1080#1084#1099' '#1086#1073#1086#1088#1091#1076#1086#1074#1072#1085#1080#1103
      OnClick = dxNavBar1Item_NRIClick
    end
  end
  object cxPageControl1: TcxPageControl
    Left = 329
    Top = 0
    Width = 176
    Height = 678
    Align = alLeft
    TabOrder = 1
    Properties.ActivePage = cxTabSheet1
    Properties.CloseButtonMode = cbmEveryTab
    Properties.CustomButtons.Buttons = <>
    Properties.Options = [pcoAlwaysShowGoDialogButton, pcoCloseButton, pcoGoDialog, pcoGradient, pcoGradientClientArea, pcoRedrawOnResize, pcoTopToBottomText, pcoUsePageColorForTab]
    ClientRectBottom = 674
    ClientRectLeft = 4
    ClientRectRight = 172
    ClientRectTop = 24
    object cxTabSheet1: TcxTabSheet
      Hint = 'sdfgsdfg'
      Caption = 'cxTabSheet1'
      ImageIndex = 0
      ParentShowHint = False
      ShowHint = False
      TabHint = 'sdfgsdfgsdfg'
      ExplicitLeft = 0
      ExplicitTop = 0
      ExplicitWidth = 0
      ExplicitHeight = 0
    end
  end
  object StatusBar1: TStatusBar
    Left = 0
    Top = 678
    Width = 791
    Height = 19
    Panels = <>
    SimplePanel = True
  end
  object dxBarManager1: TdxBarManager
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Segoe UI'
    Font.Style = []
    Categories.Strings = (
      'Default')
    Categories.ItemsVisibles = (
      2)
    Categories.Visibles = (
      True)
    PopupMenuLinks = <>
    UseSystemFont = True
    Left = 640
    Top = 32
    PixelsPerInch = 96
  end
  object FormStorage1: TFormStorage
    UseRegistry = True
    StoredValues = <
      item
      end>
    Left = 640
    Top = 96
  end
end
