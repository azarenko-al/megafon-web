﻿ unit f_Main;

interface

uses

  dm_Main,

//  u_Task_send_to_Local_MG,

  fr_AntennaTYpe,

//  fr_LinkendType_WiMax,
  fr_LinkendType,

  fr_TerminalType_WiMax,
//  fr_TerminalType_BSHPD,

  fr_Clutters,
  fr_Task,
  fr_Queue,

  fr_Equipment_Pivot,

  fr_Relief,
  fr_MapFile,
  fr_Map,

  fr_NRI,
  fr_Equipment,
  fr_settings,

  u_config,

  u_func,

  u_db,


  System.Classes, Vcl.Controls, Vcl.Forms, Dialogs,

  cxPC, dxNavBarCollns, dxNavBar,
  dxBar, RxPlacemnt, Vcl.ComCtrls, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, dxBarBuiltInMenu, cxClasses, dxNavBarBase;

type
  Tfrm_Main = class(TForm)
    cxPageControl1: TcxPageControl;
    cxTabSheet1: TcxTabSheet;
    dxBarManager1: TdxBarManager;
    dxNavBar1: TdxNavBar;
    dxNavBar1Group1: TdxNavBarGroup;
    dxNavBar1Group2: TdxNavBarGroup;
    dxNavBar1Group3: TdxNavBarGroup;
    dxNavBar1Group4: TdxNavBarGroup;
    dxNavBar1Group5: TdxNavBarGroup;
    dxNavBar1Item_Antenna: TdxNavBarItem;
    dxNavBar1Item_Clutter: TdxNavBarItem;
    dxNavBar1Item_Equipment: TdxNavBarItem;
    dxNavBar1Item_MapList: TdxNavBarItem;
    dxNavBar1Item_NRI: TdxNavBarItem;
    dxNavBar1Item_LinkendTYpe: TdxNavBarItem;
    dxNavBar1Item_Relief: TdxNavBarItem;
    dxNavBar1Item_MegaGIS_Task: TdxNavBarItem;
    dxNavBar1Item_Settings: TdxNavBarItem;
    StatusBar1: TStatusBar;
    FormStorage1: TFormStorage;
    dxNavBar1Item_Terminal_WiMAX: TdxNavBarItem;
    dxNavBar1Item_BS_WiMax: TdxNavBarItem;
    dxNavBar1Item_Map: TdxNavBarItem;
    dxNavBar1Item_Queue: TdxNavBarItem;
    dxNavBar1Item_Pivot: TdxNavBarItem;
    procedure Button1Click(Sender: TObject);
//    procedure Button1Click(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
//    procedure Button1Click(Sender: TObject);
    procedure dxNavBar1Item_NRIClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
//    procedure dxNavBar1OnCustomDrawLink(Sender: TObject; ACanvas: TCanvas;
//      AViewInfo: TdxNavBarLinkViewInfo; var AHandled: Boolean);
  end;

var
  frm_Main: Tfrm_Main;

implementation


{$R *.dfm}


procedure Tfrm_Main.Button1Click(Sender: TObject);
begin

end;

//
//
//procedure Tfrm_Main.Button1Click(Sender: TObject);
//begin
//  ShowMessage ( Task_Send_to_Local_MG_Echo ('12312312') );
//end;

//-----------------------------------------------------------------------
procedure Tfrm_Main.FormCreate(Sender: TObject);
//-----------------------------------------------------------------------
begin
//  Caption:='Íàñòðîéêà àäìèíèñòðàòîðà';

  Caption:='ONEPLAN RPLS-DB : Панель администратора '+ GetAppVersionStr() ;


/////////////////////////////////////

  dmMain.Open;

/////////////////////////////////////

  cxPageControl1.Align:=alClient;

  while cxPageControl1.Properties.PageCount>0 do
    cxPageControl1.Properties.Pages[0].Destroy;

//  Tframe_Settings.Create_Inserted(cxPageControl1);

//  Tframe_Equipment.Create_Inserted(cxPageControl1);


  dxNavBar1Item_LinkendTYpe.Caption    :='Радиорелейные станции (РРС)';

//  dxNavBar1Item_Terminal_BSHPD.Caption :='БШПД абонентские терминалы (БШПД АТ)';
  dxNavBar1Item_Terminal_WiMAX.Caption :='WiMAX абонентские терминалы (WiMAX АТ)';
  dxNavBar1Item_BS_WiMax.Caption       :='WiMAX базовые станции (WiMAX БС)';


////  dxNavBar1Item_Map.Visible:=False;

  StatusBar1.SimpleText:=db_ConnectionStr_to_GDAL_connection(g_config.ConnectionStr);
  //dmMain.ADOConnection1.ConnectionString;

  //------------------------------------------------------
//  Tframe_MegaGIS_Task.Create_Inserted(cxPageControl1);
  Tframe_map.Create_Inserted(cxPageControl1);
 // Tframe_Pivot.Create_Inserted(cxPageControl1);

end;

//-----------------------------------------------------------------------
procedure Tfrm_Main.dxNavBar1Item_NRIClick(Sender: TObject);
//-----------------------------------------------------------------------
begin
  if Sender= dxNavBar1Item_LinkendTYpe    then Tframe_LinkendTYpe.Create_Inserted(cxPageControl1)  else

  if Sender= dxNavBar1Item_BS_WiMax       then Tframe_LinkendTYpe.Create_Inserted(cxPageControl1)  else
 // if Sender= dxNavBar1Item_BS_BSHPD       then Tframe_LinkendTYpe.Create_Inserted(cxPageControl1)  else

 // if Sender= dxNavBar1Item_Terminal_BSHPD then Tframe_TerminalTYpe_BSHPD.Create_Inserted(cxPageControl1)  else
  if Sender= dxNavBar1Item_Terminal_WiMAX then Tframe_TerminalTYpe_WiMax.Create_Inserted(cxPageControl1)  else


  if Sender= dxNavBar1Item_Antenna        then Tframe_AntennaTYpe.Create_Inserted(cxPageControl1)  else
  if Sender= dxNavBar1Item_Clutter        then Tframe_Clutters.Create_Inserted(cxPageControl1) else
  if Sender= dxNavBar1Item_Equipment      then Tframe_Equipment.Create_Inserted(cxPageControl1)   else


  if Sender= dxNavBar1Item_Pivot         then Tframe_Equipment_Pivot.Create_Inserted(cxPageControl1) else

  if Sender= dxNavBar1Item_Queue          then Tframe_Queue.Create_Inserted(cxPageControl1) else
  if Sender= dxNavBar1Item_MegaGIS_Task   then Tframe_Task.Create_Inserted(cxPageControl1) else
  if Sender= dxNavBar1Item_NRI            then Tframe_NRI.Create_Inserted(cxPageControl1) else


  if Sender= dxNavBar1Item_settings       then Tframe_settings.Create_Inserted(cxPageControl1)  else

  if Sender= dxNavBar1Item_MapList        then Tframe_MapFile.Create_Inserted(cxPageControl1)  else
  if Sender= dxNavBar1Item_Relief         then Tframe_Relief.Create_Inserted(cxPageControl1)  else
  if Sender= dxNavBar1Item_Map            then Tframe_Map.Create_Inserted(cxPageControl1)  else

//
end;

procedure Tfrm_Main.FormDestroy(Sender: TObject);
begin
  while cxPageControl1.Properties.PageCount>0 do
    cxPageControl1.Properties.Pages[0].Destroy;

end;

end.

