inherited frame_NRI: Tframe_NRI
  Caption = 'frame_NRI'
  ClientHeight = 495
  ClientWidth = 1146
  ExplicitWidth = 1162
  ExplicitHeight = 534
  PixelsPerInch = 96
  TextHeight = 13
  object cxGrid1: TcxGrid [0]
    Left = 0
    Top = 0
    Width = 649
    Height = 495
    Align = alLeft
    TabOrder = 2
    object cxGrid1DBTableView1: TcxGridDBTableView
      Navigator.Buttons.CustomButtons = <>
      FilterBox.Visible = fvNever
      FindPanel.DisplayMode = fpdmAlways
      DataController.DataSource = ds_place
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <
        item
        end>
      DataController.Summary.SummaryGroups = <>
      OptionsBehavior.ImmediateEditor = False
      OptionsCustomize.ColumnsQuickCustomization = True
      OptionsView.GroupByBox = False
      object col_name: TcxGridDBColumn
        DataBinding.FieldName = 'name'
        Width = 90
      end
      object cxGrid1DBTableView1NAME_RUS: TcxGridDBColumn
        DataBinding.FieldName = 'NAME_RUS'
        Width = 186
      end
      object cxGrid1DBTableView1HEIGHT: TcxGridDBColumn
        DataBinding.FieldName = 'HEIGHT'
        Width = 59
      end
      object cxGrid1DBTableView1map_building_height: TcxGridDBColumn
        DataBinding.FieldName = 'map_building_height'
        Width = 84
      end
      object cxGrid1DBTableView1LATITUDE: TcxGridDBColumn
        DataBinding.FieldName = 'LATITUDE'
        Width = 78
      end
      object cxGrid1DBTableView1LONGITUDE: TcxGridDBColumn
        DataBinding.FieldName = 'LONGITUDE'
        Width = 76
      end
      object cxGrid1DBTableView1nri_id: TcxGridDBColumn
        DataBinding.FieldName = 'nri_id'
      end
    end
    object cxGrid1Level1: TcxGridLevel
      GridView = cxGrid1DBTableView1
    end
  end
  object cxDBVerticalGrid1: TcxDBVerticalGrid [1]
    Left = 904
    Top = 0
    Width = 242
    Height = 495
    Align = alRight
    OptionsView.ScrollBars = ssVertical
    OptionsView.RowHeaderWidth = 149
    Navigator.Buttons.CustomButtons = <>
    TabOrder = 0
    DataController.DataSource = ds_place
    Version = 1
    object cxDBVerticalGrid1name: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'name'
      ID = 0
      ParentID = -1
      Index = 0
      Version = 1
    end
    object cxDBVerticalGrid1LATITUDE: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'LATITUDE'
      ID = 1
      ParentID = -1
      Index = 1
      Version = 1
    end
    object cxDBVerticalGrid1LONGITUDE: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'LONGITUDE'
      ID = 2
      ParentID = -1
      Index = 2
      Version = 1
    end
    object cxDBVerticalGrid1nri_id: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'nri_id'
      ID = 3
      ParentID = -1
      Index = 3
      Version = 1
    end
    object cxDBVerticalGrid1NAME_RUS: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'NAME_RUS'
      ID = 4
      ParentID = -1
      Index = 4
      Version = 1
    end
    object cxDBVerticalGrid1HEIGHT: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'HEIGHT'
      ID = 5
      ParentID = -1
      Index = 5
      Version = 1
    end
    object cxDBVerticalGrid1map_building_height: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'map_building_height'
      ID = 6
      ParentID = -1
      Index = 6
      Version = 1
    end
  end
  object cxSplitter1: TcxSplitter [2]
    Left = 896
    Top = 0
    Width = 8
    Height = 495
    HotZoneClassName = 'TcxSimpleStyle'
    AlignSplitter = salRight
    Control = cxDBVerticalGrid1
  end
  inherited dxBarManager1: TdxBarManager
    Left = 772
    Top = 344
    PixelsPerInch = 96
  end
  inherited FormStorage1: TFormStorage
    Left = 772
    Top = 400
  end
  inherited ActionList_base: TActionList
    Left = 768
  end
  object t_place: TADOTable
    Connection = dmMain.ADOConnection1
    CursorType = ctStatic
    TableName = 'nri.nri_place'
    Left = 768
    Top = 200
  end
  object ds_place: TDataSource
    DataSet = t_place
    Left = 768
    Top = 248
  end
end
