﻿unit fr_NRI;

interface

uses
  dm_Main,
  u_cx_ini,
  fr_View_base,

  u_cx,

  System.Classes,
  Vcl.Controls, Vcl.Forms,


  Data.DB, Data.Win.ADODB, cxGridLevel,
  cxGridDBTableView,
  cxGrid, cxDBVGrid, cxSplitter,




  cxClasses, cxGraphics, cxControls, cxLookAndFeels, cxLookAndFeelPainters,
  cxStyles, cxCustomData, cxFilter, cxData, cxDataStorage, cxEdit, cxNavigator,
  dxDateRanges, cxDBData, cxVGrid, cxInplaceContainer, cxGridCustomTableView,
  cxGridTableView, cxGridCustomView, System.Actions, Vcl.ActnList, RxPlacemnt,
  dxBar;

type
  Tframe_NRI = class(Tframe_View_base)
    cxGrid1DBTableView1: TcxGridDBTableView;
    cxGrid1Level1: TcxGridLevel;
    cxGrid1: TcxGrid;
    cxDBVerticalGrid1: TcxDBVerticalGrid;
    cxSplitter1: TcxSplitter;
    t_place: TADOTable;
    ds_place: TDataSource;
    col_name: TcxGridDBColumn;
    cxGrid1DBTableView1LATITUDE: TcxGridDBColumn;
    cxGrid1DBTableView1LONGITUDE: TcxGridDBColumn;
    cxGrid1DBTableView1nri_id: TcxGridDBColumn;
    cxGrid1DBTableView1NAME_RUS: TcxGridDBColumn;
    cxGrid1DBTableView1HEIGHT: TcxGridDBColumn;
    cxGrid1DBTableView1map_building_height: TcxGridDBColumn;
    cxDBVerticalGrid1name: TcxDBEditorRow;
    cxDBVerticalGrid1LATITUDE: TcxDBEditorRow;
    cxDBVerticalGrid1LONGITUDE: TcxDBEditorRow;
    cxDBVerticalGrid1nri_id: TcxDBEditorRow;
    cxDBVerticalGrid1NAME_RUS: TcxDBEditorRow;
    cxDBVerticalGrid1HEIGHT: TcxDBEditorRow;
    cxDBVerticalGrid1map_building_height: TcxDBEditorRow;
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

//var
//  frame_NRI: Tframe_NRI;

implementation

{$R *.dfm}

procedure Tframe_NRI.FormCreate(Sender: TObject);
begin
  SetCaption('Позиции NRI');

  cxGrid1.Align:=alClient;


  dmMain.OpenTable(t_place);

 // ADOQuery_Places.Open;
//  dmMain.OpenTable(ADOQuery_Places);

  cx_LoadColumnCaptions_FromIni(cxGrid1DBTableView1);
  cx_LoadColumnCaptions_FromIni(cxDBVerticalGrid1);

  //cxGrid1DBTableView1.Navigator.Visible:=True;

  cx_Footer(cxGrid1DBTableView1, col_name);


end;

end.
