inherited frame_Queue: Tframe_Queue
  Caption = 'frame_Queue'
  ClientHeight = 502
  ClientWidth = 835
  ExplicitWidth = 851
  ExplicitHeight = 541
  PixelsPerInch = 96
  TextHeight = 13
  object cxGrid1: TcxGrid [0]
    Left = 0
    Top = 30
    Width = 505
    Height = 472
    Align = alLeft
    TabOrder = 0
    object cxGrid1DBTableView_tasks: TcxGridDBTableView
      Navigator.Buttons.CustomButtons = <>
      FilterBox.Visible = fvNever
      FindPanel.DisplayMode = fpdmAlways
      DataController.DataSource = ds_Task
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <
        item
        end>
      DataController.Summary.SummaryGroups = <>
      OptionsCustomize.ColumnsQuickCustomization = True
      OptionsData.Editing = False
      OptionsSelection.MultiSelect = True
      OptionsSelection.HideSelection = True
      OptionsView.GroupByBox = False
      OptionsView.Indicator = True
      object cxGrid1DBTableView_tasksid: TcxGridDBColumn
        DataBinding.FieldName = 'id'
        Options.Editing = False
        SortIndex = 0
        SortOrder = soAscending
        Width = 53
      end
      object cxGrid1DBTableView_taskshost_created: TcxGridDBColumn
        DataBinding.FieldName = 'host_created'
        Width = 87
      end
      object cxGrid1DBTableView_tasksDate_created: TcxGridDBColumn
        DataBinding.FieldName = 'Date_created'
        Options.Editing = False
        Width = 105
      end
      object cxGrid1DBTableView_tasksguid: TcxGridDBColumn
        DataBinding.FieldName = 'guid'
        Width = 170
      end
      object cxGrid1DBTableView_tasksbody: TcxGridDBColumn
        DataBinding.FieldName = 'body'
        PropertiesClassName = 'TcxBlobEditProperties'
        Properties.BlobEditKind = bekMemo
        Properties.PopupHeight = 500
        Properties.PopupWidth = 500
        Options.Editing = False
        Width = 68
      end
      object cxGrid1DBTableView_taskshost_started: TcxGridDBColumn
        DataBinding.FieldName = 'host_started'
        Width = 68
      end
      object cxGrid1DBTableView_tasksDate_started: TcxGridDBColumn
        DataBinding.FieldName = 'Date_started'
        Width = 79
      end
      object cxGrid1DBTableView_tasksESB_response: TcxGridDBColumn
        DataBinding.FieldName = 'response'
      end
      object cxGrid1DBTableView_tasksDate_finished: TcxGridDBColumn
        DataBinding.FieldName = 'Date_finished'
      end
    end
    object cxGrid1Level1: TcxGridLevel
      GridView = cxGrid1DBTableView_tasks
    end
  end
  inherited dxBarManager1: TdxBarManager
    Left = 588
    Top = 168
    PixelsPerInch = 96
    DockControlHeights = (
      0
      0
      30
      0)
    inherited dxBarManager1Bar1: TdxBar
      ItemLinks = <
        item
          UserDefine = [udWidth]
          UserWidth = 69
          Visible = True
          ItemName = 'dxBarSpinEdit_RecordMaxCount'
        end
        item
          Visible = True
          ItemName = 'dxBarButton2'
        end
        item
          BeginGroup = True
          UserDefine = [udWidth]
          UserWidth = 276
          Visible = True
          ItemName = 'cxBarEditItem_Search'
        end
        item
          Visible = True
          ItemName = 'dxBarButton3'
        end>
      Visible = True
    end
    object dxBarButton2: TdxBarButton
      Action = act_Refresh
      Category = 0
    end
    object cxBarEditItem_Search: TcxBarEditItem
      Caption = 'New Item'
      Category = 0
      Hint = 'New Item'
      Visible = ivNever
      InternalEditValue = nil
    end
    object dxBarButton3: TdxBarButton
      Action = act_Search
      Category = 0
      Visible = ivNever
    end
    object dxBarSpinEdit_RecordMaxCount: TdxBarSpinEdit
      Caption = 'New Item'
      Category = 0
      Hint = 'New Item'
      Visible = ivAlways
      Value = 1000.000000000000000000
    end
  end
  inherited FormStorage1: TFormStorage
    StoredProps.Strings = (
      'cxBarEditItem_Search.EditValue')
    Left = 588
    Top = 224
  end
  inherited ActionList_base: TActionList
    Left = 688
    Top = 40
    object act_Refresh: TAction
      Caption = #1054#1073#1085#1086#1074#1080#1090#1100
      OnExecute = act_RefreshExecute
    end
    object act_Search: TAction
      Caption = #1055#1086#1080#1089#1082
    end
  end
  object ds_Task: TDataSource
    DataSet = sp_Queue_SEL
    Left = 592
    Top = 96
  end
  object ADOStoredProc_House_points: TADOStoredProc
    DataSource = ds_Task
    ProcedureName = 'map.sp_House_points_By_LatLon'
    Parameters = <>
    Left = 65100
    Top = 384
  end
  object sp_Queue_SEL: TADOStoredProc
    LockType = ltReadOnly
    Parameters = <>
    Left = 584
    Top = 312
  end
end
