unit fr_Queue;

interface

uses
  u_cx,
  u_cx_ini,

  dm_Main,

  fr_View_base,



  System.Classes,
  Vcl.Controls, Vcl.Forms, Variants, Messages,
  Data.DB, Data.Win.ADODB, cxGridLevel,
  cxGridDBTableView,
  cxGrid, Vcl.ExtCtrls,
  dxBar,
  Vcl.ActnList, cxGraphics, cxControls, cxLookAndFeels, cxLookAndFeelPainters,
  cxStyles, cxCustomData, cxFilter, cxData, cxDataStorage, cxEdit, cxNavigator,
  dxDateRanges, cxDBData, cxBlobEdit, System.Actions, cxGridCustomTableView,
  cxGridTableView, cxGridCustomView, RxPlacemnt, cxClasses, cxTextEdit,
  cxBarEditItem, dxBarExtItems;

type
  Tframe_Queue = class(Tframe_View_base)
    cxGrid1DBTableView_tasks: TcxGridDBTableView;
    cxGrid1Level1: TcxGridLevel;
    cxGrid1: TcxGrid;
    ds_Task: TDataSource;
    ADOStoredProc_House_points: TADOStoredProc;
    act_Refresh: TAction;
    cxGrid1DBTableView_tasksid: TcxGridDBColumn;
    cxGrid1DBTableView_tasksDate_created: TcxGridDBColumn;
    cxGrid1DBTableView_tasksbody: TcxGridDBColumn;
    cxGrid1DBTableView_tasksguid: TcxGridDBColumn;
    cxGrid1DBTableView_taskshost_created: TcxGridDBColumn;
    cxGrid1DBTableView_taskshost_started: TcxGridDBColumn;
    cxGrid1DBTableView_tasksDate_started: TcxGridDBColumn;
    cxGrid1DBTableView_tasksESB_response: TcxGridDBColumn;
    cxGrid1DBTableView_tasksDate_finished: TcxGridDBColumn;
    dxBarButton2: TdxBarButton;
    cxBarEditItem_Search: TcxBarEditItem;
    dxBarButton3: TdxBarButton;
    act_Search: TAction;
    dxBarSpinEdit_RecordMaxCount: TdxBarSpinEdit;
    sp_Queue_SEL: TADOStoredProc;
//    procedure FormDestroy(Sender: TObject);
    procedure act_RefreshExecute(Sender: TObject);
//    procedure act_Show_on_MapExecute(Sender: TObject);
//    procedure act_Task_Show_on_MapExecute(Sender: TObject);
//    procedure Button2Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure Timer1Timer(Sender: TObject);
//    procedure ToolButton1Click(Sender: TObject);
//    procedure t_TaskAfterScroll(DataSet: TDataSet);
//    procedure cxGrid1DBTableView1StylesGetContentStyle(
//      Sender: TcxCustomGridTableView; ARecord: TcxCustomGridRecord;
//      AItem: TcxCustomGridTableItem; var AStyle: TcxStyle);
//    procedure cxGrid_tasksDBTableView1StylesGetContentStyle(
//      Sender: TcxCustomGridTableView; ARecord: TcxCustomGridRecord;
//      AItem: TcxCustomGridTableItem; var AStyle: TcxStyle);
//    procedure TabSheet1Resize(Sender: TObject);
  private
//    Fframe_Map_view: Tframe_Map_view;


 //   procedure WMCopyData(var Msg: TWMCopyData);

  public
//    procedure NewMethod;

  end;

//var
//  frame_Queue: Tframe_Queue;


implementation

{$R *.dfm}

procedure Tframe_Queue.act_RefreshExecute(Sender: TObject);
//var
//  e: Extended;
begin
// e:=dxBarSpinEdit_RecordMaxCount.CurValue;

//dxBarSpinEdit_RecordMaxCount.op

  dmMain.StoredProc_Open(sp_Queue_SEL, 'WebService.sp_Queue_SEL',
      ['count',dxBarSpinEdit_RecordMaxCount.Value  ]);

end;


//-------------------------------------------------------------------
procedure Tframe_Queue.FormCreate(Sender: TObject);
//-------------------------------------------------------------------
begin

  SetCaption('�������');
  cxGrid1.Align:=alClient;

  act_Refresh.Execute;

 // dmMain.OpenTable(t_queue);


//  s:= FormStorage1.StoredProps.Text;

 // dmMain.OpenQuery(ADOQuery_Relief);


 //  Tframe_WebBrowser.Create_(TabSheet_Map);

  cx_LoadColumnCaptions_FromIni(cxGrid1DBTableView_tasks);


 // cxGrid1DBTableView1.Navigator.Visible:=True;

  cx_Footer(cxGrid1DBTableView_tasks,   cxGrid1DBTableView_tasksDate_created);




end;


procedure Tframe_Queue.Timer1Timer(Sender: TObject);
begin
  //act_Refresh.Execute;

end;




end.



{

//-------------------------------------------------------------------
procedure Tframe_Task.t_TaskAfterScroll(DataSet: TDataSet);
//-------------------------------------------------------------------
var
  sDir: string;
  sGuid: string;
begin
{
  sGuid := t_Task.FieldByName('guid').AsString;
  sDir:= g_Config.GetRequestDir_ByGUID (sGuid);

  if FileExists(sDir + 'profile.png') then
    Image_profile.Picture.LoadFromFile(sDir + 'profile.png')
  else
    Image_profile.Picture := nil;


  if FileExists(sDir + 'map.png') then
    Image_Map.Picture.LoadFromFile(sDir + 'map.png')
  else
    Image_Map.Picture := nil;
 }


end;


