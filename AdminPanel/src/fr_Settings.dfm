inherited frame_Settings: Tframe_Settings
  Caption = 'frame_Settings'
  ClientHeight = 582
  ClientWidth = 1229
  ExplicitWidth = 1245
  ExplicitHeight = 621
  PixelsPerInch = 96
  TextHeight = 13
  object cxGrid1: TcxGrid [0]
    Left = 464
    Top = 29
    Width = 537
    Height = 260
    TabOrder = 0
    object cxGrid1DBBandedTableView1: TcxGridDBBandedTableView
      Navigator.Buttons.CustomButtons = <>
      Navigator.Visible = True
      DataController.DataSource = ds_settings_client_antenna_height
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <>
      DataController.Summary.SummaryGroups = <>
      OptionsView.GroupByBox = False
      Bands = <
        item
          Caption = #1059#1089#1083#1086#1074#1080#1103' '#1076#1083#1103' '#1088#1072#1089#1095#1077#1090#1072
          Width = 452
        end>
      object cxGrid1DBBandedTableView1Column1: TcxGridDBBandedColumn
        DataBinding.FieldName = 'clutter_code'
        Width = 101
        Position.BandIndex = 0
        Position.ColIndex = 0
        Position.RowIndex = 0
      end
      object cxGrid1DBBandedTableView1Column2: TcxGridDBBandedColumn
        DataBinding.FieldName = 'clutter_name'
        Width = 141
        Position.BandIndex = 0
        Position.ColIndex = 1
        Position.RowIndex = 0
      end
      object cxGrid1DBBandedTableView1Column3: TcxGridDBBandedColumn
        DataBinding.FieldName = 'height'
        Width = 117
        Position.BandIndex = 0
        Position.ColIndex = 2
        Position.RowIndex = 0
      end
      object cxGrid1DBBandedTableView1Column4: TcxGridDBBandedColumn
        DataBinding.FieldName = 'is_terminate_calc'
        Width = 93
        Position.BandIndex = 0
        Position.ColIndex = 3
        Position.RowIndex = 0
      end
    end
    object cxGrid1Level2: TcxGridLevel
      GridView = cxGrid1DBBandedTableView1
    end
  end
  object cxDBVerticalGrid1: TcxDBVerticalGrid [1]
    Left = 0
    Top = 29
    Width = 449
    Height = 553
    Align = alLeft
    OptionsView.ScrollBars = ssVertical
    OptionsView.RowHeaderWidth = 197
    Navigator.Buttons.CustomButtons = <>
    TabOrder = 1
    DataController.DataSource = ds_Settings
    Version = 1
    object cxDBVerticalGrid1CategoryRow2: TcxCategoryRow
      Properties.Caption = #1050#1083#1080#1077#1085#1090
      ID = 0
      ParentID = -1
      Index = 0
      Version = 1
    end
    object cxDBVerticalGrid1DBEditorRow4: TcxDBEditorRow
      Properties.Caption = #1053#1072#1095#1072#1083#1100#1085#1072#1103' '#1074#1099#1089#1086#1090#1072' '#1072#1085#1090#1077#1085#1085#1099', [m]'
      Properties.DataBinding.FieldName = 'client_AMC_height'
      ID = 1
      ParentID = 0
      Index = 0
      Version = 1
    end
    object cxDBVerticalGrid1iteration_count: TcxDBEditorRow
      Properties.Caption = #1050#1086#1083'-'#1074#1086' '#1080#1090#1077#1088#1072#1094#1080#1081
      Properties.DataBinding.FieldName = 'client_iteration_count'
      ID = 2
      ParentID = 0
      Index = 1
      Version = 1
    end
    object cxDBVerticalGrid1iteration_antenna_h: TcxDBEditorRow
      Properties.Caption = #1042#1099#1089#1086#1090#1072'.. '#1080#1090#1077#1088#1072#1094#1080#1081
      Properties.DataBinding.FieldName = 'client_iteration_antenna_h'
      ID = 3
      ParentID = 0
      Index = 2
      Version = 1
    end
    object cxDBVerticalGrid1Data_dir: TcxDBEditorRow
      Properties.Caption = #1050#1072#1090#1072#1083#1086#1075' '#1089' '#1076#1072#1085#1085#1099#1084#1080
      Properties.DataBinding.FieldName = 'Dir_Data'
      ID = 4
      ParentID = -1
      Index = 1
      Version = 1
    end
    object cxDBVerticalGrid1DBEditorRow2: TcxDBEditorRow
      Properties.EditPropertiesClassName = 'TcxLookupComboBoxProperties'
      Properties.EditProperties.KeyFieldNames = 'id'
      Properties.EditProperties.ListColumns = <
        item
          FieldName = 'name'
        end>
      Properties.EditProperties.ListSource = ds_clutterModel
      Properties.DataBinding.FieldName = 'clutter_model_id'
      ID = 5
      ParentID = -1
      Index = 2
      Version = 1
    end
    object cxDBVerticalGrid1CategoryRow1: TcxCategoryRow
      Properties.Caption = #1055#1072#1088#1072#1084#1077#1090#1088#1099' '#1080#1079#1086#1073#1088#1072#1078#1077#1085#1080#1081
      ID = 6
      ParentID = -1
      Index = 3
      Version = 1
    end
    object cxDBVerticalGrid1IMAGE_map_WIDTH: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'IMAGE_map_WIDTH'
      ID = 7
      ParentID = 6
      Index = 0
      Version = 1
    end
    object cxDBVerticalGrid1IMAGE_map_HEIGHT: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'IMAGE_map_HEIGHT'
      ID = 8
      ParentID = 6
      Index = 1
      Version = 1
    end
    object cxDBVerticalGrid1IMAGE_PROFILE_HEIGHT: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'IMAGE_PROFILE_HEIGHT'
      ID = 9
      ParentID = 6
      Index = 2
      Version = 1
    end
    object cxDBVerticalGrid1IMAGE_PROFILE_WIDTH: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'IMAGE_PROFILE_WIDTH'
      ID = 10
      ParentID = 6
      Index = 3
      Version = 1
    end
    object cxDBVerticalGrid1DBEditorRow1: TcxDBEditorRow
      Properties.EditPropertiesClassName = 'TcxCheckBoxProperties'
      Properties.EditProperties.NullStyle = nssUnchecked
      Properties.DataBinding.FieldName = 'is_make_map_images'
      ID = 11
      ParentID = 6
      Index = 4
      Version = 1
    end
    object cxDBVerticalGrid1CategoryRow3: TcxCategoryRow
      Properties.Caption = 'Auto'
      Visible = False
      ID = 12
      ParentID = -1
      Index = 4
      Version = 1
    end
    object cxDBVerticalGrid1DBEditorRow5: TcxDBEditorRow
      Properties.Caption = #1050#1086#1083'-'#1074#1086' '#1087#1086#1090#1086#1082#1086#1074
      Properties.DataBinding.FieldName = 'thread_count'
      ID = 13
      ParentID = -1
      Index = 5
      Version = 1
    end
    object cxDBVerticalGrid1CategoryRow4: TcxCategoryRow
      Properties.Caption = #1040#1074#1090#1086#1084#1072#1090#1080#1095#1077#1089#1082#1080#1081' '#1088#1072#1089#1095#1077#1090
      ID = 14
      ParentID = -1
      Index = 6
      Version = 1
    end
    object cxDBVerticalGrid1DBEditorRow3: TcxDBEditorRow
      Properties.EditPropertiesClassName = 'TcxCheckBoxProperties'
      Properties.EditProperties.NullStyle = nssUnchecked
      Properties.DataBinding.FieldName = 'is_can_change_antenna_height'
      ID = 15
      ParentID = 14
      Index = 0
      Version = 1
    end
    object cxDBVerticalGrid1DBEditorRow6: TcxDBEditorRow
      Properties.EditPropertiesClassName = 'TcxCheckBoxProperties'
      Properties.EditProperties.NullStyle = nssUnchecked
      Properties.DataBinding.FieldName = 'is_can_change_antenna_location'
      ID = 16
      ParentID = 14
      Index = 1
      Version = 1
    end
    object cxDBVerticalGrid1DBEditorRow7: TcxDBEditorRow
      Properties.Caption = ' '#1050#1086#1083'-'#1074#1086' '#1091#1089#1087#1077#1096#1085#1099#1093' '#1088#1072#1089#1095#1077#1090#1086#1074
      Properties.DataBinding.FieldName = 'max_good_calcs'
      ID = 17
      ParentID = 14
      Index = 2
      Version = 1
    end
    object cxDBVerticalGrid1CategoryRow5: TcxCategoryRow
      Properties.Caption = #1055#1083#1086#1097#1072#1076#1082#1072
      ID = 18
      ParentID = -1
      Index = 7
      Version = 1
    end
    object cxDBVerticalGrid1DBEditorRow8: TcxDBEditorRow
      Properties.Caption = #1042#1099#1089#1086#1090#1072' '#1040#1052#1057' '#1085#1072#1076' '#1079#1076#1072#1085#1080#1077#1084', [m]'
      Properties.DataBinding.FieldName = 'place_AMC_height'
      ID = 19
      ParentID = 18
      Index = 0
      Version = 1
    end
    object cxDBVerticalGrid1DBEditorRow9: TcxDBEditorRow
      Properties.Caption = 'M'#1072#1082#1089#1080#1084#1072#1083#1100#1085#1072#1103' '#1074#1099#1089#1086#1090#1072' '#1040#1052#1057' '#1085#1072' '#1079#1076#1072#1085#1080#1080', [m]'
      Properties.DataBinding.FieldName = 'max_antenna_height_on_building'
      ID = 20
      ParentID = 18
      Index = 1
      Version = 1
    end
    object cxDBVerticalGrid1CategoryRow6: TcxCategoryRow
      Properties.Caption = 'WiMax'
      ID = 21
      ParentID = -1
      Index = 8
      Version = 1
    end
    object cxDBVerticalGrid1DBEditorRow10: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'wimax_antenna_dna_width'
      ID = 22
      ParentID = 21
      Index = 0
      Version = 1
    end
  end
  object ToolBar1: TToolBar [2]
    Left = 0
    Top = 0
    Width = 1229
    Height = 29
    BorderWidth = 1
    ButtonHeight = 25
    Caption = 'ToolBar1'
    TabOrder = 2
    object DBNavigator1: TDBNavigator
      Left = 0
      Top = 0
      Width = 96
      Height = 25
      DataSource = ds_Settings
      VisibleButtons = [nbPost, nbCancel, nbRefresh]
      TabOrder = 0
    end
  end
  inherited dxBarManager1: TdxBarManager
    Left = 500
    Top = 520
    PixelsPerInch = 96
  end
  inherited FormStorage1: TFormStorage
    Left = 500
    Top = 328
  end
  inherited ActionList_base: TActionList
    Left = 504
    Top = 456
  end
  object t_Settings: TADOTable
    Active = True
    Connection = dmMain.ADOConnection1
    CursorType = ctStatic
    TableName = 'settings'
    Left = 632
    Top = 328
  end
  object ds_Settings: TDataSource
    DataSet = t_Settings
    Left = 632
    Top = 384
  end
  object t_settings_Clutter_Logic: TADOTable
    Connection = dmMain.ADOConnection1
    CursorType = ctStatic
    TableName = 'settings_Clutter_Logic'
    Left = 1008
    Top = 328
  end
  object ds_settings_client_antenna_height: TDataSource
    DataSet = t_settings_Clutter_Logic
    Left = 1008
    Top = 384
  end
  object t_clutterModel: TADOTable
    Connection = dmMain.ADOConnection1
    CursorType = ctStatic
    TableName = 'lib.clutterModel'
    Left = 728
    Top = 328
  end
  object ds_clutterModel: TDataSource
    DataSet = t_clutterModel
    Left = 728
    Top = 384
  end
  object t_link_calc_method: TADOTable
    Connection = dmMain.ADOConnection1
    CursorType = ctStatic
    TableName = 'lib.link_calc_method'
    Left = 840
    Top = 328
  end
  object ds_link_calc_method: TDataSource
    DataSet = t_link_calc_method
    Left = 840
    Top = 384
  end
end
