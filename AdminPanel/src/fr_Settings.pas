﻿unit fr_Settings;

interface

uses

  dm_Main,
  fr_View_base,

  u_cx_ini,

  System.Classes,  Vcl.Controls, Vcl.Forms,

  Data.DB, Data.Win.ADODB, cxGridLevel, cxGrid, cxVGrid, cxDBVGrid,  Vcl.DBCtrls, Vcl.ComCtrls,
  cxGridDBBandedTableView, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, cxStyles, cxCustomData, cxFilter, cxData,
  cxDataStorage, cxEdit, cxNavigator, dxDateRanges, cxDBData,
  cxDBLookupComboBox, cxCheckBox, Vcl.ExtCtrls, Vcl.ToolWin, cxInplaceContainer,
  cxGridCustomTableView, cxGridTableView, cxGridBandedTableView,
  cxGridCustomView, System.Actions, Vcl.ActnList, RxPlacemnt, cxClasses, dxBar;

type
  Tframe_Settings = class(Tframe_View_base)
    cxGrid1: TcxGrid;
    t_Settings: TADOTable;
    ds_Settings: TDataSource;
    cxDBVerticalGrid1: TcxDBVerticalGrid;
    cxDBVerticalGrid1iteration_count: TcxDBEditorRow;
    cxDBVerticalGrid1iteration_antenna_h: TcxDBEditorRow;
    cxDBVerticalGrid1Data_dir: TcxDBEditorRow;
    t_settings_Clutter_Logic: TADOTable;
    ds_settings_client_antenna_height: TDataSource;
    t_clutterModel: TADOTable;
    ds_clutterModel: TDataSource;
    cxDBVerticalGrid1DBEditorRow2: TcxDBEditorRow;
    t_link_calc_method: TADOTable;
    ds_link_calc_method: TDataSource;
    cxDBVerticalGrid1IMAGE_PROFILE_WIDTH: TcxDBEditorRow;
    cxDBVerticalGrid1IMAGE_PROFILE_HEIGHT: TcxDBEditorRow;
    cxDBVerticalGrid1IMAGE_map_HEIGHT: TcxDBEditorRow;
    cxDBVerticalGrid1IMAGE_map_WIDTH: TcxDBEditorRow;
    cxDBVerticalGrid1CategoryRow1: TcxCategoryRow;
    ToolBar1: TToolBar;
    DBNavigator1: TDBNavigator;
    cxDBVerticalGrid1DBEditorRow4: TcxDBEditorRow;
    cxDBVerticalGrid1CategoryRow2: TcxCategoryRow;
    cxGrid1DBBandedTableView1: TcxGridDBBandedTableView;
    cxGrid1Level2: TcxGridLevel;
    cxGrid1DBBandedTableView1Column1: TcxGridDBBandedColumn;
    cxGrid1DBBandedTableView1Column2: TcxGridDBBandedColumn;
    cxGrid1DBBandedTableView1Column3: TcxGridDBBandedColumn;
    cxGrid1DBBandedTableView1Column4: TcxGridDBBandedColumn;
    cxDBVerticalGrid1CategoryRow3: TcxCategoryRow;
    cxDBVerticalGrid1DBEditorRow5: TcxDBEditorRow;
    cxDBVerticalGrid1CategoryRow4: TcxCategoryRow;
    cxDBVerticalGrid1DBEditorRow3: TcxDBEditorRow;
    cxDBVerticalGrid1DBEditorRow6: TcxDBEditorRow;
    cxDBVerticalGrid1DBEditorRow7: TcxDBEditorRow;
    cxDBVerticalGrid1DBEditorRow8: TcxDBEditorRow;
    cxDBVerticalGrid1CategoryRow5: TcxCategoryRow;
    cxDBVerticalGrid1DBEditorRow1: TcxDBEditorRow;
    cxDBVerticalGrid1DBEditorRow9: TcxDBEditorRow;
    cxDBVerticalGrid1CategoryRow6: TcxCategoryRow;
    cxDBVerticalGrid1DBEditorRow10: TcxDBEditorRow;
    procedure FormCreate(Sender: TObject);
//    procedure t_SettingsAfterPost(DataSet: TDataSet);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

//var
//  fra_TerminalTYpe: Tfra_TerminalTYpe;

implementation

{$R *.dfm}

procedure Tframe_Settings.FormCreate(Sender: TObject);
begin
  SetCaption('Настройка');
//  SetCaption('Ïàðàìåòðû ðàñ÷åòà');
 // cxGrid1.Align:=alClient;

  dmMain.OpenTable (t_Settings);
  dmMain.OpenTable (t_settings_Clutter_Logic);


  dmMain.OpenTable (t_clutterModel);
  dmMain.OpenTable (t_link_calc_method);

//  dmMain.OpenTable (t_Equipment);


  cx_LoadColumnCaptions_FromIni(cxGrid1DBBandedTableView1);
  cx_LoadColumnCaptions_FromIni(cxDBVerticalGrid1);


end;



end.

