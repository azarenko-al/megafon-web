unit fr_View_base;

interface

uses

  CodeSiteLogging,
  System.Classes,
  Vcl.Controls, Vcl.Forms, cxPC, dxBar, RxPlacemnt,
  Vcl.ActnList, System.Actions, cxClasses;

type
  Tframe_View_base = class(TForm)
    dxBarManager1: TdxBarManager;
    dxBarManager1Bar1: TdxBar;
    FormStorage1: TFormStorage;
    ActionList_base: TActionList;
    procedure FormCreate(Sender: TObject);
  private

  public
    class procedure Create_Inserted(aPageControl: TcxPageControl);
    procedure SetCaption(aCaption: string);

  end;


implementation

{$R *.dfm}

procedure Tframe_View_base.FormCreate(Sender: TObject);
begin

end;

//-------------------------------------------------------------------
class procedure Tframe_View_base.Create_Inserted(aPageControl: TcxPageControl);
//-------------------------------------------------------------------
var
  cxTabSheet1: TcxTabSheet;
  oForm: TForm;
  I: Integer;

begin
  CodeSite.Send( ClassName );


  for I := 0 to aPageControl.PageCount-1 do
   if aPageControl.Pages[i].Hint=ClassName then
   begin
     aPageControl.ActivePage := aPageControl.Pages[i];
     Exit;
   end;



  cxTabSheet1 := TcxTabSheet.Create(aPageControl);
 // cxTabSheet1.Caption := 'Caption';
  cxTabSheet1.PageControl := aPageControl;
  aPageControl.ActivePage:=cxTabSheet1;
  cxTabSheet1.Hint:=ClassName;


  oForm:=Create(cxTabSheet1);
  oForm.ManualDock(cxTabSheet1);
  oForm.Show;


  cxTabSheet1.Caption := oForm.Caption;

 // cxTabSheet1.Tag

//end;

end;

//-------------------------------------------------------------------
procedure Tframe_View_base.SetCaption(aCaption: string);
//-------------------------------------------------------------------
begin
  Caption:=aCaption;
  //pn_Header.Caption:=aCaption;

end;


end.
