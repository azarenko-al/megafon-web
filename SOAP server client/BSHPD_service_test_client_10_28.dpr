program BSHPD_service_test_client_10_28;

uses
  FastMM4,
  Vcl.Forms,
  f_Test in 'f_Test.pas' {frm_Test_client},
  u_Task_classes in '..\server\src\task\u_Task_classes.pas',
  u_Config in '..\.Shared\u_Config.pas',
  api_local_ApiService_09_16 in '..\.Shared\WSDL\api_local_ApiService_09_16.pas',
  u_Task_send_to_local_ApiService in '..\.Shared\WSDL\u_Task_send_to_local_ApiService.pas',
  u_Logger in 'W:\common XE\u_Logger.pas',
  u_const_msg in '..\.Shared\u_const_msg.pas',
  dm_Main in '..\.Shared\dm_Main.pas' {dmMain: TDataModule};

{$R *.res}

begin
  Application.Initialize;
  Application.MainFormOnTaskbar := True;
  Application.CreateForm(TdmMain, dmMain);
  Application.CreateForm(Tfrm_Test_client, frm_Test_client);
  Application.Run;
end.
