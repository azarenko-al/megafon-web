object frm_Test_client: Tfrm_Test_client
  Left = 0
  Top = 0
  Caption = 'Service client test'
  ClientHeight = 640
  ClientWidth = 906
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Visible = True
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object cxSplitter2: TcxSplitter
    Left = 0
    Top = 632
    Width = 906
    Height = 8
    AlignSplitter = salBottom
    Visible = False
  end
  object PageControl_Tasks: TPageControl
    Left = 320
    Top = 169
    Width = 586
    Height = 343
    ActivePage = TabSheet5
    Align = alRight
    TabOrder = 1
    object TabSheet1: TTabSheet
      Caption = 'Tasks'
      object cxGrid_tasks: TcxGrid
        Left = 0
        Top = 0
        Width = 578
        Height = 209
        Align = alTop
        TabOrder = 0
        object cxGrid_tasksDBTableView1: TcxGridDBTableView
          Navigator.Buttons.CustomButtons = <>
          FindPanel.DisplayMode = fpdmManual
          DataController.DataSource = ds_Task
          DataController.Summary.DefaultGroupSummaryItems = <>
          DataController.Summary.FooterSummaryItems = <
            item
            end>
          DataController.Summary.SummaryGroups = <>
          OptionsBehavior.ImmediateEditor = False
          OptionsCustomize.ColumnsQuickCustomization = True
          OptionsView.GroupByBox = False
          OptionsView.Indicator = True
          object cxGrid_tasksDBTableView1id: TcxGridDBColumn
            DataBinding.FieldName = 'id'
            Visible = False
            Width = 57
          end
          object col_Name: TcxGridDBColumn
            DataBinding.FieldName = 'name'
            Width = 83
          end
          object col_guid: TcxGridDBColumn
            DataBinding.FieldName = 'CalculationCode'
            Width = 173
          end
          object cxGrid_tasksDBTableView1client_lat: TcxGridDBColumn
            DataBinding.FieldName = 'ClientLatitude'
            Width = 61
          end
          object cxGrid_tasksDBTableView1client_lon: TcxGridDBColumn
            DataBinding.FieldName = 'ClientLongitude'
            Width = 51
          end
          object cxGrid_tasksDBTableView1client_antenna_lat: TcxGridDBColumn
            DataBinding.FieldName = 'ClientAntennaLatitude'
            Width = 50
          end
          object cxGrid_tasksDBTableView1client_antenna_lon: TcxGridDBColumn
            DataBinding.FieldName = 'ClientAntennaLongitude'
            Width = 64
          end
          object cxGrid_tasksDBTableView1client_antenna_height: TcxGridDBColumn
            DataBinding.FieldName = 'ClientAntennaHeight'
            Width = 88
          end
          object cxGrid_tasksDBTableView1is_can_change_antenna_height: TcxGridDBColumn
            DataBinding.FieldName = 'CanChangePositionH'
            Width = 99
          end
          object cxGrid_tasksDBTableView1is_can_change_antenna_pos: TcxGridDBColumn
            DataBinding.FieldName = 'CanChangePositionV'
            Width = 92
          end
          object cxGrid_tasksDBTableView1bitrate_Mbps: TcxGridDBColumn
            DataBinding.FieldName = 'MinSpeed'
            Width = 81
          end
          object cxGrid_tasksDBTableView1Column2_Technology: TcxGridDBColumn
            DataBinding.FieldName = 'Technology'
            Width = 170
          end
        end
        object cxGrid_tasksLevel1: TcxGridLevel
          GridView = cxGrid_tasksDBTableView1
        end
      end
      object cxGrid_places: TcxGrid
        Left = 0
        Top = 144
        Width = 578
        Height = 171
        Align = alBottom
        TabOrder = 1
        object cxGridDBTableView_places: TcxGridDBTableView
          Navigator.Buttons.CustomButtons = <>
          FindPanel.DisplayMode = fpdmManual
          DataController.DataSource = ds_Task_place
          DataController.Summary.DefaultGroupSummaryItems = <>
          DataController.Summary.FooterSummaryItems = <
            item
              Column = col_ObjectID
            end>
          DataController.Summary.SummaryGroups = <>
          OptionsBehavior.ImmediateEditor = False
          OptionsCustomize.ColumnsQuickCustomization = True
          OptionsView.GroupByBox = False
          OptionsView.Indicator = True
          object cxGridDBTableView_placesid: TcxGridDBColumn
            DataBinding.FieldName = 'id'
            Visible = False
          end
          object cxGridDBTableView_placestask_id: TcxGridDBColumn
            DataBinding.FieldName = 'task_id'
            Visible = False
          end
          object cxGridDBTableView_places_: TcxGridDBColumn
            DataBinding.FieldName = '_'
            Visible = False
          end
          object col_ObjectID: TcxGridDBColumn
            DataBinding.FieldName = 'ObjectID'
          end
          object cxGridDBTableView_placesplace_name: TcxGridDBColumn
            DataBinding.FieldName = 'place_name'
          end
          object cxGridDBTableView_placesLatitude: TcxGridDBColumn
            DataBinding.FieldName = 'ObjectLatitude'
          end
          object cxGridDBTableView_placesLongitude: TcxGridDBColumn
            DataBinding.FieldName = 'ObjectLongitude'
          end
          object cxGridDBTableView_placesHeight: TcxGridDBColumn
            DataBinding.FieldName = 'ObjectAntennaHeight'
          end
          object cxGridDBTableView_placesSysname: TcxGridDBColumn
            DataBinding.FieldName = 'ObjectTypeSysname'
          end
        end
        object cxGridLevel1: TcxGridLevel
          GridView = cxGridDBTableView_places
        end
      end
      object cxSplitter1: TcxSplitter
        Left = 0
        Top = 136
        Width = 578
        Height = 8
        HotZoneClassName = 'TcxSimpleStyle'
        AlignSplitter = salBottom
        Control = cxGrid_places
      end
    end
    object TabSheet5: TTabSheet
      Caption = 'Queue'
      ImageIndex = 1
      object cxGrid1: TcxGrid
        Left = 0
        Top = 0
        Width = 578
        Height = 209
        Align = alTop
        TabOrder = 0
        object cxGridDBTableView_queue: TcxGridDBTableView
          Navigator.Buttons.CustomButtons = <>
          FindPanel.DisplayMode = fpdmManual
          DataController.DataSource = ds_Queue
          DataController.Summary.DefaultGroupSummaryItems = <>
          DataController.Summary.FooterSummaryItems = <
            item
            end>
          DataController.Summary.SummaryGroups = <>
          OptionsBehavior.ImmediateEditor = False
          OptionsCustomize.ColumnsQuickCustomization = True
          OptionsView.GroupByBox = False
          OptionsView.Indicator = True
          object cxGridDBTableView_queueid: TcxGridDBColumn
            DataBinding.FieldName = 'id'
            SortIndex = 0
            SortOrder = soAscending
            Width = 55
          end
          object cxGridDBTableView_queueDate_created: TcxGridDBColumn
            DataBinding.FieldName = 'Date_created'
          end
          object cxGridDBTableView_queuehost: TcxGridDBColumn
            DataBinding.FieldName = 'host_created'
            Width = 78
          end
          object cxGridDBTableView_queuebody: TcxGridDBColumn
            DataBinding.FieldName = 'body'
            Width = 82
          end
          object cxGridDBTableView_queueguid: TcxGridDBColumn
            DataBinding.FieldName = 'guid'
            Width = 231
          end
          object cxGridDBTableView_queuehost_started: TcxGridDBColumn
            DataBinding.FieldName = 'host_started'
            Width = 102
          end
          object cxGridDBTableView_queueDate_started: TcxGridDBColumn
            DataBinding.FieldName = 'Date_started'
          end
          object cxGridDBTableView_queueESB_response: TcxGridDBColumn
            DataBinding.FieldName = 'response'
          end
          object cxGridDBTableView_queueDate_finished: TcxGridDBColumn
            DataBinding.FieldName = 'Date_finished'
          end
        end
        object cxGridLevel2: TcxGridLevel
          GridView = cxGridDBTableView_queue
        end
      end
    end
  end
  object PageControl_top: TPageControl
    Left = 0
    Top = 0
    Width = 906
    Height = 169
    ActivePage = TabSheet3
    Align = alTop
    TabOrder = 2
    object TabSheet3: TTabSheet
      Caption = 'ApiService'
      object Button2: TButton
        Left = 16
        Top = 43
        Width = 121
        Height = 25
        Caption = 'Echo'
        TabOrder = 0
        OnClick = Button2Click
      end
      object Edit_Echo: TEdit
        Left = 16
        Top = 74
        Width = 121
        Height = 21
        TabOrder = 1
        Text = '11111'
      end
      object Edit_Echo_response: TEdit
        Left = 16
        Top = 101
        Width = 121
        Height = 21
        TabOrder = 2
      end
      object Button1: TButton
        Left = 156
        Top = 43
        Width = 75
        Height = 25
        Caption = 'Send'
        TabOrder = 3
        OnClick = Button1Click
      end
      object Edit1: TEdit
        Left = 156
        Top = 74
        Width = 121
        Height = 21
        TabOrder = 4
      end
      object cb_WSDL: TComboBox
        Left = 16
        Top = 3
        Width = 569
        Height = 21
        TabOrder = 5
        Text = 'http://localhost/ApiService/Service.svc?singleWsdl'
        Items.Strings = (
          'http://localhost/ApiService/Service.svc?singleWsdl'
          'http://10.63.193.53:10073/mgbshpd/bshpd?wsdl')
      end
      object cb_Add_: TCheckBox
        Left = 156
        Top = 101
        Width = 97
        Height = 17
        Caption = 'Add '#39'--'#39
        TabOrder = 6
      end
      object cb_random_GUID: TCheckBox
        Left = 235
        Top = 101
        Width = 97
        Height = 17
        Caption = 'random GUID'
        TabOrder = 7
      end
      object bSendMany: TButton
        Left = 278
        Top = 43
        Width = 75
        Height = 25
        Caption = 'Send many'
        TabOrder = 8
        OnClick = bSendManyClick
      end
      object ed_Count: TLabeledEdit
        Left = 359
        Top = 45
        Width = 121
        Height = 21
        EditLabel.Width = 29
        EditLabel.Height = 13
        EditLabel.Caption = 'Count'
        TabOrder = 9
        Text = '10'
      end
      object Button5: TButton
        Left = 405
        Top = 72
        Width = 75
        Height = 25
        Action = FileOpen1
        TabOrder = 10
      end
      object bClearAll: TButton
        Left = 504
        Top = 43
        Width = 75
        Height = 25
        Caption = 'Clear All'
        TabOrder = 11
        OnClick = bClearAllClick
      end
      object Button3: TButton
        Left = 504
        Top = 74
        Width = 113
        Height = 25
        Action = FileOpen_Add_to_Queue
        TabOrder = 12
        OnClick = Button3Click
      end
      object DirectoryEdit_XML: TDirectoryEdit
        Left = 474
        Top = 105
        Width = 343
        Height = 21
        NumGlyphs = 1
        TabOrder = 13
        Text = 'D:\megafon request '
        Visible = False
      end
    end
  end
  object PageControl1: TPageControl
    Left = 0
    Top = 512
    Width = 906
    Height = 120
    ActivePage = TabSheet2
    Align = alBottom
    MultiLine = True
    TabOrder = 3
    object TabSheet2: TTabSheet
      Caption = 'Log'
      object Memo_Log: TMemo
        Left = 0
        Top = 0
        Width = 710
        Height = 92
        Align = alLeft
        Anchors = [akLeft, akTop, akRight, akBottom]
        ScrollBars = ssVertical
        TabOrder = 0
      end
    end
  end
  object FormStorage1: TFormStorage
    UseRegistry = True
    StoredProps.Strings = (
      'PageControl_Tasks.ActivePage'
      'PageControl_top.ActivePage'
      'cxGrid_places.Height'
      'cb_Add_.Checked'
      'cb_random_GUID.Checked'
      'ed_Count.Text'
      'cb_WSDL.Text'
      'DirectoryEdit_XML.Text')
    StoredValues = <>
    Left = 32
    Top = 384
  end
  object t_Task: TADOTable
    Connection = dmMain.ADOConnection1
    CursorType = ctStatic
    TableName = 'megagis_tests.Task'
    Left = 29
    Top = 200
  end
  object ds_Task: TDataSource
    DataSet = t_Task
    Left = 29
    Top = 248
  end
  object t_Task_places: TADOTable
    Connection = dmMain.ADOConnection1
    CursorType = ctStatic
    IndexFieldNames = 'task_id'
    MasterFields = 'id'
    MasterSource = ds_Task
    TableName = 'megagis_tests.Task_places'
    Left = 109
    Top = 200
  end
  object ds_Task_place: TDataSource
    DataSet = t_Task_places
    Left = 109
    Top = 248
  end
  object ActionList1: TActionList
    Left = 32
    Top = 432
    object FileOpen1: TFileOpen
      Category = 'File'
      Caption = '&Open...'
      Dialog.DefaultExt = '*.xml'
      Dialog.Filter = '*.xml|*.xml'
      Dialog.Options = [ofHideReadOnly, ofAllowMultiSelect, ofFileMustExist, ofEnableSizing]
      Hint = 'Open|Opens an existing file'
      ImageIndex = 7
      ShortCut = 16463
      OnAccept = FileOpen1Accept
    end
    object FileOpen_Add_to_Queue: TFileOpen
      Category = 'File'
      Caption = 'Add to queue...'
      Dialog.DefaultExt = '*.xml'
      Dialog.Filter = '*.xml|*.xml'
      Dialog.Options = [ofHideReadOnly, ofAllowMultiSelect, ofFileMustExist, ofEnableSizing]
      Hint = 'Open|Opens an existing file'
      ImageIndex = 7
      ShortCut = 16463
      OnAccept = FileOpen_Add_to_QueueAccept
    end
  end
  object ADOStoredProc1: TADOStoredProc
    Parameters = <>
    Left = 104
    Top = 312
  end
  object t_Queue: TADOTable
    Connection = dmMain.ADOConnection1
    CursorType = ctStatic
    TableName = 'WebService.Queue'
    Left = 205
    Top = 200
  end
  object ds_Queue: TDataSource
    DataSet = t_Queue
    Left = 205
    Top = 248
  end
  object ADOStoredProc_Queue_ins: TADOStoredProc
    Parameters = <>
    Left = 176
    Top = 392
  end
end
