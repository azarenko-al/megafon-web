unit f_Test;

interface

uses
  u_Task_classes,
  dm_Main,

  u_func,
  u_cx,

//  dm_Task_Calc,

  u_Task_send_to_local_ApiService,
 // u_Task_send_to_local_MG,

//  api_local_mg_10_25,

//  u_Task_send_to_local_ApiService,

  api_local_ApiService_09_16,
//  api_local_ApiService_09_02,


//  ApiCalc_Intf,
  StrUtils, IOUtils,

  System.DateUtils,
  System.SysUtils, System.Variants, System.Classes,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs,

  Data.DB,
  Data.Win.ADODB, RxPlacemnt, Vcl.StdCtrls, cxSplitter,
  cxGridLevel, cxGridDBTableView,
  cxGrid, Vcl.ExtCtrls, Vcl.ComCtrls,
  Vcl.ActnList, Vcl.StdActns, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, cxStyles, cxCustomData, cxFilter, cxData,
  cxDataStorage, cxEdit, cxNavigator, dxDateRanges, cxDBData, System.Actions,
  cxGridCustomTableView, cxGridTableView, cxClasses, cxGridCustomView, Vcl.Mask,
  RxToolEdit;

type
  Tfrm_Test_client = class(TForm)
    FormStorage1: TFormStorage;
    t_Task: TADOTable;
    ds_Task: TDataSource;
    t_Task_places: TADOTable;
    ds_Task_place: TDataSource;
    cxSplitter2: TcxSplitter;
    PageControl_Tasks: TPageControl;
    TabSheet1: TTabSheet;
    cxGrid_tasks: TcxGrid;
    cxGrid_tasksDBTableView1: TcxGridDBTableView;
    cxGrid_tasksDBTableView1id: TcxGridDBColumn;
    col_Name: TcxGridDBColumn;
    col_guid: TcxGridDBColumn;
    cxGrid_tasksDBTableView1client_lat: TcxGridDBColumn;
    cxGrid_tasksDBTableView1client_lon: TcxGridDBColumn;
    cxGrid_tasksDBTableView1client_antenna_lat: TcxGridDBColumn;
    cxGrid_tasksDBTableView1client_antenna_lon: TcxGridDBColumn;
    cxGrid_tasksDBTableView1client_antenna_height: TcxGridDBColumn;
    cxGrid_tasksDBTableView1bitrate_Mbps: TcxGridDBColumn;
    cxGrid_tasksDBTableView1is_can_change_antenna_height: TcxGridDBColumn;
    cxGrid_tasksDBTableView1is_can_change_antenna_pos: TcxGridDBColumn;
    cxGrid_tasksLevel1: TcxGridLevel;
    cxGrid_places: TcxGrid;
    cxGridDBTableView_places: TcxGridDBTableView;
    cxGridDBTableView_placesid: TcxGridDBColumn;
    cxGridDBTableView_placestask_id: TcxGridDBColumn;
    cxGridDBTableView_places_: TcxGridDBColumn;
    col_ObjectID: TcxGridDBColumn;
    cxGridDBTableView_placesplace_name: TcxGridDBColumn;
    cxGridDBTableView_placesLatitude: TcxGridDBColumn;
    cxGridDBTableView_placesLongitude: TcxGridDBColumn;
    cxGridDBTableView_placesHeight: TcxGridDBColumn;
    cxGridDBTableView_placesSysname: TcxGridDBColumn;
    cxGridLevel1: TcxGridLevel;
    cxGrid_tasksDBTableView1Column2_Technology: TcxGridDBColumn;
    PageControl_top: TPageControl;
    TabSheet3: TTabSheet;
    Button2: TButton;
    Edit_Echo: TEdit;
    Edit_Echo_response: TEdit;
    Button1: TButton;
    Edit1: TEdit;
    cb_WSDL: TComboBox;
    cxSplitter1: TcxSplitter;
    cb_Add_: TCheckBox;
    cb_random_GUID: TCheckBox;
    bSendMany: TButton;
    ed_Count: TLabeledEdit;
    ActionList1: TActionList;
    FileOpen1: TFileOpen;
    PageControl1: TPageControl;
    TabSheet2: TTabSheet;
    Memo_Log: TMemo;
    ADOStoredProc1: TADOStoredProc;
    Button5: TButton;
    bClearAll: TButton;
    TabSheet5: TTabSheet;
    cxGrid1: TcxGrid;
    cxGridDBTableView_queue: TcxGridDBTableView;
    cxGridLevel2: TcxGridLevel;
    t_Queue: TADOTable;
    ds_Queue: TDataSource;
    cxGridDBTableView_queueid: TcxGridDBColumn;
    cxGridDBTableView_queueDate_created: TcxGridDBColumn;
    cxGridDBTableView_queuehost: TcxGridDBColumn;
    cxGridDBTableView_queuebody: TcxGridDBColumn;
    cxGridDBTableView_queueguid: TcxGridDBColumn;
    FileOpen_Add_to_Queue: TFileOpen;
    Button3: TButton;
    cxGridDBTableView_queuehost_started: TcxGridDBColumn;
    cxGridDBTableView_queueDate_started: TcxGridDBColumn;
    cxGridDBTableView_queueESB_response: TcxGridDBColumn;
    cxGridDBTableView_queueDate_finished: TcxGridDBColumn;
    DirectoryEdit_XML: TDirectoryEdit;
    ADOStoredProc_Queue_ins: TADOStoredProc;
    procedure bSendManyClick(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
//    procedure Button3Click(Sender: TObject);
    procedure bClearAllClick(Sender: TObject);
    procedure Button3Click(Sender: TObject);
//    procedure Button4Click(Sender: TObject);
    procedure FileOpen1Accept(Sender: TObject);
    procedure FileOpen_Add_to_QueueAccept(Sender: TObject);
//    procedure FileOpen_QueueBeforeExecute(Sender: TObject);
//    procedure Button1_calcClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
//    procedure LoadFile(aFileName: string);
//    procedure Calc;
//    procedure Log(aMsg: string);
 // TThreadList
    { Private declarations }
  public
    function DbToTask: TTask;
    { Public declarations }
  end;

var
  frm_Test_client: Tfrm_Test_client;

implementation

{$R *.dfm}



//-------------------------------------------------------------------
procedure Tfrm_Test_client.bSendManyClick(Sender: TObject);
//-------------------------------------------------------------------
var
  I: Integer;
  oTask: TTask;
  s: string;

  dt_start: TDateTime;
begin
  dt_start:=Now;
//  CodeSite.Send( 'procedure TdmRelief.OpenByTask...' );


  oTask:=DbToTask;


  for I := 0 to StrToInt(ed_Count.Text)-1 do
  begin
    oTask.GUID_str := '--' +GenerateGUID();


    s:= IntToStr(i) + ' '+  Task_Send_to_Local_ApiService(oTask, cb_WSDL.Text);
    Memo_Log.Lines.Add(s);

    Application.ProcessMessages;
  end;

  FreeAndNil(oTask);

  t_Queue.Requery([]);

  s:= 'function TdmTask.AddFromXML(aXML: string): Integer;; done ' +
                 intToStr( MilliSecondsBetween(dt_start, Now)) + ' msec, '+
                 intToStr( SecondsBetween(dt_start, Now)) + ' sec';

  Memo_Log.Lines.Add(s);

end;

//-------------------------------------------------------------------
procedure Tfrm_Test_client.Button1Click(Sender: TObject);
//-------------------------------------------------------------------
var
  oTask: TTask;
  s: string;

  dt_start: TDateTime;
begin


  dt_start:=Now;
//  CodeSite.Send( 'procedure TdmRelief.OpenByTask...' );

  oTask:=DbToTask;
  s:=Task_Send_to_Local_ApiService(oTask, cb_WSDL.Text);

  FreeAndNil(oTask);

  Memo_Log.Lines.Add(s);

  s:= 'function TdmTask.AddFromXML(aXML: string): Integer;; done ' +
                 intToStr( MilliSecondsBetween(dt_start, Now)) + ' msec, '+
                 intToStr( SecondsBetween(dt_start, Now)) + ' sec';

  Memo_Log.Lines.Add(s);

  t_Queue.Requery([]);

end;


procedure Tfrm_Test_client.Button2Click(Sender: TObject);
var
  v: ICalcService;
begin

  v:=GetICalcService (True, cb_WSDL.Text);
  Edit_Echo_response.Text := v.Echo(Edit_Echo.Text);

end;




procedure Tfrm_Test_client.bClearAllClick(Sender: TObject);
begin
  dmMain.ExecStoredProc(ADOStoredProc1, 'WebService.sp_Queue_DEL_ALL',[]);
  t_Queue.Requery([]);
end;


procedure Tfrm_Test_client.Button3Click(Sender: TObject);
begin

end;

//-------------------------------------------------------------------
procedure Tfrm_Test_client.FormCreate(Sender: TObject);
//-------------------------------------------------------------------
begin
  cxGrid_tasks.Align:=alClient;
  cxGrid1.Align:=alClient;
  Memo_Log.Align:=alClient;

  PageControl_Tasks.Align:=alClient;

  dmMain.OpenTable(t_Queue);

  dmMain.OpenTable(t_Task);
  dmMain.OpenTable(t_Task_places);

  cx_Footer(cxGridDBTableView_places,  col_ObjectID);
  cx_Footer(cxGrid_tasksDBTableView1,  col_Name);

  cx_Footer(cxGridDBTableView_queue,  cxGridDBTableView_queueid);



end;



//-------------------------------------------------------------------
function Tfrm_Test_client.DbToTask: TTask;
//-------------------------------------------------------------------
var
  oTask: TTask;
  oTaskPlace: TTaskPlace;
begin
  Result := TTask.Create;

  oTask:=Result;


  with t_Task do
  begin
    oTask.GUID_str := IFthen(cb_Add_.Checked,  '--', '')+
                      IFthen(cb_random_GUID.Checked, GenerateGUID(),  FieldValues['CalculationCode'] );


//    function GenerateGUID: string;


    //cb_random_GUID

    oTask.wgs.client_lat          := FieldByName('ClientLatitude').AsFloat;
    oTask.wgs.client_lon          := FieldByName('ClientLongitude').AsFloat;

    oTask.wgs.client_antenna_lat    := FieldByName('ClientAntennaLatitude').AsFloat;
    oTask.wgs.client_antenna_lon    := FieldByName('ClientAntennaLongitude').AsFloat;
    oTask.client_antenna_height     := FieldByName('ClientAntennaHeight').AsFloat;

    oTask.is_can_change_antenna_height   := FieldByName('CanChangePositionH').AsBoolean;
    oTask.is_can_change_antenna_location := FieldByName('CanChangePositionV').AsBoolean;

    oTask.bitrate_Mbps        := FieldByName('MinSpeed').AsFloat;

    oTask.Technology      := FieldByName('Technology').AsString;

  end;

  assert (t_Task_places.RecordCount > 0);

//  SetLength (oCalcRequest.Points, t_Task_places.RecordCount);

  t_Task_places.DisableControls;
  t_Task_places.First;


  with t_Task_places do
    while not EOF do
    begin
      oTaskPlace:=oTask.Places.AddItem;

      oTaskPlace.ObjectID  := FieldByName('ObjectID').AsLargeInt;
 //     oTaskPlace.Sysname   :=FieldByName('ObjectTypeSysname').AsString;

      oTaskPlace.place_name   :=FieldByName('place_name').AsString;

      oTaskPlace.Pos_wgs.Lat   :=FieldByName('ObjectLatitude').AsFloat;
      oTaskPlace.Pos_wgs.Lon   :=FieldByName('ObjectLongitude').AsFloat;
      oTaskPlace.Height    :=FieldByName('ObjectAntennaHeight').AsFloat;

      Next;
    end;

  t_Task_places.EnableControls;
  t_Task_places.First;


  // TODO -cMM: Tfrm_Test.DbToTask default body inserted
end;

//-------------------------------------------------------------------
procedure Tfrm_Test_client.FileOpen1Accept(Sender: TObject);
//-------------------------------------------------------------------
var
  I: Integer;
  k: Integer;
  sFile: string;
begin
  for I := 0 to TFileOpen (Sender).Dialog.Files.Count-1 do
  begin
    sFile:=TFileOpen (Sender).Dialog.Files[i];

    k:=dmMain.ExecStoredProc (ADOStoredProc1,  'MegaGIS_tests.sp_Load_XML',
     [
       'XML_STR', TFile.ReadAllText(sFile, TEncoding.UTF8)
     ]);

  end;

//    LoadFile (TFileOpen (Sender).Dialog.Files[i]);


  t_Task.Requery([]);
  t_Task_places.Requery([]);

//  db_UpdateRecord_NoPost(FDataset, [FLD_filename_clu, TFileOpen (Sender).Dialog.FileName]);

//
end;

//-------------------------------------------------------------------
procedure Tfrm_Test_client.FileOpen_Add_to_QueueAccept(Sender: TObject);
//-------------------------------------------------------------------
var
  I: Integer;
  k: Integer;
  s: string;
  sFile: string;

begin
  for I := 0 to TFileOpen (Sender).Dialog.Files.Count-1 do
  begin
    sFile:=TFileOpen (Sender).Dialog.Files[i];

    s:=TFile.ReadAllText(sFile, TEncoding.UTF8);

    if LeftStr(s,1)<>'<?' then
      s:='<?x' + s;


    k:=dmMain.ExecStoredProc (ADOStoredProc1,  'WebService.sp_Queue_INS',
     [
       'BODY', s,
       'request_source', 'local'
     ]);

    Assert (k>0, 'WebService.sp_Queue_INS');
  end;

//    LoadFile (TFileOpen (Sender).Dialog.Files[i]);

  t_Queue.Requery([]);

  PageControl_Tasks.ActivePageIndex:=1;

end;



end.


