program BSHPD_server_monitor;

uses
  Vcl.Forms,
  f_Main in 'f_Main.pas' {frm_BSHPD_Server_monitor},
  u_const_msg in '..\.Shared\u_const_msg.pas';

{$R *.res}

begin
  Application.Initialize;
  Application.MainFormOnTaskbar := True;
  Application.CreateForm(Tfrm_BSHPD_Server_monitor, frm_BSHPD_Server_monitor);
  Application.Run;
end.
