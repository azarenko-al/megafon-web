object frm_BSHPD_Server_monitor: Tfrm_BSHPD_Server_monitor
  Left = 0
  Top = 0
  Caption = 'frm_BSHPD_Server_monitor'
  ClientHeight = 285
  ClientWidth = 661
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object PageControl1: TPageControl
    Left = 0
    Top = 64
    Width = 661
    Height = 221
    ActivePage = TabSheet2
    Align = alBottom
    Anchors = [akLeft, akTop, akRight, akBottom]
    MultiLine = True
    TabOrder = 0
    object TabSheet1: TTabSheet
      Caption = 'Log'
      ExplicitWidth = 532
      ExplicitHeight = 188
      object Memo_Log: TMemo
        Left = 0
        Top = 0
        Width = 620
        Height = 193
        Align = alLeft
        Anchors = [akLeft, akTop, akRight, akBottom]
        ScrollBars = ssVertical
        TabOrder = 0
        ExplicitWidth = 499
        ExplicitHeight = 188
      end
    end
    object TabSheet2: TTabSheet
      Caption = 'TabSheet2'
      ImageIndex = 1
      object Button1: TButton
        Left = 16
        Top = 24
        Width = 75
        Height = 25
        Caption = 'Button1'
        TabOrder = 0
        OnClick = Button1Click
      end
      object Button2: TButton
        Left = 112
        Top = 24
        Width = 75
        Height = 25
        Caption = 'Button1'
        TabOrder = 1
        OnClick = Button2Click
      end
      object Edit1: TEdit
        Left = 16
        Top = 80
        Width = 121
        Height = 21
        TabOrder = 2
        Text = 'Edit1'
      end
    end
  end
  object ed_Tasks: TLabeledEdit
    Left = 8
    Top = 27
    Width = 49
    Height = 21
    EditLabel.Width = 27
    EditLabel.Height = 13
    EditLabel.Caption = 'Tasks'
    ParentColor = True
    ReadOnly = True
    TabOrder = 1
  end
  object cxProgressBar1: TcxProgressBar
    Left = 63
    Top = 27
    Position = 34.000000000000000000
    Properties.BeginColor = 54056
    Properties.PeakValue = 67.000000000000000000
    Properties.ShowOverload = True
    Properties.ShowTextStyle = cxtsPosition
    TabOrder = 2
    Width = 177
  end
  object FormStorage1: TFormStorage
    Options = [fpPosition]
    UseRegistry = True
    StoredProps.Strings = (
      'ed_Tasks.Text')
    StoredValues = <>
    Left = 280
    Top = 8
  end
end
