unit f_Main;

interface

uses
  u_const_msg,

  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, Vcl.ComCtrls, RxPlacemnt,
  cxGraphics, cxControls, cxLookAndFeels, cxLookAndFeelPainters, cxContainer,
  cxEdit, cxProgressBar, Vcl.ExtCtrls;

type
  Tfrm_BSHPD_Server_monitor = class(TForm)
    FormStorage1: TFormStorage;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    Memo_Log: TMemo;
    ed_Tasks: TLabeledEdit;
    cxProgressBar1: TcxProgressBar;
    TabSheet2: TTabSheet;
    Button1: TButton;
    Button2: TButton;
    Edit1: TEdit;
    procedure Button1Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
//    procedure FormCreate(Sender: TObject);
  private

  procedure WMCopyData(var Msg: TWMCopyData); message wm_CopyData;

    { Private declarations }
  public
    { Public declarations }
  end;

var
  frm_BSHPD_Server_monitor: Tfrm_BSHPD_Server_monitor;

implementation

{$R *.dfm}

procedure Tfrm_BSHPD_Server_monitor.Button1Click(Sender: TObject);
begin
  Monitor_Send_Log (Edit1.Text);
end;

procedure Tfrm_BSHPD_Server_monitor.Button2Click(Sender: TObject);
begin
   Monitor_Send_Task_Count (345);
end;

procedure Tfrm_BSHPD_Server_monitor.FormCreate(Sender: TObject);
begin
  Caption:='monitor';

  Memo_Log.Align:=alClient;

end;


procedure Tfrm_BSHPD_Server_monitor.WMCopyData(var Msg: TWMCopyData);
var
  s: string;
begin

  case Msg.CopyDataStruct.dwData of
    Integer(mtLog): begin
                      s := pchar(Msg.CopyDataStruct.lpData) ;
                      Memo_Log.Lines.Add( s);
                    end;

    Integer(mtTaskCount): begin
//                      s := pchar(Msg.CopyDataStruct.lpData) ;
                      ed_Tasks.Text:= IntToStr(Msg.CopyDataStruct.cbData);
                    end;

  end;


end;

end.


//  DataStruct.dwData := 0;
//  DataStruct.cbData := length(Edit1.text)+1;
//  DataStruct.lpData := pchar(Edit1.text);


//
//{
//type//  TReceiverMainForm = class(TForm)//  private//   procedure WMCopyData(var Msg : TWMCopyData) ; message WM_COPYDATA;//{ ... }//implementation//{ ... }//procedure TReceiverMainForm.WMCopyData(var Msg: TWMCopyData) ;//var//  s : string;//begin//  s := PChar(Msg.CopyDataStruct.lpData) ;
////������� ���-������ �����//  msg.Result := 2006;//end;
//
//procedure TForm2.FormCreate(Sender: TObject);
//begin
//
//end;
//

