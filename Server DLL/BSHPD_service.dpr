library BSHPD_service;

uses
  ComServ,
  CalcService_TLB in 'CalcService_TLB.pas',
  x_CalcService in 'x_CalcService.pas' {CalcServiceX: CoClass},
  dm_Queue in '..\Server\src\task\dm_Queue.pas' {dmQueue: TDataModule},
  dm_Task in '..\server\src\task\dm_Task.pas' {dmTask: TDataModule},
  dm_Task_Calc in '..\Server\src\task\dm_Task_Calc.pas' {dmTask_calc: TDataModule},
  dm_Relief in '..\Server\src\dm_Relief.pas' {dmRelief: TDataModule},
  u_Logger in 'W:\common XE\u_Logger.pas',
  u_Config in '..\.Shared\u_Config.pas',
  u_Task_classes in '..\server\src\task\u_Task_classes.pas',
  fr_Profile_Rel_Graph in '..\Server\src\fr_Profile_Rel_Graph.pas' {frame_Profile_Rel_Graph},
  dm_Map in '..\Server\src\Map\dm_Map.pas',
  dm_Main in '..\.Shared\dm_Main.pas' {dmMain: TDataModule};

exports
  DllGetClassObject,
  DllCanUnloadNow,
  DllRegisterServer,
  DllUnregisterServer,
  DllInstall;

{$R *.TLB}

{$R *.RES}

begin
end.
