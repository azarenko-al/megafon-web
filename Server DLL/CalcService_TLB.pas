unit CalcService_TLB;

// ************************************************************************ //
// WARNING
// -------
// The types declared in this file were generated from data read from a
// Type Library. If this type library is explicitly or indirectly (via
// another type library referring to this type library) re-imported, or the
// 'Refresh' command of the Type Library Editor activated while editing the
// Type Library, the contents of this file will be regenerated and all
// manual modifications will be lost.
// ************************************************************************ //

// $Rev: 52393 $
// File generated on 30.10.2020 19:04:30 from Type Library described below.

// ************************************************************************  //
// Type Lib: W:\WEB\Server DLL\Project2 (1)
// LIBID: {91AE8A49-C72C-4960-9081-441D5970754E}
// LCID: 0
// Helpfile:
// HelpString:
// DepndLst:
//   (1) v2.0 stdole, (C:\Windows\SysWOW64\stdole2.tlb)
//   (2) v4.0 StdVCL, (stdvcl40.dll)
// SYS_KIND: SYS_WIN32
// ************************************************************************ //
{$TYPEDADDRESS OFF} // Unit must be compiled without type-checked pointers.
{$WARN SYMBOL_PLATFORM OFF}
{$WRITEABLECONST ON}
{$VARPROPSETTER ON}
{$ALIGN 4}

interface

uses Winapi.Windows, System.Classes, System.Variants, System.Win.StdVCL, Vcl.Graphics, Vcl.OleServer, Winapi.ActiveX;


// *********************************************************************//
// GUIDS declared in the TypeLibrary. Following prefixes are used:
//   Type Libraries     : LIBID_xxxx
//   CoClasses          : CLASS_xxxx
//   DISPInterfaces     : DIID_xxxx
//   Non-DISP interfaces: IID_xxxx
// *********************************************************************//
const
  // TypeLibrary Major and minor versions
  CalcServiceMajorVersion = 1;
  CalcServiceMinorVersion = 0;

  LIBID_CalcService: TGUID = '{91AE8A49-C72C-4960-9081-441D5970754E}';

  IID_ICalcServiceX: TGUID = '{9D2FC03F-BCD9-4270-8553-6000A317767F}';
  CLASS_CalcServiceX: TGUID = '{1CF1E67E-13B1-4EB0-9614-B0FD8B13C092}';
type

// *********************************************************************//
// Forward declaration of types defined in TypeLibrary
// *********************************************************************//
  ICalcServiceX = interface;

// *********************************************************************//
// Declaration of CoClasses defined in Type Library
// (NOTE: Here we map each CoClass to its Default Interface)
// *********************************************************************//
  CalcServiceX = ICalcServiceX;


// *********************************************************************//
// Interface: ICalcServiceX
// Flags:     (256) OleAutomation
// GUID:      {9D2FC03F-BCD9-4270-8553-6000A317767F}
// *********************************************************************//
  ICalcServiceX = interface(IUnknown)
    ['{9D2FC03F-BCD9-4270-8553-6000A317767F}']
    function Method1: HResult; stdcall;
  end;

// *********************************************************************//
// The Class CoCalcServiceX provides a Create and CreateRemote method to
// create instances of the default interface ICalcServiceX exposed by
// the CoClass CalcServiceX. The functions are intended to be used by
// clients wishing to automate the CoClass objects exposed by the
// server of this typelibrary.
// *********************************************************************//
  CoCalcServiceX = class
    class function Create: ICalcServiceX;
    class function CreateRemote(const MachineName: string): ICalcServiceX;
  end;

implementation

uses System.Win.ComObj;

class function CoCalcServiceX.Create: ICalcServiceX;
begin
  Result := CreateComObject(CLASS_CalcServiceX) as ICalcServiceX;
end;

class function CoCalcServiceX.CreateRemote(const MachineName: string): ICalcServiceX;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_CalcServiceX) as ICalcServiceX;
end;

end.

