unit Options_TLB;

// ************************************************************************ //
// WARNING                                                                    
// -------                                                                    
// The types declared in this file were generated from data read from a       
// Type Library. If this type library is explicitly or indirectly (via        
// another type library referring to this type library) re-imported, or the   
// 'Refresh' command of the Type Library Editor activated while editing the   
// Type Library, the contents of this file will be regenerated and all        
// manual modifications will be lost.                                         
// ************************************************************************ //

// $Rev: 52393 $
// File generated on 02.06.2017 15:15:39 from Type Library described below.

// ************************************************************************  //
// Type Lib: W:\ActiveX_RPLS_DB\Options\Server\Project18 (1)
// LIBID: {71B1B8A5-C32D-480E-99B5-006AAEE0E40D}
// LCID: 0
// Helpfile:
// HelpString: 
// DepndLst: 
//   (1) v2.0 stdole, (C:\Windows\system32\stdole2.tlb)
//   (2) v4.0 StdVCL, (stdvcl40.dll)
// SYS_KIND: SYS_WIN32
// ************************************************************************ //
{$TYPEDADDRESS OFF} // Unit must be compiled without type-checked pointers. 
{$WARN SYMBOL_PLATFORM OFF}
{$WRITEABLECONST ON}
{$VARPROPSETTER ON}
{$ALIGN 4}

interface

uses ComObj;


// *********************************************************************//
// GUIDS declared in the TypeLibrary. Following prefixes are used:        
//   Type Libraries     : LIBID_xxxx                                      
//   CoClasses          : CLASS_xxxx                                      
//   DISPInterfaces     : DIID_xxxx                                       
//   Non-DISP interfaces: IID_xxxx                                        
// *********************************************************************//
const
  // TypeLibrary Major and minor versions
  OptionsMajorVersion = 1;
  OptionsMinorVersion = 0;

  LIBID_Options: TGUID = '{26FE5A0B-4DC0-4DA1-A6D6-C2ECFBBBF3D3}';

  IID_IOptionsX: TGUID = '{E4F03F41-106D-46D5-870B-974E0B0A3BCC}';
  CLASS_OptionsX: TGUID = '{B05FE238-4392-48B5-907A-D6135D3A5287}';
type

// *********************************************************************//
// Forward declaration of types defined in TypeLibrary                    
// *********************************************************************//
  IOptionsX = interface;

// *********************************************************************//
// Declaration of CoClasses defined in Type Library                       
// (NOTE: Here we map each CoClass to its Default Interface)              
// *********************************************************************//
  OptionsX = IOptionsX;


// *********************************************************************//
// Declaration of structures, unions and aliases.                         
// *********************************************************************//
  TOptionStyleX11111111111 = record
    LineColor: Integer;
    LineWidth: Integer;
    LineStyle: Integer;
    SymbolCharacter: Integer;
    SymbolFontName: WideString;
    SymbolFontColor: Integer;
    SymbolFontSize: Integer;
    RegionBorderWidth: Integer;
    RegionBorderStyle: Integer;
    RegionBorderColor: Integer;
    RegionColor: Integer;
    RegionBackColor: Integer;
  end;


// *********************************************************************//
// Interface: IOptionsX
// Flags:     (256) OleAutomation
// GUID:      {E4F03F41-106D-46D5-870B-974E0B0A3BCC}
// *********************************************************************//
  IOptionsX = interface(IUnknown)
    ['{E4F03F41-106D-46D5-870B-974E0B0A3BCC}']
    function Dlg_ChangeCoordSys: HResult; stdcall;
    function Get_CoordSys: HResult; stdcall;
  end;

// *********************************************************************//
// The Class CoOptionsX provides a Create and CreateRemote method to          
// create instances of the default interface IOptionsX exposed by              
// the CoClass OptionsX. The functions are intended to be used by             
// clients wishing to automate the CoClass objects exposed by the         
// server of this typelibrary.                                            
// *********************************************************************//
  CoOptionsX = class
    class function Create: IOptionsX;
    class function CreateRemote(const MachineName: string): IOptionsX;
  end;

implementation


class function CoOptionsX.Create: IOptionsX;
begin
  Result := CreateComObject(CLASS_OptionsX) as IOptionsX;
end;

class function CoOptionsX.CreateRemote(const MachineName: string): IOptionsX;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_OptionsX) as IOptionsX;
end;

end.

