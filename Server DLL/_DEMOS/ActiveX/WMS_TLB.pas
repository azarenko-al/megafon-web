unit WMS_TLB;

// ************************************************************************ //
// WARNING
// -------
// The types declared in this file were generated from data read from a
// Type Library. If this type library is explicitly or indirectly (via
// another type library referring to this type library) re-imported, or the
// 'Refresh' command of the Type Library Editor activated while editing the
// Type Library, the contents of this file will be regenerated and all
// manual modifications will be lost.
// ************************************************************************ //

// $Rev: 52393 $
// File generated on 01.08.2016 15:09:16 from Type Library described below.

// ************************************************************************  //
// Type Lib: W:\WMS\ActiveX\Project17 (1)
// LIBID: {9EC5117B-A882-4258-9D39-A091DB3CC45D}
// LCID: 0
// Helpfile:
// HelpString:
// DepndLst:
//   (1) v2.0 stdole, (C:\Windows\system32\stdole2.tlb)
//   (2) v4.0 StdVCL, (stdvcl40.dll)
// SYS_KIND: SYS_WIN32
// ************************************************************************ //
{$TYPEDADDRESS OFF} // Unit must be compiled without type-checked pointers.
{$WARN SYMBOL_PLATFORM OFF}
{$WRITEABLECONST ON}
{$VARPROPSETTER ON}
{$ALIGN 4}

interface

uses ComObj, ActiveX;

// *********************************************************************//
// GUIDS declared in the TypeLibrary. Following prefixes are used:        
//   Type Libraries     : LIBID_xxxx                                      
//   CoClasses          : CLASS_xxxx                                      
//   DISPInterfaces     : DIID_xxxx                                       
//   Non-DISP interfaces: IID_xxxx                                        
// *********************************************************************//
const
  // TypeLibrary Major and minor versions
  wmsMajorVersion = 1;
  wmsMinorVersion = 0;

  LIBID_wms: TGUID = '{53281203-752C-4ACD-8980-4565F14C2F36}';

  IID_ITileManagerX: TGUID = '{AEF308AE-3148-4B18-9C8C-BE75B21D5761}';
  CLASS_TileManagerX: TGUID = '{2695D3EA-9B19-43BC-B2F5-9224DFF3012C}';

// *********************************************************************//
// Declaration of Enumerations defined in Type Library                    
// *********************************************************************//
// Constants for enum TActions
type
  TActions = TOleEnum;
const
  DEF_DLG_SET_CENTER = $00000000;
  DEF_DLG_LOG = $00000001;

type

// *********************************************************************//
// Forward declaration of types defined in TypeLibrary                    
// *********************************************************************//
  ITileManagerX = interface;

// *********************************************************************//
// Declaration of CoClasses defined in Type Library                       
// (NOTE: Here we map each CoClass to its Default Interface)              
// *********************************************************************//
  TileManagerX = ITileManagerX;


// *********************************************************************//
// Declaration of structures, unions and aliases.                         
// *********************************************************************//
  PInteger1 = ^Integer; {*}


// *********************************************************************//
// Interface: ITileManagerX
// Flags:     (256) OleAutomation
// GUID:      {AEF308AE-3148-4B18-9C8C-BE75B21D5761}
// *********************************************************************//
  ITileManagerX = interface(IUnknown)
    ['{AEF308AE-3148-4B18-9C8C-BE75B21D5761}']

    function Clear: HResult; stdcall;
    function Dlg_Log: HResult; stdcall;
    function Get_Layers_text(var aResult: WideString): HResult; stdcall;
    function Get_Z: Integer; stdcall;
    function Init(const aMapX: IDispatch): HResult; stdcall;
    function Load_CMap: HResult; stdcall;
    function Set_Center(aLat: Double; aLon: Double; aZ: Integer): HResult; stdcall;
    function Set_Dir(const Value: WideString): HResult; stdcall;
    function Set_WMS_layer(const aName: WideString): HResult; stdcall;
    function Set_WMS_layer_Index(aIndex: Integer): HResult; stdcall;
    function Set_Z(Value: Integer): HResult; stdcall;

    {
    function Clear: HResult; stdcall;
    function Set_Dir(const Value: WideString): HResult; stdcall;
    function Set_Z(Value: Integer): HResult; stdcall;
    function Get_Z: Integer; stdcall;
    function Set_WMS_layer(const aName: WideString): HResult; stdcall;
    function Load_CMap: HResult; stdcall;
    function Set_Center(aLat: Double; aLon: Double; aZ: Integer): HResult; stdcall;
    function Init(const aMapX: IDispatch): HResult; stdcall;
    function GetClassGUID: TGUID; stdcall;
    function Exec(Value: Integer): HResult; stdcall;
    }

  end;

// *********************************************************************//
// The Class CoTileManagerX provides a Create and CreateRemote method to          
// create instances of the default interface ITileManagerX exposed by              
// the CoClass TileManagerX. The functions are intended to be used by             
// clients wishing to automate the CoClass objects exposed by the         
// server of this typelibrary.                                            
// *********************************************************************//
  CoTileManagerX = class
    class function Create: ITileManagerX;
    class function CreateRemote(const MachineName: string): ITileManagerX;
  end;

implementation

 

class function CoTileManagerX.Create: ITileManagerX;
begin
  Result := CreateComObject(CLASS_TileManagerX) as ITileManagerX;
end;

class function CoTileManagerX.CreateRemote(const MachineName: string): ITileManagerX;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_TileManagerX) as ITileManagerX;
end;

end.

