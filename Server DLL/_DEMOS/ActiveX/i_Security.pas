unit i_Security;

interface

uses
  Windows, shellapi,  Forms, Sysutils, Dialogs,

  u_debug,

  u_com,
  Security_TLB, snmp_TLB;


type
  TComClient_Security = class
  private
  //  FFileName : string;

    dll: THandle;
    fIntf: ISecurityX;
  public
//    class procedure Init;

    constructor Create;
    destructor Destroy; override;

//    procedure NewMethod;

//    function Get_ITileManagerX: ITileManagerX;

    property Intf: ISecurityX read fIntf;
  end;


function Init_ISecurityX: ISecurityX;


implementation


// ---------------------------------------------------------------
function Init_ISecurityX: ISecurityX;
// ---------------------------------------------------------------
const
  DEF_FILE = 'Security.dll';

var
  dll: Integer;
  sFile: string;

begin

  sFile:= ExtractFilePath(Application.ExeName) + 'DLL\' + DEF_FILE;

//  if not FileExists(sFile) then
//    sFile:= ExtractFilePath(Application.ExeName) + 'WMS\' + DEF_FILE;

  Assert(FileExists(sFile), sFile);

  dll:=GetComObject(sFile, CLASS_SecurityX, ISecurityX, Result);

  Assert(Assigned(Result));

  Assert (dll>0, 'GetComObject dll=0');

  Debug_int (dll, 'function Init_ISecurityX: IFilterX; dll - ');

end;

//var
//  FISnmp: ISnmp;



// ---------------------------------------------------------------
constructor TComClient_Security.Create;
// ---------------------------------------------------------------
const
  DEF_FILE = 'Security.dll';

var
  dll: Integer;
  sFile: string;

begin

  sFile:= ExtractFilePath(Application.ExeName) + 'DLL\' + DEF_FILE;

//  if not FileExists(sFile) then
//    sFile:= ExtractFilePath(Application.ExeName) + 'WMS\' + DEF_FILE;

  Assert(FileExists(sFile), sFile);

  dll:=GetComObject(sFile, CLASS_SecurityX, ISecurityX, fIntf);
//  dll:=GetComObject(sFile, CLASS_SnmpX_, ISnmpX_, fIntf);

  Assert (dll>0, 'ISecurityX dll=0');

  Assert (Assigned (fIntf), 'ISecurityX');



end;

destructor TComClient_Security.Destroy;
begin
  FIntf:=nil;

  if dll >= 32 then
    FreeLibrary(dll);

  inherited;
end;


end.


(*


{



//-------------------------------------------------------------------
function GetComObject(aDllFileName: PChar; const CLSID, IID: TGUID; var aObj): THandle;
//-------------------------------------------------------------------
type
  TGetObject = function(const CLSID, IID: TGUID; var aObj): HResult; stdcall;
var
  GetObject: TGetObject;
  vFactory: IClassFactory;

begin
  Result:=LoadLibrary(aDllFileName);
  Integer(aObj):=0;

  if Result >= 32 then
  begin
    @GetObject:=GetProcAddress(Result,'DllGetClassObject');

    if Assigned(GetObject) then
      if GetObject(CLSID,IClassFactory,vFactory) = S_OK then
        vFactory.CreateInstance(nil,IID,aObj);

    vFactory:=nil
  end
end;

}



type

  TComClient11111111 = class
//    constructor Create(aFileName: string);
    destructor Destroy; override;
  private
    dll: THandle;

    FFileName: string;

//    fIntf: ITileManagerX;
  public
    procedure OpenFile(aFileName: string);

  //  constructor Create(aFileName: string);

//    procedure OpenFile;

  //  property Intf: ITileManagerX read fIntf;
  end;



  TWmsComClient11111111111111 = class
    constructor Create;
    destructor Destroy; override;
  private
    FFileName : string;

    dll: THandle;
    fIntf: ITileManagerX;
  public
    class procedure Init;
//    procedure NewMethod;

    function Get_ITileManagerX: ITileManagerX;

    property Intf: ITileManagerX read fIntf;
  end;



  

class procedure TWmsComClient11111111111111.Init;
begin
  // TODO -cMM: TWmsComClient11111111111111.Init default body inserted
end;



// ---------------------------------------------------------------
constructor TWmsComClient11111111111111.Create;
// ---------------------------------------------------------------
  // C:\ONEGA\RPLS_DB\bin\WMS
const
//  DEF_FILE = 'WMS_10_21.dll';
  DEF_FILE = 'wms.dll';

var
  sFile: string;
begin
  inherited;

  //ITileManagerX

  sFile:= ExtractFilePath(Application.ExeName) + DEF_FILE;
  if not FileExists(sFile) then
    sFile:= ExtractFilePath(Application.ExeName) + 'WMS\' + DEF_FILE;

  Assert(FileExists(sFile), sFile);


  FFileName:=sFile;


  dll:=GetComObject(sFile, // 'dll_cut.dll',
                    CLASS_TileManagerX, ITileManagerX, FIntf);

end;


destructor TWmsComClient11111111111111.Destroy;
var
  b: Boolean;

begin
//  FIntf:=nil;

  if dll >= 32 then
  begin
 //   b:=FreeLibrary(dll);

  end;

end;


function TWmsComClient11111111111111.Get_ITileManagerX: ITileManagerX;
begin
  dll:= GetComObject(FFileName, // 'dll_cut.dll',
                    CLASS_TileManagerX, ITileManagerX, Result);

                    
end;



destructor TComClient11111111.Destroy;
begin
  if dll >= 32 then
    FreeLibrary(dll);

  inherited;
end;

procedure TComClient11111111.OpenFile(aFileName: string);
begin
  // TODO -cMM: TComClient11111111.OpenFile default body inserted
end;

