object Form2: TForm2
  Left = 965
  Top = 242
  Width = 784
  Height = 647
  Caption = 'Form2'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object Button1: TButton
    Left = 32
    Top = 32
    Width = 75
    Height = 25
    Caption = 'ISnmp'
    TabOrder = 0
    OnClick = Button1Click
  end
  object Button2: TButton
    Left = 32
    Top = 144
    Width = 281
    Height = 25
    Caption = 'Init_IMapEngineX.Dlg_Label_style(sText);'
    TabOrder = 1
    OnClick = Button2Click
  end
  object Button3: TButton
    Left = 40
    Top = 288
    Width = 193
    Height = 25
    Caption = 'Init_IAuditX.ExecDlg'
    TabOrder = 2
    OnClick = Button3Click
  end
  object Button4: TButton
    Left = 32
    Top = 200
    Width = 281
    Height = 25
    Caption = '  Init_IMapEngineX.Dlg_style(sText);'
    TabOrder = 3
    OnClick = Button4Click
  end
  object DBGrid1: TDBGrid
    Left = 0
    Top = 482
    Width = 776
    Height = 137
    Align = alBottom
    DataSource = ds_ObjectMaps
    TabOrder = 4
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
  end
  object DBMemo1: TDBMemo
    Left = 432
    Top = 240
    Width = 257
    Height = 209
    DataField = 'style'
    DataSource = ds_ObjectMaps
    TabOrder = 5
    WordWrap = False
  end
  object Button5: TButton
    Left = 32
    Top = 336
    Width = 201
    Height = 25
    Caption = 'Init_ISecurityX.Dlg('
    TabOrder = 6
    OnClick = Button5Click
  end
  object Button6: TButton
    Left = 32
    Top = 384
    Width = 201
    Height = 25
    Caption = '  i_GeoCoding,'
    TabOrder = 7
    OnClick = Button6Click
  end
  object ADOConnection1: TADOConnection
    Connected = True
    ConnectionString = 
      'Provider=MSDASQL.1;Password=sa;Persist Security Info=True;User I' +
      'D=sa;Data Source=onega_link'
    LoginPrompt = False
    Mode = cmShareDenyNone
    Left = 472
    Top = 16
  end
  object ds_ObjectMaps: TDataSource
    DataSet = ADOStoredProc_Object_Maps
    Left = 472
    Top = 188
  end
  object ADOStoredProc_Object_Maps: TADOStoredProc
    Active = True
    Connection = ADOConnection1
    CursorType = ctStatic
    Filter = 'mapobject_name='#39'link'#39' or mapobject_name='#39'property'#39
    Filtered = True
    LockType = ltBatchOptimistic
    ProcedureName = 'sp_MapDesktop_select_ObjectMaps'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = 9
      end
      item
        Name = '@ID'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@CHECKED'
        Attributes = [paNullable]
        DataType = ftBoolean
        Value = Null
      end>
    Left = 472
    Top = 136
  end
end
