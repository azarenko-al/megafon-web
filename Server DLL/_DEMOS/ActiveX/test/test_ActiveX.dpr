program test_ActiveX;

uses
  Forms,
  Unit2 in 'Unit2.pas' {Form2},
  i_GeoCoding in '..\i_GeoCoding.pas',
  Map_Engine_TLB in '..\Map_Engine_TLB.pas',
  Security_TLB in '..\Security_TLB.pas',
  i_Security in '..\i_Security.pas',
  i_SNMP in '..\i_SNMP.pas',
  i_Relief in '..\i_Relief.pas',
  Relief_TLB in '..\Relief_TLB.pas',
  snmp_TLB in '..\snmp_TLB.pas',
  i_Filter in '..\i_Filter.pas',
  i_WMS in '..\i_WMS.pas',
  Options_TLB in '..\Options_TLB.pas',
  GeoCoding_TLB in '..\GeoCoding_TLB.pas';

{$R *.res}

begin
  Application.Initialize;
  Application.CreateForm(TForm2, Form2);
  Application.Run;
end.
