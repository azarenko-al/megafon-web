unit x_CalcService;

{$WARN SYMBOL_PLATFORM OFF}

interface

uses
  CalcService_TLB,

  dm_Main,
  dm_Queue,
  dm_Map,
  dm_Task,
  dm_Task_calc,
  dm_Relief,


  Forms,SysUtils,
  Dialogs, Windows, ActiveX, Classes, ComObj, StdVcl;

type
 {$IFDEF dll}
  TCalcServiceX = class(TTypedComObject, ICalcServiceX)
  protected
    procedure Initialize; override;
  {$ELSE}
  TCalcServiceX = class(TInterfacedObject, ICalcServiceX)
  {$ENDIF}
  private
    procedure InitVars;

  public
    destructor Destroy; override;

    function Method1: HResult; stdcall;
  end;

//
//  {$IFDEF dll}
//  TTileManagerX = class(TTypedComObject,  ITileManagerX)
//  {$ELSE}
//  TTileManagerX = class(TInterfacedObject, ITileManagerX)
//  {$ENDIF}


implementation

uses ComServ;

procedure TCalcServiceX.InitVars;
begin
  Application.CreateForm(TdmMain, dmMain);
  Application.CreateForm(TdmQueue, dmQueue);
  Application.CreateForm(TdmMap, dmMap);
  Application.CreateForm(TdmTask, dmTask);
  Application.CreateForm(TdmTask_calc, dmTask_calc);
  Application.CreateForm(TdmRelief, dmRelief);

//  Application.CreateForm(TdmAPI_main, dmAPI_main);

end;

destructor TCalcServiceX.Destroy;
begin

  FreeAndNil(dmMain );
  FreeAndNil(dmQueue );
  FreeAndNil(dmMap );
  FreeAndNil(dmTask );
  FreeAndNil(dmTask_calc );
  FreeAndNil(dmRelief );
  FreeAndNil(dmQueue );
//  FreeAndNil(TdmAPI_main, dmAPI_main);


  inherited;
  //
end;

{$IFDEF dll}
procedure TCalcServiceX.Initialize;
begin
  inherited;
  InitVars();
end;
{$ENDIF}

function TCalcServiceX.Method1: HResult;
begin

  ShowMessage ('111111111111111111');


end;

initialization
{$IFDEF dll}

  TTypedComObjectFactory.Create(ComServer, TCalcServiceX, Class_CalcServiceX,
    ciMultiInstance, tmApartment);

{$ENDIF}
end.
