program BSHPD_service;
{$APPTYPE GUI}

uses
// -- FastMM4,
  Vcl.Forms,
  Web.WebReq,
  f_Main_server_ in 'f_Main_server_.pas' {frm_Main},
  dm_Main in '..\AdminPanel\src\dm_Main.pas' {dmMain: TDataModule},
  u_Config in '..\.Shared\u_Config.pas',
  dm_Relief in 'src\dm_Relief.pas' {dmRelief: TDataModule},
  dm_Task_Calc in 'src\task\dm_Task_Calc.pas' {dmTask_calc: TDataModule},
  u_Task_classes in 'src\task\u_Task_classes.pas',
  u_web in 'W:\common XE\Web\u_web.pas',
  dm_API_main in 'src\MSMQ\dm_API_main.pas' {dmAPI_main: TDataModule},
  api_remote_mg_09_01 in '..\.Shared\WSDL\api_remote_mg_09_01.pas',
  d_Map_print in 'src\Map\d_Map_print.pas' {dlg_Map_print},
  u_const_msg in '..\.Shared\u_const_msg.pas',
  dm_Map in '..\AdminPanel\src\Map\dm_Map.pas' {dmMap: TDataModule},
  u_Task_send_to_Local_MG in '..\.Shared\WSDL\u_Task_send_to_Local_MG.pas',
  u_Task_send_to_Remote_MG_prom in '..\.Shared\WSDL\u_Task_send_to_Remote_MG_prom.pas',
  fr_Profile_Rel_Graph in '..\AdminPanel\src\Profile\fr_Profile_Rel_Graph.pas' {frame_Profile_Rel_Graph},
  u_Logger in 'W:\common XE\u_Logger.pas',
  XmlSerializer in 'W:\common XE\XML\XmlSerializer.pas',
  dm_Queue in 'src\task\dm_Queue.pas' {dmQueue: TDataModule},
  dm_Task in 'src\task\dm_Task.pas' {dmTask: TDataModule},
  api_local_mg_10_20 in '..\.Shared\WSDL\api_local_mg_10_20.pas',
  api_mg_prom_10_21 in '..\.Shared\WSDL\api_mg_prom_10_21.pas',
  u_Task_send_to_Remote_MG_test in '..\.Shared\WSDL\u_Task_send_to_Remote_MG_test.pas',
  api_local_mg_10_21 in '..\.Shared\WSDL\api_local_mg_10_21.pas';

{$R *.res}

begin
 // if WebRequestHandler <> nil then
  //  WebRequestHandler.WebModuleClass := WebModuleClass;

  Application.Initialize;
  Application.CreateForm(TdmMain, dmMain);
  Application.CreateForm(TdmQueue, dmQueue);
  Application.CreateForm(TdmMap, dmMap);
  Application.CreateForm(TdmTask, dmTask);
  Application.CreateForm(TdmTask_calc, dmTask_calc);
  Application.CreateForm(TdmRelief, dmRelief);
  Application.CreateForm(TdmQueue, dmQueue);
  Application.CreateForm(TdmAPI_main, dmAPI_main);
  Application.CreateForm(Tfrm_Main, frm_Main);
  // Application.CreateForm(Tfrm_Test, frm_Test);
  Application.Run;
end.
