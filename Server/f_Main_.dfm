object frm_Main: Tfrm_Main
  Left = 271
  Top = 114
  Caption = 'frm_Main'
  ClientHeight = 459
  ClientWidth = 830
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object ButtonStart: TButton
    Left = 16
    Top = 8
    Width = 75
    Height = 25
    Action = act_Start
    TabOrder = 0
  end
  object btnStop: TButton
    Left = 97
    Top = 8
    Width = 75
    Height = 25
    Action = act_Stop
    TabOrder = 1
  end
  object PageControl1: TPageControl
    Left = 0
    Top = 120
    Width = 830
    Height = 320
    ActivePage = TabSheet1
    Align = alBottom
    Anchors = [akLeft, akTop, akRight, akBottom]
    MultiLine = True
    TabOrder = 2
    object TabSheet2: TTabSheet
      Caption = #1046#1091#1088#1085#1072#1083
      object Memo_Log: TMemo
        Left = 0
        Top = 0
        Width = 756
        Height = 292
        Align = alLeft
        Anchors = [akLeft, akTop, akRight, akBottom]
        ScrollBars = ssVertical
        TabOrder = 0
      end
    end
    object TabSheet1: TTabSheet
      Caption = #1054#1090#1083#1072#1076#1082#1072
      ImageIndex = 1
      object DirectoryEdit1: TDirectoryEdit
        Left = 12
        Top = 11
        Width = 413
        Height = 21
        DialogKind = dkWin32
        NumGlyphs = 1
        TabOrder = 0
        Text = 'D:\_megafon_blank_calc'
      end
      object bLoad: TButton
        Left = 12
        Top = 38
        Width = 75
        Height = 25
        Caption = 'Load'
        TabOrder = 1
        OnClick = bLoadClick
      end
      object ProgressBar1: TProgressBar
        Left = 12
        Top = 96
        Width = 413
        Height = 17
        TabOrder = 2
      end
    end
  end
  object StatusBar1: TStatusBar
    Left = 0
    Top = 440
    Width = 830
    Height = 19
    Panels = <
      item
        Width = 140
      end
      item
        Width = 100
      end
      item
        Width = 3000
      end>
  end
  object Button1: TButton
    Left = 605
    Top = 39
    Width = 75
    Height = 25
    Caption = 'test'
    TabOrder = 4
    Visible = False
  end
  object ed_Tasks: TLabeledEdit
    Left = 16
    Top = 59
    Width = 49
    Height = 21
    EditLabel.Width = 43
    EditLabel.Height = 13
    EditLabel.Caption = #1047#1072#1076#1072#1085#1080#1081
    ParentColor = True
    ReadOnly = True
    TabOrder = 5
  end
  object Button2: TButton
    Left = 605
    Top = 8
    Width = 155
    Height = 25
    Caption = 'dmAPI_main.Test;'
    TabOrder = 6
    Visible = False
  end
  object bRunTop: TButton
    Left = 184
    Top = 8
    Width = 75
    Height = 25
    Caption = 'Run Top'
    TabOrder = 7
    OnClick = bRunTopClick
  end
  object LabeledEdit1: TLabeledEdit
    Left = 296
    Top = 59
    Width = 49
    Height = 21
    EditLabel.Width = 43
    EditLabel.Height = 13
    EditLabel.Caption = #1055#1086#1090#1086#1082#1086#1074
    ParentColor = True
    ReadOnly = True
    TabOrder = 8
    Text = '5'
  end
  object bClearAll: TButton
    Left = 265
    Top = 8
    Width = 75
    Height = 25
    Caption = 'Clear All'
    TabOrder = 9
    OnClick = bClearAllClick
  end
  object bRefreshStatus: TButton
    Left = 346
    Top = 8
    Width = 95
    Height = 25
    Caption = 'Refresh Status'
    TabOrder = 10
    OnClick = bRefreshStatusClick
  end
  object cb_Send_to_MG: TCheckBox
    Left = 392
    Top = 61
    Width = 97
    Height = 17
    Caption = 'Send to MG'
    Checked = True
    State = cbChecked
    TabOrder = 11
  end
  object bMegaGIS_local_echo: TButton
    Left = 707
    Top = 39
    Width = 81
    Height = 25
    Caption = 'test mg local'
    TabOrder = 12
    Visible = False
  end
  object cb_Send_to_local_MG: TCheckBox
    Left = 392
    Top = 84
    Width = 97
    Height = 17
    Caption = 'Send to local MG'
    TabOrder = 13
  end
  object Button3: TButton
    Left = 447
    Top = 8
    Width = 98
    Height = 25
    Action = FileOpen_Add_to_Queue
    TabOrder = 14
  end
  object cb_Restart_service: TCheckBox
    Left = 16
    Top = 89
    Width = 243
    Height = 17
    Caption = 'Restarting service on connect error'
    TabOrder = 15
  end
  object Button4: TButton
    Left = 736
    Top = 80
    Width = 75
    Height = 25
    Caption = 'Button4'
    TabOrder = 16
    Visible = False
    OnClick = Button4Click
  end
  object RxProgress1: TRxProgress
    Left = 82
    Top = 59
    Width = 199
    Height = 19
    Min = 0
    Max = 100
    Position = 0
    ProgressColor = clHighlight
    Text = ''
    Ctl3D = True
    ShowPosition = True
    ShowPercent = True
  end
  object FormStorage1: TFormStorage
    Active = False
    Options = [fpPosition]
    UseRegistry = True
    StoredProps.Strings = (
      'cb_Send_to_MG.Checked'
      'cb_Send_to_local_MG.Checked'
      'DirectoryEdit1.Text'
      'PageControl1.ActivePage')
    StoredValues = <>
    Left = 544
    Top = 56
  end
  object ActionList1: TActionList
    OnUpdate = ActionList1Update
    Left = 552
    Top = 127
    object FileOpen_Add_to_Queue: TFileOpen
      Category = 'File'
      Caption = 'Add to queue...'
      Dialog.DefaultExt = '*.xml'
      Dialog.Filter = '*.xml|*.xml'
      Dialog.Options = [ofHideReadOnly, ofAllowMultiSelect, ofFileMustExist, ofEnableSizing]
      Hint = 'Open|Opens an existing file'
      ImageIndex = 7
      ShortCut = 16463
      OnAccept = FileOpen_Add_to_QueueAccept
    end
    object act_Stop: TAction
      Caption = 'Stop'
      OnExecute = act_StopExecute
    end
    object act_Start: TAction
      Caption = 'Start'
      OnExecute = act_StartExecute
    end
  end
  object Timer_restart_service: TTimer
    Enabled = False
    Interval = 60000
    OnTimer = Timer_restart_serviceTimer
    Left = 560
    Top = 232
  end
end
