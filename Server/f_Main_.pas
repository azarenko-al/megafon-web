﻿unit f_Main_;

interface

uses

  u_log_,

  u_logger,
  u_Config,
  dm_Main,
  u_func,
  u_files,
  dm_API_Main,
  dm_Queue,

  IOUtils,  StrUtils,  Messages, Dialogs,
  System.SysUtils,   System.Classes, Vcl.Controls, Vcl.Forms,
  Vcl.StdCtrls, RxPlacemnt,  Vcl.ComCtrls, Vcl.ExtCtrls, Types,
  Vcl.ActnList, Vcl.StdActns,  RxCtrls, System.Actions, Vcl.Mask, RxToolEdit;

type
  IMainFormX = interface
  ['{A8B21AD8-7489-4E09-9C34-FAB86F09C787}']
     procedure RestartService;
  end;


  Tfrm_Main = class(TForm, IMainFormX)
    ButtonStart: TButton;
    btnStop: TButton;
    FormStorage1: TFormStorage;
    PageControl1: TPageControl;
    TabSheet2: TTabSheet;
    Memo_Log: TMemo;
    StatusBar1: TStatusBar;
    Button1: TButton;
    ed_Tasks: TLabeledEdit;
    Button2: TButton;
    bRunTop: TButton;
    LabeledEdit1: TLabeledEdit;
    bClearAll: TButton;
    bRefreshStatus: TButton;
    cb_Send_to_MG: TCheckBox;
    bMegaGIS_local_echo: TButton;
    cb_Send_to_local_MG: TCheckBox;
    ActionList1: TActionList;
    FileOpen_Add_to_Queue: TFileOpen;
    Button3: TButton;
    Timer_restart_service: TTimer;
    cb_Restart_service: TCheckBox;
    Button4: TButton;
    act_Stop: TAction;
    act_Start: TAction;
    RxProgress1: TRxProgress;
    TabSheet1: TTabSheet;
    DirectoryEdit1: TDirectoryEdit;
    bLoad: TButton;
    ProgressBar1: TProgressBar;



    procedure ActionList1Update(Action: TBasicAction; var Handled: Boolean);
    procedure act_StartExecute(Sender: TObject);
    procedure act_StopExecute(Sender: TObject);
//    procedure ApplicationEvents1Message(var Msg: tagMSG; var Handled: Boolean);
//    procedure ApplicationEvents1Exception(Sender: TObject; E: Exception);
procedure FileOpen_Add_to_QueueAccept(Sender: TObject);


    procedure FormCreate(Sender: TObject);
//    procedure ApplicationEvents1Idle(Sender: TObject; var Done: Boolean);
    procedure bClearAllClick(Sender: TObject);
    procedure bLoadClick(Sender: TObject);
//    procedure bMegaGIS_local_echoClick(Sender: TObject);
    procedure bRefreshStatusClick(Sender: TObject);

    procedure bRunTopClick(Sender: TObject);
    procedure Button4Click(Sender: TObject);
    procedure btnStopClick(Sender: TObject);
    procedure Timer_restart_serviceTimer(Sender: TObject);
//    procedure FileOpen_Add_to_QueueAccept(Sender: TObject);

  private
    procedure DoOnExecute(Sender: TObject);

    procedure DoProgress(aPosition, aMax: word);
    procedure LoadFromDir;
    procedure UpdateStatus;

    procedure Log(aMsg: string);

//    procedure OnRestartService(var Msg: TMessage); message WM_RESTART_SERVICE;

  public
    procedure RestartService;

//    procedure UpdateTaskList1;

  end;

var
  frm_Main: Tfrm_Main;



implementation

{$R *.dfm}




//-----------------------------------------------------------------
procedure Tfrm_Main.ActionList1Update(Action: TBasicAction; var Handled:  Boolean);
//-----------------------------------------------------------------
var
  bActive: Boolean;
begin
  bActive:=dmAPI_main.Active;

  ButtonStart.Enabled := not bActive;
  btnStop.Enabled  := bActive;

  bClearAll.Enabled      := not bActive;
  bRunTop.Enabled        := not bActive;
  cb_Send_to_MG.Enabled  := not bActive;
  bRefreshStatus.Enabled := bActive;


//  StatusBar1.SimpleText:= inttostr( CurrentProcessMemory );

  StatusBar1.Panels[0].Text:='Memory: '+ FloatToStrF(GetMemoryUsed, ffNumber, 10, 0);

 // dmMSMQ_main.Timer1.Enabled:=True;

//  dmMSMQ_Main.tim
//  EditPort.Enabled := not FServer.Active;
//
//  StatusBar1.Panels[0].Text:=IfThen(FServer.Active, 'Server.Active', 'Server.Closed');

end;

//-----------------------------------------------------------------
procedure Tfrm_Main.act_StartExecute(Sender: TObject);
//-----------------------------------------------------------------
begin
TLog.SendNote('Tfrm_Main.act_StartExecute');

  dmAPI_main.params.IsSend_to_local_MG:=cb_Send_to_local_MG.Checked;
  dmAPI_main.params.IsSend_to_ESB_MG:=cb_Send_to_MG.Checked;
  if not dmAPI_main.StartServer then
    RestartService;

end;


//-------------------------------------------------------------------
procedure Tfrm_Main.FormCreate(Sender: TObject);
//-------------------------------------------------------------------
begin
 // ShowMessage ('procedure Tfrm_Main.FormCreate(Sender: TObject);');


//  Caption:='ONEPLAN RPLS-DB : Ìîäóëü ðàñ÷åòà ÁØÏÄ : '+ GetAppVersionStr() ;
  Caption:='ONEPLAN RPLS-DB BSHPD : Модуль расчета БШПД : '+ GetAppVersionStr() ;

  PageControl1.ActivePageIndex:=0;

//  FCaption:= APPLICATION_CAPTION1 + GetAppVersionStr() + '  ñîçäàí: '
 //   + FormatDateTime ('yyyy-mm-dd hh:mm', GetFileDate (Application.Exename));





 // FServer := TIdHTTPWebBrokerBridge.Create(Self);

//  StartServer;

//  Memo_Tasks.Align:=alClient;
  Memo_Log.Align:=alClient;

  //ed_GetCurrentProcessId.Text:= IntToStr ( GetCurrentProcessId );

//  StatusBar1.Panels[0].Text:=Format('CurrentProcessId: %d',[GetCurrentProcessId]);


//          SetProcessWorkingSetSize(GetCurrentProcess, $FFFFFFFF, $FFFFFFFF);
//  StatusBar1.Panels[1].Text:= IntToStr ( GetCurrentProcessId );
//  StatusBar1.Panels[1].Text:= IntToStr ( Application.Handle );



  StatusBar1.Panels[2].Text:=g_Config.ConnectionStr;


 // Log ('start.');


//  dmAPI_main.OnTaskAdd:=DoTaskAdd;
//  dmAPI_main.OnTaskRemoved:=DoTaskRemoved;


  Timer_restart_service.Interval:= 1000 * g_Config.OnDisconnect_Delay_Sec;

//  dmAPI_main.OnLog:=Log;

//  dmTask_Calc.OnLog:=Log;

  g_Logger.OnLog:=Log;
  g_Logger.OnProgress:=DoProgress;


  RxProgress1.Position:=0;

  //ed_Tasks.Text:= IntToStr ( dmMSMQ_main.GetTaskCount );
//  Update_Status;

  dmAPI_main.OnExecute:=DoOnExecute;

  if g_Config.AutoRun_Enabled then
//  if ParamCount>0 then
    act_Start.Execute;
//    ShowMessage ('ParamCount>0');

 // DoOnExecute(nil);

 // procedure Tfrm_Main.Log(aMsg: string);

end;


procedure Tfrm_Main.bClearAllClick(Sender: TObject);
begin
  dmQueue.ClearAll;
  UpdateStatus;
end;

//
//procedure Tfrm_Main.bMegaGIS_local_echoClick(Sender: TObject);
//begin
//  Log (Task_Send_to_Local_MG_Echo ('12312312') );
//end;


procedure Tfrm_Main.bRefreshStatusClick(Sender: TObject);
begin
  UpdateStatus
end;



procedure Tfrm_Main.bRunTopClick(Sender: TObject);
begin
  dmAPI_main.params.IsSend_to_local_MG:=cb_Send_to_local_MG.Checked;
  dmAPI_main.params.IsSend_to_ESB_MG:=cb_Send_to_MG.Checked;
  dmAPI_main.Run_top;
end;

procedure Tfrm_Main.Button4Click(Sender: TObject);
begin
  (Application.MainForm as IMainFormX).RestartService;

//  PostMessage(Application.MainForm.Handle, WM_RESTART_SERVICE, 0,0);
end;

procedure Tfrm_Main.btnStopClick(Sender: TObject);
begin

end;


//-------------------------------------------------------------------
procedure Tfrm_Main.Log(aMsg: string);
//-------------------------------------------------------------------
begin

  if Memo_Log.Lines.Count>100000 then
    Memo_Log.Lines.Clear;

  Memo_Log.Lines.Add( DateTimeToStr(Now()) +' '+  aMsg);


 //   LogToDir (aMsg);


end;


procedure Tfrm_Main.DoProgress(aPosition, aMax: word);
begin
//  ProgressBar1.Max:=aMax;
//  ProgressBar1.Position:=aPosition;

  RxProgress1.Max:=aMax;
  RxProgress1.Position:=aPosition;

end;



procedure Tfrm_Main.DoOnExecute(Sender: TObject);
begin
  UpdateStatus;

  //dmMSMQ_main.Timer1.Enabled:=True;
end;


procedure Tfrm_Main.UpdateStatus;
begin
  ed_Tasks.Text:= IntToStr ( dmQueue.GetQueueCount );

  //dmMSMQ_main.Timer1.Enabled:=True;
end;




//-------------------------------------------------------------------
procedure Tfrm_Main.FileOpen_Add_to_QueueAccept(Sender: TObject);
//-------------------------------------------------------------------
var
  I: Integer;
  k: Integer;
  s: string;
  sFile: string;

begin
  for I := 0 to TFileOpen (Sender).Dialog.Files.Count-1 do
  begin
    sFile:=TFileOpen (Sender).Dialog.Files[i];

//    s:=TFile.ReadAllText(sFile);
    s:=TFile.ReadAllText(sFile, TEncoding.UTF8);

    if LeftStr(s,1)<>'<?' then
      s:='<?x' + s;


    k:=dmMain.StoredProc_Exec (dmMain.ADOStoredProc1,  'WebService.sp_Queue_INS',
     [
       'BODY', s,
       'request_source', 'local'
     ]);

    Assert (k>0, 'WebService.sp_Queue_INS');
  end;

//    LoadFile (TFileOpen (Sender).Dialog.Files[i]);

 // t_Queue.Requery([]);

//  PageControl_Tasks.ActivePageIndex:=1;

end;

//procedure Tfrm_Main.OnRestartService(var Msg: TMessage);
//begin
//  RestartService;
//end;

//-----------------------------------------------------------------
procedure Tfrm_Main.Timer_restart_serviceTimer(Sender: TObject);
//-----------------------------------------------------------------
begin
TLog.SendNote('start Tfrm_Main.Timer_restart_serviceTimer');
  g_Logger.Add('Timer RestartService...');

  Timer_restart_service.Enabled:=False;
  dmAPI_main.StartServer;

  cb_Restart_service.Visible:=not dmAPI_main.Active;

  if not dmAPI_main.Active then
    Timer_restart_service.Enabled:=true;


end;


procedure Tfrm_Main.act_StopExecute(Sender: TObject);
begin
TLog.SendNote('Tfrm_Main.act_StopExecute');

  Timer_restart_service.Enabled:=False;
  dmAPI_main.StopServer;
end;


procedure Tfrm_Main.bLoadClick(Sender: TObject);
begin
  LoadFromDir
end;

//-----------------------------------------------------------------
procedure Tfrm_Main.LoadFromDir;
//-----------------------------------------------------------------
var
  arr: TStringDynArray;
  I: Integer;
  k: integer;
  sDir: string;
  oStream: TStringStream;
  s: string;
  sFile: string;
begin
  sDir:= IncludeTrailingBackslash(DirectoryEdit1.Text);

//  arr:=TDirectory.GetFiles(sDir, '*', TSearchOption.soAllDirectories);
  arr:=TDirectory.GetFiles (IncludeTrailingBackslash(sDir) , '*.xml', TSearchOption.soAllDirectories) ;

//  arr:=TDirectory.GetFiles(sDir, '*', TSearchOption.soAllDirectories);

  oStream:=TStringStream.Create;

  ProgressBar1.Max:= Length(arr);

//  TFileOpen (Sender).Dialog.Files.to

//  for I := 0 to TFileOpen (Sender).Dialog.Files.Count-1 do
  for I := 0 to High(arr) do
  begin
    sFile:=arr[i];
    ProgressBar1.Position:= i;

    //  s:='{''r' + TFile.ReadAllText(aFileName, TEncoding.UTF8);


//    s:=TFile.ReadAllText(sFile);
    oStream.LoadFromFile(sFile);
    s:=oStream.DataString;

//    s:=TFile.ReadAllText(sFile, TEncoding.UTF8);

//    if LeftStr(s,1)<>'<?' then
//      s:='<?x' + s;


    k:=dmMain.StoredProc_Exec (dmMain.ADOStoredProc1,  'WebService.sp_Queue_INS',
     [
       'BODY', s,
       'request_source', 'local'
     ]);

    Assert (k>0, 'WebService.sp_Queue_INS');
  end;

  FreeAndNil (oStream);
  ProgressBar1.Position:=0;

end;



procedure Tfrm_Main.RestartService;
begin
  TLog.SendNote('Tfrm_Main.RestartService');
  g_Logger.Add('RestartService...');

  dmAPI_main.StopServer;

  cb_Restart_service.Checked:=True;
  Timer_restart_service.Enabled:=true;

end;

//
//procedure Tfrm_Main.ApplicationEvents1Message(var Msg: tagMSG; var Handled:
//    Boolean);
//begin
//
////  case Msg.message of
////    WM_RESTART_SERVICE:
////      RestartService;
////  end;
////
//end;
//



end.

