object frm_Main: Tfrm_Main
  Left = 271
  Top = 114
  Caption = 'frm_Main'
  ClientHeight = 548
  ClientWidth = 749
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 24
    Top = 48
    Width = 20
    Height = 13
    Caption = 'Port'
  end
  object ButtonStart: TButton
    Left = 24
    Top = 8
    Width = 75
    Height = 25
    Caption = 'Start'
    TabOrder = 0
    OnClick = ButtonStartClick
  end
  object ButtonStop: TButton
    Left = 105
    Top = 8
    Width = 75
    Height = 25
    Caption = 'Stop'
    TabOrder = 1
    OnClick = ButtonStopClick
  end
  object EditPort: TEdit
    Left = 24
    Top = 67
    Width = 121
    Height = 21
    TabOrder = 2
    Text = '8090'
  end
  object ButtonOpenBrowser: TButton
    Left = 24
    Top = 94
    Width = 107
    Height = 25
    Caption = 'Open Browser'
    TabOrder = 3
    OnClick = ButtonOpenBrowserClick
  end
  object PageControl1: TPageControl
    Left = 0
    Top = 352
    Width = 749
    Height = 177
    ActivePage = TabSheet1
    Align = alBottom
    Anchors = [akLeft, akTop, akRight, akBottom]
    MultiLine = True
    TabOrder = 4
    object TabSheet1: TTabSheet
      Caption = #1054#1095#1077#1088#1077#1076#1100' '#1079#1072#1076#1072#1085#1080#1081
      object Memo_Tasks: TMemo
        Left = 0
        Top = 0
        Width = 675
        Height = 149
        Align = alLeft
        Anchors = [akLeft, akTop, akRight, akBottom]
        ScrollBars = ssVertical
        TabOrder = 0
      end
    end
  end
  object PageControl2: TPageControl
    Left = 0
    Top = 136
    Width = 749
    Height = 216
    ActivePage = TabSheet2
    Align = alBottom
    MultiLine = True
    TabOrder = 5
    object TabSheet2: TTabSheet
      Caption = #1046#1091#1088#1085#1072#1083
      object Memo_Log: TMemo
        Left = 0
        Top = 0
        Width = 675
        Height = 188
        Align = alLeft
        Anchors = [akLeft, akTop, akRight, akBottom]
        ScrollBars = ssVertical
        TabOrder = 0
      end
    end
  end
  object StatusBar1: TStatusBar
    Left = 0
    Top = 529
    Width = 749
    Height = 19
    Panels = <
      item
        Width = 150
      end
      item
        Width = 50
      end>
  end
  object Button1: TButton
    Left = 440
    Top = 43
    Width = 75
    Height = 25
    Caption = 'test'
    TabOrder = 7
    Visible = False
    OnClick = Button1Click
  end
  object DirectoryEdit1: TDirectoryEdit
    Left = 200
    Top = 10
    Width = 185
    Height = 21
    NumGlyphs = 1
    TabOrder = 8
    Text = 'e:\log___'
  end
  object ed_Tasks: TLabeledEdit
    Left = 161
    Top = 67
    Width = 33
    Height = 21
    EditLabel.Width = 45
    EditLabel.Height = 13
    EditLabel.Caption = 'ed_Tasks'
    ParentColor = True
    ReadOnly = True
    TabOrder = 9
  end
  object cxProgressBar1: TcxProgressBar
    Left = 208
    Top = 67
    Position = 34.000000000000000000
    Properties.BeginColor = 54056
    Properties.PeakValue = 67.000000000000000000
    Properties.ShowOverload = True
    Properties.ShowTextStyle = cxtsPosition
    TabOrder = 10
    Width = 177
  end
  object Button2: TButton
    Left = 512
    Top = 8
    Width = 155
    Height = 25
    Caption = 'dmAPI_main.Test;'
    TabOrder = 11
    OnClick = Button2Click
  end
  object ApplicationEvents1: TApplicationEvents
    OnIdle = ApplicationEvents1Idle
    Left = 552
    Top = 72
  end
  object FormStorage1: TFormStorage
    Options = [fpPosition]
    UseRegistry = True
    StoredProps.Strings = (
      'DirectoryEdit1.Text')
    StoredValues = <>
    Left = 624
    Top = 72
  end
end
