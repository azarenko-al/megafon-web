unit f_Main_server;

interface

uses
//  f_Test,

  u_calc_thread,


  dm_Main,

  u_Config,
  u_logger,

  u_func,
  dm_API_Main,

  dm_Task_calc,

  WinApi.Windows, Winapi.ShellApi, StrUtils,
  Messages,

  System.SysUtils, IOutils,
  System.Classes, Vcl.Controls, Vcl.Forms,
  Vcl.AppEvnts, Vcl.StdCtrls, IdHTTPWebBrokerBridge, RxPlacemnt,
  RxToolEdit, Vcl.ComCtrls, Vcl.Mask, Vcl.ExtCtrls, cxGraphics, cxControls,
  cxLookAndFeels, cxLookAndFeelPainters, cxContainer, cxEdit, cxProgressBar;

type
  Tfrm_Main = class(TForm)
    ButtonStart: TButton;
    ButtonStop: TButton;
    EditPort: TEdit;
    Label1: TLabel;
    ApplicationEvents1: TApplicationEvents;
    ButtonOpenBrowser: TButton;
    FormStorage1: TFormStorage;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    Memo_Tasks: TMemo;
    PageControl2: TPageControl;
    TabSheet2: TTabSheet;
    Memo_Log: TMemo;
    StatusBar1: TStatusBar;
    Button1: TButton;
    DirectoryEdit1: TDirectoryEdit;
    ed_Tasks: TLabeledEdit;
    cxProgressBar1: TcxProgressBar;
    Button2: TButton;
//    procedure ApplicationEvents1ActionExecute(Action: TBasicAction; var Handled:
 //       Boolean);
    procedure FormCreate(Sender: TObject);
    procedure ApplicationEvents1Idle(Sender: TObject; var Done: Boolean);
    procedure btn_DB_closeClick(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
//    procedure Button1Click(Sender: TObject);
    procedure ButtonStartClick(Sender: TObject);
    procedure ButtonStopClick(Sender: TObject);
    procedure ButtonOpenBrowserClick(Sender: TObject);
    procedure Button_Test_localClick(Sender: TObject);
  private
    FServer: TIdHTTPWebBrokerBridge;

//    procedure LogToDir(aMsg: string);
    procedure StartServer;

    procedure DoProgress(aPosition, aMax: word);


//    procedure DoListChanged(Sender: TObject);
    procedure DoTaskAdd(aMsg: string);
    procedure DoTaskRemoved(aMsg: string);

  public
    procedure Log(aMsg: string);
//    procedure UpdateTaskList1;

  end;

var
  frm_Main: Tfrm_Main;

implementation

{$R *.dfm}

var
  csCriticalSection: TRTLCriticalSection;



//-------------------------------------------------------------------
procedure Tfrm_Main.FormCreate(Sender: TObject);
//-------------------------------------------------------------------
begin
  Caption:='ONEPLAN RPLS-DB BSHPD : ������ ������� ���� : '+ GetAppVersionStr() ;


//  FCaption:= APPLICATION_CAPTION1 + GetAppVersionStr() + '  ������: '
 //   + FormatDateTime ('yyyy-mm-dd hh:mm', GetFileDate (Application.Exename));



  FServer := TIdHTTPWebBrokerBridge.Create(Self);

  StartServer;

  Memo_Tasks.Align:=alClient;
  Memo_Log.Align:=alClient;

 // Log ('start.');


  dmAPI_main.OnTaskAdd:=DoTaskAdd;
  dmAPI_main.OnTaskRemoved:=DoTaskRemoved;



//  dmAPI_main.OnLog:=Log;

//  dmTask_Calc.OnLog:=Log;

  g_Logger.OnLog:=Log;
  g_Logger.OnProgress:=DoProgress;


  cxProgressBar1.Position:=0;


 // procedure Tfrm_Main.Log(aMsg: string);

end;


procedure Tfrm_Main.ApplicationEvents1Idle(Sender: TObject; var Done: Boolean);
begin
  ButtonStart.Enabled := not FServer.Active;
  ButtonStop.Enabled := FServer.Active;
  EditPort.Enabled := not FServer.Active;

  StatusBar1.Panels[0].Text:=IfThen(FServer.Active, 'Server.Active', 'Server.Closed');

end;

procedure Tfrm_Main.btn_DB_closeClick(Sender: TObject);
begin
 // dmMain.ADOConnection1.Close;
end;


procedure Tfrm_Main.Button1Click(Sender: TObject);
var
  receiverHandle: THandle;
  DataStruct : CopyDataStruct;
  s: string;
begin
  receiverHandle:=FindWindow('Tfrm_BSHPD_Server_monitor', nil); // (PChar('Tfrm_BSHPD_Server_monitor') ) ;

  s:='Edit111111111111111111.text';

  DataStruct.dwData := 0;
//  DataStruct.cbData := length(Edit1.text)+1;
  DataStruct.cbData := 2*(Length(S)+1);
  DataStruct.lpData := pchar(s);

  SendMessage(receiverHandle, WM_CopyData, handle, integer(@DataStruct));
end;

procedure Tfrm_Main.Button2Click(Sender: TObject);
begin
  dmAPI_main.Run_Thread;
end;

procedure Tfrm_Main.ButtonOpenBrowserClick(Sender: TObject);
var
  LURL: string;
begin
  StartServer;
  LURL := Format('http://localhost:%s', [EditPort.Text]);
  ShellExecute(0,
        nil,
        PChar(LURL), nil, nil, SW_SHOWNOACTIVATE);
end;

procedure Tfrm_Main.ButtonStartClick(Sender: TObject);
begin
  StartServer;
end;


procedure Tfrm_Main.ButtonStopClick(Sender: TObject);
begin
  FServer.Active := False;
  FServer.Bindings.Clear;
end;

procedure Tfrm_Main.Button_Test_localClick(Sender: TObject);
begin
//  Application.CreateForm(Tfrm_Test, frm_Test);
end;


procedure Tfrm_Main.DoTaskAdd(aMsg: string);
begin
 // Memo_Tasks.Lines.Delete(-1);

  Memo_Tasks.Lines.Add(aMsg);
  ed_Tasks.Text:=IntToStr ( dmAPI_main.GetTaskCount + 1);
end;


procedure Tfrm_Main.DoTaskRemoved(aMsg: string);
begin
  Memo_Tasks.Lines.Delete(Memo_Tasks.Lines.IndexOf(aMsg));
  ed_Tasks.Text:= IntToStr ( dmAPI_main.GetTaskCount );

  cxProgressBar1.Position:=0;
end;

//-------------------------------------------------------------------
procedure Tfrm_Main.Log(aMsg: string);
//-------------------------------------------------------------------
begin
  EnterCriticalSection(csCriticalSection);
  try
    if Memo_Log.Lines.Count>1000 then
      Memo_Log.Lines.Clear;

    Memo_Log.Lines.Add( DateTimeToStr(Now()) +' '+  aMsg);


 //   LogToDir (aMsg);

  finally
    LeaveCriticalSection(csCriticalSection);
  end;
end;


procedure Tfrm_Main.DoProgress(aPosition, aMax: word);
begin
//  ProgressBar1.Max:=aMax;
//  ProgressBar1.Position:=aPosition;

  cxProgressBar1.Properties.Max:=aMax;
  cxProgressBar1.Position:=aPosition;

end;

//-------------------------------------------------------------------
procedure Tfrm_Main.StartServer;
//-------------------------------------------------------------------
begin
  if not FServer.Active then
  begin
    FServer.Bindings.Clear;
    FServer.DefaultPort := StrToInt(EditPort.Text);
    FServer.Active := True;


    g_Logger.RootDir:=DirectoryEdit1.Text;
  end;
end;




initialization

  InitializeCriticalSection(csCriticalSection);


finalization
  DeleteCriticalSection(csCriticalSection);

end.

  (*
     {


   EnterCriticalSection(csCriticalSection);
   try
     http_Log_Request (ARequestInfo, FOnLog) ;

  finally
    LeaveCriticalSection(csCriticalSection);
  end;




procedure PostMsg;
var
  h: hwnd;
begin
    h:=FindWindow (PChar('Tfrm_Main_MDI'), nil);
    if h>0 then
      SendMessage (h, WM_EXTERNAL_PROJECT_IMPORT_DONE,0,0);

end;



//-------------------------------------------------------------------
procedure Tfrm_Main.LogToDir(aMsg: string);
//-------------------------------------------------------------------
var
  sDir: string;

//  f: TextFile;
  sFile: string;

begin
  DateTimeToString(sDir, 'yyyy mm dd', Date());

  sFile:=sDir + 'log.txt';

  sDir:= IncludeTrailingBackslash ( g_Config.Log_Dir + sDir);

//  ForceDirectories(sDir);

  TFile.AppendAllText(sFile, aMsg);

//
//  sFile:=sDir + 'log.txt';
//  if FileExists(sFile) then
//    AssignFile(f, sFile)
//  else
//    Rewrite(f, sFile);
//
//  Append(f);
//  Writeln(f, DateTimeToStr(Now()) + ' '+  aMsg);
//
////  Flush(f);  { Ensures that the text was actually written to file. }
//  CloseFile(f);

end;



procedure Tfrm_Main.UpdateTaskList1;
begin
//  dmApi_main.TaskList.SaveToStrings(Memo_Tasks.Lines);

end;


procedure Tfrm_Main.DoListChanged(Sender: TObject);
begin
{
  EnterCriticalSection(csCriticalSection);
  try
    dmApi_main.TaskList.SaveToStrings(Memo_Tasks.Lines);
  finally
    LeaveCriticalSection(csCriticalSection);
  end;
  }
end;

receiverHandle


procedure Tfrm_Main.Button1Click(Sender: TObject);
var  receiverHandle : THandle;  res : integer;begin  receiverHandle:=FindWindow(PChar('Tfrm_BSHPD_Server_monitor') )) ;
  DataStruct.dwData := 0;
  DataStruct.cbData := length('a')+1;
  DataStruct.lpData := pchar('a');

  SendMessage(receiverHandle, WM_CopyData, handle, integer(@rDataStruct));

//end;

//  res := SendMessage(receiverHandle, WM_COPYDATA, Integer(Handle),//          Integer(@copyDataStruct)) ;end;


var DataStruct : CopyDataStruct;
begin
  DataStruct.dwData := 0;
  DataStruct.cbData := length(edMsg.text)+1;
  DataStruct.lpData := pchar(EdMsg.text);

  SendMessage(FindWindow('TForm1', 'Recipient'),
                          WM_CopyData, Form1.handle, integer(@DataStruct));
