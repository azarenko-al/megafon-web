{ Invokable implementation File for TTest which implements ITest }

unit ApiCalc_Impl;

interface

uses
  u_Logger,

//  f_main_server,
  u_Task_classes,

  u_task_import,

  dm_API_main,

  Winapi.Windows, IOUtils, System.SysUtils, Forms,   Soap.InvokeRegistry,  CodeSiteLogging,

   ApiCalc_Intf;

type

  { TTest }
  TRPLS_CalcService = class(TInvokableClass, IRPLS_CalcService)
  public
    constructor Create;  override;
    destructor Destroy; override;

    function Calc(const Value: TRPLS_CalcRequest): string; stdcall;
    function Echo(const Value: string): string; stdcall;

    procedure Log(aMsg: string);


  end;

implementation

//var
//  csCriticalSection1: TRTLCriticalSection;


constructor TRPLS_CalcService.Create;
begin
  inherited;
//  CodeSite.Send( 'constructor TCalcService.Create;' );

//  Log ('constructor TRPLS_CalcService.Create;' );

end;

destructor TRPLS_CalcService.Destroy;
begin
  inherited;
//  CodeSite.Send( 'destructor TCalcService.Destroy;' );

 // Log ('destructor TRPLS_CalcService.Destroy;' );

end;


//-------------------------------------------------------------------
function TRPLS_CalcService.Calc(const Value: TRPLS_CalcRequest): string;
//-------------------------------------------------------------------
var
  oTask: TTask;
begin

//  EnterCriticalSection(csCriticalSection1);
  try
    Log ('function TCalcService.Calc(const Value: TCalcRequest): string; GUID:' + Value.GUID);

    oTask:=TTask.Create;

    TTask_LoadFromSOAP(oTask, Value);

    dmAPI_main.AddTask(oTask);


 except on E: Exception do
  //  LeaveCriticalSection(csCriticalSection1);
  end;


end;

//-------------------------------------------------------------------
function TRPLS_CalcService.Echo(const Value: string): string;
//-------------------------------------------------------------------
begin
  Log ('function TCalcService.Echo(const Value: string): string; - '+ Value);

  Result := Value;
end;


procedure TRPLS_CalcService.Log(aMsg: string);
begin
  g_Logger.Add (aMsg);
  CodeSite.Send(aMsg);

  // TODO -cMM: TCalcService.Log default body inserted
end;



initialization
{ Invokable classes must be registered }
   InvRegistry.RegisterInvokableClass(TRPLS_CalcService);
end.

//
//initialization
//  InitializeCriticalSection(csCriticalSection1);
//
//finalization
//  DeleteCriticalSection(csCriticalSection1);
end.
