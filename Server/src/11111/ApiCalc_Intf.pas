{ Invokable interface ITest }

unit ApiCalc_Intf;

interface

uses Soap.InvokeRegistry;

type
  TRPLS_CalcPoint = class(TRemotable)
  private
    FObjectLatitude: Double;

    FObjectAntennaHeight: Double;
    FObjectID: String;
    FObjectTypeSysname: String;
    FObjectLongitude: Double;

  published

    property ObjectTypeSysname: String read FObjectTypeSysname write FObjectTypeSysname;
    property ObjectID: String read FObjectID write FObjectID;

    property ObjectLatitude: Double read FObjectLatitude write FObjectLatitude;
    property ObjectLongitude: Double read FObjectLongitude write FObjectLongitude;

    property ObjectAntennaHeight: Double read FObjectAntennaHeight write    FObjectAntennaHeight;

  end;


  TRPLS_CalcPointArray = array of TRPLS_CalcPoint;


  TRPLS_CalcRequest = class(TRemotable)
  private
    FBitrate_Mbps: Double;
    FClient_Antenna_Height: Double;
    FClient_Antenna_Lat: Double;
    FClient_Antenna_Lon: Double;
    FClient_Lat: Double;
    FClient_Lon: Double;
    FEquipment_Id: Integer;
    FGUID: String;
    FIs_Can_Change_Antenna_Height: Boolean;
    FIs_Can_Change_Antenna_location: Boolean;
    FPoints: TRPLS_CalcPointArray;
    FTechnology: String;

  public
   // Points1: TList<TRPLS_CalcPoint>;

  published
    property GUID: String read FGUID write FGUID;

    property Client_Lat: Double read FClient_Lat write FClient_Lat;
    property Client_Lon: Double read FClient_Lon write FClient_Lon;

    property Client_Antenna_Lat: Double read FClient_Antenna_Lat write FClient_Antenna_Lat;
    property Client_Antenna_Lon: Double read FClient_Antenna_Lon write FClient_Antenna_Lon;
    property Client_Antenna_Height: Double read FClient_Antenna_Height write  FClient_Antenna_Height;

    property Bitrate_Mbps: Double read FBitrate_Mbps write FBitrate_Mbps;

    property Is_Can_Change_Antenna_Height: Boolean read FIs_Can_Change_Antenna_Height write FIs_Can_Change_Antenna_Height;
    property Is_Can_Change_Antenna_location: Boolean read FIs_Can_Change_Antenna_location write FIs_Can_Change_Antenna_location;


    property Points: TRPLS_CalcPointArray read FPoints write FPoints;

    property Technology: String read FTechnology write FTechnology;


  end;


  { Invokable interfaces must derive from IInvokable }
  IRPLS_CalcService = interface(IInvokable)
  ['{0D3A5E24-CC2C-449C-8856-DF61A8BF4BAF}']

    function Echo(const Value: string): string; stdcall;
    function Calc(const Value: TRPLS_CalcRequest): string; stdcall;


  end;

implementation

initialization
  { Invokable interfaces must be registered }
  InvRegistry.RegisterInterface(TypeInfo(IRPLS_CalcService));

end.
