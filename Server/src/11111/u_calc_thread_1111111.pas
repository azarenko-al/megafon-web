unit u_calc_thread_1111111;

interface
uses

  dm_Main,
  dm_Task_calc,

  Forms,
  System.SysUtils, System.Types, System.UITypes, System.Classes,
  System.Variants, u_Task_classes;


type
  TCalcThread1111111 = class(TThread)
  type
    TLogEvent = procedure (aMsg: string) of object;
    TProgressEvent = procedure (aPosition,aMax : word) of object;

  private
    FOnProgress: TProgressEvent;
    FOnTerminate: TNotifyEvent;


//    FdmMain: TdmMain;
//    FdmTask_calc: TdmTask_calc;

    procedure Progress(aPosition,aMax : word);
//    FOnThreadData: TDownloadThreadDataEvent;

//  protected
//    FURL, FFileName: string;
//    FStartPoint, FEndPoint: Int64;
//    FThreadNo: Integer;
//    FTimeStart: Cardinal;

//    procedure ReceiveDataEvent(const Sender: TObject; AContentLength: Int64; AReadCount: Int64; var Abort: Boolean);
  public
    Task: TTask;
    ProgressHandle: THandle;

    constructor Create;
    destructor Destroy; override;

    procedure SetData;

    procedure Execute; override;

    procedure RunTask(aTask: TTask);

    property OnProgress: TProgressEvent read FOnProgress write FOnProgress;

 //   property OnTerminate: TNotifyEvent read FOnTerminate write FOnTerminate;


//    property OnThreadData: TDownloadThreadDataEvent write FOnThreadData;
  end;



implementation


constructor TCalcThread1111111.Create;
begin
  inherited;

 // Priority:=IDLE_PRIORITY_CLASS;
 // SetPriorityClass(GetCurrentProcess, IDLE_PRIORITY_CLASS);

//  FdmMain:=TdmMain.Create(Application);
//  FdmTask_calc:=TdmTask_calc.Create(Application);

end;


destructor TCalcThread1111111.Destroy;
begin

//  FreeAndNil(FdmMain);
//  FreeAndNil(FdmTask_calc);

  inherited;
end;



//-------------------------------------------------------------------
procedure TCalcThread1111111.RunTask(aTask: TTask);
//-------------------------------------------------------------------
var
  sResponse: string;
begin
  Task:=aTask;

 // Assert (not FInProcess);
//  FInProcess:=true;

 // Sleep(1000);

  {

  dmTask_calc.Calc(aTask);

  if Copy (aTask.GUID_str,1,1)='-' then
  begin
    g_Logger.Add('Send_to_Local_MG');
    sResponse:=Task_Send_to_Local_MG(aTask);

  end
  else begin
    g_Logger.Add('Send_to_IApiIntegrationService_MG');
    sResponse:=Task_Send_to_IApiIntegrationService_MG(aTask);

  end;

  g_Logger.Add (sResponse);


  if Assigned(FOnTaskRemoved) then
    FOnTaskRemoved( aTask.GetDisplayName());

  FreeAndNil(aTask);
    }

  //FInProcess:=False;

end;

procedure TCalcThread1111111.Execute;
var
  I: Integer;
begin
  inherited;
 // FreeOnTerminate:=true;



  for I := 0 to 1 do
  begin
    Sleep(1000);

    Application.ProcessMessages;

//    Synchronize( Progress(i,1000 );
  end;
end;


procedure TCalcThread1111111.Progress(aPosition,aMax : word);
begin
   Synchronize(procedure
     begin
        if Assigned (FOnProgress) then
          FOnProgress (aPosition,aMax);
     end);
end;


procedure TCalcThread1111111.SetData;
begin

end;

end.


{

type
  TDownloadThreadDataEvent = procedure(const Sender: TObject; ThreadNo, ASpeed: Integer; AContentLength: Int64; AReadCount: Int64; var Abort: Boolean) of object;
  TDownloadThread = class(TThread)
  private
    FOnThreadData: TDownloadThreadDataEvent;

  protected
    FURL, FFileName: string;
    FStartPoint, FEndPoint: Int64;
    FThreadNo: Integer;
    FTimeStart: Cardinal;

    procedure ReceiveDataEvent(const Sender: TObject; AContentLength: Int64; AReadCount: Int64; var Abort: Boolean);
  public
    constructor Create(const URL, FileName: string; ThreadNo: Integer; StartPoint, EndPoint: Int64);
    destructor Destroy; override;
    procedure Execute; override;

    property OnThreadData: TDownloadThreadDataEvent write FOnThreadData;
  end;


procedure TDownloadThread.Execute;
var
  LResponse: IHTTPResponse;
  LStream: TFileStream;
  LHttpClient: THTTPClient;
begin
  inherited;
  LHttpClient := THTTPClient.Create;
  try
    LHttpClient.OnReceiveData := ReceiveDataEvent;
    LStream := TFileStream.Create(FFileName, fmOpenWrite or fmShareDenyNone);
    try
      FTimeStart := GetTickCount;
      LStream.Seek(FStartPoint, TSeekOrigin.soBeginning);
      LResponse := LHttpClient.GetRange(FURL, FStartPoint, FEndPoint, LStream);
    finally
      LStream.Free;
    end;
  finally
    LHttpClient.Free;
  end;
end;



procedure Tfrm_Main.Button1Click(Sender: TObject);
var  receiverHandle : THandle;  res : integer;begin  receiverHandle:=FindWindow(PChar('Tfrm_BSHPD_Server_monitor') )) ;
  DataStruct.dwData := 0;
  DataStruct.cbData := length('a')+1;
  DataStruct.lpData := pchar('a');

  SendMessage(receiverHandle, WM_CopyData, handle, integer(@rDataStruct));

//end;


// Set priority
procedure TForm1.Button1Click(Sender: TObject);
var
  NewThread: TTestThread;
begin
  NewThread:=TTestThread.Create(False);
  case RadioGroup2.ItemIndex of
    0: NewThread.Priority:=tpIdle;
    1: NewThread.Priority:=tpLowest;
    2: NewThread.Priority:=tpNormal;
    3: NewThread.Priority:=tpHighest;
    4: NewThread.Priority:=tpTimeCritical;
  end;
  case Form1.RadioGroup1.ItemIndex of
    0: SetPriorityClass(GetCurrentProcess, IDLE_PRIORITY_CLASS);
    1: SetPriorityClass(GetCurrentProcess, NORMAL_PRIORITY_CLASS);
    2: SetPriorityClass(GetCurrentProcess, HIGH_PRIORITY_CLASS);
    3: SetPriorityClass(GetCurrentProcess, REALTIME_PRIORITY_CLASS);
  end;
end;

