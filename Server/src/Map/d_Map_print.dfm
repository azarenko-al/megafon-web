object dlg_Map_print: Tdlg_Map_print
  Left = 0
  Top = 0
  Caption = 'dlg_Map_print'
  ClientHeight = 480
  ClientWidth = 851
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 851
    Height = 89
    Align = alTop
    Caption = 'Panel1'
    TabOrder = 0
    Visible = False
    object Button1: TButton
      Left = 112
      Top = 46
      Width = 75
      Height = 25
      Caption = 'test'
      TabOrder = 0
    end
    object Edit1: TEdit
      Left = 112
      Top = 19
      Width = 401
      Height = 21
      TabOrder = 1
      Text = 'file://W:\WEB\_html\leaflet.html'
    end
    object Button2: TButton
      Left = 519
      Top = 17
      Width = 50
      Height = 25
      Caption = 'go'
      TabOrder = 2
    end
    object Button3: TButton
      Left = 632
      Top = 17
      Width = 75
      Height = 25
      Caption = 'setview'
      TabOrder = 3
    end
    object Button4_DrawLine: TButton
      Left = 720
      Top = 17
      Width = 75
      Height = 25
      Caption = 'DrawLine'
      TabOrder = 4
    end
    object Button5: TButton
      Left = 632
      Top = 48
      Width = 75
      Height = 25
      Caption = 'ScreenShot'
      TabOrder = 5
    end
    object Button6: TButton
      Left = 728
      Top = 48
      Width = 75
      Height = 25
      Caption = 'SetTextArea'
      TabOrder = 6
    end
    object Button7: TButton
      Left = 16
      Top = 17
      Width = 75
      Height = 25
      Caption = 'Button7'
      TabOrder = 7
    end
    object CheckBox_ready: TCheckBox
      Left = 240
      Top = 56
      Width = 97
      Height = 17
      Caption = 'ready'
      TabOrder = 8
    end
    object Button4: TButton
      Left = 519
      Top = 48
      Width = 75
      Height = 25
      Caption = 'Button4'
      TabOrder = 9
    end
  end
  object StatusBar1: TStatusBar
    Left = 0
    Top = 461
    Width = 851
    Height = 19
    Panels = <>
    SimplePanel = True
  end
  object pn_WebBrowser: TPanel
    Left = 8
    Top = 159
    Width = 424
    Height = 313
    BorderWidth = 15
    Caption = 'pn_WebBrowser'
    TabOrder = 2
    object WebBrowser1: TWebBrowser
      Left = 16
      Top = 16
      Width = 392
      Height = 281
      Align = alClient
      TabOrder = 0
      OnBeforeNavigate2 = WebBrowser1BeforeNavigate2
      OnNavigateComplete2 = WebBrowser1NavigateComplete2
      ExplicitLeft = 8
      ExplicitTop = 15
      ControlData = {
        4C000000842800000B1D00000000000000000000000000000000000000000000
        000000004C000000000000000000000001000000E0D057007335CF11AE690800
        2B2E126208000000000000004C0000000114020000000000C000000000000046
        8000000000000000000000000000000000000000000000000000000000000000
        00000000000000000100000000000000000000000000000000000000}
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 89
    Width = 851
    Height = 56
    Align = alTop
    Caption = 'Panel2'
    TabOrder = 3
    object Button8: TButton
      Left = 16
      Top = 15
      Width = 147
      Height = 25
      Caption = 'FreeMemory'
      TabOrder = 0
    end
    object Button9: TButton
      Left = 190
      Top = 15
      Width = 147
      Height = 25
      Caption = 'WebBrowser1.Navigate'
      TabOrder = 1
    end
    object Button10: TButton
      Left = 519
      Top = 6
      Width = 75
      Height = 25
      Caption = 'blank'
      TabOrder = 2
      OnClick = Button10Click
    end
  end
  object FormStorage1: TFormStorage
    Options = [fpPosition]
    UseRegistry = True
    StoredValues = <>
    Left = 512
    Top = 160
  end
  object Timer1: TTimer
    OnTimer = Timer1Timer
    Left = 512
    Top = 224
  end
end
