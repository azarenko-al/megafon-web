﻿unit d_Map_print;

interface

uses
  u_log_,

  u_Task_classes,
  u_web,
  u_func,
//  u_web,

  u_Config,


  windows,
  StrUtils, System.Generics.Defaults,
  IOUtils, DB,  Forms, System.SysUtils,

  Dialogs,  MSHTML_TLB, RxPlacemnt, SHDocVw, Vcl.StdCtrls,
  System.Classes, Vcl.Controls,  System.Variants,  Vcl.ExtCtrls, Vcl.ComCtrls,
  Vcl.OleCtrls;

type
  Tdlg_Map_print = class(TForm)
    Panel1: TPanel;
    Button1: TButton;
    Edit1: TEdit;
    Button2: TButton;
    Button3: TButton;
    Button4_DrawLine: TButton;
    Button5: TButton;
    Button6: TButton;
    Button7: TButton;
    CheckBox_ready: TCheckBox;
    FormStorage1: TFormStorage;
    Button4: TButton;
    StatusBar1: TStatusBar;
    Timer1: TTimer;
    pn_WebBrowser: TPanel;
    WebBrowser1: TWebBrowser;
    Panel2: TPanel;
    Button8: TButton;
    Button9: TButton;
    Button10: TButton;
    procedure Button10Click(Sender: TObject);
 //   procedure Button8Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure Timer1Timer(Sender: TObject);
    procedure WebBrowser1BeforeNavigate2(ASender: TObject; const pDisp: IDispatch;
        const URL, Flags, TargetFrameName, PostData, Headers: OLEVariant; var
        Cancel: WordBool);
//    procedure WebBrowser1DocumentComplete(ASender: TObject; const pDisp: IDispatch;
//        const URL: OLEVariant);
    procedure WebBrowser1NavigateComplete2(ASender: TObject; const pDisp:
        IDispatch; const URL: OLEVariant);

  private
    FRunCount: integer;

  //
    FIDoc: IHTMLDocument2;
    FScript: Variant;

//    procedure Open;
//    procedure DrawLineAndSite(aLat, aLon, aLat_site, aLon_site: double; aLabel,  aColorStr: string);
//    procedure DrawLine(aLat1, aLon1, aLat2, aLon2: double; aLabel, aColorStr:  string);
    procedure SaveToPNG(aFileName: string);
    procedure WebBrowser_ReInit;

//    procedure Load_GeoJSON(aGeoJSON: string);

  public
    procedure TaskPlace_MakeMapImagePNG(aTaskPlace: TTaskPlace; aFileName: string);
//    procedure Clear;

    function Task_MakeMapImagePNG(aTask: TTask ; aFileName: string): Boolean;
//    procedure Load_GeoJSON(aGeoJSON: string);

  end;

implementation

{$R *.dfm}

const
  DEF_RunCount_MAX = 10;


procedure Tdlg_Map_print.Button10Click(Sender: TObject);
begin
  WebBrowser_ReInit

//  Open;
end;


//-------------------------------------------------------------------
procedure Tdlg_Map_print.FormCreate(Sender: TObject);
//-------------------------------------------------------------------
var
  h: HResult;
  I: Integer;
//  s: string;
  vFlags: OLEVariant;
begin
 // ShowMessage('procedure Tdlg_Map_print.FormCreate(Sender: TObject);');

  inherited;

  pn_WebBrowser.BorderWidth:=0;


  Caption:='Печать карты';


//  s:=WebBrowser1.Owner.Name;


  pn_WebBrowser.top :=0;
  pn_WebBrowser.Left:=0;

  pn_WebBrowser.Height:= g_Config.IMAGE_Map_HEIGHT;
  pn_WebBrowser.Width := g_Config.IMAGE_MAP_WIDTH;

  WebBrowser1.Align:=alClient;

  Assert(FileExists ( g_Config.Map_print_HTML_template), g_Config.Map_print_HTML_template);

  vFlags:=4; //NavNoReadFromCache
  WebBrowser1.Navigate2('file://' + g_Config.Map_print_HTML_template, vFlags);


//  while WebBrowser1.ReadyState <> READYSTATE_COMPLETE do
//    Application.ProcessMessages;

  WebBrowser1.Tag:=1;
  while WebBrowser1.Tag <> 0  do
  begin
    Application.ProcessMessages;
    Sleep(200);
  end;


//   WebBrowser1.Document.QueryInterface(IHTMLDocument2, FIDoc);
   h:=WebBrowser1.DefaultInterface.Document.QueryInterface(IHTMLDocument2, FIDoc);
   FScript := FIDoc.Script;


//   Sleep(1000);
//   for I := 0 to 1000 do
//     Application.ProcessMessages;

//   FScript.Clear;

//As a workaround, you can manually call Release, or you can patch the VCL (see here), or you can avoid the problematic properties (for example, by using browser.DefaultInterface.Document instead of browser.Document).

end;


procedure Tdlg_Map_print.WebBrowser1NavigateComplete2(ASender: TObject; const
    pDisp: IDispatch; const URL: OLEVariant);
begin
 WebBrowser1.Tag:=0;
//  ShowMessage('WebBrowser1NavigateComplete2');
end;

//
//procedure Tdlg_Map_print.WebBrowser1DocumentComplete(ASender: TObject; const
//    pDisp: IDispatch; const URL: OLEVariant);
//begin
////  WebBrowser1.Tag:=0;
////  ShowMessage('WebBrowser1DocumentComplete');
//
////  WebBrowser1.Tag:=0;
//end;



//-----------------------------------------------------------------
procedure Tdlg_Map_print.WebBrowser_ReInit;
//-----------------------------------------------------------------
var
  h: HResult;

begin
  FreeAndNil (WebBrowser1);

  WebBrowser1:=TWebBrowser.Create(Self);
  pn_WebBrowser.InsertControl (WebBrowser1);

  WebBrowser1.Align:=alClient;
  WebBrowser1.OnNavigateComplete2:= WebBrowser1NavigateComplete2;


  WebBrowser1.Navigate2('file://' + g_Config.Map_print_HTML_template);

//  while WebBrowser1.ReadyState <> READYSTATE_COMPLETE do
//    Application.ProcessMessages;

  WebBrowser1.Tag:=1;
  while WebBrowser1.Tag <> 0  do
  begin
    Application.ProcessMessages;
    Sleep(200);
  end;


//   WebBrowser1.Document.QueryInterface(IHTMLDocument2, FIDoc);
  h:=WebBrowser1.DefaultInterface.Document.QueryInterface(IHTMLDocument2, FIDoc);
  FScript := FIDoc.Script;

//  FScript.Clear;

//  Sleep(200);
//  Application.ProcessMessages;

end;



procedure Tdlg_Map_print.SaveToPNG(aFileName: string);
begin
  WebBrowserScreenShot_PNG(WebBrowser1, aFileName);

  Assert(FileExists(aFileName), aFileName);

end;



// ---------------------------------------------------------------
procedure Tdlg_Map_print.TaskPlace_MakeMapImagePNG(aTaskPlace: TTaskPlace;
    aFileName: string);
// ---------------------------------------------------------------
//var
//  oForm: Tdlg_Map_print;
//  sDir: string;
//  i: Integer;
//  b: Boolean;
//  d: Double;

//  dt_start: TDateTime;
begin
  TLog.Send ('Tdlg_Map_print.TaskPlace_MakeMapImagePNG');

//  dt_start:=Now;
//  CodeSite.Send( 'procedure TdmTask_profile.TaskPlace_MakeMapImagePNG...' );

//  oForm.ShowModal;

//  Delay(500);


  Assert(aTaskPlace.Pos_WGS.lat > 0);
  Assert(aTaskPlace.Pos_WGS.lon > 0);

  Assert(aTaskPlace.CalcResult.WGS.client_antenna.lat > 0);
  Assert(aTaskPlace.CalcResult.WGS.client_antenna.lon > 0);


  try
    FScript.Clear;

    FScript.DrawLineAndSite(
    //FTask. WGS.client_lat, FTask.WGS.client_lon,
             aTaskPlace.CalcResult.WGS.client_antenna.lat,
             aTaskPlace.CalcResult.WGS.client_antenna.lon,

             aTaskPlace.Pos_WGS.lat,
             aTaskPlace.Pos_WGS.lon ,

             aTaskPlace.place_name + Format(' (h=%s)', [aTaskPlace.Height.ToString]),


             DEF_ResultType_COLOR[aTaskPlace.CalcResult.ResultType]
             //'red'
             );

    FScript.Fit;


    Delay_msec(g_Config.map_print_delay);

//    Delay(1000);

    ForceDirectories( ExtractFilePath (aFileName) );

    SaveToPNG(aFileName);

    aTaskPlace.CalcResult.FileName_Map:=aFileName;
//    aTaskPlace.CalcResult.FileName_Zone:=aFileName;

    //ConvertFileToBase64(aFileName, ChangeFileExt(aFileName, '.Base64'));


  except
  // LeaveCriticalSection(csCriticalSection);
  end;


end;




//-------------------------------------------------------------------
function Tdlg_Map_print.Task_MakeMapImagePNG(aTask: TTask ; aFileName: string):
    Boolean;
//-------------------------------------------------------------------
var
  oPlace: TTaskPlace;
  s: string;

begin
  s:=aTask.GetGeoJSON();

  TFile.WriteAllText('d:\1.json', s);

  Inc (FRunCount);
  if FRunCount > DEF_RunCount_MAX then
  begin
    FRunCount:=0;
    WebBrowser_ReInit;
  end;


  try

    FScript.Clear;

  {
  FWebBrowser.DrawMarker_Client (aTask.WGS.client_lat, aTask.WGS.client_lon);

  if aTask.WGS.client_antenna_lat<>0 then
  begin
    FWebBrowser.DrawMarker_Client_ant (aTask.WGS.client_antenna_lat, aTask.WGS.client_antenna_lon);

    aTask.WGS.client_lat:=aTask.WGS.client_antenna_lat;
    aTask.WGS.client_lon:=aTask.WGS.client_antenna_lon;

  end;
   }



    for oPlace in aTask.Places do
      if oPlace.CalcResult.WGS.client_antenna.lat <> 0 then
    begin



  Assert(oPlace.Pos_WGS.lat > 0);
  Assert(oPlace.Pos_WGS.lon > 0);

  Assert(oPlace.CalcResult.WGS.client_antenna.lat > 0);
  Assert(oPlace.CalcResult.WGS.client_antenna.lon > 0);


      FScript.DrawLineAndSite
        (oPlace.CalcResult.WGS.client_antenna.lat,
         oPlace.CalcResult.WGS.client_antenna.lon,

         oPlace.Pos_WGS.Lat,
         oPlace.Pos_WGS.Lon,

         oPlace.place_name + Format(' (h=%s)', [oPlace.Height.ToString]),

//         oPlace.place_name + ' h='+ oPlace.Height.ToString,

  //       'red'
         DEF_ResultType_COLOR [oPlace.CalcResult.ResultType]
        );

    end;

    FScript.Fit;


    Delay_msec(g_Config.map_print_delay);

//    Delay(1000);


    ForceDirectories( ExtractFilePath (aFileName) );

    SaveToPNG(aFileName);

    if aTask.Places.Count>0 then
      aTask.Places[0].CalcResult.FileName_Zone :=aFileName;

    Result:=true;
  except
    Result:=false;

  end;
   // Assert (aTask.WGS.client_lat <> 0);

 // SetCenterLatLon(aTask.WGS.client_lat, aTask.WGS.client_lon);

end;

procedure Tdlg_Map_print.Timer1Timer(Sender: TObject);
begin
 // StatusBar1.SimpleText:= FloatToStrF(CurrentProcessMemory, ffNumber, 10, 0);

  StatusBar1.SimpleText:= FloatToStrF(GetMemoryUsed, ffNumber, 10, 0);


end;

procedure Tdlg_Map_print.WebBrowser1BeforeNavigate2(ASender: TObject; const
    pDisp: IDispatch; const URL, Flags, TargetFrameName, PostData, Headers:
    OLEVariant; var Cancel: WordBool);
begin
  if Pos('##',URL)>0 then
    WebBrowser1.Tag:=0;
end;


end.





//
//procedure Tdlg_Map_print.Button8Click(Sender: TObject);
//
//    procedure FreeMemory;
//    begin
//        if Win32Platform = VER_PLATFORM_WIN32_NT then
//        SetProcessWorkingSetSize(GetCurrentProcess, $FFFFFFFF, $FFFFFFFF);
//    end;
//
//begin
//  FreeMemory
//
// // (WebBrowser1.Document as IPersistStreamInit).InitNew;
//end;


//procedure Tdlg_Map_print.Button9Click(Sender: TObject);
//begin
////   WebBrowser1.Navigate('file://' + g_Config.Map_print_HTML_template);
//end;

//
//procedure Tdlg_Map_print.Load_GeoJSON(aGeoJSON: string);
//begin
//
//end;

{

procedure Tframe_Map_view.WebBrowser1BeforeNavigate2(ASender: TObject; const
    pDisp: IDispatch; const URL, Flags, TargetFrameName, PostData, Headers:
    OLEVariant; var Cancel: WordBool);
var
  s: string;
  r: TWebCallbackResponse;
begin

  StatusBar1.SimpleText:=URL;

  if Pos('##',URL)>0 then
  begin
    s:=Copy(URL, Pos('##',URL)+2, 1000);


    r:=ExtractMapCallback(s);

    if Assigned(FOnMapClick_LatLon) then
      FOnMapClick_LatLon(r.Lat, r.Lon);

//        property OnMapClick_LatLon: TOnMapClickEvent read FOnMapClick_LatLon write FOnMapClick_LatLon;

  end;

end;

