object dmMap: TdmMap
  OldCreateOrder = False
  Height = 339
  Width = 354
  object ADOStoredProc_House_Points: TADOStoredProc
    Connection = dmMain.ADOConnection1
    CursorType = ctStatic
    LockType = ltReadOnly
    ProcedureName = 'map.sp_House_points_By_LatLon'
    Parameters = <>
    Left = 79
    Top = 16
  end
  object ds_DataSource_House_Points: TDataSource
    DataSet = ADOStoredProc_House_Points
    Left = 80
    Top = 72
  end
  object ADOStoredProc1: TADOStoredProc
    Connection = dmMain.ADOConnection1
    CursorType = ctStatic
    LockType = ltReadOnly
    ProcedureName = 'map.sp_House_points_By_LatLon'
    Parameters = <>
    Left = 79
    Top = 168
  end
  object ADOStoredProc_House: TADOStoredProc
    Connection = dmMain.ADOConnection1
    CursorType = ctStatic
    LockType = ltReadOnly
    ProcedureName = 'map.sp_House_points_By_LatLon'
    Parameters = <>
    Left = 255
    Top = 16
  end
  object ds_ADOStoredProc_House: TDataSource
    DataSet = ADOStoredProc_House
    Left = 256
    Top = 72
  end
end
