unit dm_Map;

interface

uses

//  u_Logger,
  dm_Main ,
  u_geo,

  SysUtils, Classes,  Forms, Variants, Data.DB, Data.Win.ADODB;


type
  TdmMap = class(TDataModule)
    ADOStoredProc_House_Points: TADOStoredProc;
    ds_DataSource_House_Points: TDataSource;
    ADOStoredProc1: TADOStoredProc;
    ADOStoredProc_House: TADOStoredProc;
    ds_ADOStoredProc_House: TDataSource;
//    procedure DataModuleCreate(Sender: TObject);

  public
 //   FdmMain: TDmMain;
//    procedure SetDataManager(aDmMain: TdmMain);
  type
      TNRI_Site =  record
        name: string;
        name_rus: string;
        Lat: double;
        Lon: double;
        height: integer;
        map_building_height: double;

      end;

  public
    function FindHousePoints_WGS(aBLPoint_WGS: TBLPoint; var aHeight: Double):
        Boolean;

    function House_points_By_LatLon(aADOStoredProc1: TADOStoredProc; aLat, aLon:
        Double): Integer;


    class procedure Init;


  end;

var
  dmMap: TdmMap;

implementation

{%CLASSGROUP 'Vcl.Controls.TControl'}

{$R *.dfm}


class procedure TdmMap.Init;
begin
  if not Assigned(dmMap) then
    Application.CreateForm(TdmMap, dmMap);

end;


//-------------------------------------------------------------------
function TdmMap.FindHousePoints_WGS(aBLPoint_WGS: TBLPoint; var aHeight: Double): Boolean;
//-------------------------------------------------------------------
var
  k: Integer;
begin

  k:=dmMain.StoredProc_Open(ADOStoredProc_House_Points, 'map.sp_House_points_By_LatLon',
    [
      'LAT', aBLPoint_WGS.B,
      'LON', aBLPoint_WGS.L
    ]);


  k:=dmMain.StoredProc_Open(ADOStoredProc_House, 'map.sp_House_By_LatLon',
    [
      'LAT', aBLPoint_WGS.B,
      'LON', aBLPoint_WGS.L
    ]);

  Result:=not ADOStoredProc_House.IsEmpty;
  if Result then
    aHeight:=ADOStoredProc_House.FieldByName('Height').AsFloat;

end;


function TdmMap.House_points_By_LatLon(aADOStoredProc1: TADOStoredProc; aLat, aLon: Double): Integer;
begin
  Result:=dmMain.StoredProc_Open(aADOStoredProc1, 'map.sp_House_points_By_LatLon',
    [
      'LAT', aLat,
      'LON', aLon
    ]);
end;



end.

