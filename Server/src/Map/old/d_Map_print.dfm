object dlg_Map_print: Tdlg_Map_print
  Left = 0
  Top = 0
  Caption = 'dlg_Map_print'
  ClientHeight = 330
  ClientWidth = 836
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 836
    Height = 89
    Align = alTop
    Caption = 'Panel1'
    TabOrder = 0
    object Button1: TButton
      Left = 112
      Top = 46
      Width = 75
      Height = 25
      Caption = 'test'
      TabOrder = 0
    end
    object Edit1: TEdit
      Left = 112
      Top = 19
      Width = 401
      Height = 21
      TabOrder = 1
      Text = 'file://W:\WEB\_html\leaflet.html'
    end
    object Button2: TButton
      Left = 519
      Top = 17
      Width = 50
      Height = 25
      Caption = 'go'
      TabOrder = 2
    end
    object Button3: TButton
      Left = 632
      Top = 17
      Width = 75
      Height = 25
      Caption = 'setview'
      TabOrder = 3
      OnClick = Button3Click
    end
    object Button4_DrawLine: TButton
      Left = 720
      Top = 17
      Width = 75
      Height = 25
      Caption = 'DrawLine'
      TabOrder = 4
      OnClick = Button4_DrawLineClick
    end
    object Button5: TButton
      Left = 632
      Top = 48
      Width = 75
      Height = 25
      Caption = 'ScreenShot'
      TabOrder = 5
    end
    object Button6: TButton
      Left = 728
      Top = 48
      Width = 75
      Height = 25
      Caption = 'SetTextArea'
      TabOrder = 6
    end
    object Button7: TButton
      Left = 16
      Top = 17
      Width = 75
      Height = 25
      Caption = 'Button7'
      TabOrder = 7
    end
    object CheckBox_ready: TCheckBox
      Left = 240
      Top = 56
      Width = 97
      Height = 17
      Caption = 'ready'
      TabOrder = 8
    end
  end
  object WebBrowser1: TWebBrowser
    Left = 0
    Top = 89
    Width = 300
    Height = 222
    Align = alLeft
    TabOrder = 1
    ExplicitLeft = 37
    ExplicitTop = 208
    ExplicitHeight = 150
    ControlData = {
      4C000000021F0000F21600000000000000000000000000000000000000000000
      000000004C000000000000000000000001000000E0D057007335CF11AE690800
      2B2E126208000000000000004C0000000114020000000000C000000000000046
      8000000000000000000000000000000000000000000000000000000000000000
      00000000000000000100000000000000000000000000000000000000}
  end
  object StatusBar1: TStatusBar
    Left = 0
    Top = 311
    Width = 836
    Height = 19
    Panels = <>
    SimplePanel = True
  end
  object FormStorage1: TFormStorage
    UseRegistry = True
    StoredValues = <>
    Left = 624
    Top = 160
  end
end
