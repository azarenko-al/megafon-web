﻿unit d_Map_print;

interface

uses

  u_web,

//  u_web,

  u_Config,


  MSHTML_TLB, windows, ActiveX,
  System.DateUtils,  System.SysUtils, System.Variants, System.Classes,
  Vcl.Controls, Vcl.Forms, Vcl.OleCtrls, SHDocVw, Vcl.ExtCtrls, Vcl.StdCtrls,
  Vcl.ComCtrls, RxPlacemnt;

type
  Tdlg_Map_print = class(TForm)
    Panel1: TPanel;
    Button1: TButton;
    Edit1: TEdit;
    Button2: TButton;
    Button3: TButton;
    Button4_DrawLine: TButton;
    Button5: TButton;
    Button6: TButton;
    Button7: TButton;
    CheckBox_ready: TCheckBox;
    WebBrowser1: TWebBrowser;
    StatusBar1: TStatusBar;
    FormStorage1: TFormStorage;
    procedure FormDestroy(Sender: TObject);
    procedure Button3Click(Sender: TObject);
    procedure Button4_DrawLineClick(Sender: TObject);
//    procedure Button6Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure WebBrowser1DocumentComplete(ASender: TObject; const pDisp: IDispatch;
        const URL: OLEVariant);

  private

  //
    FIDoc: IHTMLDocument2;
  ////  v: OleVariant;
    FScript: Variant;
  //begin
//

//  end;
//begin
//  try
//     aWebbrowser.Document.QueryInterface(IHTMLDocument2, vIDoc);
//  except
//
//  end;
//



    procedure ClearMarkers;
    procedure Exec(aParam: string);
    procedure SetView(aLat, aLon: double);
  public

    procedure DrawLine(aLat1, aLon1, aLat2, aLon2: double; aLabel: string);
    procedure Insert_WebBrowser;

    procedure SaveToPNG(aFileName: string);

    class function Init: Tdlg_Map_print;
    class procedure ExecDlg;

  end;


implementation

{$R *.dfm}

var
  dlg_Map_print: Tdlg_Map_print = nil;

var
  csCriticalSection: TRTLCriticalSection;


//-------------------------------------------------------------------
class procedure Tdlg_Map_print.ExecDlg;
//-------------------------------------------------------------------
var
  oMap_print: Tdlg_Map_print;
  dt_start: TDateTime;


begin
  with Tdlg_Map_print.Create(Application) do
  begin
    dt_start:=Now;

    Insert_WebBrowser;
    DrawLine(60.02, 30, 60.4, 30.1, '34-222');

    StatusBar1.SimpleText:=
                 intToStr( MilliSecondsBetween(dt_start, Now)) + ' msec, '+
                 intToStr( SecondsBetween(dt_start, Now)) + ' sec';

    ShowModal;

    Free;
  end;


end;




class function Tdlg_Map_print.Init: Tdlg_Map_print;
begin
  if not Assigned(dlg_Map_print) then
  begin
    Application.CreateForm(Tdlg_Map_print, dlg_Map_print);

//    dlg_Map_print.Show;
    dlg_Map_print.SendToBack;
  //  oForm.Show;

    dlg_Map_print.Insert_WebBrowser;

  end;


  Result:=dlg_Map_print;

end;


procedure Tdlg_Map_print.FormCreate(Sender: TObject);
begin
  FreeAndNil(Webbrowser1);
end;

procedure Tdlg_Map_print.FormDestroy(Sender: TObject);
begin
  FreeAndNil(WebBrowser1);
end;



procedure Tdlg_Map_print.Exec(aParam: string);
var
  k: Integer;
begin

//  if FIWin <> nil then
//  try
//    FIWin.ExecScript(aParam, 'JavaScript');
//  except
////          vIWin := nil;
//  end;

  ExecuteScript(Webbrowser1, aParam);

//  k:=WebBrowser1.ReadyState;
//  k:=READYSTATE_COMPLETE;

//  Delay (100);

end;


procedure Tdlg_Map_print.Button3Click(Sender: TObject);
begin
  SetView ( 61, 40 );
end;

procedure Tdlg_Map_print.Button4_DrawLineClick(Sender: TObject);
begin
  DrawLine(60.02, 30, 60.4, 30.1, '34-222');

end;


//-------------------------------------------------------------------
procedure Tdlg_Map_print.Insert_WebBrowser;
//-------------------------------------------------------------------
var
  dt : TDateTime;
  k: Integer;
begin

   //tworzenien nowego okna www
//   WebBrowser1:= TWebBrowser.Create(Self);
   WebBrowser1:= TWebBrowser.Create(Application);

   with WebBrowser1 do
   begin
    ParentWindow:=Self.handle; //w tym momecie podczas odpalenia programu wywala sie ten bląd

  //  top:=0;
  //  left:=0;
    Height:= g_Config.IMAGE_Map_HEIGHT;
    Width := g_Config.IMAGE_MAP_WIDTH;

//      oForm.SetWebBrowserSize (g_Config.IMAGE_MAP_WIDTH, g_Config.IMAGE_Map_HEIGHT);

//g_Config.Map_HTML_template
    OnDocumentComplete:=WebBrowser1DocumentComplete;

   end;


   WebBrowser1.Navigate('file://' + g_Config.Map_print_HTML_template);
//   WebBrowser1.Navigate('file://W:\WEB\_html\map_print.html');

//   WebBrowser1.Navigate2();


  dt := Now;

//    if (Sender as TWebBrowser).ReadyState = READYSTATE_COMPLETE then
//    if (Sender as TWebBrowser).ReadyState = READYSTATE_COMPLETE then // Чтобы уже точно !!!
//       ShowMessage('Complete document was loaded');

 // k:= WebBrowser1.ReadyState;

//   READYSTATE_UNINITIALIZED = $00000000;
//  READYSTATE_LOADING = $00000001;
//  READYSTATE_LOADED = $00000002;
//  READYSTATE_INTERACTIVE = $00000003;
//  READYSTATE_COMPLETE = $00000004;



  while WebBrowser1.ReadyState <> READYSTATE_COMPLETE do
  begin
    Application.ProcessMessages;
    Sleep(1);

    if SecondsBetween(dt, Now)> 15 then
      raise Exception.Create('Timeout 15 sec');
//      Exit;
  end;
//
//  vIDoc: IHTMLDocument2;
////  v: OleVariant;
//  VScript, V: Variant;
//begin
//
//end;

  try
    WebBrowser1.Document.QueryInterface(IHTMLDocument2, FIDoc);
    FScript := FIDoc.Script;

  except

  end;
end;


procedure Tdlg_Map_print.SetView(aLat, aLon: double);
begin
//  FScript.SetView (aLat, aLon);

//  Exec (Format('SetView (%1.6f, %1.6f)',[aLat, aLon]));
end;


procedure Tdlg_Map_print.ClearMarkers;
begin
//  Exec (Format('SetView (%1.6f, %1.6f)',[aLat, aLon]));
end;

procedure Tdlg_Map_print.DrawLine(aLat1, aLon1, aLat2, aLon2: double; aLabel:
    string);
//var
//  s: string;
var
  v: Variant;

begin
 // FScript := FIDoc.Script;
  try
    v:=FScript.DrawLine (aLat1, aLon1, aLat2, aLon2, aLabel);
  finally
  //  v:=null;
  end;

//  FScript:=null;
//  s := Format('DrawLine (%1.6f, %1.6f, %1.6f, %1.6f, ''%s'')',
//               [aLat1, aLon1, aLat2, aLon2, aLabel]);
//  Exec (s);

end;


procedure Tdlg_Map_print.SaveToPNG(aFileName: string);
begin
  WebBrowserScreenShot_PNG(WebBrowser1, aFileName);

  Assert(FileExists(aFileName));

end;


procedure Tdlg_Map_print.WebBrowser1DocumentComplete(ASender: TObject; const pDisp:
    IDispatch; const URL: OLEVariant);
begin
  //FDocumentComplete:=true;
 // CodeSite.Send( 'WebBrowser1DocumentComplete' );

end;



initialization
  InitializeCriticalSection(csCriticalSection);

  //g_Disable_log:=False;

finalization
  DeleteCriticalSection(csCriticalSection);

  if Assigned(dlg_Map_print) then
    FreeAndNil(dlg_Map_print);



end.


(*

  oForm:=Create(cxTabSheet1);
  oForm.ManualDock(cxTabSheet1);
  oForm.Show;


WB.Navigate('file://|' + ExtractFilePath(Application.ExeName) +

uses
  MSHTML_TLB, SHDocVw, ShellAPI;

function ExecuteScript(doc: IHTMLDocument2; script: string; language: string): Boolean;
var
  win: IHTMLWindow2;
  Olelanguage: Olevariant;
begin
  if doc <> nil then
  begin
    try
      win := doc.parentWindow;
      if win <> nil then
      begin
        try
          Olelanguage := language;
          win.ExecScript(script, Olelanguage);
        finally
          win := nil;
        end;
      end;
    finally
      doc := nil;
    end;
  end;
end;
Sample usage:

IDoc: IHTMLDocument2;
Webbrowser1.Document.QueryInterface(IHTMLDocument2, iDoc);
ExecuteScript(iDoc, 'document.login.submit()', 'JavaScript');






procedure Tframe_Map.WebBrowser1DocumentComplete(ASender: TObject;  const pDisp: IDispatch; const URL: OLEVariant);
begin
//  ShowMessage ('WebBrowser1DocumentComplete');
end;

procedure Tframe_Map.WebBrowser1DownloadBegin(Sender: TObject);
begin
 CodeSite.Send( 'WebBrowser1DownloadBegin' );


end;

procedure Tframe_Map.WebBrowser1DownloadComplete(Sender: TObject);
begin
  CodeSite.Send( 'WebBrowser1DownloadComplete' );

// ShowMessage ('WebBrowser1DownloadComplete');
end;

procedure Tframe_Map.WebBrowser1NavigateComplete2(ASender: TObject; const pDisp: IDispatch; const URL: OLEVariant);
begin
//ShowMessage ('WebBrowser1NavigateComplete2');
end;



To get _ANYINT variable from your html sample use this code:

function Read_ANYINT: integer;
var
  v: OleVariant;
begin
  v := Browser.Document;
  Result := v.parentWindow._ANYINT;
end;

r := Read_ANYINT;
With this html code:

<html>
<head>
<script>
var myVariable={name: "LA LA LA", x: 3, y: 5, mul: function () {return this.x * this.y}};
var myArray=["Yello", "Green"];
</script>
</head>
<body>
</body>
</html>

procedure Tframe_Map_print.Load_GeoJSON(aGeoJSON: string);
begin
//   Exec ( Format( 'Load_GeoJSON (''%s'')', [aGeoJSON] ));

//   Exec (  'Load_GeoJSON (''[{"type": "LineString",	"coordinates": [[29.39, 60], [29.3, 60.3], [29.56, 60.5]]}]'')' ) ;


////[{"type": "LineString",	"coordinates": [[29.39, 60], [29.3, 60.3], [29.56, 60.5]]}]


//
// function Load_GeoJSON ( p_value )
// {
//	layerGroup.clearLayers();
//
//	L.geoJSON(p_value).addTo(map);
//  //alert ('test 111111111');
// }
//
//
// function SetView ( p_lat, p_lon )
// {
//	map.setView([p_lat, p_lon], 10);
//
//	// layerGroup.clearLayers();



  // TODO -cMM: Tframe_WebBrowser.Load_GeoJSON default body inserted
end;


////-------------------------------------------------------------------
//procedure Tframe_Map_print.SetWebBrowserSize(aWidth, aHeight: Integer);
////-------------------------------------------------------------------
//begin
//  Assert(Assigned(WebBrowser1));
//
//  WebBrowser1.Align:=alNone;
//  WebBrowser1.Width :=aWidth;
//  WebBrowser1.Height:=aHeight;
//
// // WebBrowser1.Refresh;
//
//  Application.ProcessMessages;
////
//end;


procedure Tframe_Map_print.Button1Click(Sender: TObject);
//var
//  IDoc: IHTMLDocument2;
begin
//  Webbrowser1.Document.QueryInterface(IHTMLDocument2, iDoc);
//  ExecuteScript(Webbrowser1, 'Test()');

end;



//
//
////-------------------------------------------------------------------
//procedure WebBrowserScreenShot_PNG(const aWB: TWebBrowser; const aFileName:
//    TFileName);
////-------------------------------------------------------------------
// var
//   viewObject : IViewObject;
//   r : TRect;
//   bitmap : TBitmap;
// begin
//   if aWB.Document <> nil then
//   begin
//     aWB.Document.QueryInterface(IViewObject, viewObject) ;
//     if Assigned(viewObject) then
//     try
//       bitmap := TBitmap.Create;
//       try
//         r := Rect(0, 0, aWB.Width, aWB.Height) ;
//
//         bitmap.Height := aWB.Height;
//         bitmap.Width := aWB.Width;
//
//         viewObject.Draw(DVASPECT_CONTENT, 1, nil, nil, Application.Handle, bitmap.Canvas.Handle, @r, nil, nil, 0) ;
//
//
//
//         with TPNGObject.Create do
//         try
//           Assign(bitmap) ;
//           SaveToFile(aFileName) ;
//         finally
//           Free;
//         end;
//       finally
//         bitmap.Free;
//       end;
//     finally
//       viewObject._Release;
//     end;
//   end;
// end;
//
////-----------------------------------------------------------------------
//function ExecuteScript(aWebbrowser: TWebbrowser; script: string): Boolean;
////-----------------------------------------------------------------------
//var
//  win: IHTMLWindow2;
//  Olelanguage: Olevariant;
//
//  vIDoc: IHTMLDocument2;
//begin
//  aWebbrowser.Document.QueryInterface(IHTMLDocument2, vIDoc);
//
//  Assert (vIDoc <> nil);
//
//  if vIDoc <> nil then
//  begin
//    try
//      win := vIDoc.parentWindow;
//      if win <> nil then
//      begin
//        try
//          Olelanguage := 'JavaScript';
//          win.ExecScript(script, Olelanguage);
//        finally
//          win := nil;
//        end;
//      end;
//    finally
//      vIDoc := nil;
//    end;
//  end;
//end;


procedure Tframe_Map_print.WebBrowser1DownloadBegin(Sender: TObject);
begin
 ///  ShowMessage ('procedure Tframe_Map.WebBrowser1DownloadBegin');

end;

procedure Tframe_Map_print.WebBrowser1DownloadComplete(Sender: TObject);
begin
  //  ShowMessage ('procedure Tframe_Map.WebBrowser1DownloadComplete');

end;




 EnterCriticalSection(csCriticalSection);

  try

    oForm.DrawLine(FTask_Rec.WGS.client_lat, FTask_Rec.WGS.client_lon,
                   FTask_Rec.WGS.place_lat, FTask_Rec.WGS.place_lon ,

                   FTask_Rec.place_name
                   );

//    for I := 0 to 10 do
//      oForm.DrawLine(FTask_Rec.WGS.client_lat+i*0.01, FTask_Rec.WGS.client_lon+i*0.001,
//                   FTask_Rec.WGS.place_lat+i*0.001, FTask_Rec.WGS.place_lon+i*0.001 ,
//
//                   FTask_Rec.place_name
//                   );


    Delay(100);

    oForm.SaveToPNG(aFileName);
  finally
   LeaveCriticalSection(csCriticalSection);
  end;


   dt_start: TDateTime;
begin
  dt_start:=Now;
  CodeSite.Send( 'procedure TdmTask_profile.MakeMapImagePNG...' );

