unit dm_API_main;

interface

uses
  u_db,
  u_log_,
  dm_Queue,
  dm_Main,


//..  u_const,

  u_Task_send_to_Remote_MG_prom,
//  u_Task_send_to_Remote_MG_test,
  u_Task_send_to_Local_MG,

  u_Task_classes,
  dm_Task_calc,

  u_Logger,

  Variants, StrUtils,  Dialogs, Forms,  System.SysUtils, System.Classes, Vcl.ExtCtrls,
  Data.Win.ADODB, Data.DB, Vcl.AppEvnts;


type
  TdmAPI_main = class(TDataModule)
    Timer2: TTimer;
    ADOStoredProc1: TADOStoredProc;
    ADOStoredProc_count: TADOStoredProc;
    ApplicationEvents1: TApplicationEvents;
    procedure ApplicationEvents1Exception(Sender: TObject; E: Exception);
    procedure DataModuleCreate(Sender: TObject);

    procedure Timer2Timer(Sender: TObject);
//..    procedure Timer_Net_errorTimer(Sender: TObject);

  type
    TStrEvent = procedure (aMsg: string) of object;
  private

  private
    FOnExecute: TNotifyEvent;

    function RunTask_XML(aRec: TdmQueue.TQueueItemRec; var aResponse: string):
        Boolean;
   /// procedure DoOnCalcTerminate(Sender: TObject);


//    procedure RunTask_new(aTask: TTask);
  public
//    Terminated : boolean;
    Active : boolean;

    Params :record
      IsSend_to_ESB_MG : boolean;
      IsSend_to_local_MG : boolean;
    end;

    procedure Run_top;

    function StartServer: Boolean;
    procedure StopServer;

    property OnExecute: TNotifyEvent read FOnExecute write FOnExecute;


  end;

var
  dmAPI_main: TdmAPI_main;

implementation
uses
  f_Main_;

{%CLASSGROUP 'Vcl.Controls.TControl'}

{$R *.dfm}


//-----------------------------------------------------------------
procedure TdmAPI_main.ApplicationEvents1Exception(Sender: TObject; E: Exception);
//-----------------------------------------------------------------
begin
  TLog.SendNote('TdmAPI_main.ApplicationEvents1Exception');

  if E is TException_DBNETLIB_ConnectionWrite then
  begin
    dmMain.ADOConnection1.Close;
    g_Logger.Add('TdmAPI_main.ApplicationEvents1Exception - ' +E.Message);

    Active:=False;

    Timer2.Enabled:=False;

    (Application.MainForm as IMainFormX).RestartService;

//    PostMessage(Application.MainForm.Handle, WM_RESTART_SERVICE, 0,0);

  //  g_Logger.Add('Timer_Net_error.Enabled:=True;');
//    Timer_Net_error.Enabled:=True;
  end;
//    Memo1.Lines.Add( 'ApplicationEvents1Exception - '+ TException_DBNETLIB_ConnectionWrite.ClassName + ' - '+  E.Message);
end;


procedure TdmAPI_main.DataModuleCreate(Sender: TObject);
begin
  Assert (Assigned(dmQueue));

 // Application.OnException:=ApplicationEvents1Exception;

//  Timer_Net_error.Interval:= 1000 * g_Config.OnDisconnect_Delay_Sec;

end;


//-------------------------------------------------------------------
function TdmAPI_main.RunTask_XML(aRec: TdmQueue.TQueueItemRec; var aResponse:
    string): Boolean;
//-------------------------------------------------------------------
var
//  b: Boolean;
//  sResponse: string;
  oTask: TTask;

//  oXmlSerializer: TXmlSerializer;
//  I: Integer;
//  s: string;
begin
  TLog.SendNote('TdmAPI_main.RunTask_XML');


 // Assert (not FInProcess);
 // FInProcess:=true;
  Result:=false;


  oTask:=dmTask_calc.Calc_XML(aRec.Msg);

//
  g_Logger.Add(oTask.GetResultTypeStr);
  g_Logger.Add(oTask.CalcResult.Description);


//  dmTask_calc.Calc(aTask);
  //-------------------------------------------------------------------
  if params.IsSend_to_ESB_MG then
    if LeftStr(oTask.GUID_str,1)<>'-' then
  //-------------------------------------------------------------------
  begin

    g_Logger.Add('Send_to_IApiIntegrationService_MG prom');
    Result:=Task_Send_to_IApiIntegrationService_MG_prom(oTask, aResponse);

{
    if (aRec.request_source = 'test') or
       (aRec.request_source = '') then
    begin
      g_Logger.Add('Send_to_IApiIntegrationService_MG test');
      Result:=Task_Send_to_IApiIntegrationService_MG_test(oTask, aResponse);
    end;

    if aRec.request_source = 'prom' then
    begin
      g_Logger.Add('Send_to_IApiIntegrationService_MG prom');
      Result:=Task_Send_to_IApiIntegrationService_MG_prom(oTask, aResponse);
    end;
 }

    g_Logger.Add (aResponse);
    g_Logger.Add('');

  end;


  //-------------------------------------------------------------------
  if  params.IsSend_to_local_MG then
  //-------------------------------------------------------------------
  begin
//    Assert (aRec.URL <> '');

    g_Logger.Add('Send_to_Local_MG');
    Result:=Task_Send_to_Local_MG(oTask, aResponse);


    g_Logger.Add (aResponse);
    g_Logger.Add('');

  end;



//  if Assigned(FOnTaskRemoved) then
//    FOnTaskRemoved( aTask.GetDisplayName());

  FreeAndNil(oTask);

  if Assigned(FOnExecute) then
    FOnExecute(Self);

  g_Logger.Progress(0,0);

//  FInProcess:=False;

end;


//-------------------------------------------------------------------
procedure TdmAPI_main.Run_top;
//-------------------------------------------------------------------
var
//  iID: Integer;
//  sMsg: string;
  sResponse: string;
  rec: TdmQueue.TQueueItemRec;

begin
  if Active then
    Exit;

  //  SleepEx(500,true);


  if dmQueue.Queue_Start_item(rec) then
  begin
    if RunTask_XML(rec,  sResponse) then
      dmQueue.Queue_Stop_item(rec, sResponse);

  end;
//  if dmQueue.GetQueueMessage(sMsg) then
   // dmTask_calc.Calc_XML(sMsg);

end;


//-------------------------------------------------------------------
procedure TdmAPI_main.Timer2Timer(Sender: TObject);
//-------------------------------------------------------------------
var
//  iID: Integer;
//  sMsg: string;
  rec: TdmQueue.TQueueItemRec;
  sResponse: string;
begin
  TLog.SendNote('TdmAPI_main.Timer1Timer');

  if not (Active and dmMain.ADOConnection1.Connected) then
  begin
    g_Logger.Add('not (Active and dmMain.ADOConnection1.Connected)...');
    Exit;
  end;

//  g_Logger.Add('Timer...');

  Timer2.Enabled:=False;
//  Terminated:=False;

  while Active and dmMain.ADOConnection1.Connected  do
//  while dmQueue.GetQueueMessage(sMsg)
//    and Active do
  begin
    if not dmQueue.Queue_Start_item(rec) then
      Break;

    if RunTask_XML(rec, sResponse) then
      dmQueue.Queue_Stop_item(rec, sResponse);

//    dmTask_calc.Calc_XML(sMsg);

  //  dmTask.
//    if Assigned(FOnExecute) then
//      FOnExecute(Self);

//    Sleep(100);

    Application.ProcessMessages;
  end;
  //  TMailSender.Create(Msg, UserGUID, Self);

  if Active then
    Timer2.Enabled:=True;

//  Timer1.Enabled:=False;


//  Timer1.Enabled:=False;

end;


//-------------------------------------------------------------------
function TdmAPI_main.StartServer: Boolean;
//-------------------------------------------------------------------
begin
  TLog.SendNote('TdmAPI_main.StartServer');
//  Active:=True;
  Active:=False;

  if not dmMain.Open then
  begin
    TLog.Error('TdmAPI_main.StartServer; - dmMain.Open - not connected');

    g_Logger.Add('dmMain.Open - not connected');
    Exit(False);
  end;

  dmMain.Config_Load_;

  Timer2.Enabled:=True;

  Active:=True;

  Result:=True;
//  Terminated:=False;
//  dmMSMQ_Main.tim

//  g_Logger.RootDir:=DirectoryEdit1.Text;

end;

//-------------------------------------------------------------------
procedure TdmAPI_main.StopServer;
//-------------------------------------------------------------------
begin
  TLog.SendNote('TdmAPI_main.StopServer');

  Active:=False;

  dmMain.Close;

  Timer2.Enabled:=False;
 // Terminated:=True;
end;



end.

//
//procedure TdmAPI_main.Timer_Net_errorTimer(Sender: TObject);
//begin
//  g_Logger.Add('procedure TdmAPI_main.Timer_Net_error Timer');
//
//  Timer_Net_error.Enabled:=False;
//  Timer1.Enabled:=True;
//  Active:=True;
//end;

