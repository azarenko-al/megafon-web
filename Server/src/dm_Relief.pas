unit dm_Relief;

interface

uses
//  u_Logger,
  dm_Main ,

  u_geo,
  u_geo_convert_new,
  u_db,

  fr_Profile_Rel_Graph,

  u_rel_Matrix_base,
  u_rel_engine,
  u_rel_profile,

  I_rel_Matrix1,


  System.DateUtils, CodeSiteLogging,
  SysUtils, Classes,  Forms, Variants, Data.DB, Data.Win.ADODB;

type
  TdmRelief = class(TDataModule)
    DataSource_Relief: TDataSource;
    ADOStoredProc_Relief: TADOStoredProc;
    ADOStoredProc1: TADOStoredProc;
    procedure DataModuleDestroy(Sender: TObject);
    procedure DataModuleCreate(Sender: TObject);
  private

    FRelProfile: TrelProfile;

  public
    RelEngine: TRelEngine;

    procedure Dlg_MakeProfileImagePNG(aVector: TLatLonVector);
  //  procedure Load_RLF_files_from_dir(aDir : string);

    class procedure Init;

    function FindPointByPos_WGS(aBLPoint_WGS: TBLPoint; var aRec: Trel_Record):
        Boolean;

    function OpenByTask(aTask_ID: integer): Boolean;
    procedure Close;

//    function OpenByVector(aTask_ID: integer ; aRelEngine: TRelEngine): Boolean;

  end;


var
  dmRelief: TdmRelief;

implementation
{$R *.dfm}

procedure TdmRelief.Close;
begin
  RelEngine.Clear;
end;

procedure TdmRelief.DataModuleCreate(Sender: TObject);
begin
  RelEngine:=TRelEngine.Create;
  FRelProfile:=TrelProfile.Create;

//  FdmMain:= dmMain;
end;


procedure TdmRelief.DataModuleDestroy(Sender: TObject);
begin
  FreeAndNil(RelEngine);
  FreeAndNil(FRelProfile);
end;


class procedure TdmRelief.Init;
begin
  if not Assigned(dmRelief) then
    Application.CreateForm(TdmRelief, dmRelief);

end;


//procedure TdmRelief.SetDataManager(aDmMain: TdmMain);
//begin
////  FdmMain:= aDmMain;
//end;
//


//-------------------------------------------------------------------
function TdmRelief.FindPointByPos_WGS(aBLPoint_WGS: TBLPoint; var aRec:   Trel_Record): Boolean;
//-------------------------------------------------------------------
var
  bl_ck: TBLpoint;
  k: Integer;

begin
  CodeSite.Send( 'function TdmRelief.FindPointByPos_WGS(aBLPoint_WGS: TBLPoint; aRec:  Trel_Record): Boolean;..' );

  bl_ck:= geo_WGS84_to_Pulkovo42(aBLPoint_WGS);


  RelEngine.Clear;

  k:=dmMain.StoredProc_Open(ADOStoredProc_Relief, 'GIS.sp_Relief_by_LatLon_SEL',
    [
    'LAT', aBLPoint_WGS.B,
    'LON', aBLPoint_WGS.L
    ]);


  with ADOStoredProc_Relief do
    while not EOF do
    begin
      RelEngine.AddFile (FieldValues[FLD_FILENAME]);

      Next;
    end;

  Result:=RelEngine.FindPointBL (bl_ck, aRec);

end;



//-------------------------------------------------------------------
function TdmRelief.OpenByTask(aTask_ID: integer): Boolean;
//-------------------------------------------------------------------
var
  k: Integer;
  s: string;

  dt_start: TDateTime;
begin
  dt_start:=Now;
  CodeSite.Send( 'procedure TdmRelief.OpenByTask...' );


//  if Assigned(aRelEngine) then
  RelEngine.Clear;

  k:=dmMain.StoredProc_Open(ADOStoredProc_Relief, 'GIS.sp_Relief_by_Task_SEL',
    [
    'TASK_ID', aTask_ID
    ]);


  Result:=k>0;


  with ADOStoredProc_Relief do
    while not EOF do
    begin
      s:=FieldValues[FLD_FILENAME];

      if Assigned(RelEngine) then
        RelEngine.AddFile (s);

//      g_RelEngine.AddFile (s);

      CodeSite.Send('rel added: '+ s);

      Next;
    end;

  CodeSite.Send( 'procedure TdmRelief.OpenByTask done ' +
                 intToStr( MilliSecondsBetween(dt_start, Now)) + ' msec, '+
                 intToStr( SecondsBetween(dt_start, Now)) + ' sec');

//  Result:=true;
end;


// ---------------------------------------------------------------
procedure TdmRelief.Dlg_MakeProfileImagePNG(aVector: TLatLonVector);
// ---------------------------------------------------------------
var
  oForm: Tframe_Profile_Rel_Graph;
  sDir: string;
  i: Integer;
  dRefraction: double;
//  dRefraction, dOldRefraction: double;
  b: Boolean;
  d: Double;
//  iDirection: integer;
//  is_profile_reversed: Boolean;

//  dt_start: TDateTime;
begin
 // dt_start:=Now;
//  CodeSite.Send( 'procedure TdmTask_profile.MakeProfileImagePNG...' );


  oForm:=Tframe_Profile_Rel_Graph.Create(Application);

 // oForm.SetChartSize (550,400);

//  oForm.Show;
 // oForm.BringToFront;

//  oForm.SetChartSize(g_Config.IMAGE_PROFILE_WIDTH, g_Config.IMAGE_PROFILE_HEIGHT);// 550;

//  oForm.Width :=g_Config.IMAGE_PROFILE_WIDTH;// 550;
//  oForm.Height:=g_Config.IMAGE_PROFILE_HEIGHT;//400;

//  oForm.Height:=g_Config.IMAGE_PROFILE_HEIGHT;//400;

  oForm.RelProfile_Ref:=FRelProfile;



//  oForm.Params.SetSiteGroupCount(1);
  oForm.Params.Freq_MHz:=10 * 1000;
//  oForm.Params.Freq_MHz:=aRec.freq_GHz  * 1000;

  oForm.Params.Distance_KM:=FRelProfile.Data.Distance_KM;

  //RelProfile.GeoDistance_KM();

//  iDirection:=ADOStoredProc_Link.FieldByName(FLD_CALC_DIRECTION).AsInteger;
//
//  Assert (aTaskPlace.CalcResult.client_antenna_height > 0);

{
  oForm.Params.Site1.AntennaHeight:= aTaskPlace.CalcResult.client_antenna_height;
  oForm.Params.Site1.HouseHeight  := FTask.Client.map_building_height; // aTaskPlace.map_building_height;

//  oForm.Params.SiteGroups[0].Site1.AntennaHeights:= MakeDoubleArray([FTask.client_antenna_height]);
  oForm.Params.Site2.AntennaHeight:= aTaskPlace.height;
  oForm.Params.Site2.HouseHeight  := aTaskPlace.map_building_height;


  oForm.Params.Site1.Rel_H:=FRelProfile.FirstItem.Rel_H;
  oForm.Params.Site2.Rel_H:=FRelProfile.LastItem.Rel_H;
 }

        // oForm.Params.SiteGroups[0].ReflectionPointArr

  oForm.ShowGraph;

  oForm.ShowModal;
  FreeAndNil(oForm);


end;



end.


