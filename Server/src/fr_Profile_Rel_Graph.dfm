object frame_Profile_Rel_Graph: Tframe_Profile_Rel_Graph
  Left = 606
  Top = 223
  Anchors = [akLeft, akTop, akBottom]
  Caption = 'frame_Profile_Rel_Graph'
  ClientHeight = 518
  ClientWidth = 813
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object ToolBar1: TToolBar
    Left = 0
    Top = 0
    Width = 813
    Height = 25
    Caption = 'ToolBar1'
    Color = clAppWorkSpace
    ParentColor = False
    TabOrder = 0
    object Button1: TButton
      Left = 0
      Top = 0
      Width = 75
      Height = 22
      Caption = 'Button1'
      TabOrder = 0
      OnClick = Button1Click
    end
    object Button2: TButton
      Left = 75
      Top = 0
      Width = 75
      Height = 22
      Caption = 'Button2'
      TabOrder = 1
      OnClick = Button2Click
    end
  end
  object Panel1: TPanel
    Left = 0
    Top = 25
    Width = 713
    Height = 493
    Align = alLeft
    BevelOuter = bvNone
    TabOrder = 1
    object Chart1: TChart
      Left = 0
      Top = 0
      Width = 713
      Height = 493
      BackWall.Brush.Style = bsClear
      Foot.Visible = False
      Legend.Visible = False
      MarginBottom = 5
      MarginLeft = 0
      MarginRight = 0
      MarginTop = 2
      Title.Text.Strings = (
        'TChart')
      Title.Visible = False
      Title.AdjustFrame = False
      BottomAxis.Automatic = False
      BottomAxis.AutomaticMaximum = False
      BottomAxis.Axis.Width = 1
      BottomAxis.AxisValuesFormat = '#,##0.##'
      BottomAxis.ExactDateTime = False
      BottomAxis.Grid.Style = psDashDotDot
      BottomAxis.Grid.SmallDots = True
      BottomAxis.Increment = 3.000000000000000000
      BottomAxis.LabelStyle = talValue
      BottomAxis.Maximum = 25.000000000000000000
      BottomAxis.MinorTickCount = 4
      LeftAxis.Axis.Color = clGray
      LeftAxis.Axis.Width = 1
      LeftAxis.ExactDateTime = False
      LeftAxis.Grid.Style = psDashDot
      LeftAxis.Grid.SmallDots = True
      LeftAxis.Grid.Visible = False
      LeftAxis.Increment = 10.000000000000000000
      LeftAxis.LabelsOnAxis = False
      LeftAxis.LabelsSeparation = 0
      LeftAxis.LabelsSize = 25
      LeftAxis.MinorTickCount = 4
      LeftAxis.TickInnerLength = 1
      LeftAxis.TickLength = 2
      LeftAxis.Ticks.Color = 4194368
      LeftAxis.TitleSize = 22
      RightAxis.Automatic = False
      RightAxis.AutomaticMaximum = False
      RightAxis.Axis.Color = 4194368
      RightAxis.Axis.Width = 1
      RightAxis.Grid.Visible = False
      RightAxis.Maximum = 992.500000000000000000
      RightAxis.MinorTickCount = 4
      TopAxis.Axis.Width = 1
      TopAxis.MinorTickLength = 0
      View3D = False
      OnAfterDraw = Chart1AfterDraw
      Align = alClient
      BevelOuter = bvNone
      Color = clWhite
      AutoSize = True
      TabOrder = 0
      DefaultCanvas = 'TGDIPlusCanvas'
      ColorPaletteIndex = 13
      object ser_Earth_bottom: TFastLineSeries
        SeriesColor = clGray
        LinePen.Color = clGray
        XValues.Name = 'X'
        XValues.Order = loAscending
        YValues.Name = 'Y'
        YValues.Order = loNone
        Left = 4
        Data = {
          00190000000000000000708C400000000000C48D400000000000408A40000000
          0000A0894000000000000089400000000000C087400000000000688A40000000
          0000B489400000000000E486400000000000DC84400000000000C08740000000
          0000EC884000000000005089400000000000EC88400000000000048A40000000
          00006489400000000000B088400000000000B088400000000000B88540000000
          0000C8844000000000000886400000000000FC82400000000000788440000000
          0000AC87400000000000748840}
      end
      object ser_Earth_area_Top: TAreaSeries
        SeriesColor = clWhite
        VertAxis = aBothVertAxis
        AreaBrush = bsHorizontal
        AreaChartBrush.Color = clGray
        AreaChartBrush.Style = bsHorizontal
        AreaChartBrush.BackColor = clDefault
        AreaChartBrush.Image.Data = {
          07544269746D61707E000000424D7E000000000000003E000000280000001000
          0000100000000100010000000000400000000000000000000000020000000200
          000000000000FFFFFF00EEEE0000FFFF0000BBBB0000FFFF0000EEEE0000FFFF
          0000BBBB0000FFFF0000EEEE0000FFFF0000BBBB0000FFFF0000EEEE0000FFFF
          0000BBBB0000FFFF0000}
        AreaLinesPen.Color = clGray
        AreaLinesPen.Visible = False
        DrawArea = True
        Pointer.InflateMargins = False
        Pointer.Style = psRectangle
        Pointer.Visible = False
        XValues.Name = 'X'
        XValues.Order = loAscending
        YValues.Name = 'Y'
        YValues.Order = loNone
        Left = 4
        Data = {
          00190000000000000000888A400000000080EF8A409A99999919DA8B40343333
          33B39C8A40CECCCCCCCC648B400100000000578B40CECCCCCCCC508A40CECCCC
          CCCC508A409B999999992D8B409B99999999198A406866666666B18A40686666
          66666C8A4068666666E6A28B4035333333B33A8C409B99999999B78B40CECCCC
          CCCCEE8B40CECCCCCC4C878B409B999999190B8B4068666666E6048A409B9999
          9999A38A400100000080AA8A400100000000128B409B99999999198A40010000
          0000EA88409B99999919288940}
      end
      object ser_Earth_top: TFastLineSeries
        SeriesColor = clGray
        LinePen.Color = clGray
        XValues.Name = 'X'
        XValues.Order = loAscending
        YValues.Name = 'Y'
        YValues.Order = loNone
        Left = 4
        Data = {
          001900000099999999194587403333333333C88740CDCCCCCC4CF286409A9999
          9999988640CDCCCCCCCC4586406766666666EB8640CDCCCCCCCC0086409A9999
          991931864034333333333E8740CECCCCCCCC00864068666666E66F85409B9999
          99990E86409B99999919D88440CECCCCCC4C408440CECCCCCCCC098340010000
          0080ED83406766666666F4834067666666E6028340CDCCCCCCCC938340000000
          0080ED83409A9999999970844067666666E66F85409A99999919A78540343333
          33B34C86409A999999198A8740}
      end
      object ser_Earth_area_bottom: TAreaSeries
        Legend.Visible = False
        SeriesColor = clWhite
        ShowInLegend = False
        AreaChartBrush.Color = clBlack
        AreaChartBrush.BackColor = clDefault
        AreaLinesPen.Visible = False
        DrawArea = True
        Pointer.InflateMargins = True
        Pointer.Style = psRectangle
        Pointer.Visible = False
        XValues.Name = 'X'
        XValues.Order = loAscending
        YValues.Name = 'Y'
        YValues.Order = loNone
        Left = 4
        Data = {
          00190000003433333333A28C400100000080D28C4067666666E60F8E40CDCCCC
          CC4C7E8E4067666666E6548E40CDCCCCCCCCE58E406766666666778E40343333
          33B3EC8E4034333333B3768F406766666666778E400100000080B58E40676666
          66668B8F40CDCCCCCC4C4D8F40CDCCCCCC4CD78F400000000000EC8F40666666
          6666468F409999999999119040CCCCCCCCCCB48F4066666666A6759040000000
          0080189040CDCCCCCC0C5A9040CDCCCCCCCC039040CDCCCCCCCC2A8F409A9999
          991934904034333333332D9040}
      end
      object Series_Earth_lines: TFastLineSeries
        IgnoreNulls = False
        LinePen.Color = clRed
        LinePen.Style = psDot
        XValues.Name = 'X'
        XValues.Order = loNone
        YValues.Name = 'Y'
        YValues.Order = loNone
        Data = {
          001900000066666666A6F38940A145B6F36DC48840B19DEFA7E6238840012B87
          1669598840F753E3A50BA7874045B6F3FDECD98640A3703D0A476C85405DBA49
          0C22E98540F5285C8F6AA486401283C0CAE1B88740C876BE9F7A30864039B4C8
          76EE248540FA7E6ABCE44E84406DE7FBA9F94584406F1283C09A4885403AB4C8
          76EE24854040355EBA09D283404A0C022B67848440FA7E6ABCE44E8440C520B0
          72382B8440448B6CE74BD7854023DBF97E5A188740FA7E6ABCACA988404A0C02
          2B2FDF8840A8C64B3789718740}
      end
      object Series_Clu: TAreaSeries
        AreaChartBrush.Color = clGray
        AreaChartBrush.BackColor = clDefault
        AreaLinesPen.Visible = False
        Dark3D = False
        DrawArea = True
        Pointer.InflateMargins = True
        Pointer.Style = psRectangle
        Pointer.Visible = False
        XValues.Name = 'X'
        XValues.Order = loNone
        YValues.Name = 'Y'
        YValues.Order = loNone
        Data = {
          0019000000703D0AD703A68A4008AC1C5A4C618B40E5D022DBB99F8B40560E2D
          B22D948A40448B6CE713328A40AAF1D24D2A748840B81E85EB01D18640ED7C3F
          35AEF48640285C8FC275C585405DBA490C22E985401704560EFD658640C776BE
          9F7A308640AE726891452187401A5A643B3F6B8840956E128310128840CCF753
          E35D388940E7263108344A8940CE22DBF9FE3A8A4043B6F3FDB4348B403D355E
          BA99878C40623BDF4FCD4B8D4037B4C8767EDA8D4037B4C8767EDA8D40DD4F8D
          979EF28C4097999999796F8D40}
      end
      object ser_Tower: TLineSeries
        SeriesColor = clBlue
        Brush.BackColor = clDefault
        LinePen.Width = 3
        Pointer.InflateMargins = True
        Pointer.Style = psRectangle
        XValues.Name = 'X'
        XValues.Order = loNone
        YValues.Name = 'Y'
        YValues.Order = loNone
        Data = {
          0019000000B19DEFA74AD390408A6CE7FBD9399140CEF753E3C1E791403D0AD7
          A3F8069240E5D022DBB92192405C8FC2F5ACF0914060E5D0228BC89140894160
          E59C649240BA490C026BB09240C0CAA145EA8A9340068195430F0E93404A0C02
          2BF7BB934000000000F4609440C74B3789696594404C37894198BE9440E4A59B
          C4E079954026068195272595404B0C022B5BE99540C620B0722C90954062E5D0
          225323964023B07268494D95408D6CE7FBA1949540B49DEFA7122E9540C4F528
          5C8B8D9440F3D24D62B8D69340}
      end
      object ser_Site_marks: TPointSeries
        Marks.Brush.Color = clBlack
        Marks.Shadow.HorizSize = 0
        Marks.Shadow.Transparency = 5
        Marks.Shadow.VertSize = 0
        Marks.Visible = True
        Marks.Arrow.SmallDots = True
        Marks.Arrow.Visible = False
        Marks.BackColor = clWhite
        Marks.Callout.Arrow.SmallDots = True
        Marks.Callout.Arrow.Visible = False
        Marks.Callout.Length = 8
        Marks.Color = clWhite
        SeriesColor = clBlue
        ClickableLine = False
        Pointer.Brush.Color = clRed
        Pointer.HorizSize = 5
        Pointer.InflateMargins = True
        Pointer.Style = psCircle
        Pointer.VertSize = 5
        XValues.Name = 'X'
        XValues.Order = loAscending
        YValues.Name = 'Y'
        YValues.Order = loNone
        Data = {
          00190000007EAEB6E20F4A9040202E39EED41F8F4079EE3D5C23918F40514EB4
          AB68119040F01B5F7B34408F40975B5A0DE6CE8E407F52EDD315DF8E4091F7AA
          95F3808F40975B5A0DE6CE8E4004CFBD8705F49040E0FDF15E5068924099E2AA
          B2DF9892401010CCD1F047924043D47D808ACE91403ABEF6CC9ED992408EA19C
          68F4449140A3F83166CB8D91403ABEF6CC9ED992403D70CE08988092404C63B4
          8E7C7B93402B44C0A1C09694402EF697DDB93D94401C51DA1BDC9B93401F03B2
          57D5429340C390D5AD8D2A9340}
      end
      object ser_Direct: TLineSeries
        SeriesColor = clBlack
        Brush.BackColor = clDefault
        Pointer.InflateMargins = True
        Pointer.Style = psRectangle
        XValues.Name = 'X'
        XValues.Order = loAscending
        YValues.Name = 'Y'
        YValues.Order = loNone
        Data = {
          0019000000B5C876BEA88E94402FA86F19B3E49340D335936F6BCC93409B9482
          EED83F9240A4AA09A2C4349140302FC0BEAC2C914033E197FAA5D39040AA0EB9
          19B782904012C2A30DEAEE914074982F2F2455914038BEF6CC9ED992406E3480
          B7310792409E465A2AD2E6914053793B4268709240F7065F9820589240D33593
          6F6BCC934077C3B6C523B493408304C50F0F089540C76D348086309540715F07
          4E3166944077C3B6C523B493409A0D3249DFF794402FA86F19B3E4934076C3B6
          C523B49340AFEB17ECAF889240}
      end
      object ser_Frenel: TLineSeries
        Brush.BackColor = clDefault
        Pointer.InflateMargins = True
        Pointer.Style = psRectangle
        XValues.Name = 'X'
        XValues.Order = loNone
        YValues.Name = 'Y'
        YValues.Order = loNone
        Data = {
          001900000016FBCBEEDC5B8840B7D617099C9C88402D7DE882B3038B4028A089
          B0BAFD884075982F2F24D38A4081608E1E096F894052D5045162D786406ADE71
          8A32C786400B410E4AEB4F8440CF66D5E765D485405E9D634047738540E76F42
          2136C485408EAF3DB3E7528540C425C79D7A80844041B797347E7D8340FA22A1
          2D07F680406BEC12D52557814089592F86E8948040A1629CBFB88480407E9F71
          E1F6847F403EF67AF76B9379402B431CEB9A617E40B59C4B7183FA7B40D17476
          32596480406DFAB31F19E37E40}
      end
      object ser_House: TAreaSeries
        SeriesColor = 16711808
        Title = 'ser_House'
        AreaChartBrush.Color = clGray
        AreaChartBrush.BackColor = clDefault
        AreaLinesPen.Visible = False
        Dark3D = False
        DrawArea = True
        Pointer.InflateMargins = True
        Pointer.Style = psRectangle
        Pointer.Visible = False
        XValues.Name = 'X'
        XValues.Order = loNone
        YValues.Name = 'Y'
        YValues.Order = loNone
        Data = {
          0019000000DF85F7265C358F402F79F21F92378C408613E21B387C8A40000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000000000000000000000000000FB1F38F807E479400696FB
          EE69F47540446C3EEECDA37540}
      end
      object ser_House_marks: TPointSeries
        Marks.Brush.Color = clBlack
        Marks.Font.Color = 4194368
        Marks.Shadow.HorizSize = 0
        Marks.Shadow.Transparency = 5
        Marks.Shadow.VertSize = 0
        Marks.Visible = True
        Marks.Arrow.SmallDots = True
        Marks.Arrow.Visible = False
        Marks.BackColor = 8454143
        Marks.Callout.Arrow.SmallDots = True
        Marks.Callout.Arrow.Visible = False
        Marks.Callout.Length = 8
        Marks.Color = 8454143
        SeriesColor = clBlue
        ClickableLine = False
        Pointer.Brush.Color = clRed
        Pointer.HorizSize = 2
        Pointer.InflateMargins = True
        Pointer.Style = psCross
        Pointer.VertSize = 2
        XValues.Name = 'X'
        XValues.Order = loAscending
        YValues.Name = 'Y'
        YValues.Order = loNone
        Data = {
          00190000007EAEB6E20F4A9040202E39EED41F8F4079EE3D5C23918F40514EB4
          AB68119040F01B5F7B34408F40975B5A0DE6CE8E407F52EDD315DF8E4091F7AA
          95F3808F40975B5A0DE6CE8E4004CFBD8705F49040E0FDF15E5068924099E2AA
          B2DF9892401010CCD1F047924043D47D808ACE91403ABEF6CC9ED992408EA19C
          68F4449140A3F83166CB8D91403ABEF6CC9ED992403D70CE08988092404C63B4
          8E7C7B93402B44C0A1C09694402EF697DDB93D94401C51DA1BDC9B93401F03B2
          57D5429340C390D5AD8D2A9340}
      end
    end
  end
end
