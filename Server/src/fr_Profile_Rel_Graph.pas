﻿unit fr_Profile_Rel_Graph;

interface

uses
  Windows, SysUtils, Classes, Graphics, Forms, Controls,
  ActnList, Dialogs, ComCtrls, Math,

 // u_dll_img,
  u_img_PNG,

  I_rel_Matrix1,
 // I_MapView,
 // I_MapAct,

  u_Geo,

  u_rel_Profile,
  u_rel_Profile_tools,

//  u_profile_classes_,

  ToolWin, ExtCtrls, StdCtrls, //TeeTools, VclTee.TeeGDIPlus, System.Actions,
  VCLTee.Series, VCLTee.TeEngine, VCLTee.TeeProcs, VCLTee.Chart,
  VclTee.TeeGDIPlus  ;


type

  Tframe_Profile_Rel_Graph = class(TForm)
    ToolBar1: TToolBar;
    Panel1: TPanel;
    Chart1: TChart;
    ser_Earth_bottom: TFastLineSeries;
    ser_Earth_area_Top: TAreaSeries;
    ser_Earth_top: TFastLineSeries;
    ser_Earth_area_bottom: TAreaSeries;
    Series_Earth_lines: TFastLineSeries;
    Series_Clu: TAreaSeries;
    ser_Tower: TLineSeries;
    ser_Site_marks: TPointSeries;
    ser_Direct: TLineSeries;
    ser_Frenel: TLineSeries;
    Button1: TButton;
    Button2: TButton;
    ser_House: TAreaSeries;
    ser_House_marks: TPointSeries;
  //  procedure act_CrossExecute(Sender: TObject);
  //  procedure act_MagnifyExecute(Sender: TObject);
    procedure FormCreate(Sender: TObject);
//    procedure FormDestroy(Sender: TObject);
//    procedure act_SaveImageAsExecute(Sender: TObject);
//    procedure act_SaveProfileAsExecute(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure Chart1AfterDraw(Sender: TObject);


  //    procedure Chart1MouseMove(Sender: TObject;
   //       Shift: TShiftState; X, Y: Integer);

//..    procedure Chart1MouseMove(Sender: TObject; Shift: TShiftState; X,
//  ..    Y: Integer);
////    procedure Chart1DblClick(Sender: TObject);
    procedure FormResize(Sender: TObject);

  private
    FIsShowSites: Boolean;

    procedure DrawFrenelZone(aMinDistance_km, aLength_km, aNFrenel, aFreq_Mhz, aH1,
        aH2: double; aColor: Integer);

    procedure ShowFrenelZone ();


  private
//    FMagnify : TMagnifyTool;
  //  CrossOld: TPoint;


//    FOldX, FOldY: integer;

   // FIMapView: IMapViewX;

    // для анализ макс высоты
    FMaxLocalH: Double;
    FMinRelH: Double;
    FMaxRelH: Double;

    procedure BeginUpdateAll;
// TODO: AddFastLineSeries
//  function AddFastLineSeries(aTag: Integer; aLinePenStyle: TPenStyle;
//      aSeriesColor: Integer): TFastLineSeries;


    procedure UpdateMinMaxHeights;
    procedure DrawRelief(aRelProfile: TRelProfile);//; aLenOffset: double);

//    procedure DrawClutters(aRelProfile: TRelProfile);// aLenOffset_KM: double);
    procedure DrawClutters(aRelProfile: TRelProfile);
    procedure DrawEarthLines_new(aMinDistance_km, aLength_km, aRefraction: double);
    procedure EndUpdateAll;
// TODO: ShowSites
//  procedure ShowSites ();

//    procedure ShowReflectionPoints_new;

    procedure ShowSites_;
    procedure UpdateChartLeftIncrement;

  protected
    procedure UpdateChartBottomIncrement;
  public
    RelProfile_Ref: TrelProfile;

    Params: record
      Freq_MHz : double; // рабочая частота //ГГц
      NFrenel  : double; // ширина зоны Френеля

      Place_Name  : string;
      Client_Name : string;

      Distance_KM: Double;       //длина участка

      Site1, Site2: record
                     AntennaHeight: Double;
                     Rel_H: double;
                     HouseHeight: Double;
                   end;
     end;



    procedure Clear;
    procedure DrawHeader;

    procedure ShowGraph;

    procedure SetChartSize(aWidth,aHeight : integer);


//    procedure SaveToBmpFile (aFileName: string='');
    procedure SaveToPNG(aFileName: string);

  end;


//var
//  frame_Profile_Rel_Graph :Tframe_Profile_Rel_Graph;
//


//===================================================================
//===================================================================
implementation
 {$R *.DFM}

const
  REGISTRY_COMMON_FORMS  = 'Software\Onega\Common\Forms\';


const
  FRENEL_STEP_COUNT = 50; // кол-во отсчетов при рисовании Frenel
 // REFLECT_SERIES = 1;

  DEFAULT_INCREMENT = 10;

  //цвета для участков отражения
//  DEF_REFLECTION_COLORS: array [0..9] of integer =
//      (clOlive, clGreen, clLime, clYellow, clMaroon, clNavy, clRed, clBlue, clOlive, clTeal);

  //цвета для участков FRENEL
  FRENEL_COLORS: array [0..0] of integer =
      (clRed);

 // DIRECT_FRENEL_COLORS: array [0..3] of integer =
  //    (clNavy, clBlack, clBlue, clMaroon);


//  EARTH_LINE_COUNT = 50;


(*  DEF_SERIES_TYPE_EARTH_LINE = 10;
  DEF_SERIES_FRENEL_ZONE = 11;
  DEF_SERIES_DIRECT_LINE = 12;
*)

  DEF_WATER_OFFSET = 3; //5;


  REL_BOTTOM_OFFSET=0;
//  REL_TOP_MIN_OFFSET=20; // meters
  REL_TOP_MIN_OFFSET=5; // meters

//  MIF_LAYER_PROFILE_REL_POINT = 'PROFILE_REL_POINT';

//  DEF_CHART_MIN_HEIGHT = -100;
//  DEF_CHART_MIN_HEIGHT = 800;



// -------------------------------------------------------------------
procedure Tframe_Profile_Rel_Graph.FormCreate(Sender: TObject);
// -------------------------------------------------------------------
var i: integer;
 // oFastLineSeries: TFastLineSeries;
begin
  inherited;

 // Params_Refraction_2:=0.5;


//  ChartTool_Magnify1.Visible := False;


  Series_Clu.LinePen.Visible := False;


  ser_Earth_top.XValues.Order:=loNone;
  ser_Earth_area_top.XValues.Order:=loNone;

  Series_Earth_lines.SeriesColor  :=Chart1.BottomAxis.Ticks.Color;
  Series_Earth_lines.LinePen.Style:=psDot;
  Series_Earth_lines.IgnoreNulls :=False;
  Series_Earth_lines.TreatNulls  :=tnDontPaint;

  ser_Tower.Clear;
  ser_Site_marks.Clear;
  ser_House.Clear;

//  ser_Refl.XValues.Order:=loNone;
//  ser_Refl.IgnoreNulls :=False;
//  ser_Refl.TreatNulls  :=tnDontPaint;


//  Series_Earth_reflection_2.XValues.Order:=loNone;
//  Series_Earth_reflection_2.SeriesColor  :=clRed;
//  Series_Earth_reflection_2.LinePen.Style:=psDot;
//  Series_Earth_reflection_2.IgnoreNulls :=False;
//  Series_Earth_reflection_2.TreatNulls  :=tnDontPaint;


 // Series_Frenel.


  i:=Chart1.SeriesList.IndexOf(ser_House);
  Chart1.SeriesList.Move(i,0);

  i:=Chart1.SeriesList.IndexOf(Series_Clu);
  Chart1.SeriesList.Move(i,0);


  i:=Chart1.SeriesList.IndexOf(ser_Site_marks);
  Chart1.SeriesList.Move(i,Chart1.SeriesList.Count-1);

  i:=Chart1.SeriesList.IndexOf(ser_House_marks);
  Chart1.SeriesList.Move(i,Chart1.SeriesList.Count-1);

  //!!!!!!
//  i:=Chart1.SeriesList.IndexOf(Series_Earth_reflection_2);
//  Chart1.SeriesList.Move(i,Chart1.SeriesList.Count-1);



  //------------------------------------------------------
 // if assigned(FIMapView) then
  //   FIMapView.TempLayer_Create(MIF_LAYER_PROFILE_REL_POINT);
  //------------------------------------------------------


//  FRegPath:= REGISTRY_COMMON_FORMS + ClassName +'\';

//  RelProfile_Ref:=TrelProfile.Create;


  Chart1.Align:=alCLient;
  panel1.Align:=alCLient;

  with Chart1 do
  begin
  ////////             AllowZoom:=False;

    LeftAxis.LabelsSize:=33;
    RightAxis.LabelsSize:=33;

    LeftAxis.Automatic:=False;
    RightAxis.Automatic:=False;
    BottomAxis.Automatic:=False;

    LeftAxis.Minimum:=0;
    LeftAxis.Maximum:=0;
    RightAxis.Minimum:=0;
    RightAxis.Maximum:=0;

    BottomAxis.Minimum:=0;

  end;


  FIsShowSites:=True;

end;


//-----------------------------------------------------
procedure Tframe_Profile_Rel_Graph.UpdateMinMaxHeights;
//-----------------------------------------------------
var
//  eFullDistanceKM: Double;
  I: Integer;
begin
  Assert(Assigned(RelProfile_Ref), 'RelProfile_Ref not assigned');

//  RelProfile_Ref.CalcSummary1();
  TRelProfile_tools.CalcSummary1(RelProfile_Ref);

  FMaxRelH := 0;

//  for I := 0 to Length(Params.SiteGroups) - 1 do
 // begin
    FMaxRelH:=Max (FMaxRelH,
                   Max(Params.Site1.Rel_H,
                       Params.Site2.Rel_H));
 // end;

  FMinRelH:=RelProfile_Ref.Summary.MinRelH - REL_BOTTOM_OFFSET;
  FMaxRelH:=Max (RelProfile_Ref.Summary.MaxRelH +  RelProfile_Ref.Summary.MaxEarthH +
                 REL_TOP_MIN_OFFSET,

                 FMaxRelH
//                 Max(Params.SiteGroups[0].Site1.Rel_H,
  //                   Params.SiteGroups[0].Site2.Rel_H)
                 )
                ;


  FMaxLocalH:=0;
//  eFullDistanceKM:=0;

  with Params do
  begin
    Assert(Distance_KM > 0, 'Distance_KM1 = 0');

//    eFullDistanceKM:=eFullDistanceKM + Distance_KM;

    if FIsShowSites then
      FMaxLocalH:=Round(Max(Max(Site1.AntennaHeight, Site2.AntennaHeight), FMaxLocalH));
  end;


  FMaxLocalH:=Max(FMaxLocalH, RelProfile_Ref.Summary.MaxClutterH);

  FMaxLocalH:=FMaxLocalH + FMaxLocalH / 3;


  with Chart1 do
  begin
    LeftAxis.Minimum :=-10000;
    LeftAxis.Maximum :=+10000;
    RightAxis.Minimum:=-10000;
    RightAxis.Maximum:=+10000;

    LeftAxis.Maximum:=FMaxRelH + FMaxLocalH;
    LeftAxis.Minimum:=FMinRelH - 4;
//    LeftAxis.Minimum:=FMinRelH;// + 5;


//    LeftAxis.Maximum:=LeftAxis.Maximum+ ((LeftAxis.Maximum-LeftAxis.Minimum)/6);
//    LeftAxis.Minimum:=FMinRelH - 5;


    Chart1.RightAxis.Minimum :=Chart1.LeftAxis.Minimum;
    Chart1.RightAxis.Maximum :=Chart1.LeftAxis.Maximum;

    UpdateChartLeftIncrement();

    BottomAxis.Minimum:=0; //-0.01;
    BottomAxis.Maximum:=Params.Distance_KM;
  end;


//  StatusBar1.SimpleText:=Format('Distance-%f;  BottomAxis.Maximum:%f',
//                      [RelProfile.Data.Distance, Chart1.BottomAxis.Maximum ]);

end;



procedure Tframe_Profile_Rel_Graph.SaveToPNG(aFileName: string);
begin
//  SaveToBmpFile(aFileName);
  Chart1.SaveToBitmapFile (ChangeFileExt(aFileName, '.bmp'));

  img_BMP_to_PNG(ChangeFileExt(aFileName, '.bmp'), ChangeFileExt(aFileName, '.png'));
end;



//---------------------------------------------------------
procedure Tframe_Profile_Rel_Graph.DrawEarthLines_new(aMinDistance_km,
    aLength_km, aRefraction: double);
//---------------------------------------------------------

  function DoGetEarthH(aDist: double): Double;
  begin
    Result:=geo_GetEarthHeight (aDist, aLength_km, aRefraction);
//    Result:=geo_GetEarthHeight (aDist, RelProfile.GeoDistance1(), RelProfile.Refraction);
  end;


var i,j,k,iYStep,iX,

  iY: integer;
  d: Double;
  eDist: Double;
  h: Double;
  iCount: Integer;
  iStep: integer;
 //   y,
  min_y,eStep,fHeight: double;

  oSeries: TFastLineSeries;

const

  STEPS_COUNT = 50; // кол-во отсчетов при рисовании дуг

begin

//  Series_Earth_lines.Active:=True;
//  Result.Active:=False;
  oSeries := Series_Earth_lines;


  eStep:=aLength_km / STEPS_COUNT;

  min_y:=Chart1.LeftAxis.Increment;


  iCount := Round(Chart1.LeftAxis.Maximum / Chart1.LeftAxis.Increment);

  Assert(iCount<10000, 'Value <=0');

  min_y:=0;

  for k:=0 to iCount do
  begin
    h:= min_y + (k*Chart1.LeftAxis.Increment);
    if h < Chart1.LeftAxis.Minimum then
      Continue;

    oSeries.AddNull;

    for i:=0 to STEPS_COUNT do
    begin
      eDist:=eStep*i;
      fHeight:=h + DoGetEarthH(eDist);

      oSeries.AddXY (aMinDistance_km + eDist, fHeight);
    end;

  end;


end;


//--------------------------------------------------------
procedure Tframe_Profile_Rel_Graph.ShowFrenelZone();
//--------------------------------------------------------
begin
  with Params do
  begin
    Assert(Distance_KM > 0, 'Distance_KM1 = 0');

//    if Params.IsShowFrenelZoneTop then
    DrawFrenelZone (0, Distance_KM, Params.NFrenel,
                    Params.Freq_MHz,
                    Site1.Rel_H + Site1.AntennaHeight,
                    Site2.Rel_H + Site2.AntennaHeight,
                    FRENEL_COLORS[0]
                    );

  end;
end;


//--------------------------------------------------------
procedure Tframe_Profile_Rel_Graph.DrawFrenelZone(aMinDistance_km, aLength_km,
    aNFrenel, aFreq_Mhz, aH1, aH2: double; aColor: Integer);

//--------------------------------------------------------
var
  delta,tang,h,h1,h2,dx,dy: double;
  i:integer;
  k: Integer;
  oSeries: TLineSeries;
begin
  if aNFrenel=0 then
    aNFrenel:=0.3;

  oSeries:=ser_Frenel;

  oSeries.AddNull;
  oSeries.AddXY (aMinDistance_km, aH1);
  oSeries.AddXY (aMinDistance_km + aLength_km, aH2, '', clBlack);


  if (aFreq_Mhz < 0.01) or (aLength_km< 0.001) then
    Exit;

  tang :=(aH2 - aH1) / aLength_km; //наклон линии пр.видим.
  delta:=aLength_km / FRENEL_STEP_COUNT;  //  размер отрезка


//  aSeries.tr

 // aSeries.XValues.Order := loNone;
//  aSeries.YValues.Order := loNone;

  ////////////////////////////

  for k := 0 to 1 do
  begin
    oSeries.AddNull;

//
  //  if k=1 then
    //  aSeries.AddNull;


    for i:=0 to FRENEL_STEP_COUNT do
    begin
      dx:= i*delta;
      h:= tang*dx + aH1; //высота линии пр.видим.


      //ГГц
      //dy:=dx* (1 - dx/(Distance)));
      if (i=0) or (i=FRENEL_STEP_COUNT) then
        dy:=0
      else
        dy:=Sqrt(( 300/aFreq_Mhz )*
            aNFrenel * (1000*dx)* (1 - dx/aLength_km));


      if k=0 then
        h1:=h + dy   //top
      else
        h1:=h - dy;  //bottom


    //  aSeries.AddXY (aMinDistance_km + dx, h1, '', aColor);

      oSeries.AddXY (aMinDistance_km + dx, h1, '', aColor);

    end;
  end;

(*  ser_Direct.Clear;
  aSeries.Clear;
*)
end;


//--------------------------------------------------------
procedure Tframe_Profile_Rel_Graph.ShowSites_;
//--------------------------------------------------------
var h1,h2: double;
  e: Double;
  eH: Double;
  eHouseWidth: Double;
  h: Double;
  //  iAntCount1,iAntCount2: integer;
    i: integer;
begin
  ser_Site_marks.Clear;
  ser_Tower.Clear;

  ser_House.Clear;
  ser_House_marks.Clear;

 // Assert(Length(Params.SiteGroups)>0, 'Tframe_Profile_Rel_Graph.ShowSites_ - Length(Params.SiteGroups)>0');
//  eDistance:=0;


  with Params do
  begin

    Assert(Distance_KM>0, 'procedure Tframe_Profile_Rel_Graph.ShowSites_; - Distance_KM>0');

    // -------------------------
    // 1
    // -------------------------
//
      ser_Tower.AddNull;

      h:=Site1.Rel_H + Site1.AntennaHeight;
      ser_Tower.AddXY (0, 0);
    //  ser_Tower.AddXY (eDistance, Site1.Rel_H);
      ser_Tower.AddXY (0, h);


      ser_Site_marks.AddXY (0,
                       Site1.Rel_H + Site1.AntennaHeight,
                       FloatToStr(Site1.AntennaHeight) );


      if Site1.HouseHeight > 0 then
      begin
        eHouseWidth:=Distance_KM/100;
        eH:=Site1.Rel_H + Site1.HouseHeight;

        ser_House.AddNull;
        ser_House.AddXY(0, eH);
//        ser_House.AddXY(0, h-5);

        ser_House.AddXY(eHouseWidth, eH);
        ser_House_marks.AddXY (0, Site1.Rel_H, FloatToStr(Site1.HouseHeight) );

//                         FloatToStr(20) );
       // ser_House.AddNull;

      end;


//      ser_Sites.AddXY (0+0.0001, FSite1.Rel_H + Params.Site1.Antennas[i],
         //              FloatToStr(Site1.Antennas[i]) );


    // -------------------------
    // 2
    // -------------------------
      ser_Tower.AddNull;

      h:=Site2.Rel_H + Site2.AntennaHeight;


      Assert(Distance_KM > 0, 'Distance_KM1 = 0');


      ser_Tower.AddXY (+ Distance_KM, 0);
//      ser_Tower.AddXY (eDistance + Distance_KM, Site2.Rel_H);
      ser_Tower.AddXY (+ Distance_KM, h);



      ser_Site_marks.AddXY ( Distance_KM,
                       Site2.Rel_H + Site2.AntennaHeight,
                       FloatToStr(Site2.AntennaHeight) );

      if Site2.HouseHeight > 0 then
      begin
        eHouseWidth:=Distance_KM/100;
        eH:=Site2.Rel_H + Site2.HouseHeight;

        ser_House.AddNull;
        ser_House.AddXY(Distance_KM-eHouseWidth, eH);
//        ser_House.AddXY(0, h-5);

        ser_House.AddXY(Distance_KM, eH);
        ser_House_marks.AddXY (Distance_KM, Site2.Rel_H, FloatToStr(Site2.HouseHeight) );
//                         FloatToStr(20) );
       // ser_House.AddNull;

      end;
  end;
end;



procedure Tframe_Profile_Rel_Graph.Button1Click(Sender: TObject);
begin
  DrawHeader;

  Params.Distance_KM:=20;
  Params.Site1.AntennaHeight:=50;
  Params.Site2.AntennaHeight:=50;

  ShowSites_();

end;

//-------------------------------------------------------------------
procedure Tframe_Profile_Rel_Graph.DrawHeader;
//-------------------------------------------------------------------
var
  iTextHeight: Integer;
  k: Integer;
  sName: string;
begin

   With Chart1 do
   begin
      //Modify Font
      Canvas.Font.Color := clRed;

      sName:=Params.Place_Name;
      if sName='' then
        sName:='Мегафон';

      iTextHeight:=Canvas.TextHeight('a');

      //add the Text start at the midpoint of the Rectangle

      Canvas.TextOut(Chart1.Left+5 ,
              Chart1.Left + Chart1.Height - iTextHeight - 2,
              Params.Client_Name);
//              'Клиент');

      Canvas.TextOut(Chart1.Left + Chart1.Width - Canvas.TextWidth(sName) - 5,
              Chart1.Left + Chart1.Height - iTextHeight - 2,
              sName);
   end;
end;


//------------------------------------------------------------------
procedure Tframe_Profile_Rel_Graph.Clear;
//------------------------------------------------------------------
var
  I: Integer;
begin
  for I := 0 to Chart1.SeriesCount - 1 do
    Chart1.Series[i].Clear;
end;


//------------------------------------------------------------------
procedure Tframe_Profile_Rel_Graph.UpdateChartBottomIncrement;
//------------------------------------------------------------------
var
  eDist_km,eStep: Double;
begin
  //in KM
  eDist_km:=RelProfile_Ref.Data.Distance_km;

  if eDist_km < 0.1 then eStep:=0.01  else
  if eDist_km < 1 then eStep:=0.1  else
//  if eDist_km < 2 then eStep:=0.2  else
  if eDist_km < 10 then eStep:=0.5  else
 // if eDist_km < 10 then eStep:=1  else
//  if eDist_km < 20 then eStep:=1 else
  if eDist_km < 30 then eStep:=1 else
  if eDist_km < 40 then eStep:=2
//  if eDist_km < 60 then eStep:=2
  else eStep:=5;

  Chart1.BottomAxis.Increment:=eStep;
end;


//------------------------------------------------------------------
procedure Tframe_Profile_Rel_Graph.UpdateChartLeftIncrement;
//------------------------------------------------------------------
var
  eMaxH_m,eStep: Double;
begin
  eMaxH_m:=FMaxRelH + FMaxLocalH -FMinRelH;

  if eMaxH_m < 10  then eStep:=1  else
  if eMaxH_m < 50  then eStep:=5  else
//  if eMaxH_m < 100 then eStep:=10 else
  if eMaxH_m < 200  then eStep:=10  else
  if eMaxH_m < 500  then eStep:=50  else
  if eMaxH_m < 1000 then eStep:=100 else
  if eMaxH_m < 2000 then eStep:=200
                    else eStep:=200;

  Chart1.LeftAxis.Increment:=eStep;
  Chart1.RightAxis.Increment:=eStep;

end;





//--------------------------------------------------------------------
procedure Tframe_Profile_Rel_Graph.DrawClutters(aRelProfile: TRelProfile);
//--------------------------------------------------------------------
var i,j,iCluCode,iSeries: integer;
//  b: Boolean;

    rel_h,loc_h,
    earth,
    x1,x2,
    h1,h2, rel_h1,rel_h2: double;
    bool: boolean;
    dist_km: double;

    eStep_KM: double;
    e: double;
  iColor: Integer;
  y: Double;

 // eLen: double;
begin
//  Series_Clu.Clear;
 ///// ShowMessage('procedure Tframe_Profile_Rel_Graph.DrawClutters(aRelProfile: TRelProfile);');


  if not assigned(aRelProfile) then
    exit;

 // Series_Clu.BeginUpdate;


//  UpdateClutterSeriesList();

  eStep_KM :=aRelProfile.GetStep_km();


  for i:=0 to aRelProfile.Count-1 do
  begin
    with aRelProfile do
    begin
      rel_h   :=Items[i].Rel_H;
      loc_h   :=Items[i].Clutter_H;
      iCluCode:=Items[i].Clutter_Code;
      earth   :=Items[i].Earth_H;
   //  end;

      dist_km :=Items[i].Distance_km - eStep_KM/2;  // возможны отрицательные значения
   end;

    if iCluCode>0 then
    begin

      iColor := aRelProfile.Clutters.Items[iCluCode].Color;


      x1 :=aRelProfile.Items[i].Distance_km - eStep_KM/2;
      x2 :=aRelProfile.Items[i].Distance_km + eStep_KM/2;


      if iCluCode in [DEF_CLU_BOLOTO,DEF_CLU_WATER,DEF_CLU_ROAD] then
      begin
       // loc_h := 0;
      //  loc_h := 2;
     //   loc_h := 1;
        loc_h := 0;

      end;

      //  DEF_CLU_OPEN_AREA: oAreaSeries.AddXY (dist_km, DEF_CHART_MIN_HEIGHT);


    //  y:=rel_h+loc_h+earth+20;
      y:=rel_h+loc_h+earth;

      Series_Clu.AddXY(x1, y, '', iColor);
      Series_Clu.AddXY(x2, y, '', iColor);

    end else
      Series_Clu.AddNull;


  end;


//  Series_Clu.EndUpdate;

end;



//-------------------------------------
procedure Tframe_Profile_Rel_Graph.DrawRelief(aRelProfile: TRelProfile);
//-------------------------------------
const
  DEF_SMALL_OFFSET = 0; //  0.001;
var
  iOffsetH: Integer;
  i: integer;
  earth_h,min_h,max_h, dist_km: double;

  eStep_KM: double;
  e: double;
  iClu_H: double;

  x1: double;
  x2: Double;

begin
  if not assigned(aRelProfile) then
    exit;

  if aRelProfile.Count=0 then
    Exit;


//  ser_Earth_bottom.BeginUpdate;


{
  if (aRelProfile.Count>1) then
    eStep_KM :=aRelProfile.Items[1].Distance_KM- aRelProfile.Items[0].Distance_KM
  else
    eStep_KM :=0;
}

  eStep_KM:=aRelProfile.GetStep_km();

  e:=aRelProfile.Data.Distance_KM;


  with aRelProfile do
    for i:=0 to Count-1 do
  begin
    dist_km := Items[i].Distance_KM;
    earth_h := Items[i].Earth_H;

//    if Params_Refraction_2 > 0 then
//    begin
//      earth_h_2 := geo_GetEarthHeight (dist_km, aRelProfile.Data.Distance_KM, Params_Refraction_2);
//      max_h_2   := Items[i].Rel_H  + earth_h_2;
//    end;

//      function geo_GetEarthHeight(aOffset_KM, aDistance_KM: double; aRefraction:
 //     double=1.33): double;


//      Params.Refraction_2:=0.5;


//////    earth_h := 0;///////////////////


    if i <= Count then
    begin
      min_h:=FMinRelH + earth_h;;
      max_h:=Items[i].Rel_H  + earth_h;
    end else begin
      min_h:=earth_h;
      max_h:=earth_h;
    end;

    if (Items[i].Clutter_Code in [DEF_CLU_WATER,DEF_CLU_BOLOTO] ) then
//      iOffsetH:=3
      iOffsetH:=DEF_WATER_OFFSET
    else
      iOffsetH:=0;


   ////// iOffsetH := 0;
    //-------------------------------------------------------------------
    x1 := dist_km - eStep_KM/2 + DEF_SMALL_OFFSET;
    x2 := dist_km + eStep_KM/2 - DEF_SMALL_OFFSET;

    ser_Earth_area_top.AddXY (x1, max_h - iOffsetH);
    ser_Earth_area_top.AddXY (x2, max_h - iOffsetH);

    //------------------
    ser_Earth_top.AddXY (x1, max_h - iOffsetH);
    ser_Earth_top.AddXY (x2, max_h - iOffsetH);




//    ser_Earth_area_bottom.AddXY (dist_km, min_h - REL_BOTTOM_OFFSET - 4);

    ser_Earth_area_bottom.AddXY (x1, min_h - REL_BOTTOM_OFFSET - 4);

 //   ser_Earth_area_bottom.AddXY (dist_km - eStep_KM/2, min_h - REL_BOTTOM_OFFSET - 4);

   ser_Earth_bottom.AddXY (x1, min_h - REL_BOTTOM_OFFSET - 4);
   ser_Earth_bottom.AddXY (x2, min_h - REL_BOTTOM_OFFSET - 4);
//    ser_Earth_bottom.AddXY (dist_km + eStep_KM/2, min_h - REL_BOTTOM_OFFSET - 4);


  end;

  e:=aRelProfile.LastItem.Distance_km + eStep_KM/2;

//  ser_Earth_bottom.AddXY (e, DEF_CHART_MIN_HEIGHT);
  ser_Earth_area_bottom.AddXY (e, min_h - REL_BOTTOM_OFFSET - 4);

end;

procedure Tframe_Profile_Rel_Graph.FormResize(Sender: TObject);
begin
  Chart1.Realign;
end;


procedure Tframe_Profile_Rel_Graph.SetChartSize(aWidth,aHeight : integer);
begin
  Chart1.Align:=alNone;

  Chart1.Left:=0;
  Chart1.Top:=0;

  Chart1.Width:=aWidth;
  Chart1.Height:=aHeight;
end;


procedure Tframe_Profile_Rel_Graph.BeginUpdateAll;
var
  I: Integer;
begin
  for I := 0 to Chart1.SeriesCount - 1 do
    Chart1.Series[i].BeginUpdate;;
end;

procedure Tframe_Profile_Rel_Graph.Button2Click(Sender: TObject);
begin
  SetChartSize(500, 600);
end;

procedure Tframe_Profile_Rel_Graph.Chart1AfterDraw(Sender: TObject);
begin
  DrawHeader;
end;


procedure Tframe_Profile_Rel_Graph.EndUpdateAll;
var
  I: Integer;
begin
  for I := 0 to Chart1.SeriesCount - 1 do
    Chart1.Series[i].EndUpdate;;
end;


//-----------------------------------------------------
procedure Tframe_Profile_Rel_Graph.ShowGraph;
//-----------------------------------------------------
begin
  Assert(Assigned(RelProfile_Ref));


  if RelProfile_Ref.Data.Distance_KM=0 then
    exit;


  BeginUpdateAll;

  Clear;

  UpdateMinMaxHeights();


//    ShowReflectionPoints_new();

  //  Exit;

  DrawClutters (RelProfile_Ref);
  DrawRelief (RelProfile_Ref);


//  exit; //!!!!!!!!!!!!!


  with Params do
  begin
    Assert(Distance_KM > 0, 'Distance_KM1 = 0');

    DrawEarthLines_new (0, Distance_KM, RelProfile_Ref.Refraction);

  //  eOffset:=eOffset + Distance_KM;
  end;


//  if Params.IsShowReflectionPoints then
//    ShowReflectionPoints_new();

//(*
//  end
//  else
//  ; //  ClearReflectionPoints();
//*)

  if FIsShowSites then
  begin
    ShowSites_();

    ShowFrenelZone ();
  end;

   // ShowSites() ;


  UpdateChartBottomIncrement();

  EndUpdateAll;

  DrawHeader;

end;



end.



