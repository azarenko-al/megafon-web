unit dm_Queue;

interface

uses
//  u_Config,

  dm_Main,
//  u_Task_classes,

  IOUtils, DB,  Forms, System.SysUtils, System.Classes, Data.Win.ADODB;


type
  TdmQueue = class(TDataModule)
    ADOStoredProc1: TADOStoredProc;
    ADOStoredProc_count: TADOStoredProc;

  type
    TQueueItemRec = record
      ID: Integer;
      Msg: string;
   //   WSDL: string;
      //URL: string;

      request_source: string;
    end ;

  private
  public
    procedure ClearAll;
    procedure ESB_response_count_inc(aGUID : string);
    function GetQueueCount: Integer;

    procedure SendToQueue(aTaskID :integer);

    function Queue_Start_item(var aRec: TQueueItemRec): Boolean;
//    function Queue_Start_item(var aID: Integer; var aMsg: string; var aRec: TQueueItemRec): Boolean;
    procedure Queue_Stop_item(aRec: TQueueItemRec; aResponse: string);

    class procedure Init;

  end;



var
  dmQueue: TdmQueue;

implementation

{%CLASSGROUP 'Vcl.Controls.TControl'}

//uses dm_Task;


{$R *.dfm}



class procedure TdmQueue.Init;
begin
  if not Assigned(dmQueue) then
    Application.CreateForm(TdmQueue, dmQueue);
end;


procedure TdmQueue.ClearAll;
begin
  dmMain.StoredProc_Open(ADOStoredProc1, 'WebService.sp_Queue_DEL',[]);

end;

function TdmQueue.GetQueueCount: Integer;
begin
  Result:=dmMain.StoredProc_Exec(ADOStoredProc_count, 'WebService.sp_Queue_count_SEL',[]);

end;



//-------------------------------------------------------------------
function TdmQueue.Queue_Start_item(var aRec: TQueueItemRec): Boolean;
//-------------------------------------------------------------------
var
  k: Integer;
begin
  k:=dmMain.StoredProc_Open(ADOStoredProc1, 'WebService.sp_Queue_Start_Item_UPD',[]);
  Result:=not ADOStoredProc1.IsEmpty;

  if Result then
  begin
    aRec.ID  :=ADOStoredProc1['id'];
    aRec.Msg :=ADOStoredProc1.FieldByName('body').AsString;

    aRec.request_source:=LowerCase(ADOStoredProc1.FieldByName('request_source').AsString);

//    Assert (aRec.URL <> '');
  end;

end;

//-------------------------------------------------------------------
procedure TdmQueue.Queue_Stop_item(aRec: TQueueItemRec; aResponse: string);
//-------------------------------------------------------------------
var
  k: Integer;
begin
//exit;

  k:=dmMain.StoredProc_Exec(ADOStoredProc1, 'WebService.sp_Queue_Stop_Item_UPD',
  [
    'id', aRec.ID,
    'Response', aResponse

  ]);
end;

//-------------------------------------------------------------------
procedure TdmQueue.SendToQueue(aTaskID :integer);
//-------------------------------------------------------------------
var
  k: Integer;
begin
  k:=dmMain.StoredProc_Exec(ADOStoredProc1, 'MegaGIS_client.sp_Task_Send_to_Queue_INS',
    [
      'id',  aTaskID
    ]);

  Assert (k>0);
end;


//-------------------------------------------------------------------
procedure TdmQueue.ESB_response_count_inc(aGUID : string);
//-------------------------------------------------------------------
var
  k: Integer;
begin
  k:=dmMain.StoredProc_Exec(ADOStoredProc1, 'WebService.sp_Queue_ESB_response_count_inc_UPD',
    [
      'guid',  aGUID
    ]);

  Assert (k>0);
end;



end.

