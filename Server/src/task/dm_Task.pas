unit dm_Task;

interface

uses
  u_log_,
  u_func,
  u_Config,
  u_geo,

  dm_Main,
  u_Task_classes,

  CodeSiteLogging, System.DateUtils,
  IOUtils, DB,  Forms, System.SysUtils, System.Classes, Data.Win.ADODB;



type
  TdmTask = class(TDataModule)
    ADOStoredProc_Task: TADOStoredProc;
    ADOStoredProc_task_place: TADOStoredProc;
    ADOStoredProc1: TADOStoredProc;

  private
  public
    procedure SendToQueue(aTaskID :integer);

    function AddToDB(aTask: TTask): Integer;
    function AddFromXML(aXML: string; aTask: TTask): Integer;

    procedure SaveResultToDB(aTask: TTask; aTaskID: Integer);

    class procedure Init;
    procedure Clear_All_Dlg;

  end;



var
  dmTask: TdmTask;

implementation

{%CLASSGROUP 'Vcl.Controls.TControl'}


{$R *.dfm}



class procedure TdmTask.Init;
begin
  if not Assigned(dmTask) then
    Application.CreateForm(TdmTask, dmTask);
end;

//-------------------------------------------------------------------
function TdmTask.AddToDB(aTask: TTask): Integer;
//-------------------------------------------------------------------
var
  k: Integer;
  oPlace: TTaskPlace;

  dt_start: TDateTime;

begin
 TLog.Start ('function TdmTask.AddToDB(aTask: TTask): Integer;...');

  dt_start:=Now;
  CodeSite.Send( 'function TdmTask.AddToDB(aTask: TTask): Integer;...' );


  Result:=dmMain.StoredProc_Exec(ADOStoredProc_Task, 'MegaGIS_client.sp_Task_INS',
    [
      'guid',         aTask.GUID_str,
      'client_lat' ,  aTask.wgs.client_lat,
      'client_lon' ,  aTask.wgs.client_lon,

      'client_antenna_lat',    aTask.wgs.client_antenna_lat,
      'client_antenna_lon',    aTask.wgs.client_antenna_lon,
      'client_antenna_height', aTask.client_antenna_height,

      'is_can_change_antenna_height' ,  aTask.is_can_change_antenna_height,
      'is_can_change_antenna_location', aTask.is_can_change_antenna_location,

      'bitrate_Mbps', aTask.bitrate_Mbps,
      'Technology', aTask.Technology

   ]);


  Assert (Result > 0);
  aTask.DB_ID:=Result;

//  k:=aTask.Places.Count;

  for oPlace in aTask.Places do
  begin
    oPlace.DB_id:=dmMain.StoredProc_Open(ADOStoredProc_task_place, 'MegaGIS_client.sp_Task_place_INS',
      [
        'Task_id',      result,

        'ObjectID',     oPlace.ObjectID,
        //'SysName',      oPlace.SysName,

        'place_name',   oPlace.place_name,
        'place_lat',    oPlace.Pos_wgs.lat,
        'place_lon',    oPlace.Pos_wgs.lon,
        'place_height', oPlace.height
      ]);

  Assert (oPlace.DB_id > 0, 'oPlace.DB_id:=dmMain.OpenStoredProc(ADOStoredProc_task_place, ' + oPlace.place_name);
   // db_View(ADOStoredProc_task_place);

    if not ADOStoredProc_task_place.IsEmpty then
    begin
      oPlace.map_building_height:=ADOStoredProc_task_place.FieldByName('map_building_height').AsInteger;
      oPlace.length_to_client_km:=ADOStoredProc_task_place.FieldByName('length_to_client_km').AsFloat;

      oPlace.ClientHWTypeID     :=ADOStoredProc_task_place.FieldByName('result_ClientHWTypeID').AsString;
      oPlace.Freq_MHz           :=ADOStoredProc_task_place.FieldByName('result_Freq_MHz').AsFloat;

      if oPlace.Pos_wgs.lat=0 then
      begin
        oPlace.Pos_wgs.lat:=ADOStoredProc_task_place.FieldByName('place_lat').AsFloat;
        oPlace.Pos_wgs.lon:=ADOStoredProc_task_place.FieldByName('place_lon').AsFloat;
      end;
    end;

   // oPlace.DB_id:=iDB_ID;
  end;


  k:=dmMain.StoredProc_Exec(ADOStoredProc_Task, 'MegaGIS_client.sp_Task_prepare_UPD',
     ['ID', Result]);


 TLog.Stop ('');

  CodeSite.Send( 'function TdmTask.AddToDB(aTask: TTask): Integer; done ' +
                 intToStr( MilliSecondsBetween(dt_start, Now)) + ' msec, '+
                 intToStr( SecondsBetween(dt_start, Now)) + ' sec');

end;

//-------------------------------------------------------------------
function TdmTask.AddFromXML(aXML: string; aTask: TTask): Integer;
//-------------------------------------------------------------------
var
//  oTask: TTask;
  oTaskPlace: TTaskPlace;

  k: Integer;

  dt_start: TDateTime;
 // iDB_ID: Integer;
begin
  dt_start:=Now;
  CodeSite.Send( 'function TdmTask.AddFromXML(aXML: string): Integer;.' );


  Result:=dmMain.StoredProc_Open(ADOStoredProc_Task, 'MegaGIS_client.sp_Task_Load_XML',
    [
      'XML_STR',  aXML // TFile.ReadAllText(aFileName)
   ]);

Assert (Result>0, 'MegaGIS_client.sp_Task_Load_XML');

  k:=dmMain.StoredProc_Open(ADOStoredProc_task_place, 'MegaGIS_client.sp_Task_place_SEL',
    [
      'Task_ID',  Result // TFile.ReadAllText(aFileName)
   ]);

Assert (k>0, 'MegaGIS_client.sp_Task_place_SEL  place > 0');

 // oTask := TTask.Create;


  with ADOStoredProc_Task do
  begin
    aTask.GUID_str := FieldValues['guid'];

    aTask.wgs.client_lat          := FieldByName('client_lat').AsFloat;
    aTask.wgs.client_lon          := FieldByName('client_lon').AsFloat;

    aTask.wgs.client_antenna_lat  := FieldByName('client_antenna_lat').AsFloat;
    aTask.wgs.client_antenna_lon  := FieldByName('client_antenna_lon').AsFloat;
    aTask.client_antenna_height   := FieldByName('client_antenna_height').AsFloat;

    aTask.is_can_change_antenna_height   := FieldByName('is_can_change_antenna_height').AsBoolean;
    aTask.is_can_change_antenna_location := FieldByName('is_can_change_antenna_location').AsBoolean;

    aTask.bitrate_Mbps := FieldByName('bitrate_Mbps').AsFloat;
    aTask.Technology   := FieldByName('Technology').AsString;

  end;

   if aTask.Places.Count>1 then
   begin
     aTask.is_can_change_antenna_height  :=g_Config.AutoCalc.is_can_change_antenna_height;
     aTask.is_can_change_antenna_location:=g_Config.AutoCalc.is_can_change_antenna_location;

   end;



  with ADOStoredProc_task_place do
    while not EOF do
    begin
      oTaskPlace:=aTask.Places.AddItem;

      oTaskPlace.ObjectID  := FieldByName('ObjectID').AsLargeInt;
      Assert (oTaskPlace.ObjectID >= 0, 'oTaskPlace.ObjectID >= 0');

    //  oTaskPlace.Sysname   :=FieldByName('ObjectTypeSysname').AsString;

      oTaskPlace.place_name           :=FieldByName('place_name').AsString;
      oTaskPlace.place_name_display   :=FieldByName('place_name_display').AsString;

      oTaskPlace.Pos_wgs.Lat    :=FieldByName('place_lat').AsFloat;
      oTaskPlace.Pos_wgs.Lon    :=FieldByName('place_lon').AsFloat;
      oTaskPlace.Height         :=FieldByName('place_height').AsFloat;

      oTaskPlace.map_building_height:=FieldByName('map_building_height').AsInteger;
      oTaskPlace.length_to_client_km:=FieldByName('length_to_client_km').AsFloat;

      oTaskPlace.ClientHWTypeID :=FieldByName('result_ClientHWTypeID').AsString;
      oTaskPlace.Freq_MHz       :=FieldByName('result_Freq_MHz').AsFloat;

      oTaskPlace.DB_id          :=FieldByName('ID').AsInteger;;

      //aTask.Technology
      oTaskPlace.WM_Is_WiMax    :=Eq(aTask.Technology,'WiMax') and (not FieldByName('WM_Azimuth').IsNull);
      oTaskPlace.WM_Azimuth     :=FieldByName('WM_Azimuth').AsFloat;;
      oTaskPlace.WM_Is_Ready    :=FieldByName('WM_Is_Ready').AsBoolean;

      oTaskPlace.Azimuth_Place_to_Client:=
          geo_Azimuth (MakeBLPoint (oTaskPlace.Pos_wgs.Lat, oTaskPlace.Pos_wgs.Lon),
                       MakeBLPoint (aTask.wgs.client_lat, aTask.wgs.client_lon));

      oTaskPlace.Azimuth_Client_to_Place:=
          geo_Azimuth (MakeBLPoint (aTask.wgs.client_lat, aTask.wgs.client_lon),
                       MakeBLPoint (oTaskPlace.Pos_wgs.Lat, oTaskPlace.Pos_wgs.Lon));

      //function geo_Azimuth (aPoint1,aPoint2: TBLPoint): double;

      Next;
    end;

   //PROCEDURE MegaGIS_client.sp_Task_place_SEL


   Assert (aTask.Places.Count>0);


   aTask.Places.Sort_By_length_to_client;



  CodeSite.Send( 'function TdmTask.AddFromXML(aXML: string): Integer;; done ' +
                 intToStr( MilliSecondsBetween(dt_start, Now)) + ' msec, '+
                 intToStr( SecondsBetween(dt_start, Now)) + ' sec');

 // Result := ;
end;

//-------------------------------------------------------------------
procedure TdmTask.SaveResultToDB(aTask: TTask; aTaskID: Integer);
//-------------------------------------------------------------------
var
  oPlace: TTaskPlace;
begin
//  Assert (aTask.DB_ID>0);

  dmMain.StoredProc_Exec(ADOStoredProc1, 'MegaGIS_client.sp_Task_UPD',
    [
      'id',             aTaskID,
//      'id',             aTask.DB_ID,

   //   'DateCalcStart', aTask.DateCalcStart,

      'result_msg',     aTask.GetResultTypeStr(),
      'result_comment', aTask.CalcResult.Description,

      'comment',        aTask.Log.Text
    ]);



  for oPlace in aTask.Places do
  //  if oPlace.CalcResult.WGS.client_antenna.lat <> 0 then

  begin
    {
    if Eq(aTask.Technology,'���') then
      oPlace.ClientHWTypeID:='HW_RTN900_18G_XMC-3_ODU_Typical, ������� 0.6'
    else
      oPlace.ClientHWTypeID:='Cambium ePMP 1000 6GHz, ������� 0.6';
    }

 //     oPlace.Result.ZoneFileName

//assert( oPlace.CalcResult.WGS.client_antenna.lat <>0);

    dmMain.StoredProc_Exec(ADOStoredProc1, 'MegaGIS_client.sp_Task_place_UPD',
      [
        'id',                           oPlace.DB_ID,

        'result_msg',                   oPlace.GetResultTypeStr(), // TResultTypeStr[oPlace.CalcResult.ResultType],

        'result_client_antenna_lat',    oPlace.CalcResult.WGS.client_antenna.lat,
        'result_client_antenna_lon',    oPlace.CalcResult.WGS.client_antenna.lon,

        'result_client_antenna_height', oPlace.CalcResult.client_antenna_height,

        'result_ClientHWTypeID',        oPlace.ClientHWTypeID,

        'result_profile_image',         oPlace.CalcResult.FileName_Profile,
        'result_map_image',             oPlace.CalcResult.FileName_Map,
        'result_zone_image',            oPlace.CalcResult.FileName_Zone,

        'comment',                      oPlace.Log.Text
      ]);

  end;


end;

//-------------------------------------------------------------------
procedure TdmTask.SendToQueue(aTaskID :integer);
//-------------------------------------------------------------------
var
  k: Integer;
begin
  k:=dmMain.StoredProc_Exec(ADOStoredProc1, 'MegaGIS_client.sp_Task_Send_to_Queue_INS',
    [
      'id',  aTaskID
    ]);

  Assert (k>0);
end;

//-------------------------------------------------------------------
procedure TdmTask.Clear_All_Dlg;
//-------------------------------------------------------------------
var
  k: Integer;
begin
  if ConfirmDlg('Delete ?') then
    k:=dmMain.StoredProc_Exec(ADOStoredProc1, 'MegaGIS_client.sp_Task_Clear', []);

//  Assert (k>0);
end;



end.




//GetCurrentProcessId
