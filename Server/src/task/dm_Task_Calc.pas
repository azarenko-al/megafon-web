﻿unit dm_Task_Calc;

interface

uses
  u_log_,
  u_run,

  u_Task_classes,

  dm_Main,
  dm_Task,
  dm_Map,
  dm_Relief,

  fr_Profile_Rel_Graph,
  d_Map_print,

  u_Config,

  u_geo,
  u_LOgger,

  u_rel_engine,

  I_rel_Matrix1,
  u_rel_profile,


  u_profile_types,
  u_rel_Profile_tools,

  StrUtils, System.Generics.Defaults, System.DateUtils,  CodeSiteLogging,
  IOUtils, DB,  Forms, System.SysUtils, System.Classes, Data.Win.ADODB
  ;

//const
//  DEF_Freq_GHz = 6;


type
  TdmTask_calc = class(TDataModule)
    ADOStoredProc1: TADOStoredProc;
    sp_House_points_By_LatLon: TADOStoredProc;
    procedure DataModuleDestroy(Sender: TObject);
    procedure DataModuleCreate(Sender: TObject);

  private

    FInProcess : boolean;

    Fdlg_Map_print: Tdlg_Map_print;

    FTask: TTask;

    FTempDir: string;

    FProfile: TrelProfile;

    function Analisys_Profile_ck42(aTaskPlace: TTaskPlace; aClient_lat,
        aClient_lon: double): Boolean;

    function Analysis_Client: Boolean;
    function Analysis_Megafon_place(aTaskPlace: TTaskPlace): Boolean;
//    procedure MakeMapImagePNG(aTaskPlace: TTaskPlace; aFileName: string);
    procedure MakeProfileImagePNG(aTaskPlace: TTaskPlace; aFileName: string);

    procedure Exec_Task(aTask_ID: Integer);
    procedure Exec_TaskPlace(aTaskPlace: TTaskPlace; aIndex: word);


  public
    Terminated : boolean;

    procedure Calc(aTask: TTask);
    function Calc_XML(aXML: string): TTask;

    class procedure Init;
//    procedure ResultSave;

 //   property OnLog: TLogEvent read FOnLog write FOnLog;

  end;



var
  dmTask_calc: TdmTask_calc;

implementation

{%CLASSGROUP 'Vcl.Controls.TControl'}


{$R *.dfm}

class procedure TdmTask_calc.Init;
begin
  if not Assigned(dmTask_calc) then
    Application.CreateForm(TdmTask_calc, dmTask_calc);

end;

//-------------------------------------------------------------------
procedure TdmTask_calc.DataModuleCreate(Sender: TObject);
//-------------------------------------------------------------------
begin
  //FLog:=TStringList.Create;

  FProfile:=TrelProfile.Create;


  Fdlg_Map_print:=Tdlg_Map_print.Create(nil);
//  Fdlg_Map_print.Panel1.Visible:=False;

//  Fdlg_Map_print.SendToBack;

  Fdlg_Map_print.Show;


//  Fdlg_Map_print.Hide;

//  Fdlg_Map_print.BringToFront;


end;

//-------------------------------------------------------------------
procedure TdmTask_calc.DataModuleDestroy(Sender: TObject);
//-------------------------------------------------------------------
begin

  FreeAndNil(Fdlg_Map_print);
  FreeAndNil(FProfile);

end;



// ---------------------------------------------------------------
procedure TdmTask_calc.Exec_Task(aTask_ID: Integer);
// ---------------------------------------------------------------
var

//  b: Boolean;
//  h: Double;
 // rec_profile: TBuildProfileParamRec;

  s: string;
  sFileName: string;
//  sProfileXML: string;

 // sXml_FileName: string;

 //  rec: Trel_Record;

//   profileType: TrelProfileType;
  I: Integer;
  iGood_calcs: word;
  j: Integer;
  oBat: TStringList;
  sBat_file: string;
  sFiles: string;

label
  exit_;

begin
  Assert (FTask.Places.Count>0, 'FTask.Places.Count>0');


  FTempDir:= g_Config.GetRequestDir_ByGUID (FTask.GUID_str);  // aTask_ID
  Assert (FTempDir<>'');

  ForceDirectories (FTempDir);

//  dmMain.OpenStoredProc ( ADOStoredProc, 'MegaGIS_client.sp_Task_SEL', ['id', aTask_ID]);
//
//  Assert(ADOStoredProc.Active, 'ADOQuery1.Active');
//  Assert(ADOStoredProc.RecordCount=1, 'ADOQuery1.RecordCount>0');
//
//  FillChar(FTask_Rec, SizeOf(FTask_Rec), 0);
//
//  FTask_Rec.LoadFromDataset(ADOStoredProc);


  Assert ( Assigned (dmRelief));

//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
//  for I := 0 to 100 do
//    dmRelief.OpenByTask_(aTask_ID); //, FRelEngine);

  if not g_Config.TechnologyAllowed(FTask.Technology) then
  begin
    FTask.CalcResult.ResultType :=rtNone;
    FTask.CalcResult.Description:='Технология не поддерживается';
    FTask.Log.Add('Технология не поддерживается');

    Exit;
  end;

  if not dmRelief.OpenByTask(aTask_ID) then
  begin
    FTask.CalcResult.ResultType :=rtNone;
    FTask.CalcResult.Description:='Не найдены матрицы высот';

    FTask.Log.Add('Не найдены матрицы высот');

    Exit;
  end;

  //..  Assert(dmRelief.RelEngine.Count >0, 'FRelEngine.Count >0');


//  db_OpenQuery(qry_WebRequest,
//    'select * from %s where id=%d', [TBL_WebRequest, aTask_ID]);
  //  Format('select * from %s where id=%d', [TBL_WebRequest, aTask_ID]), []);

//  Assert(qry_WebRequest.RecordCount=1, 'qry_WebRequest.RecordCount>0');


//  Log('  oParams.LoadFromDataset(qry_WebRequest);');


//  oParams:=TProfileParams.Create;
//  oParams.LoadFromDataset(qry_WebRequest);

//  Log('  oParams.LoadFromDataset(qry_WebRequest); ok ');

  // ---------------------------------------------------------------
  //  1. анализ точки клиента
  // ---------------------------------------------------------------
  if not Analysis_Client() then
    Exit;

//
//  b:=g_RelEngine.FindPointBL (MakeBLPoint(FTask_Rec.client_lat, FTask_Rec.client_lon), rec);
//
//  if b then begin
//              FTask_Rec.Analisys.Client.clutter_height:=rec.Clutter_H;
//              FTask_Rec.Analisys.Client.clutter_code:=rec.Clutter_Code;
//
//            end;



  // ---------------------------------------------------------------
  //  1. анализ станции Megafon
  // ---------------------------------------------------------------

//  FTask.Places.SaveToXMLFile ('d:\1.xml') ;


  iGood_calcs:=0;

  for I := 0 to FTask.Places.Count-1 do
  begin
    g_Logger.Progress(I, FTask.Places.Count);

    Exec_TaskPlace(FTask.Places[i], i);

    //-----------------------------------------------
    // iGood_calcs
    //-----------------------------------------------
    if FTask.Places[i].CalcResult.ResultType=rtPossible then
      Inc(iGood_calcs);

     Assert (g_Config.AutoCalc.Max_good_calcs > 0, 'AutoCalc.Max_good_calcs > 0');

    if iGood_calcs >= g_Config.AutoCalc.Max_good_calcs then
    begin
//      while FTask.Places.Count do

      for j:= i+1 to  FTask.Places.Count-1 do
        FTask.Places.Delete(i+1);

//        [j].
//        Runtime.Hidden:=True;

      Break;
    end;


    if Terminated then
      Exit;
  end;


//  FTask.Places.SaveToXMLFile ('d:\2.xml') ;


  FTask.Places.Sort_By_ResultCode_and_length_to_client;

//  FTask.Places.SaveToXMLFile ('d:\3.xml') ;


//  if aTaskPlace.CalcResult.ResultType=rtPossible then
 //   Fdlg_Map_print.TaskPlace_MakeMapImagePNG (aTaskPlace, FTempDir +  Format('%d_map.png',[aIndex] )); // 'map.png');

           //нет прямой радиовидимости

  FTask.CalcResult.ResultType :=rtNo;
  FTask.CalcResult.Description:='нет прямой радиовидимости';


  for I := 0 to FTask.Places.Count-1 do
  begin
//    sFiles:=Concat(sFiles, ' ',  ExtractFileName( FTask.Places[i].CalcResult.FileName_Profile));

    if FTask.Places[i].CalcResult.ResultType=rtPossible then
    begin
      FTask.CalcResult.ResultType:=rtPossible;
      FTask.CalcResult.Description:='';

      Break;
    end;
  end;


//  sFiles:='';
//  for I := 0 to FTask.Places.Count-1 do
//    sFiles:=Concat(sFiles, ' ',  ExtractFileName( FTask.Places[i].CalcResult.FileName_Profile));


//  [GraphicsMagick]
//;enabled=true
//path=C:\Program Files\GraphicsMagick-1.3.36-Q16
//


//  MakeProfileImagePNG (aTaskPlace, FTempDir +  Format('%d_Profile.png',[aIndex]) );//  'profile.png');

//  if aTaskPlace.CalcResult.ResultType=rtPossible then

  if not Fdlg_Map_print.Task_MakeMapImagePNG (FTask, FTempDir +  'zone.png' ) then  // 'map.png');
    Exit;

//end;
  //-----------------------------------------------------------------
  // join maps
  //-----------------------------------------------------------------
  if g_Config.GraphicsMagick.Enabled then
  begin
    sFiles:='';
    for I := 0 to FTask.Places.Count-1 do
      if FileExists(FTask.Places[i].CalcResult.FileName_Profile) then
        sFiles:=Concat(sFiles, ' ',  ExtractFileName( FTask.Places[i].CalcResult.FileName_Profile));


    oBat:=TStringList.Create;

    Assert(g_Config.GraphicsMagick.Path <> '');

    oBat.Add ('set path='+ g_Config.GraphicsMagick.Path);
    oBat.Add ('');

   s:=IncludeTrailingBackslash ( g_Config.GraphicsMagick.Path );

//   QuotedStr()

//    oBat.Add (IncludeTrailingBackslash ( g_Config.GraphicsMagick.Path ) +
    oBat.Add (Format('gm convert zone.png %s -append zone_join.png', [sFiles] ));

    sBat_file:=FTempDir +  '_join_image.bat';
    oBat.SaveToFile(sBat_file);

    FreeAndNil(oBat);

    //-----------------------------------------------------------------
    SetCurrentDir(FTempDir);
    RunApp (sBat_file,'');

  //  Assert(FileExists(FTempDir +  'zone_join.png'), FTempDir +  'zone_join.png');

    if not FileExists(FTempDir + 'zone_join.png') then
      Exit;


    if FTask.Places.Count>0 then
      FTask.Places[0].CalcResult.FileName_Zone :=FTempDir +  'zone_join.png';

  end;

  exit_:

end;


// ---------------------------------------------------------------
function TdmTask_calc.Analisys_Profile_ck42(aTaskPlace: TTaskPlace;
    aClient_lat, aClient_lon: double): Boolean;
// ---------------------------------------------------------------
const
  DEF_ANTENNA_OFFSET_M  = 1 ;

var

  b: Boolean;
  eFreq_GHz: Double;
  h: Double;
  rec_profile: TBuildProfileParamRec;

  s: string;
  sFileName: string;
  sProfileXML: string;

  sXml_FileName: string;

  rec: Trel_Record;

  profileType: TrelProfileType;
  I: Integer;

begin
  Assert(aClient_lat <> 0);


  aTaskPlace.CalcResult.client_antenna_height:= FTask.client_antenna_height;

  // ---------------------------------------------------------------
  //  BuildProfileBL
  // ---------------------------------------------------------------

  FillChar(rec_profile,SizeOf(rec_profile),0);

//  aRec.Lat1, aRec.Lon1, aRec.Lat2, aRec.Lon2,

  rec_profile.BLVector   :=
     MakeBLVector_LatLon ([
//         FTask.CK42.client_lat, FTask.CK42.client_lon,

         aClient_lat, aClient_lon,
         aTaskPlace.Pos_CK42.lat, aTaskPlace.Pos_CK42.lon
         ]);



  rec_profile.Step_m     := dmRelief.RelEngine.GetStepMin(); // oParams.Step_m; //50; //aRec.Step_m;
  rec_profile.Refraction := 1.33; //aProfile.Refraction;

 // CodeSite.Send( 'rec_profile.Step_m - ' + IntToStr(rec_profile.Step_m) );

//  FProfile.Refraction:=

  Assert(dmRelief.RelEngine.Count >0);

  //  Log('  b:=GetReliefEngine.BuildProfileBL (FProfile.Data,  rec_profile,;');

//  dmTask_Relief.OpenByVector(rec_profile.BLVector);

  b:=dmRelief.RelEngine.BuildProfileBL (FProfile.Data,  rec_profile,
            rec_profile.BLVector,
            rec_profile.Step_m,
            rec_profile.Refraction
           // 0
            );

  if not b then
  begin
    aTaskPlace.CalcResult.ResultType:=rtNone;
    aTaskPlace.CalcResult.Description:='Нет данных для построения профиля';
    Exit(false);

//    FTask_Rec.Result.Comment
  end;

//    Log('  b:=GetReliefEngine.BuildProfileBL (FProfile.Data,  rec_profile,; done');

//  if aTaskPlace.height=0 then
//    aTaskPlace.height:=20;

  //!!!!!!!!!!!!!!!!!!!!!!!!
//  if FTask.client_antenna_height=0 then
//    FTask.client_antenna_height:=10;

//  Assert(FTask.client_antenna_height > 0);

  if aTaskPlace.Freq_MHz > 0 then
    eFreq_GHz:=aTaskPlace.Freq_MHz / 1000
  else
    eFreq_GHz:=g_Config.Freq_GHz;


  profileType:= TRelProfile_tools.GetType1(FProfile,
                    FTask.client_antenna_height + DEF_ANTENNA_OFFSET_M,
                    aTaskPlace.height + DEF_ANTENNA_OFFSET_M,
                    eFreq_GHz );  //aFreq_GHz: double


  if profileType = prtOpen then
  begin
    aTaskPlace.CalcResult.ResultType:=rtPossible;// ProfileClosed;
//    aTaskPlace.CalcResult.client_antenna_height:= FTask.client_antenna_height;
    aTaskPlace.CalcResult.Description:='Профиль открыт';

    Exit(True);
  end else
    aTaskPlace.CalcResult.ResultType:=rtNo;// ProfileClosed;

//  if profileType <> prtOpen then
//  begin

  if not FTask.is_can_change_antenna_height then
  begin
    aTaskPlace.CalcResult.ResultType:=rtNo;// ProfileClosed;
 //   aTaskPlace.CalcResult.client_antenna_height:= FTask.client_antenna_height;
    aTaskPlace.CalcResult.Description:='Профиль закрыт';

    aTaskPlace.Log.Add('Профиль закрыт');

    Exit(False);
  end;


  if FTask.is_can_change_antenna_height then
  begin
     aTaskPlace.Log.Add('меняем высоту антенны...');

     for I := 1 to g_Config.Client.Iteration_count do
     begin
        h:= i * g_Config.Client.Iteration_antenna_h;// FTask.client_antenna_height;

        profileType:= TRelProfile_tools.GetType1(FProfile,
                          FTask.client_antenna_height + h + DEF_ANTENNA_OFFSET_M,
                          aTaskPlace.height + DEF_ANTENNA_OFFSET_M,
                          eFreq_GHz );

        if profileType = prtOpen then
        Begin
          aTaskPlace.CalcResult.client_antenna_height:=FTask.client_antenna_height + h;

          aTaskPlace.CalcResult.ResultType:=rtPossible;// ProfileClosed;
          aTaskPlace.CalcResult.Description:='Профиль открыт';

          Exit(True);
        end;
     end;


     aTaskPlace.CalcResult.Description:='Профиль закрыт на максимальной высоте антенны клиента после итераций.';
     aTaskPlace.Log.Add('Профиль закрыт на максимальной высоте антенны клиента после итераций.');

  end;


  result:=True;

//    Exit;

//    Iteration_count : Integer;
//    Iteration_antenna_h : double;


//    FTask_Rec.Result.Comment
//  end;

end;

// ---------------------------------------------------------------
procedure TdmTask_calc.Exec_TaskPlace(aTaskPlace: TTaskPlace; aIndex: word);
// ---------------------------------------------------------------
var
  b: Boolean;
  h: Double;

  s: string;
  sFileName: string;

  I: Integer;
  eMinH: double;

  iInd: Integer;

begin
  TLog.Send ('TdmTask_calc.Exec_TaskPlace');

  // ---------------------------------------------------------------
  //  1. анализ станции Megafon
  // ---------------------------------------------------------------
  if not Analysis_Megafon_place(aTaskPlace) then
  begin
//    Analysis_Megafon_place

//Если в представлении NRI отсутствует информация о высоте подвеса,
//а также в матрице высот нет информации о высоте здания,
//то МР БШПД должен отвечать МегаГИС «расчет невозможен».

    aTaskPlace.CalcResult.ResultType:=rtNone;
    aTaskPlace.CalcResult.Description:='нет информации о высоте подвеса и высоте здания';

    aTaskPlace.CalcResult.WGS.client_antenna.Lat:=FTask.WGS.client_lat;
    aTaskPlace.CalcResult.WGS.client_antenna.Lon:=FTask.WGS.client_lon;


    Exit;
  end;


  aTaskPlace.CalcResult.CK42.client_antenna.lat:=FTask.CK42.client_lat;
  aTaskPlace.CalcResult.CK42.client_antenna.lon:=FTask.CK42.client_lon;

//  FTask.CalcResult.ResultType :=aTaskPlace.CalcResult.ResultType;
//  FTask.CalcResult.Description:=aTaskPlace.CalcResult.Description;



  // ---------------------------------------------------------------
  //  проверить основное положение клиента
  // ---------------------------------------------------------------

  b:=Analisys_Profile_ck42(aTaskPlace, FTask.CK42.client_lat, FTask.CK42.client_lon);

//  if not FTask.is_can_change_antenna_location then
//    Analisys_Profile_ck42(aTaskPlace, FTask.CK42.client_lat, FTask.CK42.client_lon)

  //else
  if not b and FTask.is_can_change_antenna_location then
  begin
    iInd:=-1;
    eMinH:=-1;

    for I := 0 to FTask.Client.HousePoints.Count-1 do
    begin
      Analisys_Profile_ck42(aTaskPlace,
        FTask.Client.HousePoints[i].CK42.Lat,
        FTask.Client.HousePoints[i].CK42.Lon );

      if aTaskPlace.CalcResult.ResultType=rtPossible then
        if (eMinH<0) or (aTaskPlace.CalcResult.client_antenna_height < eMinH) then
        begin
          iInd:=i;

//          aTaskPlace.CalcResult.CK42.client_antenna_lat:=FTask.Client_HousePoints[i].CK42.Lat;
//          aTaskPlace.CalcResult.CK42.client_antenna_lon:=FTask.Client_HousePoints[i].CK42.Lon;

          eMinH:=aTaskPlace.CalcResult.client_antenna_height;
        end;


//      FTask.CK42.client_lat, FTask.CK42.client_lon);


    end;

    if (iInd>=0) then
    begin
      aTaskPlace.CalcResult.CK42.client_antenna.lat:=FTask.Client.HousePoints[iInd].CK42.Lat;
      aTaskPlace.CalcResult.CK42.client_antenna.lon:=FTask.Client.HousePoints[iInd].CK42.Lon;

      aTaskPlace.CalcResult.client_antenna_height:=eMinH;

      Analisys_Profile_ck42(aTaskPlace,
        aTaskPlace.CalcResult.CK42.client_antenna.lat,
        aTaskPlace.CalcResult.CK42.client_antenna.lon );

    end;


  end;
//      aClient_lon: double): Boolean;

  //-----------------------------------------------------
  // WiMax
  //-----------------------------------------------------
  if aTaskPlace.WM_Is_WiMax then
  begin
    aTaskPlace.Log.Add('WiMax Azimuth='+ aTaskPlace.WM_Azimuth.ToString);

    if aTaskPlace.CalcResult.ResultType=rtPossible then
      if not aTaskPlace.WM_Is_Ready then
      begin
        aTaskPlace.Log.Add('WiMax sector не соответствует.');
        aTaskPlace.CalcResult.ResultType:=rtNo;
      end else
       aTaskPlace.ClientHWTypeID:='WiMax Azimuth=' + aTaskPlace.WM_Azimuth.ToString;


  end;


//        oTaskPlace.WM_Is_WiMax    :=Eq(aTask.Technology,'WiMax') and (FieldByName('WM_Azimuth').Value <> null);
//      oTaskPlace.WM_Azimuth     :=FieldByName('WM_Azimuth').AsFloat;;
//      oTaskPlace.WM_Is_Ready    :=FieldByName('WM_Is_Ready').AsBoolean;


//   if aTaskPlace.CalcResult.ResultType=rtPossible then



  aTaskPlace.Update_CalcResult_CK42_to_WGS;

//    Fdlg_Map_print.DrawLine(FTask.WGS.client_lat, FTask.WGS.client_lon,
//                   aTaskPlace.WGS.lat, aTaskPlace.WGS.lon ,
//                   aTaskPlace.place_name,

 //  CodeSite.Send( '!!!!!!!!!!!!!!!!!!!!!!!!-' );
//  for I := 0 to 1000 do

    MakeProfileImagePNG (aTaskPlace, FTempDir +  Format('%d_Profile.png',[aIndex]) );//  'profile.png');




//  if aTaskPlace.CalcResult.ResultType=rtPossible then
 //   for I := 0 to 1000 do
   if g_Config.is_make_map_images then
     Fdlg_Map_print.TaskPlace_MakeMapImagePNG (aTaskPlace, FTempDir +  Format('%d_map.png',[aIndex] )); // 'map.png');


end;


//-------------------------------------------------------------------
function TdmTask_calc.Calc_XML(aXML: string): TTask;
//-------------------------------------------------------------------
var
  dt_Start: TDateTime;
  iTaskID: Integer;
  iTask_ID: Integer;
//  k: Integer;

begin
  Assert(Assigned(dmTask));
  Assert(Assigned(dmMap));
  Assert(Assigned(dmRelief));


  dmMain.Config_Load_;


  Assert (not FInProcess, 'not FInProcess');

  try

    Result:=TTask.Create;

    FInProcess:=True;

  //  Assert (aXML<>'');
    FTask:=Result;

    dt_Start:=Now;
    CodeSite.Send( '------------------------------------------------' );
    CodeSite.Send( 'function TdmTask_profile.Calc...' );

  //  dmMain.ADOConnection1.Close;

    iTaskID:= dmTask.AddFromXML(aXML, FTask);

    FTask.Update_WGS_to_CK42_;

    Exec_Task(iTaskID);

    dmTask.SaveResultToDB(FTask, iTaskID);

  //  FreeAndNil(FTask);

    CodeSite.Send( 'function TdmTask_profile.Calc  done ' +
                   intToStr( MilliSecondsBetween(dt_start, Now)) + ' msec, '+
                   intToStr( SecondsBetween(dt_start, Now)) + ' sec');

    CodeSite.Send( '========================================================' );


  except on E: Exception do
    g_Logger.Error(e.Message);
  end;

  FInProcess:=False;

end;

//-------------------------------------------------------------------
procedure TdmTask_calc.Calc(aTask: TTask);
//-------------------------------------------------------------------
var
  dt_Start: TDateTime;
  iTaskID: Integer;
//  iTask_ID: Integer;
 // k: Integer;
begin
 // Assert (not FInProcess);

//  FInProcess:=true;

//  Assert (aXML<>'');
  FTask:=aTask;

  dt_Start:=Now;
  CodeSite.Send( '------------------------------------------------' );
  CodeSite.Send( 'function TdmTask_profile.Calc...' );

//  dmMain.ADOConnection1.Close;

  iTaskID:= dmTask.AddToDB(aTask);

  aTask.Update_WGS_to_CK42_;

  Exec_Task(iTaskID);
  dmRelief.Close;

  dmTask.SaveResultToDB(FTask, iTaskID);


  FTask:=nil;

  CodeSite.Send( 'function TdmTask_profile.Calc  done ' +
                 intToStr( MilliSecondsBetween(dt_start, Now)) + ' msec, '+
                 intToStr( SecondsBetween(dt_start, Now)) + ' sec');

  CodeSite.Send( '========================================================' );

 // FInProcess:=False;

//  Result:= intToStr( SecondsBetween(LTime_dt, Now)) + ' sec'

end;

//-------------------------------------------------------------------
function TdmTask_calc.Analysis_Client: Boolean;
//-------------------------------------------------------------------
// анализ точки клиента
var
  b: Boolean;
  k: Integer;
  rec: Trel_Record;

  rClutter_LOgic: TConfig_Clutter_Logic_rec;
begin
  Assert(Assigned(FTask));

  b:=dmRelief.RelEngine.FindPointBL (MakeBLPoint(FTask.ck42.client_lat, FTask.ck42.client_lon), rec);

  if not b then
  begin
    FTask.CalcResult.ResultType :=rtNone;

    FTask.CalcResult.Description:='Нет данных по рельефу в точке клиента';
    FTask.Log.Add('Нет данных по рельефу в точке клиента');

    exit(False);
  end;


  //---------------------------------------------------------
  // Проверка на дом
  //---------------------------------------------------------

  k:=dmMap.House_points_By_LatLon(sp_House_points_By_LatLon,
         FTask.wgs.client_lat, FTask.wgs.client_lon);

//////////////////!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  //------------------------------------------------
  // Анализ векторной карты
  //------------------------------------------------
  FTask.Client.HousePoints.LoadFromDataset(sp_House_points_By_LatLon);
  if FTask.Client.HousePoints.Count>0 then
    FTask.Client.map_building_height:=FTask.Client.HousePoints[0].Height;


  if FTask.client_antenna_height=0 then
    if FTask.Client.HousePoints.Count>0 then
    begin
      FTask.client_antenna_height:=FTask.Client.HousePoints[0].Height + g_Config.Client.AMC_height;// 1.5;

      FTask.Log.Add (Format('client_antenna_height=0 => height= %1.1f',  [FTask.client_antenna_height]));

    end;



//    FTask.Analisys.Client.clutter_height:=rec.Clutter_H;
//    FTask.Analisys.Client.clutter_code  :=rec.Clutter_Code;


  FTask.Log.Add (Format('Client: rel_height: %d',    [rec.rel_H]));
  FTask.Log.Add (Format('Client: clutter_height: %d',[rec.Clutter_H]));
  FTask.Log.Add (Format('Client: Clutter_Code: %d',  [rec.Clutter_Code]));

  if g_Config.ClutterLogic.TryGetValue(rec.Clutter_Code, rClutter_LOgic) then
  begin

    if rClutter_LOgic.is_terminate_calc then
    begin
      FTask.CalcResult.ResultType :=rtNone;
      FTask.CalcResult.Description:='Расчет невозможен - неверный тип местности';
      FTask.Log.Add('Расчет невозможен - неверный тип местности');

      exit(False);
    end;


    if FTask.client_antenna_height=0 then
    begin
      FTask.client_antenna_height:=rec.Clutter_H + rClutter_LOgic.height;
      FTask.Log.Add(Format('client_antenna_height=0  => height= %1.1f',[FTask.client_antenna_height]) );

    end;

   // rClutter_LOgic

  end;
//
//
//  const
//  DEF_CLU_OPEN_AREA = 0;  // 1- Лес (1)
//  DEF_CLU_FOREST    = 1;  // 1- Лес (1)
//  DEF_CLU_WATER     = 2;  // 3- ПГТ
//  DEF_CLU_COUNTRY   = 3;  // 3- ПГТ
//  DEF_CLU_ROAD      = 4;
//  DEF_CLU_RailROAD  = 5;
//  DEF_CLU_CITY      = 7;  // 7- Город
//  DEF_CLU_BOLOTO    = 8; // 31- ПСТ
//  DEF_CLU_ONE_BUILD = 73; // 73- Отд.дома
//  DEF_CLU_ONE_HOUSE = 31; // 31- ПСТ
//


//
//    function TConfig_Clutter_Logic_Dictionary.GetByCode(aCode: byte): TConfig_Clutter_Logic;
//begin
//  TryGetValue(aCode, Result); // = True);
//
//end;

 // if FTask.is_can_change_antenna_location then
 // begin

//  end;


  Result:=True;

  //!!!!!!!!!!!!!!!!!!!!!!!!
//.........
  if FTask.client_antenna_height=0 then
    FTask.client_antenna_height:=g_Config.Client.AMC_height;

end;


//-------------------------------------------------------------------
function TdmTask_calc.Analysis_Megafon_place(aTaskPlace: TTaskPlace): Boolean;
//-------------------------------------------------------------------
// анализ точки клиента
var
  b: Boolean;
  rec: Trel_Record;
begin
//Если в представлении NRI отсутствует информация о высоте подвеса,
//а также в матрице высот нет информации о высоте здания,
//то МР БШПД должен отвечать МегаГИС «расчет невозможен».
  Result:=True;


  if (aTaskPlace.Height=0) and (aTaskPlace.map_building_height>0)
  then begin
     //aTaskPlace.Height:=aTaskPlace.map_building_height;// 1.5;
     aTaskPlace.Height:=aTaskPlace.map_building_height + g_Config.Place.AMC_height;// 1.5;
//     aTaskPlace.Height:=aTaskPlace.map_building_height + g_Config.Client.AMC_height;// 1.5;
     aTaskPlace.Log.Add ('Megafon_place: высота=0  -> установлена высота здания + AMC');
   end;


  b:=dmRelief.RelEngine.FindPointBL (MakeBLPoint(aTaskPlace.Pos_CK42.lat, aTaskPlace.Pos_CK42.lon), rec);

  if b then
  begin
    aTaskPlace.Log.Add (Format('Megafon_place: clutter_height: %d',[rec.Clutter_H]));
    aTaskPlace.Log.Add (Format('Megafon_place: Clutter_Code: %d',  [rec.Clutter_Code]));


    if (aTaskPlace.Height=0) and (rec.Clutter_H>0) then
    begin
      aTaskPlace.Height:=rec.Clutter_H + g_Config.Place.AMC_height; // 1.5;
      aTaskPlace.Log.Add ('высота=0  -> установлена высота из матрицы высот + AMC');

    end;


    if (aTaskPlace.Height>0) and (rec.Clutter_H > aTaskPlace.Height) then
    begin
      aTaskPlace.Height:=rec.Clutter_H + g_Config.Place.AMC_height; // 1.5;
      aTaskPlace.Log.Add ('высота c матрицы > Height');

    end;

//    FTask_Rec.Analisys.Client.clutter_height:=rec.Clutter_H;
//    FTask_Rec.Analisys.Client.clutter_code:=rec.Clutter_Code;

  end;

  {
  if aTaskPlace.Height = 0 then
  begin
    aTaskPlace.Height:=g_Config.Place.AMC_height; // 1.5;
    aTaskPlace.Log.Add ('Place.Height = 0 -> Берем высоту (AMC)');
  end;
  }

  if aTaskPlace.Height = 0 then
  begin
    Result:=False;

//    aTaskPlace.Height:=g_Config.Place.AMC_height; // 1.5;
    aTaskPlace.Log.Add ('нет информации о высоте подвеса и высоте здания');

    aTaskPlace.CalcResult.ResultType:=rtNone;
    aTaskPlace.CalcResult.Description:='нет информации о высоте подвеса и высоте здания';
  end;

//Если в представлении NRI отсутствует информация о высоте подвеса,
//а также в матрице высот нет информации о высоте здания,
//то МР БШПД должен отвечать МегаГИС «расчет невозможен».


end;


// ---------------------------------------------------------------
procedure TdmTask_calc.MakeProfileImagePNG(aTaskPlace: TTaskPlace; aFileName:
    string);
// ---------------------------------------------------------------
var
  oForm: Tframe_Profile_Rel_Graph;
  i: Integer;
  dt_start: TDateTime;
begin
  dt_start:=Now;
  CodeSite.Send( 'procedure TdmTask_profile.MakeProfileImagePNG...' );


  oForm:=Tframe_Profile_Rel_Graph.Create(nil);

 // oForm.SetChartSize (550,400);

//  oForm.Show;
  oForm.BringToFront;

//  oForm.SetChartSize(g_Config.IMAGE_PROFILE_WIDTH, g_Config.IMAGE_PROFILE_HEIGHT);// 550;

  oForm.Width :=g_Config.IMAGE_PROFILE_WIDTH;// 550;
  oForm.Height:=g_Config.IMAGE_PROFILE_HEIGHT;//400;

//  oForm.Height:=g_Config.IMAGE_PROFILE_HEIGHT;//400;

  oForm.RelProfile_Ref:=FProfile;


//  oForm.Params.SetSiteGroupCount(1);
  if aTaskPlace.Freq_MHz > 0 then
    oForm.Params.Freq_MHz:=aTaskPlace.Freq_MHz
  else
    oForm.Params.Freq_MHz:=g_Config.Freq_GHz * 1000;

//  oForm.Params.Freq_MHz:=aRec.freq_GHz  * 1000;

  oForm.Params.Distance_KM:=FProfile.Data.Distance_KM;

//  oForm.Params.Place_Name:=aTaskPlace.place_name + Format(' %1.1f', [oTaskPlace.Azimuth_Place_to_Client]);
//  oForm.Params.Place_Name:=aTaskPlace.place_name_display + Format(' %1.1f', [oTaskPlace.Azimuth_Place_to_Client])
//
//  oForm.Params.Place_Name:=aTaskPlace.place_name;
  oForm.Params.Place_Name :=aTaskPlace.place_name_display + Format(' (%1.1f°)', [aTaskPlace.Azimuth_Place_to_Client]);
  oForm.Params.Client_Name:='Клиент'                      + Format(' (%1.1f°)', [aTaskPlace.Azimuth_Client_to_Place]);

//      oTaskPlace.Azimuth_Place_to_Client:=
//        geo_Azimuth (MakeBLPoint (oTaskPlace.Pos_wgs.Lat, oTaskPlace.Pos_wgs.Lon),
//                     MakeBLPoint (aTask.wgs.client_lat, aTask.wgs.client_lon));
//
//    oTaskPlace.Azimuth_Client_to_Place:=
//        geo_Azimuth (MakeBLPoint (aTask.wgs.client_lat, aTask.wgs.client_lon),



  //МР БШПД. Наличие номера и названия БС на растровом файле (п.3.3 Технических требований).
  //Отображать буквенное название БС на картинке с профилем интервала

  //RelProfile.GeoDistance_KM();

//  iDirection:=ADOStoredProc_Link.FieldByName(FLD_CALC_DIRECTION).AsInteger;
//
//  Assert (aTaskPlace.CalcResult.client_antenna_height > 0, 'aTaskPlace.CalcResult.client_antenna_height>0');

  if aTaskPlace.CalcResult.client_antenna_height=0 then
    aTaskPlace.CalcResult.client_antenna_height:=1;

  oForm.Params.Site1.AntennaHeight:= aTaskPlace.CalcResult.client_antenna_height;
  oForm.Params.Site1.HouseHeight  := FTask.Client.map_building_height; // aTaskPlace.map_building_height;

//  oForm.Params.SiteGroups[0].Site1.AntennaHeights:= MakeDoubleArray([FTask.client_antenna_height]);
  oForm.Params.Site2.AntennaHeight:= aTaskPlace.height;
  oForm.Params.Site2.HouseHeight  := aTaskPlace.map_building_height;


  oForm.Params.Site1.Rel_H:=FProfile.FirstItem.Rel_H;
  oForm.Params.Site2.Rel_H:=FProfile.LastItem.Rel_H;


 // oForm.Params.SiteGroups[0].ReflectionPointArr

  oForm.ShowGraph;

//  oForm.ShowModal;

//  oForm.DrawHeader;

//  Delay(500);

//.........
  { TODO : speed }
  ForceDirectories( ExtractFilePath (aFileName) );

  oForm.SaveToPNG(aFileName);

  Assert (FileExists (aFileName));

  //ConvertFileToBase64(aFileName, ChangeFileExt(aFileName, '.Base64'));

//  ShellExec(aFileName); //aFileName: string; aFileParams: string = '');

  FreeAndNil(oForm);

  aTaskPlace.CalcResult.FileName_Profile:=aFileName;

  CodeSite.Send( 'procedure TdmTask_profile.MakeProfileImagePNG done - ' +
                 intToStr( MilliSecondsBetween(dt_start, Now)) + ' msec, ' +
                 intToStr( SecondsBetween(dt_start, Now)) + ' sec');


end;



end.



(*
{
{ "type": "FeatureCollection",   "features": [{},{  "type": "Feature",
            "properties": {
                "type": "line"
            },
            "geometry": {
                "type": "LineString",
                "coordinates": [[ 49.1451072693, 55.7799519839],[ 49.153056, 55.773333]]
            }
        },{  "type": "Feature",
            "properties": {
                "type": "line"
            },
            "geometry": {
                "type": "LineString",
                "coordinates": [[ 49.1451072693, 55.7799519839],[ 49.154444, 55.790833]]
            }
        },{  "type": "Feature",
            "properties": {
                "type": "line"
            },
            "geometry": {
                "type": "LineString",
                "coordinates": [[ 49.1451072693, 55.7799519839],[ 49.15861111, 55.75916667]]
            }
        },{  "type": "Feature",
            "properties": {
                "type": "line"
            },
            "geometry": {
                "type": "LineString",
                "coordinates": [[ 49.1451072693, 55.7799519839],[ 49.112222, 55.800556]]
            }
        },{  "type": "Feature",
            "properties": {
                "type": "line"
            },
            "geometry": {
                "type": "LineString",
                "coordinates": [[ 49.1451072693, 55.7799519839],[ 49.111389, 55.796667]]
            }
        },{  "type": "Feature",
            "properties": {
                "type": "line"
            },
            "geometry": {
                "type": "LineString",
                "coordinates": [[ 49.1451072693, 55.7799519839],[ 49.10000138, 55.77110772]]
            }
        },{  "type": "Feature",
            "properties": {
                "type": "line"
            },
            "geometry": {
                "type": "LineString",
                "coordinates": [[ 49.1451072693, 55.7799519839],[ 49.036389, 55.835556]]
            }
        },{  "type": "Feature",
            "properties": {
                "type": "line"
            },
            "geometry": {
                "type": "LineString",
                "coordinates": [[ 49.1451072693, 55.7799519839],[ 49.10972222, 55.74305556]]
            }
        },{  "type": "Feature",
            "properties": {
                "type": "client",
                "name": "client",
                "height": "50"
            },
            "geometry": {
                "type": "Point",
                "coordinates": [ 49.1451072693, 55.7799519839 ]
            }
        },{  "type": "Feature",
            "properties": {
                "type": "site",
                "name": "16-1667",
                "height": "32"
            },
            "geometry": {
                "type": "Point",
                "coordinates": [ 49.153056, 55.773333 ]
            }
        },{  "type": "Feature",
            "properties": {
                "type": "site",
                "name": "16-4",
                "height": "27"
            },
            "geometry": {
                "type": "Point",
                "coordinates": [ 49.154444, 55.790833 ]
            }
        },{  "type": "Feature",
            "properties": {
                "type": "site",
                "name": "16-1037",
                "height": "30"
            },
            "geometry": {
                "type": "Point",
                "coordinates": [ 49.15861111, 55.75916667 ]
            }
        },{  "type": "Feature",
            "properties": {
                "type": "site",
                "name": "16-1261",
                "height": "41"
            },
            "geometry": {
                "type": "Point",
                "coordinates": [ 49.112222, 55.800556 ]
            }
        },{  "type": "Feature",
            "properties": {
                "type": "site",
                "name": "16-1432",
                "height": "38"
            },
            "geometry": {
                "type": "Point",
                "coordinates": [ 49.111389, 55.796667 ]
            }
        },{  "type": "Feature",
            "properties": {
                "type": "site",
                "name": "16-134",
                "height": "33"
            },
            "geometry": {
                "type": "Point",
                "coordinates": [ 49.10000138, 55.77110772 ]
            }
        },{  "type": "Feature",
            "properties": {
                "type": "site",
                "name": "16-2638",
                "height": "60"
            },
            "geometry": {
                "type": "Point",
                "coordinates": [ 49.036389, 55.835556 ]
            }
        },{  "type": "Feature",
            "properties": {
                "type": "site",
                "name": "16-1168",
                "height": "25"
            },
            "geometry": {
                "type": "Point",
                "coordinates": [ 49.10972222, 55.74305556 ]
            }
        }]}
