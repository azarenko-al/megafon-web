﻿unit u_Task_classes;

interface
uses
//  u_XmlSerializer,

//  ApiCalc_Intf,
  u_func,
  u_geo,

  u_geo_convert_new,

  u_logger,

  System.JSON.Builders,  System.JSON.Writers,
  System.Generics.Collections,  System.Generics.Defaults,  CodeSiteLogging,
  IOUtils, DB,  Forms, System.SysUtils, System.Classes
  ;

type
  TResultType = (rtNone, rtNo, rtPossible );

const
  DEF_ResultTypeStr : array[TResultType] of string =
     ('CalculationImpossible','ConnectionImpossible','ConnectionPossible');

  DEF_ResultType_COLOR : array[TResultType] of string =
     ('blue','red','green');

  ResultTypeJSONStr : array[TResultType] of string =
     ('','closed','open');

type
  TTaskPlace = class;
  TTask = class;


  //---------------------------------------
  THousePoint = record
  //---------------------------------------
    WGS: TLatLon;
    CK42: TLatLon;
    Height: integer;
  end;

  THousePointList = class( TList<THousePoint>)
  public
    procedure LoadFromDataset(aDataset: DB.TDataset);
  end;


  TTaskPlaceList = class( TObjectList<TTaskPlace>)
  private
  public
    function AddItem: TTaskPlace;
//    procedure SaveToXMLFile(aFileName: string);
    procedure Sort_By_length_to_client;
    procedure Sort_By_ResultCode_and_length_to_client;
  end;


  // ---------------------------------------------------------------
  TTask = class
  // ---------------------------------------------------------------

  public
    DateCreated   : TDateTime;
    DateCalcStart : TDateTime;

    DB_ID: integer;

    WGS: record
           client_lat: Double;
           client_Lon: Double;

           client_antenna_lat : Double ;
           client_antenna_lon : Double ;

         end;

  public
//    UseLocal_MegaGIS_service : boolean;

    GUID_str : string;
    Technology: string;

    CK42: record
      client_lat : Double ;
      client_lon : Double ;
    end;


   // Runtime_
    Client: record
      HousePoints:  THousePointList;
      map_building_height: integer;
    end;

  //  Client_

    client_antenna_height: Double ;

    is_can_change_antenna_height   : boolean;
    is_can_change_antenna_location : boolean;

    bitrate_Mbps : Double ;

    Places: TTaskPlaceList;

  public

    Log : TStringList;
  public
    CalcResult: record
      ResultType: TResultType;
      Description: string;

    end;

    constructor Create;
    destructor Destroy; override;

    procedure Update_WGS_to_CK42_;

    function GetResultTypeStr: string;
    procedure SetResultTypeByStr(aValue: string);

    function GetDisplayName: string;

    function GetGeoJSON: string;

  //  function ToXML: string;
  end;

  //----------------------------------------
  TTaskPlace = class
  //----------------------------------------
  private
  public
    Runtime: record
      Hidden : boolean;
    end;

    AdminPanel_DB_ID: integer;

    DB_id : integer;
    map_building_height: integer ;
    length_to_client_km: Double ;

    ClientHWTypeID : string;
    Freq_MHz  : double;

    ObjectID: int64;
   // SysName: string ;

    place_name: string;
    place_name_display: string;
    Height: Double ;

    WM_Is_WiMax : boolean;
    WM_Azimuth : double;
    WM_Is_Ready : boolean;

    Pos_WGS: TLatLon;
    Pos_CK42: TLatLon;

    Azimuth_Place_to_Client: double;
    Azimuth_Client_to_Place: double;

    //--------------------------------------
    CalcResult: record

      ResultType: TResultType;
      Description: string;


      CK42: record
        client_antenna : TLatLon ;
      end;

      WGS : record
         client_antenna: TLatLon;
      end;

      client_antenna_height: Double ;

      FileName_Map     : string;
      FileName_Profile : string;
      FileName_Zone    : string;

    end;

    Log : TStringList;

    constructor Create;
    destructor Destroy; override;

    procedure Update_CalcResult_CK42_to_WGS;

 //   function GetResultTypeByStr: string;
    function GetResultTypeStr: string;


    function GetResultCode: byte;

    procedure SetResultStr(aValue: string);
//    function CalcResult_client_antenna_WGS: TLatLon;

  end;


  function GetResultTypeByStr(aValue: string): TResultType;


implementation




//-------------------------------------------------------------------
procedure TTask.Update_WGS_to_CK42_;
//-------------------------------------------------------------------
var
  bl: TBLpoint;
  I: Integer;
begin
  if Wgs.client_antenna_lat<>0 then
  begin
    Log.Add('Использование client_antenna pos');

    Wgs.client_lat:=Wgs.client_antenna_lat;
    Wgs.client_lon:=Wgs.client_antenna_lon;
  end;


  bl:= geo_WGS84_to_Pulkovo42(MakeBLPoint(Wgs.client_lat, Wgs.client_Lon));
  CK42.client_lat:=bl.B;
  CK42.client_lon:=bl.L;


  for I := 0 to Places.Count-1 do
  begin
    bl:= geo_WGS84_to_Pulkovo42(MakeBLPoint(Places[i].Pos_Wgs.lat, Places[i].Pos_Wgs.lon));

    Places[i].Pos_CK42.lat:=bl.B;
    Places[i].Pos_CK42.lon:=bl.L;

  end;

end;


//-------------------------------------------------------------------
constructor TTask.Create;
//-------------------------------------------------------------------
////var
//  s: string;
begin
  inherited;

  Places:=TTaskPlaceList.Create;
  Log:=TStringList.Create;

  DateCreated:=Now;

 // s:=DateTimeToStr(DateCreated);

  Client.HousePoints:=THousePointList.Create;

  g_Logger.Add( 'constructor TTask.Create;' );
  CodeSite.Send( 'constructor TTask.Create;' );

end;


//-------------------------------------------------------------------
destructor TTask.Destroy;
//-------------------------------------------------------------------
begin
  inherited;

  FreeAndNil(Places);
  FreeAndNil(Log);
  FreeAndNil(Client.HousePoints);

  CodeSite.Send( 'destructor TTask.Destroy;' );
  g_Logger.Add( 'destructor TTask.Destroy' );


end;


function TTask.GetDisplayName: string;
begin
  Result := GUID_str +' - '+ DateTimeToStr(DateCreated);
end;




function TTaskPlaceList.AddItem: TTaskPlace;
begin
  result:=TTaskPlace.Create;
  Add(result);
end;


//-------------------------------------------------------------------
procedure THousePointList.LoadFromDataset(aDataset: DB.TDataset);
//-------------------------------------------------------------------
var
  rPoint: THousePoint;
  bl: TBLpoint;
begin
  Clear;

  with aDataset do
    while not EOF do
    begin
//      rPoint.house_id:=aDataset.FieldByName('house_id').AsInteger;

      //2636629
      rPoint.wgs.Lat:=aDataset.FieldByName('lat').AsFloat;
      rPoint.wgs.Lon:=aDataset.FieldByName('lon').AsFloat;

      rPoint.Height:=aDataset.FieldByName('Height').AsInteger;

      bl:= geo_WGS84_to_Pulkovo42(MakeBLPoint(rPoint.wgs.Lat, rPoint.wgs.Lon));
      rPoint.CK42.Lat:=bl.B;
      rPoint.CK42.lon:=bl.L;

      Add(rPoint);

      Next;
    end;
end;


constructor TTaskPlace.Create;
begin
  inherited;

  Log:=TStringList.Create;
end;


destructor TTaskPlace.Destroy;
begin
  inherited;

  FreeAndNil(Log);
end;


procedure TTaskPlace.Update_CalcResult_CK42_to_WGS;
var
  bl: TBLpoint;
begin
  bl:= geo_Pulkovo42_to_WGS84 (MakeBLPoint( CalcResult.CK42.client_antenna.lat, CalcResult.CK42.client_antenna.lon));

  CalcResult.WGS.client_antenna.Lat:=bl.B;
  CalcResult.WGS.client_antenna.Lon:=bl.L;

end;

function TTaskPlace.GetResultCode: byte;
begin
  Result := Ord(CalcResult.ResultType);
end;


function TTaskPlace.GetResultTypeStr: string;
begin
  Result := DEF_ResultTypeStr[CalcResult.ResultType];
end;


procedure TTaskPlace.SetResultStr(aValue: string);
begin
  CalcResult.ResultType:=GetResultTypeByStr (aValue);
end;

//
//procedure TTaskPlaceList.SaveToXMLFile(aFileName: string);
//begin
//  TXmlSerializer_.SerializeToFile (Self, aFileName);
//
//end;

//-------------------------------------------------------------------
procedure TTaskPlaceList.Sort_By_ResultCode_and_length_to_client;
//-------------------------------------------------------------------
var
  Comparison: TComparison<TTaskPlace>;
begin
  Comparison := function(const Left, Right: TTaskPlace): integer
                begin
                  Result := TComparer<int64>.Default.Compare(Left.GetResultCode, Right.GetResultCode);
                  if Result = 0 then
                    Result := TComparer<double>.Default.Compare(Left.length_to_client_km, Right.length_to_client_km);
                end;

  Sort(TComparer<TTaskPlace>.Construct(Comparison));

end;


//-------------------------------------------------------------------
procedure TTaskPlaceList.Sort_By_length_to_client;
//-------------------------------------------------------------------
var
  Comparison: TComparison<TTaskPlace>;
begin
  Comparison := function(const Left, Right: TTaskPlace): integer
                begin
                    Result := TComparer<double>.Default.Compare(Left.length_to_client_km, Right.length_to_client_km);
                end;

  Sort(TComparer<TTaskPlace>.Construct(Comparison));

end;



function TTask.GetResultTypeStr: string;
begin
  Result := DEF_ResultTypeStr[CalcResult.ResultType];
end;

procedure TTask.SetResultTypeByStr(aValue: string);
begin
   CalcResult.ResultType:= GetResultTypeByStr (aValue);
end;



//-------------------------------------------------------------------
function GetResultTypeByStr(aValue: string): TResultType;
//-------------------------------------------------------------------
var
  i: TResultType;
begin
  Result:= rtNone;

  for i:=Low(TResultType) to High(TResultType) do
    if Eq(DEF_ResultTypeStr[i] , aValue) then
    begin
      Result:=i;
      Exit;
    end;

//  TResultTypeStr : array[TResultType] of string =
//     ('CalculationImpossible','ConnectionPossible','ConnectionImpossible');


//  Result := TResultTypeStr[CalcResult.ResultType];
end;

//-----------------------------------------------------------------
function TTask.GetGeoJSON: string;
//-----------------------------------------------------------------
var
  I: Integer;
  oBuilder: TJSONObjectBuilder;
  s: string;
  oStringBuilder: TStringBuilder;
  oStringWriter: TStringWriter;
  oWriter: TJsonTextWriter;

  oPairs: TJSONCollectionBuilder.TPairs;

  oElements: TJSONCollectionBuilder.TElements;

 // function TJSONCollectionBuilder.TPairs.BeginArray(const AKey: string): TElements;

begin
  oStringBuilder := TStringBuilder.Create;
  oStringWriter  := TStringWriter.Create(oStringBuilder);
  oWriter        := TJsonTextWriter.Create(oStringWriter);
  oBuilder       := TJSONObjectBuilder.Create(oWriter);


  oElements:=oBuilder
              .BeginObject
                 .Add('type', 'FeatureCollection')
                 .BeginArray('features');

  for I := 0 to Places.Count-1 do
  begin
    oElements
      .BeginObject
               .BeginObject ('properties')
                 .Add('result', ResultTypeJSONStr [ Places[i].CalcResult.ResultType ])
             //   .Add('name','desc')
               .EndObject

               .BeginObject ('geometry')
                 .Add('type','LineString')
                // .Add('coordinates','desc')
                 .BeginArray('coordinates')
                   .BeginArray()
                     .Add (Places[i].Pos_WGS.Lat)
                     .Add (Places[i].Pos_WGS.Lon)
                   .EndArray

                   .BeginArray()
                     .Add (WGS.client_lat)
                     .Add (WGS.client_Lon)
                   .EndArray
                 .EndArray
               .EndObject
      .EndObject ;

    oElements
      .BeginObject
               .BeginObject ('properties')
                 .Add('name', Places[i].place_name_display)
                 .Add('type','site')
               .EndObject

               .BeginObject ('geometry')
                 .Add('type','Point')
                 .BeginArray('coordinates')
  //                 .BeginArray()
                     .Add (Places[i].Pos_WGS.Lat)
                     .Add (Places[i].Pos_WGS.Lon)
    //               .EndArray

//                   .BeginArray()
//                     .Add (WGS.client_lat)
//                     .Add (WGS.client_Lon)
//                   .EndArray
                 .EndArray
               .EndObject
      .EndObject;

  end;


  oElements
    .BeginObject
             .BeginObject ('properties')
               .Add('type','client')
             .EndObject

             .BeginObject ('geometry')
               .Add('type','Point')
               .BeginArray('coordinates')
                   .Add (WGS.client_lat)
                   .Add (WGS.client_Lon)
               .EndArray
             .EndObject
    .EndObject;



  oElements
   .EndArray //заканчиваем создание массива json
      .EndObject; //заканчиваем создание json


//,{  "type": "Feature",
//            "properties": {
//                "type": "line"
//            },
//            "geometry": {
//                "type": "LineString",
//                "coordinates": [[ 49.1451072693, 55.7799519839],[ 49.10000138, 55.77110772]]
//            }


{
  oBuilder

      .BeginObject
         .Add('type', 'FeatureCollection')
//         .Add('title','Новая задача')
         .BeginArray('features')
//каждый элемент массива - это объект, поэтому снова вызываем BeginObject
           .BeginObject
             .BeginObject ('properties')
              .Add('type','Feature')
              .Add('name','desc')
              .EndObject
            .EndObject //заканчиваем создание объекта
//            .BeginObject
//              .Add('type','href2')
//              .Add('description','desc')
//              .Add('link','http://webdelphi.ru')
//            .EndObject //заканчиваем создание объекта
         .EndArray //заканчиваем создание массива json
      .EndObject; //заканчиваем создание json
 }






  Result := oStringBuilder.ToString;


  FreeAndNil(oStringBuilder);
  FreeAndNil(oStringWriter);
  FreeAndNil(oWriter);
  FreeAndNil(oBuilder);



end;


end.



{
    System.JSON,  System.JSON.Builders,  System.JSON.Writers,

procedure TForm6.FormCreate(Sender: TObject);

var
  oBuilder: TJSONObjectBuilder;
  s: string;
  oStringBuilder: TStringBuilder;
  oStringWriter: TStringWriter;
  oWriter: TJsonTextWriter;
begin
  oStringBuilder := TStringBuilder.Create;
  oStringWriter := TStringWriter.Create(oStringBuilder);
  oWriter := TJsonTextWriter.Create(oStringWriter);
  oBuilder := TJSONObjectBuilder.Create(oWriter);

  try
   oBuilder

      .BeginObject
         .Add('type', 'FeatureCollection')
//         .Add('title','Новая задача')
         .BeginArray('features')
//каждый элемент массива - это объект, поэтому снова вызываем BeginObject
           .BeginObject
             .BeginObject ('properties')
              .Add('type','Feature')
              .Add('name','desc')
              .EndObject
            .EndObject //заканчиваем создание объекта
            .BeginObject
              .Add('type','href2')
              .Add('description','desc')
              .Add('link','http://webdelphi.ru')
            .EndObject //заканчиваем создание объекта
         .EndArray //заканчиваем создание массива json
      .EndObject; //заканчиваем создание json



//    Builder
//      .BeginObject
//        .BeginObject('Nested')
//           .Add('Value',123)
//           .Add('Value2', True)
//        .EndObject
//      .EndObject;
    //получаем строку, содержащую отформатированный json

    Memo1.Text:=oStringBuilder.ToString
  finally

  end;






(*
{
{ "type": "FeatureCollection",   "features": [{},{  "type": "Feature",
            "properties": {
                "type": "line"
            },
            "geometry": {
                "type": "LineString",
                "coordinates": [[ 49.1451072693, 55.7799519839],[ 49.153056, 55.773333]]
            }
        },{  "type": "Feature",
            "properties": {
                "type": "line"
            },
            "geometry": {
                "type": "LineString",
                "coordinates": [[ 49.1451072693, 55.7799519839],[ 49.154444, 55.790833]]
            }
        },{  "type": "Feature",
            "properties": {
                "type": "line"
            },
            "geometry": {
                "type": "LineString",
                "coordinates": [[ 49.1451072693, 55.7799519839],[ 49.15861111, 55.75916667]]
            }
        },{  "type": "Feature",
            "properties": {
                "type": "line"
            },
            "geometry": {
                "type": "LineString",
                "coordinates": [[ 49.1451072693, 55.7799519839],[ 49.112222, 55.800556]]
            }
        },{  "type": "Feature",
            "properties": {
                "type": "line"
            },
            "geometry": {
                "type": "LineString",
                "coordinates": [[ 49.1451072693, 55.7799519839],[ 49.111389, 55.796667]]
            }
        },{  "type": "Feature",
            "properties": {
                "type": "line"
            },
            "geometry": {
                "type": "LineString",
                "coordinates": [[ 49.1451072693, 55.7799519839],[ 49.10000138, 55.77110772]]
            }
        },{  "type": "Feature",
            "properties": {
                "type": "line"
            },
            "geometry": {
                "type": "LineString",
                "coordinates": [[ 49.1451072693, 55.7799519839],[ 49.036389, 55.835556]]
            }
        },{  "type": "Feature",
            "properties": {
                "type": "line"
            },
            "geometry": {
                "type": "LineString",
                "coordinates": [[ 49.1451072693, 55.7799519839],[ 49.10972222, 55.74305556]]
            }
        },{  "type": "Feature",
            "properties": {
                "type": "client",
                "name": "client",
                "height": "50"
            },
            "geometry": {
                "type": "Point",
                "coordinates": [ 49.1451072693, 55.7799519839 ]
            }
        },{  "type": "Feature",
            "properties": {
                "type": "site",
                "name": "16-1667",
                "height": "32"
            },
            "geometry": {
                "type": "Point",
                "coordinates": [ 49.153056, 55.773333 ]
            }
        },{  "type": "Feature",
            "properties": {
                "type": "site",
                "name": "16-4",
                "height": "27"
            },
            "geometry": {
                "type": "Point",
                "coordinates": [ 49.154444, 55.790833 ]
            }
        },{  "type": "Feature",
            "properties": {
                "type": "site",
                "name": "16-1037",
                "height": "30"
            },
            "geometry": {
                "type": "Point",
                "coordinates": [ 49.15861111, 55.75916667 ]
            }
        },{  "type": "Feature",
            "properties": {
                "type": "site",
                "name": "16-1261",
                "height": "41"
            },
            "geometry": {
                "type": "Point",
                "coordinates": [ 49.112222, 55.800556 ]
            }
        },{  "type": "Feature",
            "properties": {
                "type": "site",
                "name": "16-1432",
                "height": "38"
            },
            "geometry": {
                "type": "Point",
                "coordinates": [ 49.111389, 55.796667 ]
            }
        },{  "type": "Feature",
            "properties": {
                "type": "site",
                "name": "16-134",
                "height": "33"
            },
            "geometry": {
                "type": "Point",
                "coordinates": [ 49.10000138, 55.77110772 ]
            }
        },{  "type": "Feature",
            "properties": {
                "type": "site",
                "name": "16-2638",
                "height": "60"
            },
            "geometry": {
                "type": "Point",
                "coordinates": [ 49.036389, 55.835556 ]
            }
        },{  "type": "Feature",
            "properties": {
                "type": "site",
                "name": "16-1168",
                "height": "25"
            },
            "geometry": {
                "type": "Point",
                "coordinates": [ 49.10972222, 55.74305556 ]
            }
        }]}
