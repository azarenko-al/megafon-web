﻿unit Unit6;

interface

uses
  System.JSON,  System.JSON.Builders,  System.JSON.Writers,

  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls;

type
  TForm6 = class(TForm)
    Memo1: TMemo;
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form6: TForm6;

implementation

{$R *.dfm}

procedure TForm6.FormCreate(Sender: TObject);

var
  oBuilder: TJSONObjectBuilder;
  s: string;
  oStringBuilder: TStringBuilder;
  oStringWriter: TStringWriter;
  oWriter: TJsonTextWriter;


  oPairs: TJSONCollectionBuilder.TPairs;

begin
  oStringBuilder := TStringBuilder.Create;
  oStringWriter := TStringWriter.Create(oStringBuilder);
  oWriter := TJsonTextWriter.Create(oStringWriter);
  oBuilder := TJSONObjectBuilder.Create(oWriter);

  try
   oBuilder

      .BeginObject
         .Add('type', 'FeatureCollection')
//         .Add('title','Новая задача')
         .BeginArray('features')
//каждый элемент массива - это объект, поэтому снова вызываем BeginObject
           .BeginObject
             .BeginObject ('properties')
              .Add('type','Feature')
              .Add('name','desc')
              .EndObject
            .EndObject //заканчиваем создание объекта
            .BeginObject
              .Add('type','href2')
              .Add('description','desc')
              .Add('link','http://webdelphi.ru')
            .EndObject //заканчиваем создание объекта
         .EndArray //заканчиваем создание массива json
      .EndObject; //заканчиваем создание json


//  @FEATURE_Point varchar(max) =
//      '{  "type": "Feature",
//            "properties": {
//                "type": ":type",
//                "name": ":name",
//                "height": ":height"
//            },
//            "geometry": {
//                "type": "Point",
//                "coordinates": [ :coordinates  ]
//            }
//        }',



//    Builder
//      .BeginObject
//        .BeginObject('Nested')
//           .Add('Value',123)
//           .Add('Value2', True)
//        .EndObject
//      .EndObject;
    //получаем строку, содержащую отформатированный json

    Memo1.Text:=oStringBuilder.ToString
  finally

  end;
end;

end.

(*
    {

declare
  @s varchar(max)='{}',

  @FEATURE_Point varchar(max) =
      '{  "type": "Feature",
            "properties": {
                "type": ":type",
                "name": ":name",
                "height": ":height"
            },
            "geometry": {
                "type": "Point",
                "coordinates": [ :coordinates  ]
            }
        }',

  @FEATURE_LINE varchar(max) =
      '{  "type": "Feature",
            "properties": {
                "type": ":type"
            },
            "geometry": {
                "type": "LineString",
                "coordinates": [[ :coordinates1 ],[ :coordinates2 ]]
            }
        }'



-------------

-------------
  SELECT @s=@s+','+
  		replace (replace (replace (@FEATURE_LINE,
      									':coordinates1 ', dbo.fn_LatLon_to_str (T.client_lat, T.client_lon) ), -- FORMAT(T.client_lon,'#.##########')  +', '+ FORMAT(T.client_lat,'#.##########') ),
      									':coordinates2 ', dbo.fn_LatLon_to_str (P.place_lat, P.place_lon) ), --FORMAT(P.place_lon,'#.##########')  +', '+ FORMAT(P.place_lat,'#.##########') ),
      	                 ':type',  'line' )

    FROM Admin_Panel.Task_places P,  Admin_Panel.Task T



  SELECT  @s=@s+','+
  		replace(replace(replace(replace (@FEATURE_Point,
      									 ':coordinates ',  dbo.fn_LatLon_to_str (client_lat, client_lon) ), -- FORMAT(client_lat,'#.##########')  +', '+ FORMAT(client_lon,'#.##########') ),
      	                 ':height', isnull(client_antenna_height,'') ),
      	                 ':name',  'client' ),
      	                 ':type',  'client' )

    FROM Admin_Panel.Task

-------------
  SELECT  @s=@s+','+
  		replace(replace (replace(replace (@FEATURE_Point,
        							   ':coordinates ',  dbo.fn_LatLon_to_str (place_lat, place_lon) ),
      	                 ':name',   isnull(place_name,'') ),
                         ':height', isnull(place_height,0) + isnull(map_building_height,0) ),
      	                 ':type',  'site' )

    FROM Admin_Panel.Task_places




select '{ "type": "FeatureCollection",   "features": ['+ @s + ']}'

  /*


{   "type": "FeatureCollection",   "features": [




  {
            "type": "Feature",
            "properties": {
                "type": "poi",
                "name": "Капитолий",
                "address": "Кабельная 4-я, 2а ст1"
            },
            "geometry": {
                "type": "Point",
                "coordinates": [
                    37.718868255615234,
                    55.740627288818381
                ]
            }
        },
*/
  /* Procedure body */
END



var
//System.JSON.

  oBuilder: TJSONObjectBuilder;
  oPlace:  TTaskPlace;

  oWriter: TJsonWriter;

begin
 {
  oWriter:=TJsonWriter.create;
//  oWriter.f

oBuilder := TJSONObjectBuilder.Create(oWriter);
  oBuilder
    .BeginObject
      .BeginArray('colors')
        .BeginObject
          .Add('name', 'red')
          .Add('hex', '#f00')
        .EndObject
    .EndArray
  .EndObject;

   Builder
      .BeginObject
         .Add('kind', 'tasks#task')
         .Add('title','Новая задача')
         .BeginArray('links')
//каждый элемент массива - это объект, поэтому снова вызываем BeginObject
            .BeginObject
              .Add('type','href')
              .Add('description','desc')
              .Add('link','http://webdelphi.ru')
            .EndObject //заканчиваем создание объекта
            .BeginObject
              .Add('type','href2')
              .Add('description','desc')
              .Add('link','http://webdelphi.ru')
            .EndObject //заканчиваем создание объекта
         .EndArray //заканчиваем создание массива json
      .EndObject; //заканчиваем создание json

  }