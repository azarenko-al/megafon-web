object Form2: TForm2
  Left = 0
  Top = 0
  Caption = 'Prop '
  ClientHeight = 878
  ClientWidth = 1345
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object DBGrid1: TDBGrid
    Left = 8
    Top = 24
    Width = 537
    Height = 153
    DataSource = ds_Task_full11111
    TabOrder = 0
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'Tahoma'
    TitleFont.Style = []
  end
  object cxGrid1: TcxGrid
    Left = 0
    Top = 529
    Width = 1345
    Height = 349
    Align = alBottom
    TabOrder = 1
    object cxGrid1BandedTableView1: TcxGridBandedTableView
      Navigator.Buttons.CustomButtons = <>
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <>
      DataController.Summary.SummaryGroups = <>
      Bands = <
        item
        end>
    end
    object cxGrid1DBTableView1: TcxGridDBTableView
      Navigator.Buttons.CustomButtons = <>
      DataController.DataSource = ds_Task_full11111
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <>
      DataController.Summary.SummaryGroups = <>
      OptionsData.Deleting = False
      OptionsData.Editing = False
      OptionsData.Inserting = False
      OptionsView.CellAutoHeight = True
      OptionsView.GroupByBox = False
      OptionsView.Indicator = True
      object cxGrid1DBTableView1bitrate: TcxGridDBColumn
        DataBinding.FieldName = 'bitrate'
      end
      object cxGrid1DBTableView1DBColumn1: TcxGridDBColumn
        DataBinding.FieldName = '1'
        Width = 68
      end
      object cxGrid1DBTableView1DBColumn2: TcxGridDBColumn
        DataBinding.FieldName = '2'
        Width = 68
      end
      object cxGrid1DBTableView1DBColumn3: TcxGridDBColumn
        DataBinding.FieldName = '3'
        Width = 68
      end
      object cxGrid1DBTableView1DBColumn4: TcxGridDBColumn
        DataBinding.FieldName = '4'
        Width = 68
      end
      object cxGrid1DBTableView1DBColumn5: TcxGridDBColumn
        DataBinding.FieldName = '5'
        Width = 68
      end
      object cxGrid1DBTableView1DBColumn6: TcxGridDBColumn
        DataBinding.FieldName = '6'
        Width = 68
      end
      object cxGrid1DBTableView1DBColumn7: TcxGridDBColumn
        DataBinding.FieldName = '7'
        Width = 68
      end
      object cxGrid1DBTableView1DBColumn8: TcxGridDBColumn
        DataBinding.FieldName = '8'
        Width = 68
      end
      object cxGrid1DBTableView1DBColumn9: TcxGridDBColumn
        DataBinding.FieldName = '9'
        Width = 68
      end
      object cxGrid1DBTableView1DBColumn10: TcxGridDBColumn
        DataBinding.FieldName = '10'
        Width = 68
      end
      object cxGrid1DBTableView1DBColumn11: TcxGridDBColumn
        DataBinding.FieldName = '11'
        Width = 68
      end
      object cxGrid1DBTableView1DBColumn12: TcxGridDBColumn
        DataBinding.FieldName = '12'
        Width = 68
      end
      object cxGrid1DBTableView1DBColumn13: TcxGridDBColumn
        DataBinding.FieldName = '13'
        Width = 68
      end
      object cxGrid1DBTableView1DBColumn15: TcxGridDBColumn
        DataBinding.FieldName = '15'
        Width = 68
      end
      object cxGrid1DBTableView1DBColumn17: TcxGridDBColumn
        DataBinding.FieldName = '17'
        Width = 68
      end
      object cxGrid1DBTableView1DBColumn20: TcxGridDBColumn
        DataBinding.FieldName = '20'
        Width = 68
      end
      object cxGrid1DBTableView1DBColumn21: TcxGridDBColumn
        DataBinding.FieldName = '21'
      end
      object cxGrid1DBTableView1DBColumn22: TcxGridDBColumn
        DataBinding.FieldName = '22'
      end
      object cxGrid1DBTableView1DBColumn25: TcxGridDBColumn
        DataBinding.FieldName = '25'
      end
      object cxGrid1DBTableView1DBColumn30: TcxGridDBColumn
        DataBinding.FieldName = '30'
      end
      object cxGrid1DBTableView1DBColumn35: TcxGridDBColumn
        DataBinding.FieldName = '35'
      end
    end
    object cxGrid1Level1: TcxGridLevel
      GridView = cxGrid1DBTableView1
    end
  end
  object Button1: TButton
    Left = 8
    Top = 304
    Width = 75
    Height = 25
    Caption = 'Refresh'
    TabOrder = 2
    OnClick = Button1Click
  end
  object cxGrid2: TcxGrid
    Left = 808
    Top = 0
    Width = 537
    Height = 529
    Align = alRight
    TabOrder = 3
    ExplicitTop = -6
    object cxGridDBTableView1: TcxGridDBTableView
      Navigator.Buttons.CustomButtons = <>
      DataController.DataSource = ds_bitrate_distance_xref
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <>
      DataController.Summary.SummaryGroups = <>
      NewItemRow.Visible = True
      OptionsBehavior.ImmediateEditor = False
      OptionsCustomize.ColumnsQuickCustomization = True
      OptionsView.CellAutoHeight = True
      OptionsView.ColumnAutoWidth = True
      OptionsView.GroupByBox = False
      OptionsView.Indicator = True
      object cxGridDBTableView1id: TcxGridDBColumn
        DataBinding.FieldName = 'id'
        Width = 40
      end
      object cxGridDBTableView1bitrate: TcxGridDBColumn
        DataBinding.FieldName = 'bitrate'
        Width = 107
      end
      object cxGridDBTableView1distance_km: TcxGridDBColumn
        DataBinding.FieldName = 'distance_km'
        Width = 106
      end
      object cxGridDBTableView1technology: TcxGridDBColumn
        DataBinding.FieldName = 'technology'
        Width = 270
      end
    end
    object cxGridLevel1: TcxGridLevel
      GridView = cxGridDBTableView1
    end
  end
  object DBMemo1: TDBMemo
    Left = 584
    Top = 282
    Width = 185
    Height = 137
    DataField = 'technology'
    DataSource = ds_bitrate_distance_xref
    TabOrder = 4
  end
  object ADOStoredProc1: TADOStoredProc
    Connection = dmMain.ADOConnection1
    CursorType = ctStatic
    LockType = ltReadOnly
    ProcedureName = '_test.sp_Pivot'
    Parameters = <>
    Left = 600
    Top = 32
  end
  object ds_Task_full11111: TDataSource
    DataSet = ADOStoredProc1
    Left = 600
    Top = 88
  end
  object t_bitrate_distance_xref: TADOTable
    Connection = dmMain.ADOConnection1
    CursorType = ctStatic
    TableName = '_test.bitrate_distance_xref'
    Left = 600
    Top = 168
  end
  object ds_bitrate_distance_xref: TDataSource
    DataSet = t_bitrate_distance_xref
    Left = 600
    Top = 224
  end
  object FormStorage1: TFormStorage
    UseRegistry = True
    StoredValues = <>
    Left = 240
    Top = 376
  end
  object cxStyleRepository1: TcxStyleRepository
    Left = 392
    Top = 432
    PixelsPerInch = 96
    object cxStyle1: TcxStyle
      AssignedValues = [svColor]
      Color = 16776176
    end
  end
end
