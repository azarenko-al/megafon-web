unit Unit2;

interface

uses
  dm_Main,
  u_cx,

  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,

  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Data.DB, Vcl.Grids, Vcl.DBGrids,
  Data.Win.ADODB, dxmdaset, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, cxStyles, cxCustomData, cxFilter, cxData,
  cxDataStorage, cxEdit, cxNavigator, dxDateRanges, cxDBData, Vcl.StdCtrls,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, cxGridLevel,
  cxClasses, cxGridCustomView, cxGrid, Vcl.DBCtrls, RxPlacemnt, cxTextEdit,
  cxGridBandedTableView;

type
  TForm2 = class(TForm)
    DBGrid1: TDBGrid;
    ADOStoredProc1: TADOStoredProc;
    ds_Task_full11111: TDataSource;
    t_bitrate_distance_xref: TADOTable;
    ds_bitrate_distance_xref: TDataSource;
    cxGrid1Level1: TcxGridLevel;
    cxGrid1: TcxGrid;
    Button1: TButton;
    cxGrid2: TcxGrid;
    cxGridDBTableView1: TcxGridDBTableView;
    cxGridLevel1: TcxGridLevel;
    cxGridDBTableView1id: TcxGridDBColumn;
    cxGridDBTableView1bitrate: TcxGridDBColumn;
    cxGridDBTableView1distance_km: TcxGridDBColumn;
    cxGridDBTableView1technology: TcxGridDBColumn;
    DBMemo1: TDBMemo;
    FormStorage1: TFormStorage;
    cxStyleRepository1: TcxStyleRepository;
    cxStyle1: TcxStyle;
    cxGrid1BandedTableView1: TcxGridBandedTableView;
    cxGrid1DBTableView1: TcxGridDBTableView;
    cxGrid1DBTableView1bitrate: TcxGridDBColumn;
    cxGrid1DBTableView1DBColumn1: TcxGridDBColumn;
    cxGrid1DBTableView1DBColumn2: TcxGridDBColumn;
    cxGrid1DBTableView1DBColumn3: TcxGridDBColumn;
    cxGrid1DBTableView1DBColumn4: TcxGridDBColumn;
    cxGrid1DBTableView1DBColumn5: TcxGridDBColumn;
    cxGrid1DBTableView1DBColumn6: TcxGridDBColumn;
    cxGrid1DBTableView1DBColumn7: TcxGridDBColumn;
    cxGrid1DBTableView1DBColumn8: TcxGridDBColumn;
    cxGrid1DBTableView1DBColumn9: TcxGridDBColumn;
    cxGrid1DBTableView1DBColumn10: TcxGridDBColumn;
    cxGrid1DBTableView1DBColumn11: TcxGridDBColumn;
    cxGrid1DBTableView1DBColumn12: TcxGridDBColumn;
    cxGrid1DBTableView1DBColumn13: TcxGridDBColumn;
    cxGrid1DBTableView1DBColumn15: TcxGridDBColumn;
    cxGrid1DBTableView1DBColumn17: TcxGridDBColumn;
    cxGrid1DBTableView1DBColumn20: TcxGridDBColumn;
    cxGrid1DBTableView1DBColumn21: TcxGridDBColumn;
    cxGrid1DBTableView1DBColumn22: TcxGridDBColumn;
    cxGrid1DBTableView1DBColumn25: TcxGridDBColumn;
    cxGrid1DBTableView1DBColumn30: TcxGridDBColumn;
    cxGrid1DBTableView1DBColumn35: TcxGridDBColumn;
    procedure Button1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form2: TForm2;

implementation

{$R *.dfm}

procedure TForm2.Button1Click(Sender: TObject);
var
  I: Integer;
begin
  dmMain.OpenStoredProc(ADOStoredProc1, '_test.sp_Pivot', []);



  cxGrid1DBTableView1.BeginUpdate();

  cxGrid1DBTableView1.Columns[0].Width:=50;

  while cxGrid1DBTableView1.ColumnCount>1 do
    cxGrid1DBTableView1.Columns[1].Free;

  cxGrid1DBTableView1.DataController.CreateAllItems(true);


  for I := 1 to cxGrid1DBTableView1.ColumnCount-1 do
  begin
    cxGrid1DBTableView1.Columns[i].Width:=70;
    cxGrid1DBTableView1.Columns[i].PropertiesClass:= TcxTextEditProperties;


    with TcxTextEditProperties(cxGrid1DBTableView1.Columns[i].Properties) do
      Alignment.Vert := taTopJustify;

    //TextEdit

  end;

  cxGrid1DBTableView1.EndUpdate;


//  cxGrid1DBTableView1.OptionsView. ColumnAutoWidth = false;
//  cxGrid1DBTableView1.BestFitColumns();


//To disable auditing at the SQL Server:

  //cxGrid1DBTableView1DBColumn3.hea

end;

end.
