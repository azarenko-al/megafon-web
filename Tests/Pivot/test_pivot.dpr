program test_pivot;

uses
  FastMM4,
  Vcl.Forms,
  Unit2 in 'Unit2.pas' {Form2},
  dm_Main in '..\..\AdminPanel\src\dm_Main.pas' {dmMain: TDataModule},
  u_Config in '..\..\.Shared\u_Config.pas',
  dm_Queue in '..\..\SOAP server\src\task\dm_Queue.pas' {dmQueue: TDataModule},
  u_Task_classes in '..\..\SOAP server\src\task\u_Task_classes.pas',
  u_Logger in 'W:\common XE\u_Logger.pas',
  u_web in 'W:\common XE\Web\u_web.pas';

{$R *.res}

begin
  Application.Initialize;
  Application.MainFormOnTaskbar := True;
  Application.CreateForm(TdmMain, dmMain);
  Application.CreateForm(TdmQueue, dmQueue);
  Application.CreateForm(TForm2, Form2);
  Application.Run;
end.
