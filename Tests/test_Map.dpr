program test_Map;

uses
  Vcl.Forms,
  u_Config in '..\.Shared\u_Config.pas',
  d_Map_print in '..\SOAP server\src\Map\d_Map_print.pas' {dlg_Map_print},
  fr_Map_View in '..\AdminPanel\src\fr_Map_View.pas' {frame_Map_view};

{$R *.res}

begin
  Application.Initialize;
  Application.MainFormOnTaskbar := True;
  Application.CreateForm(Tframe_Map_view, frame_Map_view);
  Application.Run;
end.
