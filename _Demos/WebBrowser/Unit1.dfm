object Form1: TForm1
  Left = 0
  Top = 0
  Caption = 'Form1'
  ClientHeight = 450
  ClientWidth = 591
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  DesignSize = (
    591
    450)
  PixelsPerInch = 96
  TextHeight = 13
  object WebBrowser1: TWebBrowser
    Left = 0
    Top = 112
    Width = 593
    Height = 337
    Anchors = [akLeft, akTop, akRight, akBottom]
    TabOrder = 0
    OnDocumentComplete = WebBrowser1DocumentComplete
    ControlData = {
      4C0000004A3D0000D42200000000000000000000000000000000000000000000
      000000004C000000000000000000000001000000E0D057007335CF11AE690800
      2B2E12620A000000000000004C0000000114020000000000C000000000000046
      8000000000000000000000000000000000000000000000000000000000000000
      00000000000000000100000000000000000000000000000000000000}
  end
  object Edit1: TEdit
    Left = 8
    Top = 8
    Width = 300
    Height = 21
    TabOrder = 1
    Text = 'https://echo.msk.ru/'
  end
  object Button1: TButton
    Left = 352
    Top = 8
    Width = 75
    Height = 25
    Caption = 'Button1'
    TabOrder = 2
    OnClick = Button1Click
  end
  object Button2: TButton
    Left = 480
    Top = 8
    Width = 75
    Height = 25
    Caption = 'Button2'
    TabOrder = 3
    OnClick = Button2Click
  end
  object StatusBar1: TStatusBar
    Left = 0
    Top = 431
    Width = 591
    Height = 19
    Panels = <>
    SimplePanel = True
    ExplicitLeft = 496
    ExplicitTop = 64
    ExplicitWidth = 0
  end
  object FormPlacement1: TFormPlacement
    UseRegistry = True
    Left = 352
    Top = 40
  end
end
