unit Unit1;

interface

uses
  ActiveX,
  jpeg,

  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, Vcl.OleCtrls, SHDocVw, RxPlacemnt, Vcl.ComCtrls;

type
  TForm1 = class(TForm)
    WebBrowser1: TWebBrowser;
    Edit1: TEdit;
    Button1: TButton;
    FormPlacement1: TFormPlacement;
    Button2: TButton;
    StatusBar1: TStatusBar;
    procedure Button1Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure WebBrowser1DocumentComplete(ASender: TObject; const pDisp: IDispatch;
        const URL: OLEVariant);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form1: TForm1;

implementation

{$R *.dfm}



procedure WebBrowserScreenShot(const wb: TWebBrowser; const fileName: TFileName) ;
 var
   viewObject : IViewObject;
   r : TRect;
   bitmap : TBitmap;
 begin
   if wb.Document <> nil then
   begin
     wb.Document.QueryInterface(IViewObject, viewObject) ;
     if Assigned(viewObject) then
     try
       bitmap := TBitmap.Create;
       try
         r := Rect(0, 0, wb.Width, wb.Height) ;

         bitmap.Height := wb.Height;
         bitmap.Width := wb.Width;

         viewObject.Draw(DVASPECT_CONTENT, 1, nil, nil, Application.Handle, bitmap.Canvas.Handle, @r, nil, nil, 0) ;

         with TJPEGImage.Create do
         try
           Assign(bitmap) ;
           SaveToFile(fileName) ;
         finally
           Free;
         end;
       finally
         bitmap.Free;
       end;
     finally
       viewObject._Release;
     end;
   end;
 end;


 procedure TForm1.Button1Click(Sender: TObject);
begin
  StatusBar1.SimpleText:='';

  WebBrowser1.Navigate(Edit1.Text);
end;

procedure TForm1.Button2Click(Sender: TObject);
begin
  WebBrowserScreenShot(WebBrowser1,'d:\WebBrowserImage.jpg') ;
end;

procedure TForm1.WebBrowser1DocumentComplete(ASender: TObject; const pDisp:
    IDispatch; const URL: OLEVariant);
begin
  StatusBar1.SimpleText:='WebBrowser1DocumentComplete';
end;



end.
